// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMMessageTypes.h"

@class BBMMessageSendParameters;

/*!
 General Utility methods for interacting with BBMDS and the BBM data
 model.
 */
@interface BBMAccess : NSObject


#pragma mark - Lifecycle Util

/*!
 @details
 Send the BBMDS SetupRetry message to the BBM Service
 */
+ (void)sendSetupRetry;

/*!
 @details
 Send the BBMDS WipeMessage to the BBM Service
 */
+ (void)sendWipe;


#pragma mark - User Util

/*!
 @details
 Returns the current signed in user (or nil if there is no user currently signed in)
 @return BBMUser the current signed in user or nil if no user is signed in
 */
+ (BBMUser *)currentUser;

/*!
 @details
 Returns the PIN of the current user.  PIN should be rarely used.  Users should be uniquely
 identified by their registration ID.
 @return NSString the pin of the current signed in user or nil if no user is signed in
 */
+ (NSString *)currentUserPin;

/*!
 @details
 Returns the user for the given registration ID.  This method may return a user in the "Missing"
 state (user does not exist in the model) or the "Pending" state (user exists but must be loaded
 into the model).  This will not return nil.
 @param userRegId The registration id of the user to search for
 @return BBMUser The user.  Return value is never nil.  A user in the "Missing" stated will be
                 returned if no match is found.
 */
+ (BBMUser *)getUserForUserRegId:(NSNumber *)userRegId;

/*!
 @details
 Returns the user for the given userUri.  Nil if the user does not exist.  This method may  the
 "Pending" state (user exists but must be loaded into the model)
 @param userUri The URI of the user to search for
 @return BBMUser The search result.  nil if the user does not exist in the model
 */
+ (BBMUser *)getUserForUserUri:(NSString *)userUri;


#pragma mark - Messages

/*!
 @details
 Marks the given messages as read.  Note that if message "n" in a chat is marked as read, all
 older messages (0 -> n-1) will also be automatically marked as read.
 @param unreadMessages An array of BBMChatMessage objects to mark as read
 */
+ (void)markMessagesRead:(NSArray *)unreadMessages;


#pragma mark - Chats

/*!
 @details
 Returns the chatMessage LiveMap from the data model.
 @return BBMLiveMap The chatMessageMap
 */
+ (BBMLiveMap *)chatMessageMap;

/*!
 @details
 Returns YES if the LiveList of chats is "current" (all data is loaded and no transactions are
 pending).
 */
+ (BOOL)isChatListCurrent;

/*!
 @details
 Returns the newest message in the given \a chat
 @param chat The chat we wish to get the newest message for
 @return BBMChatMessage The newest message in the chat
 */
+ (BBMChatMessage*)resolvedLastMessageForChat:(BBMChat *)chat;

/*!
 Returns the chat message with the given \a messageId in the given \a chat.   If the chat is
 empty, this will return nil.  If the chat message does not exist, this will return a message
 in the "missing" state.
 @param messageId The messageId we're looking for
 @param chat The chat to which the message belongs
 @return BBMChatMessage The search result.
 */
+ (BBMChatMessage *)messageForId:(long long)messageId inChat:(BBMChat *)chat;

/*!
 @details
 Returns the LiveList of chats as an NSArray.  Equivalent to model.chats.array
 @return NSArray An array of BBMChat objects
 */
+ (NSArray *)getChats;

/*!
 @details
 Returns the chat with the given \a chatId.  The returned object may be nil, pending or missing.
 @param chatId The chat Id to search for
 @return BBMChat the search result
 */
+ (BBMChat *)getChatForId:(NSString *)chatId;

/*!
 @details
 Returns a BBMLiveList of BBMChatParticipant objects for the given \a chat.  The returned list is a
 singleton object (there is only ever one list of participants for each chat).
 @param chat The chat
 @return BBMLiveList The live list of participants
 */
+ (BBMLiveList *)participantsForChat:(BBMChat *)chat;

/*!
 @details
 Leaves the the given \a chat
 @param chat The chat to leave
 */
+ (void)leaveChat:(BBMChat *)chat;

/*!
 @details
 Hides the the given \a chat
 @param chat The chat to end
 */
+ (void)hideChat:(BBMChat *)chat;


/*!
 @details
 Sends a typing notification for the given \a chat
 @param chat The chat in which the user is or is not currently typing
 */
+ (void)sendTypingNotification:(BBMChat *)chat;

/*!
 @details
 Gets an array of BBMUsers who are currently typing in the given \a chat
 @param chat The chat to query
 @return NSArray An array of BBMUsers who have set the typing notification to YES
 */
+ (NSArray *)getTypingUsersForChat:(BBMChat *)chat;

/*
 @details
 Update the chat \a subject for a given \a chat
 @param subject The new subject string.  See the BBMDS docs for the size limitations
 @param chat The chat to set the subject for
 */
+ (void)updateChatSubject:(NSString *)subject forChat:(BBMChat *)chat;

/*!
 @details
 Sends the ShredChatMessage for the given \a chat.  This will retract all messages send by the
 current user in the \a chat
 @param chat The chat in which to retract all messages
 */
+ (void)retractChat:(BBMChat *)chat;


#pragma mark - Message Retraction

/*!
 @details
 Recalls (retracts and deletes) the message with the given \a chatId and \a messageId.
 @param chatId The chat containing the message
 @parm messageId The message Id
 */
+ (void)recallMessageWithChatId:(NSString *)chatId andID:(long long)messageID;
+ (void)recallMessage:(BBMChatMessage *)message;

/*!
 @details
 Locally deletes the message with the given \a chatId and \a messageId.
 @param chatId The chat containing the message
 @parm messageId The message Id
 */
+ (void)deleteMessageWithChatId:(NSString *)chatId andID:(long long)messageID;
+ (void)deleteMessage:(BBMChatMessage *)message;

#pragma mark - Message Status

/*!
 @details
 Requests the delivery status for a particular message in an multi-person chat.  Returns the
 associated cookie.  The caller should observer incoming BBMChatMessageStateMessage message with
 the \a cookie to get the message delivery state for each individual participant in the associate
 chat.
 @param message The message we which to query the states for
*/
 + (NSString *)getMessageStatusForMessage:(BBMChatMessage *)message;


#pragma mark - Endpoints

+ (NSString*)updateEndpoint:(NSString*)endpointId name:(NSString*)name description:(NSString*)description;

/*!
 @details
 Sends a BBMEndpointsGetMessage message requesting the list of endpoints.
 */
+ (void)requestEndpoints;

/*!
 @details
 Sends a BBMEndpointDeregisterMessage message to de-register an endpoint
 @param endpointId of the endpoint to de-register.
 */
+ (NSString*)sendEndpointDeregister:(NSString*)endpointId;


#pragma mark - Message Attachments

/*!
 @details
 Sends a message with the given \a parameters
 @param param The message parameters
 */
+ (void)sendMessage:(BBMMessageSendParameters *)param;

/*!
 @details
 Sends a picture messages with the given parameters
 @param path The path to the image file
 @param thumbnailPath The path to the image thumbnail file
 @param description The message text
 @param chatId The chat to send the message in
 @param priority The message priority
 */
+ (void)sendPicture:(NSString *)path
          thumbnail:(NSString *)thumbnailPath
    withDescription:(NSString *)description
           toChatId:(NSString *)chatId
       withPriority:(MessagePriority)priority;

/*!
 @details
 Saves a data object to the temporary transfer directory and returns the path
 @param data The data to save
 @return The file path.  Nil if an error occurred.
 */
+ (NSString *)saveDataToTmpTransferDir:(NSData *)data;

/*!
 @details
 Saves a data object to the temporary transfer directory and returns the path.
 This will overwrite any existing temporary files with the same name.
 @param data The data to save
 @param filename The filename to use.
 @return The file path.  Nil if an error occurred.
 */
+ (NSString *)saveDataToTmpTransferDir:(NSData *)data
                              filename:(NSString *)filename;

/*!
 @details
 Writes a data object to the given file using a unique name and returns the resulting path
 @param data The data to save
 @param directory The directory to save the data to
 @param filename Optional filename.  A UUID will be used if this is set to nil.
 @return The path to the saved file.  Nil if an error occurred.
 */
+ (NSString *)saveData:(NSData *)data
           toDirectory:(NSString *)directory
              filename:(NSString *)filename;


#pragma mark - Messages

/*!
 @details
 Sends a timed message with the given parameters
 @param message The message text
 @param chatId The chatId to send the message in
 @param priority The message priority
 @param duration The duration for timed messages (0 for a normal message)
 */
+ (void)sendMessage:(NSString *)message
           toChatId:(NSString *)chatId
       withPriority:(MessagePriority)priority
       withDuration:(BBMTimedMessageValue)duration;

/*!
 @details
 Sends a quoted message with the given parameters
 @param message The message text
 @param quotedMessage The message to quote
 @param chatId The id of the chat to send the message in
 @param priority The message priority
 */
+ (void)sendMessage:(NSString *)message
          inReplyTo:(BBMChatMessage *)quotedMessage
           toChatId:(NSString *)chatId
       withPriority:(MessagePriority)priority;


#pragma mark - Chat Admin

+ (void)promoteParticipant:(BBMChatParticipant *)participant;

+ (void)demoteParticipant:(BBMChatParticipant *)participant;

+ (void)removeParticipantFromChat:(BBMChatParticipant *)participant;


#pragma mark - Utilities

/*!
 Returns the path to the temporary transfer directory
 @return NSString The local file system path
 */
+ (NSString *)getTempTransferDirectory;


#pragma mark - Search

/*!
 Sends a search request with the given string.
 @param searchString The string to be searched
 */
+ (void)inAppSearch:(NSString *)searchString;

@end


@interface BBMMessageSendParameters : NSObject
/*! @brief The message tag */
@property (nonatomic, strong) NSString *tag;

/*! @brief The Id of the chat to send the message to */
@property (nonatomic, strong) NSString *chatId;

/*! @brief The message text */
@property (nonatomic, strong) NSString *textMessage;

/*! @brief An optional path to a picture attachment */
@property (nonatomic, strong) NSString *picturePath;

/*! @brief An optional path to a picture thumbnail image */
@property (nonatomic, strong) NSString *pictureThumbnailPath;

/*! @brief An optional message to quote */
@property (nonatomic, strong) BBMChatMessage *quotedMessage;

/*! @brief An optional message that this one will replace */
@property (nonatomic, strong) BBMChatMessage *editedMessage;

/*! @brief The message priority */
@property (nonatomic, assign) MessagePriority priority;

/*! @brief An optional duration of the message is timed */
@property (nonatomic, assign) BBMTimedMessageValue duration;

@end
