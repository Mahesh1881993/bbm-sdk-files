// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <BBMEnterprise/BBMEnterprise.h>

typedef void (^ChatCreationCallback)(NSString *chatId, BBMChatStartFailedMessageReason failReason);

/*!
 @details
 Convenience class for starting a chat.   This class will automatically listen to the BBMDS interface
 for the incoming "startChat" message and call the supplied callbacks
 */
@interface BBMChatCreator : NSObject


/*!
 @details
 Stars a 1-1 chat with the user with the given \a regId and \a subject.  Will call the supplied
 \a callback on success or failure.   The \a regId \a subject and \a callback are all required
 parameters.
 @param regId The regId of the other party for the chat.
 @param subject The chat subject
 @param callback Callback to be run on success or failure.
 */
- (void)startChatWithRegId:(NSNumber *)regId
                   subject:(NSString *)subject
                  callback:(ChatCreationCallback)callback;

/*!
 @details
 Stars a  conference with the user with the given \a regIds and \a subject.  Will call the supplied
 \a callback on success or failure.  The \a regIds \a subject and \a callback are all required
 parameters.
 @param regIds The regIds of the other party for the chat.
 @param subject The chat subject
 @param callback Callback to be run on success or failure.
 */
- (void)startConferenceWithRegIds:(NSArray *)regIds
                          subject:(NSString *)subject
                         callback:(ChatCreationCallback)callback;

@end
