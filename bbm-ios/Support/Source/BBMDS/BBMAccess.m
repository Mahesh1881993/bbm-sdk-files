// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMAccess.h"
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMUIWidgets.h"
#import "SharedData.h"


static NSString *const kChatURIFormat = @"bbmpim://chat/%@";

@implementation BBMAccess


#pragma mark - Lifcycle

+ (void)sendSetupRetry
{
    BBMJSONMessage *msg = [[BBMSetupRetryMessage alloc] init];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (void)sendWipe
{
    BBMWipeMessage *msg = [[BBMWipeMessage alloc] init];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}


#pragma mark - User


+ (NSString *)currentUserURI
{
    return [[[BBMEnterpriseService service] model] globalLocalUri];
}

+ (NSString *)currentUserPin
{
    return [[[BBMEnterpriseService service] model] globalLocalPin];
}

+ (BBMUser *)currentUser
{
    if (!self.currentUserURI) {
        return nil;
    }
    return [[[BBMEnterpriseService service] model].user valueForKey:self.currentUserURI];
}

+ (BBMUser *)getUserForUserRegId:(NSNumber *)userRegId
{
    if(nil == userRegId) {
        return nil;
    }
    return [[[BBMEnterpriseService service] model] userWithRegId:userRegId];
}

+ (BBMUser *)getUserForUserUri:(NSString *)userUri
{
    if (nil == userUri) {
        return nil;
    }
    BBMLiveMap *userLiveMap = [[BBMEnterpriseService service] model].user;
    BBMUser *user = [userLiveMap objectForKeyedSubscript:userUri];
    return user;
}


#pragma mark - Messages


+ (void)markMessagesRead:(NSArray *)unreadMessages
{
    if (unreadMessages.count == 0) {
        return;
    }

    for (BBMChatMessage *message in unreadMessages) {
        BBMChatMessageReadMessage *msg = [[BBMChatMessageReadMessage alloc] initWithChatId:message.chatId
                                                                                 messageId:message.messageId];
        [[BBMEnterpriseService service] sendMessageToService:msg];
    }
}


#pragma mark - Chat Utils

+ (BBMLiveMap *)chatMessageMap
{
    return [[BBMEnterpriseService service] model].chatMessage;
}

+ (BBMChatMessage*)resolvedLastMessageForChat:(BBMChat *)chat
{
    if(chat.numMessages == 0 || chat == nil || chat.chatId == nil) {
        return nil;
    }

    BBMChatMessageKey *key = [BBMChatMessageKey keyWithChatId:chat.chatId messageId:chat.lastMessage];
    BBMChatMessage *message = [[BBMEnterpriseService service] model].chatMessage[key];
    return message;
}


+ (BBMChatMessage *)messageForId:(long long)messageId inChat:(BBMChat *)chat;
{
    if(chat.numMessages == 0 || chat == nil || chat.chatId == nil) {
        return nil;
    }
    
    BBMChatMessageKey *key = [BBMChatMessageKey keyWithChatId:chat.chatId messageId:messageId];
    BBMChatMessage *message = [[BBMEnterpriseService service] model].chatMessage[key];
    return message;
}

+ (BOOL)isChatListCurrent
{
    return [[BBMEnterpriseService service] model].chat.bbmState == kBBMStateCurrent;
}

+ (NSArray *)getChats
{
    return [[BBMEnterpriseService service] model].chat.array;
}

+ (BBMChat *)getChatForId:(NSString *)chatId
{
    if (nil == chatId || chatId.length == 0) {
        return nil;
    }
    return [[BBMEnterpriseService service] model].chat[chatId];
}


+ (BBMLiveList *)participantsForChat:(BBMChat *)chat
{
    if(nil == chat) {
        return nil;
    }

    BBMChatParticipantCriteria *criteria = [[BBMChatParticipantCriteria alloc] init];
    criteria.chatId = chat.chatId;
    BBMLiveList *list = [[[BBMEnterpriseService service] model] chatParticipantWithCriteria:criteria];
    return list;
}

+ (void)hideChat:(BBMChat *)chat
{
    if(nil == chat) {
        return;
    }
    BBMChatHideMessage *msg = [[BBMChatHideMessage alloc] initWithChatIds:@[chat.chatId]];
    [[BBMEnterpriseService service] sendMessageToService:msg];


}

+ (void)leaveChat:(BBMChat *)chat
{
    if(nil == chat) {
        return;
    }
    BBMChatLeaveMessage *msg = [[BBMChatLeaveMessage alloc] initWithChatIds:@[chat.chatId]];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (void)sendTypingNotification:(BBMChat *)chat
{
    if(nil == chat) {
        return;
    }
    BBMChatTypingMessage *msg = [[BBMChatTypingMessage alloc] initWithChatId:chat.chatId];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (NSArray *)getTypingUsersForChat:(BBMChat *)chat
{
    if(nil == chat) {
        return @[];
    }

    NSArray *typingUsers = [[[BBMEnterpriseService service] model].typing array];

    //Filter the list to only include users typing in the given conversation
    NSMutableArray *typingUsersInChat = [[NSMutableArray alloc] init];
    for (BBMTyping *typingUser in typingUsers) {
        if ([typingUser.chatId isEqualToString:chat.chatId]) {
            [typingUsersInChat addObject:typingUser];
        }
    }
    return [typingUsersInChat copy];
}

+ (void)updateChatSubject:(NSString *)subject forChat:(BBMChat *)chat;
{
    if(nil == chat) {
        return;
    }

    NSArray *elements = @[@{@"chatId":chat.chatId, @"subject":(subject ?: @"")}];
    BBMJSONMessage *msg = [[BBMRequestListChangeMessage alloc] initWithElements:elements
                                                                           type:@"chat"];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (void)retractChat:(BBMChat*)chat
{
    if(nil == chat) {
        return;
    }

    BBMChatShredMessage *msg = [[BBMChatShredMessage alloc] initWithChatId:chat.chatId];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

#pragma mark - Message Retraction

+ (void)recallMessage:(BBMChatMessage *)message
{
    [self recallMessageWithChatId:message.chatId andID:message.messageId];
}

+ (void)recallMessageWithChatId:(NSString *)chatId andID:(long long)messageID
{
    if(nil == chatId) {
        return;
    }

    BBMChatMessageRecallMessage *msg = [[BBMChatMessageRecallMessage alloc] initWithChatId:chatId
                                                                                messageId:messageID];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (void)deleteMessage:(BBMChatMessage *)message
{
    [self deleteMessageWithChatId:message.chatId andID:message.messageId];
}

+ (void)deleteMessageWithChatId:(NSString *)chatId andID:(long long)messageID
{
    if(nil == chatId) {
        return;
    }

    BBMChatMessageDeleteMessage *msg = [[BBMChatMessageDeleteMessage alloc] initWithChatId:chatId
                                                                                identifier:messageID];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (NSString *)getMessageStatusForMessage:(BBMChatMessage *)message
{
    BBMChat *chat = message.resolvedChatId;
    BBMChatMessageStateGetMessage *msg = [[BBMChatMessageStateGetMessage alloc] initWithChatId:chat.chatId messageId:message.messageId];
    [[BBMEnterpriseService service] sendMessageToService:msg];
    return msg.cookie;
}


#pragma mark - Endpoints

+ (NSString*)updateEndpoint:(NSString*)endpointId name:(NSString*)name description:(NSString*)description
{
    BBMEndpointUpdateMessage *msg = [[BBMEndpointUpdateMessage alloc] initWithObjectDescription:description nickname:name];
    if(endpointId) {
        msg.endpointId = endpointId;
    }
    [[BBMEnterpriseService service] sendMessageToService:msg];
    return msg.cookie;
}

+ (void)requestEndpoints
{
    BBMEndpointsGetMessage *msg = [[BBMEndpointsGetMessage alloc] init];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (NSString*)sendEndpointDeregister:(NSString*)endpointId
{
    BBMEndpointDeregisterMessage *msg = [[BBMEndpointDeregisterMessage alloc] initWithEndpointId:endpointId];
    [[BBMEnterpriseService service] sendMessageToService:msg];
    return msg.cookie;
}


#pragma mark - Message

+ (void)sendMessage:(BBMMessageSendParameters *)param
{
    //If a message is being edited first recall the original
    if (param.editedMessage) {
        //Recall the original message.  We'll resend below.
        [self recallMessageWithChatId:param.editedMessage.chatId
                                andID:param.editedMessage.messageId];
    }

    //The message could be a quoted message, a picture, or a text message
    if ([param.tag isEqualToString:kMessageTag_Quote]) {
        [self sendMessage:param.textMessage
                inReplyTo:param.quotedMessage
                 toChatId:param.chatId
             withPriority:param.priority];
    }
    else if ([param.tag isEqualToString:kMessageTag_Picture]) {
        [self sendPicture:param.picturePath
                thumbnail:param.pictureThumbnailPath
          withDescription:param.textMessage
                 toChatId:param.chatId
             withPriority:param.priority];
    }
    else if ([param.tag isEqualToString:kBBMChatMessage_TagText]) {
        [self sendMessage:param.textMessage
                 toChatId:param.chatId
             withPriority:param.priority
             withDuration:param.duration];
    }else{
        NSLog(@"Message not send.  Unhandled type or tag");
    }
}

+ (void)sendPicture:(NSString *)path
          thumbnail:(NSString *)thumbnailPath
    withDescription:(NSString *)description
           toChatId:(NSString *)chatId
       withPriority:(MessagePriority)priority
{
    BBMChatMessageSendMessage *chatMessage = [[BBMChatMessageSendMessage alloc] initWithChatId:chatId tag:kMessageTag_Picture];

    chatMessage.file = path;
    chatMessage.thumb = thumbnailPath;
    chatMessage.content = description ?: @"";
    chatMessage.thumbPolicy = kBBMChatMessageSendMessage_ThumbPolicyMove;
    chatMessage.filePolicy = kBBMChatMessageSendMessage_FilePolicyMove;

    NSMutableDictionary *msgData = [NSMutableDictionary dictionary];
    msgData[kMessageDataKey_FileExtension] = @"jpg";
    msgData[kMessageDataKey_FileUTIType] = @"public.jpeg";
    msgData[kMessageDataKey_FileName] = [path lastPathComponent];
    if (priority == kMessagePriority_High) {
        msgData[kMessageDataKey_Priority] = kMessageDataValue_PriorityHigh;
    }
    chatMessage.rawData = msgData;

    [[BBMEnterpriseService service] sendMessageToService:chatMessage];
}

+ (NSString *)saveDataToTmpTransferDir:(NSData *)data
{
    return [self saveDataToTmpTransferDir:data filename:nil];
}

+ (NSString *)saveDataToTmpTransferDir:(NSData *)data
                              filename:(NSString *)filename
{
    
    return [self saveData:data
              toDirectory:[self getTempTransferDirectory]
                 filename:filename];
}

+ (NSString *)saveData:(NSData *)data
           toDirectory:(NSString *)directory
              filename:(NSString *)filename
{
    if (directory && directory.length > 0) {
        NSString *tempFileName = filename ?: [[NSUUID UUID] UUIDString];
        NSString *dataPath = [directory stringByAppendingPathComponent:tempFileName];

        NSError *error;
        if ([data writeToFile:dataPath options:0 error:&error]) {
            if(nil == error){
                return dataPath;
            }
        }
        NSLog(@"Failed to create a file for a picture. %@",[error localizedDescription]);
    }
    return nil;
}

+ (void)sendMessage:(NSString *)message
           toChatId:(NSString *)chatId
       withPriority:(MessagePriority)priority
       withDuration:(BBMTimedMessageValue)duration
{
    NSString *tag = duration > 0 ? kMessageTag_TimedMessage : kBBMChatMessage_TagText;
    BBMChatMessageSendMessage *chatMessage = [[BBMChatMessageSendMessage alloc] initWithChatId:chatId tag:tag];
    chatMessage.content = message;

    if (priority == kMessagePriority_High) {
        chatMessage.rawData = @{kMessageDataKey_Priority: kMessageDataValue_PriorityHigh};
    }
    if(duration > 0) {
        chatMessage.rawData = @{kMessageDataKey_TimedMessage: @(duration)};
    }

    [[BBMEnterpriseService service] sendMessageToService:chatMessage];
}

+ (void)sendMessage:(NSString *)message
          inReplyTo:(BBMChatMessage *)quotedMessage
           toChatId:(NSString *)chatId
       withPriority:(MessagePriority)priority
{
    BBMChatMessageSendMessage *chatMessage = [[BBMChatMessageSendMessage alloc] initWithChatId:chatId tag:kMessageTag_Quote];
    chatMessage.content = message;

    NSMutableDictionary *data = [@{kMessageDataKey_QuoteSource: [BBMUIUtilities displayNameForUser:quotedMessage.resolvedSenderUri],
                                   kMessageDataKey_QuoteText: quotedMessage.content,
                                   kMessageDataKey_QuoteTime: [NSNumber numberWithUnsignedLongLong:quotedMessage.timestamp]} mutableCopy];

    if (priority == kMessagePriority_High) {
        data[kMessageDataKey_Priority] = kMessageDataValue_PriorityHigh;
    }
    chatMessage.rawData = [data copy];

    [[BBMEnterpriseService service] sendMessageToService:chatMessage];
}


#pragma mark - Temp Directories

+ (NSString *)getTempTransferDirectory
{
    static NSString *s_transferDir;
    if(nil == s_transferDir) {
        NSString *libraryFolder = nil;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        if (paths) {
            libraryFolder = [paths objectAtIndex:0];
            s_transferDir = [libraryFolder stringByAppendingPathComponent:@"bbm/fileTransfers"];
            [[NSFileManager defaultManager] createDirectoryAtPath:s_transferDir
                                      withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:nil];
        }
    }
    return s_transferDir;
}


#pragma mark - Search

+ (void)inAppSearch:(NSString *)searchString
{
    BBMSearchMessage *searchMsg = [[BBMSearchMessage alloc] initWithText:searchString];
    [[BBMEnterpriseService service] sendMessageToService:searchMsg];
}


#pragma mark - Chat Admin

+ (void)promoteParticipant:(BBMChatParticipant *)participant
{
    BBMParticipantPromoteMessage *msg = [[BBMParticipantPromoteMessage alloc] initWithChatId:participant.chatId
                                                                                     userUri:participant.userUri];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (void)demoteParticipant:(BBMChatParticipant *)participant
{
    BBMParticipantDemoteMessage *msg = [[BBMParticipantDemoteMessage alloc] initWithChatId:participant.chatId
                                                                                     userUri:participant.userUri];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}

+ (void)removeParticipantFromChat:(BBMChatParticipant *)participant
{
    BBMParticipantRemoveMessage *msg = [[BBMParticipantRemoveMessage alloc] initWithChatId:participant.chatId
                                                                                   userUri:participant.userUri];
    [[BBMEnterpriseService service] sendMessageToService:msg];
}


@end


@implementation BBMMessageSendParameters

- (NSString *)tag
{
    //Use the message parameters to determine the correct tag
    if(self.picturePath.length > 0) {
        return kMessageTag_Picture;
    }

    if([BBMUIUtilities stringForMessageCellForChatMessage:self.quotedMessage].length > 0) {
        return kMessageTag_Quote;
    }
    if(self.textMessage.length > 0){
        return kBBMChatMessage_TagText;
    }
    else {
        return kMessageTag_Picture;
    }
    
    return nil;
}

@end
