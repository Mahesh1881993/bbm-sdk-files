// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMChatCreator.h"

#import <BBMEnterprise/BBMEnterprise.h>

@interface BBMChatCreator () <BBMIncomingMessageListener>

@property NSMutableDictionary *callbacksByCookie;

@end

@implementation BBMChatCreator

- (id)init
{
    if( (self = [super init]) ) {
        self.callbacksByCookie = [NSMutableDictionary dictionary];
        [[BBMEnterpriseService service] addListener:self forMessageNames:@[[BBMListAddMessage messageName], [BBMChatStartFailedMessage messageName]]];
    }
    return self;
}

- (void)dealloc
{
    [[BBMEnterpriseService service] removeListener:self forMessageNames:@[[BBMListAddMessage messageName], [BBMChatStartFailedMessage messageName]]];
}

#pragma mark - Public Interface



- (void)startChatWithRegId:(NSNumber *)regId
                   subject:(NSString *)subject
                  callback:(ChatCreationCallback)callback
{
    NSAssert(nil != regId, @"You must supply a regId");
    
    [self startChatWithRegIds:@[regId]
                      subject:subject
                   isOneToOne:YES
                     callback:callback];
}


- (void)startConferenceWithRegIds:(NSArray *)regIds
                          subject:(NSString *)subject
                         callback:(ChatCreationCallback)callback
{
    [self startChatWithRegIds:regIds
                      subject:subject
                   isOneToOne:NO
                     callback:callback];
}


#pragma mark - Private Interface

- (void)startChatWithRegIds:(NSArray *)regIds
                    subject:(NSString *)subject
                 isOneToOne:(BOOL)isOneToOne
                   callback:(ChatCreationCallback)callback
{
    NSAssert(nil != callback, @"Callback cannot be nil");
    NSAssert(nil != regIds, @"RegIds cannot be nil");
    NSAssert(nil != subject, @"Subject mus be set");

    NSMutableArray *invitees = [[NSMutableArray alloc] init];
    for (NSNumber *regId in regIds) {
        BBMChatStartMessage_Invitees *invitee = [[BBMChatStartMessage_Invitees alloc] init];
        invitee.regId = regId;
        [invitees addObject:invitee];
    }
    
    BBMChatStartMessage *chatStart = [[BBMChatStartMessage alloc] initWithInvitees:invitees
                                                                           subject:subject];
    chatStart.isOneToOne = isOneToOne;
    [[BBMEnterpriseService service] sendMessageToService:chatStart];
    self.callbacksByCookie[chatStart.cookie] = [callback copy];
}

#pragma mark - BBMDSConsumer

- (void)receivedIncomingMessage:(BBMIncomingMessage *)incomingMessage
{
    //Listen for the list add for the chat or the failure message.  One or the other is always
    //posted back on the connection when we request a hosted chat.  This way, we don't have
    //to care about looking up participants or iterating through lists.  Core does it all
    //for us.
    if ([incomingMessage.messageName isEqualToString:[BBMListAddMessage messageName]]) {
        BBMListAddMessage *listAddMessage = (BBMListAddMessage *)incomingMessage;
        ChatCreationCallback callback = self.callbacksByCookie[listAddMessage.cookie];
        if (callback) {
            //This is the list add... Yeah! We found a chat
            if (listAddMessage.elements.count == 0) {
                return;
            }
            NSString *chatId = listAddMessage.elements[0][@"chatId"];
            callback(chatId, kBBMChatStartFailedMessage_Reason_Unspecified);
            
            [self.callbacksByCookie removeObjectForKey:listAddMessage.cookie];
        }
    } else {
        BBMChatStartFailedMessage *chatStartFailedMessage = (BBMChatStartFailedMessage*)incomingMessage;
        ChatCreationCallback callback = self.callbacksByCookie[chatStartFailedMessage.cookie];
        if (callback) {
            callback(nil, chatStartFailedMessage.reason);
            
            [self.callbacksByCookie removeObjectForKey:chatStartFailedMessage.cookie];
        }
    }
}

@end
