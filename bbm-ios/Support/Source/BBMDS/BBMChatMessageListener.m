// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMChatMessageListener.h"

#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMAccess.h"
#import "BBMUtilities.h"


#import "BBMMessageNotificationView.h"
#import "BBMLocalNotificationManager.h"

@interface BBMChatMessageListener ()

@property (nonatomic) NSMutableSet *excludedConvs;
@property (nonatomic) ObservableMonitorSet *monitorSet;

@end

@implementation BBMChatMessageListener

- (id)init
{
    self = [super init];
    if(self){
        self.excludedConvs = [[NSMutableSet alloc] init];
        self.monitorSet = [[ObservableMonitorSet alloc] init];
    }
    return self;
}

-(void)startListening {
    [[BBMEnterpriseService service] addListener:self forMessageTypes:@[@"chatMessage"]];
}

-(void)stopListening {
    [[BBMEnterpriseService service] removeListener:self forMessageTypes:@[@"chatMessage"]];
}

- (void)excludeConv:(NSString*)conv
{
    if(nil == conv){ return; }
    [self.excludedConvs addObject:conv];
}

- (void)stopExcludingConv:(NSString*)conv
{
    if(nil == conv){ return; }
    [self.excludedConvs removeObject:conv];
}

- (void)showNotificationForMessage:(BBMChatMessage*)message
{
    //Before showing the notification we need to make sure we have the sender's name ready.
    //We add the monitor to the monitor so that we can handle multiple messages at the same time.
    //Once the monitor has everything it needs it returns YES to terminate itself so that it gets
    //discarded.
    [self.monitorSet addMonitorActivatedWithName:@"MessageNotificationViewMessageMonitor" selfTerminatingBlock:^BOOL{
        BBMUser *sender = message.resolvedSenderUri;
        if(message.bbmState == kBBMStatePending || sender.bbmState == kBBMStatePending) {
            return NO;
        }
        //If a message is received while the app is in the background we need to post a local
        //notification so the user will know there is a new message, otherwise we show the in-app banner
        if (![BBMUtilities isApplicationInForeground]) {
            [[BBMLocalNotificationManager sharedInstance] notifyForIncomingMessage:message];
        }
        else {
            [BBMMessageNotificationView showNotification:message];
        }
        return YES;
    }];
}

- (void)receivedIncomingMessage:(BBMIncomingMessage *)incomingMessage
{
    if ([incomingMessage.messageName isEqualToString:[BBMListAddMessage messageName]]) {
        BBMListAddMessage *listAddMessage = (BBMListAddMessage*)incomingMessage;
        NSDictionary *element = listAddMessage.elements.firstObject;
        if(element == nil) {
            return;
        }
        
        NSString *conv = element[@"chatId"];
        //Only exclude conversations when the app is in the foreground
        if([BBMUtilities isApplicationInForeground] && [self.excludedConvs containsObject:conv]) {
            return;
        }
        //Get the identifier which we will be used to get the message from the model
        NSString *identifier = [BBMChatMessage identifierOfElement:element];
        BBMChatMessage *message = [[BBMAccess chatMessageMap] valueForKey:identifier];
        //Now that we have the message notify the user about it.
        [self showNotificationForMessage:message];
    }
}

@end
