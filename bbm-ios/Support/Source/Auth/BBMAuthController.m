// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMAuthController.h"
#import "ConfigSettings.h"
#import "BBMAuthenticatedAccount.h"
#import "BBMAuthenticationDelegate.h"
#import "BBMSecUtil.h"
#import "BBMKeyManager.h"
#import "BBMUserManager.h"

#import "BBMKeyStorageProvider.h"
#import "BBMAppUserSource.h"
#import "BBMUserManager.h"
#import "BBMKeyManager.h"
#import "BBMUIUtilities.h"
#import "BBMEndpointManager.h"

#import <BBMEnterprise/BBMEnterprise.h>


@interface BBMAuthController() <BBMAuthenticationDelegate>

@property (nonatomic, strong) NSHashTable *delegates;
@property (nonatomic, readwrite) id<BBMTokenManager> tokenManager;
@property (nonatomic, readwrite) BBMUserManager *userManager;
@property (nonatomic, readwrite) BBMKeyManager *keyManager;
@property (nonatomic, readwrite) BBMEndpointManager *endpointManager;

@property (nonatomic, assign) Class<BBMTokenManager> tokenManagerClass;
@property (nonatomic, assign) Class<BBMAppUserSource> userSourceClass;
@property (nonatomic, assign) Class<BBMKeyStorageProvider> keyProviderClass;

@property (nonatomic, readwrite) BBMAuthState *authState;
@property (nonatomic, readwrite) BOOL serviceStarted;

@property (nonatomic, readwrite) NSString *domain;
@property (nonatomic, assign) BBMConfigEnvironment environment;

@property (nonatomic, strong) ObservableMonitor *managementKeyMonitor;
@property (nonatomic, strong) ObservableMonitor *stateMonitor;

@end

#pragma mark -

@implementation BBMAuthController
@synthesize authState = _authState;

- (id)init
{
    NSAssert(NO, @"Use initWithAuthDelegate:rootController:tokenManager");
    return [self initWithTokenManager:nil userSource:nil keyStorageProvider:nil domain:nil environment:0];
}

- (instancetype)initWithTokenManager:(Class<BBMTokenManager>)tokenManagerClass
                          userSource:(Class<BBMAppUserSource>)userSourceClass
                  keyStorageProvider:(Class<BBMKeyStorageProvider>)keyStorageClass
                              domain:(NSString*)domain
                         environment:(BBMConfigEnvironment)environment
{
    self = [super init];
    if(self) {
        NSAssert(tokenManagerClass != Nil, @"A token manager class is required");
        self.delegates = [NSHashTable weakObjectsHashTable];
        self.authState = [BBMAuthState emptyAuthState];

        self.tokenManagerClass = tokenManagerClass;
        self.keyProviderClass = keyStorageClass;
        self.userSourceClass = userSourceClass;

        if(tokenManagerClass != Nil && userSourceClass == Nil) {
            NSLog(@"If using a token manager, a user source must also be specified");
        }

        self.serviceStarted = NO;
        self.domain = domain;
        self.environment = environment;

        //We have to remove any cached management keys on a fresh install.  It is not guaranteed that
        //the keychain entries will be cleared.
        NSNumber *fisrtBoot = [[NSUserDefaults standardUserDefaults] objectForKey:@"com.bbm.sdk.firstStart"];
        if(nil == fisrtBoot) {
            [BBMSecUtil removeCachedManagementKeys];
            [[NSUserDefaults standardUserDefaults] setObject:@(1) forKey:@"com.bbm.sdk.firstStart"];
        }
    }
    return self;
}


- (void)addDelegate:(id<BBMAuthControllerDelegate>)delegate
{
    if(delegate) {
        [self.delegates addObject:delegate];
    }
}

- (void)removeDelegate:(id<BBMAuthControllerDelegate>)delegate
{
    if(delegate) {
        [self.delegates removeObject:delegate];
    }
}

- (void)startBBMEnterpriseService
{
    //This will start the BBM service. You are required to supply a domain (supplied by Blackberry)
    //and environment.  Sandbox for testing or Production for deployment.  The supplied callback
    //will be invoked when the service has or has not successfully started.
    typeof(self) __weak weakSelf = self;
    [[BBMEnterpriseService service] start:self.domain environment:self.environment completionBlock:^(BOOL success) {
        self.serviceStarted = success;
         NSHashTable *delegates = [self.delegates copy];
        if(success) {
            id userSource = [self.userSourceClass instance];
            self.userManager = [[BBMUserManager alloc] initWithSource:userSource];

            //The static utility methods in the UI Support library require a user manager for
            //fetching display names and other pertinent user data
            [BBMUIUtilities setDefaultUserManager:weakSelf.userManager];

            id storageProvider = [self.keyProviderClass instance];
            [weakSelf.keyManager stopKeyManager];
            BBMKeyManager *keyManager = [[BBMKeyManager alloc] initWithKeyStorageProvider:storageProvider
                                                                              userManager:weakSelf.userManager];
            keyManager.sdkServiceDomain = weakSelf.domain;

            [storageProvider setTokenManager:weakSelf.tokenManager ];
            self.keyManager = keyManager;
        }
        for(id<BBMAuthControllerDelegate> delegate in delegates) {
            if([delegate respondsToSelector:@selector(serviceStateChanged:)]) {
                [delegate serviceStateChanged:success];
            }
        }
        [self monitorManagementKeys];
        [self monitorSetupAndAuthStates];
    }];

    self.tokenManager = [self.tokenManagerClass tokenManagerWithDelegate:self];
    NSAssert(self.tokenManager != nil, @"Couldn't instantiate the token manager");
}


- (void)signOut
{
    [self.keyManager stopAutomaticKeySync];

    [self.tokenManager signOut];
    self.authState = [BBMAuthState emptyAuthState];

    [[BBMEnterpriseService service] resetService];
    [BBMSecUtil removeCachedManagementKeys];
    [self.managementKeyMonitor deActivate];
    self.managementKeyMonitor = nil;
    [self.keyManager resetKeyManager];
}

- (void)signInSilently
{
    if([self.tokenManager respondsToSelector:@selector(signInSilently)]) {
        [self.tokenManager signInSilently];
    }
}

- (void)monitorManagementKeys
{
    //The BBMUserPasswordManager on RichChatApp will monitor and handle the passwordIncorrect and
    //password required states.
    typeof(self) __weak weakSelf = self;
    self.managementKeyMonitor = [ObservableMonitor monitorActivatedWithName:@"mgmtKeyMon" block:^{
        //We can only change our password if we have a valid set of management keys
        BBMManagementKeyState state =  weakSelf.keyManager.managementKeyState;

        //There are also several management key states that imply a fatal failure that we should deal with
        //here.  You may elect to handle the read and write failures differently.  For read/write
        //failures, retry.  For corrupted key data, you will likely need to purge the users private
        //key store.  For simplicity, here we log out.  For the reset state - which will occur after
        //calling resetPassword which in turn clears the users key-store entries, we must sign out
        //to ensure all local cached keys are cleared.
        if(state == kBBMMgmtKeyCorruptedKeyData ||
           state == kBBMMgmtKeyRemoteReadFailure ||
           state == kBBMMgmtKeyRemoteWriteFailure ||
           state == kBBMMgmtKeyReset)
        {
            [weakSelf signOut];
        }
    }];
}

-(void)registerLocalUser
{
    if(nil == self.authState ||
       self.authState.regId.longLongValue == 0 ||
       self.authState.account == nil)
    {
        //We don't have a valid regId for the user yet.
        return;
    }

    [self.tokenManager startDataServices:self.authState.account.accessToken
                                  idToken:self.authState.account.idToken
                                callback:^(NSString *uid, NSError *error){
                                    if(error) {
                                        NSLog(@"Database sign-in error: %@",error.localizedDescription);
                                    }
                                    else {
                                        //The accountId we want for firebase is the firebase accountId.  This will allow us
                                        //to set read/write permissions on private keys so that only the user that sets the
                                        //keys can read those keys
                                        [self monitorManagementKeys];
                                        [self.userManager registerLocalUserWithAccount:self.authState.account
                                                                                 regId:self.authState.regId.stringValue];
                                        [self.userManager refreshUserList];
                                        [self.keyManager syncProfileKeysForLocalUser:uid regId:self.authState.regId.stringValue];
                                        [self.keyManager startAutomaticKeySync];
                                    }
                                }];

}

#pragma mark - Observable Properties

- (void)setAuthState:(BBMAuthState *)authState
{
    _authState = authState;
    NSHashTable *delegates = [self.delegates copy];
    for(id<BBMAuthControllerDelegate> delegate in delegates) {
        if([delegate respondsToSelector:@selector(authStateChanged:)]) {
            [delegate authStateChanged:_authState];
        }
    }
}

- (BBMAuthState *)authState
{
    [ObservableTracker getterCalledForObject:self propertyName:@"authState"];
    return _authState;
}

- (BOOL)serviceStarted
{
    [ObservableTracker getterCalledForObject:self propertyName:@"serviceStarted"];
    return _serviceStarted;
}

- (BOOL)startedAndAuthenticated
{
    BOOL startedAndAuthenticated = self.serviceStarted &&
                                    [self.authState.authTokenState isEqualToString:kBBMAuthStateOK] &&
                                   ([self.authState.setupState isEqualToString:kBBMSetupStateSuccess] ||
                                    [self.authState.setupState isEqualToString:kBBMSetupStateFull]) &&
                                    ![self.authState.setupState isEqualToString:kBBMSetupStateOngoing];
    NSLog(@"startedAndAuthenticated--> %ld",(long) startedAndAuthenticated);
    return startedAndAuthenticated;
}

- (BBMEndpointManager *)endpointManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        BBMEndpointManager *endpointManager = [[BBMEndpointManager alloc] init];
        self.endpointManager = endpointManager;
        // The endpoint manager needs to know about auth changes to register an endpoint
        [self addDelegate:_endpointManager];
    });
    return _endpointManager;
}

- (void)monitorSetupAndAuthStates
{
    typeof(self) __weak weakSelf = self;
    self.stateMonitor = [ObservableMonitor monitorActivatedWithName:@"stateMonitor" block:^{
        NSString *authState = [[[BBMEnterpriseService sharedService] model] globalAuthTokenState];
        NSString *setupState = [[[BBMEnterpriseService sharedService] model] globalSetupStateState];

        //Disable tracking to prevent this monitor from firing when self.authState is changed
        //elsewhere.
        [ObservableTracker setTrackingDisable:YES];
        BBMAuthState *state = weakSelf.authState;
        state.authTokenState= authState;
        state.setupState = setupState;
        [ObservableTracker setTrackingDisable:NO];

        weakSelf.authState = state;
    }];
}

#pragma mark - BBMAuthenticationDelegate

- (void)localAuthDataChanged:(BBMAuthenticatedAccount *)accountData error:(NSError *)error
{
    //Handle changes in the auth state.  This method will be called whenever the authenticated
    //account information changes.
    if(error) {
        NSLog(@"Sign in error %@", error.localizedDescription);
    }

    //No account data means we have logged out.
    if(nil == accountData) {
        BBMAuthState *authState = [BBMAuthState emptyAuthState];
        self.authState = authState;
        return;
    }

    SetupStateBlock callback = ^(NSString *authTokenState, NSString *setupState, NSNumber *regId) {
        BBMAuthState *authState = [[BBMAuthState alloc] init];
        authState.account = accountData;
        authState.authTokenState = authTokenState;
        authState.setupState = setupState;

        authState.regId = regId;
        self.authState = authState;

        [self registerLocalUser];
    };

    //Send the access token and userId to the BBM service.  The supplied callback will be invoked
    //with the resulting tokenState, setupState and registrationId for the local user.  The callback
    //may be invoked several times as the setup progresses through the various states.  States such
    //as "deviceSwitch" may require user interaction.
    [[BBMEnterpriseService service] sendAuthToken:accountData.accessToken
                                      forUserName:accountData.accountId
                                  setupStateBlock:callback];
}

- (UIViewController *)authController
{
    //Provide the token manager with a root controller for presenting a modal sign in window.
    return self.rootController;
}

@end

#pragma mark -

@implementation BBMAuthState

+ (BBMAuthState *)emptyAuthState
{
    return [[BBMAuthState alloc] init];
}
@end
