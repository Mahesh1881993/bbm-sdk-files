// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>


@class BBMAuthenticatedAccount;
@class UIViewController;
@protocol BBMAuthenticationDelegate;

#pragma mark - Token manager

@protocol BBMTokenManager <NSObject>

@required

/*!
 @details
 Initialize the TokenUpdater with the supplied \a delegate.  This method will be used to instantiate
 instances of your token controller by BBMAuthController.  You implementation should invoke the
 BBMAuthenticationDelegate methods on the supplied \a delegate as needed.
 */
+ (instancetype)tokenManagerWithDelegate:(id<BBMAuthenticationDelegate>)delegate;

/*!
 @details
 This should start the back end auth services with the given access and id tokens.
 */
- (void)startDataServices:(NSString *)accessToken
                  idToken:(NSString *)idToken
                 callback:(void (^)(NSString *, NSError *))callback;

/*!
 @details
 Revoke the credentials and sign out
 */
- (void)signOut;

/*!
 Optional methods for different oAuth or OpenId implementations.  Some implementation
 implement these behaviours internally (such as GoogleId).
 */
@optional
/*! @brief Sign in silently using the last valid set of credentials */
- (void)signInSilently;

/*! @brief Trigger the sign in UI if neccesary */
- (void)signIn;

/*! @brief Returns the current users access token */
- (NSString *)accessToken;

/*! @brief Returns the access token asyncronously and refreshes it if requred */
- (void)getAccessToken:(void (^)(NSString*))callback;

@end



#pragma mark - BBMAuthenticationDelegate

@protocol BBMAuthenticationDelegate <NSObject>

@required

/*!
 @details
 Called when the authentication data changes.  The user is not authenticated and/or authentication
 has failed if accountData is nil
 */
- (void)localAuthDataChanged:(BBMAuthenticatedAccount *)accountData error:(NSError *)error;

/*!
 @details
 The delegate should return a view controller suitable for presenting a modal view controller
 */
- (UIViewController *)authController;

@end
