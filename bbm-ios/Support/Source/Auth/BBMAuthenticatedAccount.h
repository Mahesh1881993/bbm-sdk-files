// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

/*!
 @details
 Represents an authenticated account.
 */
@interface BBMAuthenticatedAccount : NSObject

+ (instancetype)accountDataWithId:(NSString *)accountId
                          idToken:(NSString *)idToken
                      accessToken:(NSString *)accessToken
                             name:(NSString *)name
                            email:(NSString *)email
                        avatarURL:(NSURL    *)avatarURL;

@property (nonatomic, readonly) NSString *accountId;
@property (nonatomic, readonly) NSString *idToken;
@property (nonatomic, readonly) NSString *accessToken;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *email;
@property (nonatomic, readonly) NSURL    *avatarUrl;


@end
