// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <BBMEnterprise/BBMEnterprise.h>


@class BBMAuthenticatedAccount;
@class BBMAuthState;
@class BBMKeyManager;
@class BBMUserManager;
@class BBMEndpointManager;

@protocol BBMAuthControllerDelegate;
@protocol BBMTokenManager;
@protocol BBMKeyStorageProvider;
@protocol BBMAppUserSource;

/*!
 @details
 BBMAuthController encapsulates the code for starting the service as well as authenticating users
 via a BBMTokenManager instance.  You may either implement BBMAuthControllerDelegate or use an
 ObservableMonitor and observe changes to authState and serviceStarted.
 */
@interface BBMAuthController : NSObject

@property (nonatomic, readonly) BBMAuthState *authState;      ///< Local user auth state.  Observable.
@property (nonatomic, readonly) BOOL serviceStarted;          ///< BBMEnterprise service state. Observable.
@property (nonatomic, readonly) BOOL startedAndAuthenticated; ///< YES if the service is started and we have an authenticated user.  Observable.


/*!
 @details
 Instantiates a new auth controller with the given \a delegate and \a rootController.  The supplied
 token, user and key storage classes will be instantiated via the designated class initializer
 in their protocol specifications when startBBMEnterpriseService is called.  Refer to the provided
 tokenManager, userSrouce and keyStorageProvider implementations.

 @param tokenManagerClass A class that implements the BBMTokenManager protocol.  The returned
                          instance of BBMAuthController will instantiate this and act as it's delegate.
 @param userSourceClass A class that implements the BBMAppUserSource protocol or nil
 @param keyStorageClass A class that implements the BBMKeyStorageProvider protocol.  If this is
                           specified, a \a userManager must also be specified
 @param domain The service domain.
 @param environment The SDK environment
 */
- (instancetype)initWithTokenManager:(Class<BBMTokenManager>)tokenManagerClass
                          userSource:(Class<BBMAppUserSource>)userSourceClass
                  keyStorageProvider:(Class<BBMKeyStorageProvider>)keyStorageClass
                              domain:(NSString*)domain
                         environment:(BBMConfigEnvironment)environment NS_DESIGNATED_INITIALIZER;

/*!
 @details
 Start the BBMEnterprise Service and create the BBMTokenManager, BBMUserManager and BBMKeyManager
 instances.
 */
- (void)startBBMEnterpriseService;


/*! @brief The supplied SDK domain */
@property (nonatomic, readonly) NSString *domain;

/*! @brief Root controller from which the tokenManager should push modal view controllers */
@property (nonatomic, weak) UIViewController *rootController;

/*!
 @brief The token manager.  This will be nil until the service has been started
 @see BBMTokenManager
 */
@property (nonatomic, readonly) id<BBMTokenManager> tokenManager;

/*!
 @brief The key manager.  This will be nil until the service has been started
 @see BBMKeyManager
 */
@property (nonatomic, readonly) BBMKeyManager *keyManager;

/*!
 @brief The user manager.  This will be nil until the service has been started
 @see BBMUserManager
 */
@property (nonatomic, readonly) BBMUserManager *userManager;

/*!
 @brief Use to manage endpoints.  Do not call this if you wish to do your own endpoint management
 @see BBMEndpointManager
 */
@property (nonatomic, readonly) BBMEndpointManager *endpointManager;

/*!
 @details
 Add a delegate to the authController
 @param delegate Delegate to add
 */
- (void)addDelegate:(id<BBMAuthControllerDelegate>)delegate;

/*!
 @details
 Remove a delegate from the authController
 @param delegate Delegate to remove
 */
- (void)removeDelegate:(id<BBMAuthControllerDelegate>)delegate;

/*!
 @details
 Signs the user out
 */
- (void)signOut;

/*!
 @details
 Signs the user in silently (resumes previous session) if supported by the token manager
 */
- (void)signInSilently;


@end


#pragma mark -

@protocol BBMAuthControllerDelegate <NSObject>
@optional
/*! @brief Called when the BBMEnterprise Service state changes (started or not started) */
- (void)serviceStateChanged:(BOOL)serviceStarted;
/*! @brief Called when a users credentials change */
- (void)authStateChanged:(BBMAuthState *)authState;
@end



/*! @brief Encapsulates the user credentials and BBM Enterprise authentication state */
@interface BBMAuthState : NSObject

+ (BBMAuthState *)emptyAuthState;

@property (nonatomic, strong) BBMAuthenticatedAccount *account;
@property (nonatomic, strong) NSString *authTokenState;
@property (nonatomic, strong) NSString *setupState;
@property (nonatomic, strong) NSNumber *regId;

@end
