// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMGoogleTokenManager.h"
#import "BBMAuthenticatedAccount.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "ConfigSettings.h"

#ifndef IGNORE_FIREBASE
#import <Firebase/Firebase.h>
#endif

#import "ConfigSettings.h"

@interface BBMGoogleTokenManager () <GIDSignInUIDelegate, GIDSignInDelegate>
@property (nonatomic, weak) id<BBMAuthenticationDelegate> delegate;
@end

@implementation BBMGoogleTokenManager 

+ (id)tokenManagerWithDelegate:(id<BBMAuthenticationDelegate>)delegate
{
    BBMGoogleTokenManager *retVal = [[BBMGoogleTokenManager alloc] init];
    if(retVal) {
        retVal.delegate = delegate;
    }

    [GIDSignIn sharedInstance].clientID = GOOGLE_SIGNIN_CLIENTID;
    [GIDSignIn sharedInstance].uiDelegate = retVal;
    [GIDSignIn sharedInstance].delegate = retVal;

    return retVal;
}


- (void)signOut
{
    [[GIDSignIn sharedInstance] disconnect];
}

- (void)signInSilently
{
    [[GIDSignIn sharedInstance] signInSilently];
}

- (void)startDataServices:(NSString *)accessToken
                  idToken:(NSString *)idToken
                 callback:(void (^)(NSString *, NSError *))callback
{

#ifndef IGNORE_FIREBASE
    //Register the local user with our Firebase instance.  This will upload the local user
    //credentials to shared storage so that they can be searched/fetched by other users
    FIRAuthCredential *credential = [FIRGoogleAuthProvider credentialWithIDToken:idToken
                                                                     accessToken:accessToken];


    //Start Firebase with the GoogleSignIn credentials
    [[FIRAuth auth] signInWithCredential:credential completion:^(FIRUser * _Nullable firebaseUser, NSError * _Nullable error) {
        callback(firebaseUser.uid, error);
    }];
#endif

}

#pragma mark - Google Sign-In Delegate

- (void)googleSignInStateChanged:(NSError *)error
{
    GIDGoogleUser *user = [GIDSignIn sharedInstance].currentUser;

    if(nil == user) {
        [self.delegate localAuthDataChanged:nil error:error];
        return;
    }

    BBMAuthenticatedAccount *accountData = [BBMAuthenticatedAccount accountDataWithId:user.userID
                                                                                idToken:user.authentication.idToken
                                                                          accessToken:user.authentication.accessToken
                                                                                 name:user.profile.name
                                                                                email:user.profile.email
                                                                            avatarURL:[user.profile imageURLWithDimension:120]];

    [self.delegate localAuthDataChanged:accountData error:error];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{

    [self googleSignInStateChanged:error];
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    [self googleSignInStateChanged:error];
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [[self.delegate authController] presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
{
    [[self.delegate authController] dismissViewControllerAnimated:YES completion:nil];
}


@end
