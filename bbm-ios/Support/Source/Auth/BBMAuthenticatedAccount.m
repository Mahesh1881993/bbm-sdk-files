// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#include "BBMAuthenticatedAccount.h"
#import "SharedData.h"

@interface BBMAuthenticatedAccount ()

@property (nonatomic, readwrite) NSString *accountId;
@property (nonatomic, readwrite) NSString *idToken;
@property (nonatomic, readwrite) NSString *accessToken;
@property (nonatomic, readwrite) NSString *name;
@property (nonatomic, readwrite) NSString *email;
@property (nonatomic, readwrite) NSURL    *avatarUrl;

@end

@implementation BBMAuthenticatedAccount

+ (instancetype)accountDataWithId:(NSString *)accountId
                          idToken:(NSString *)idToken
                      accessToken:(NSString *)accessToken
                             name:(NSString *)name
                            email:(NSString *)email
                        avatarURL:(NSURL    *)avatarURL
{
    BBMAuthenticatedAccount *retVal = [[BBMAuthenticatedAccount alloc] init];
    retVal.accountId = [accountId copy];
    retVal.idToken = [idToken copy];
    retVal.accessToken = [accessToken copy];
    retVal.name = [name copy];
    retVal.email = [email copy];
    retVal.avatarUrl = [avatarURL copy];
    [SharedData sharedData].userName = retVal.name;
    
    /*Here accessToken and user_name has been stored in NSUser Defaults for Later Use in the application */
    
    [[NSUserDefaults standardUserDefaults] setObject:retVal.accessToken forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] setObject:retVal.name forKey:@"user_name"];


    return retVal;

}



@end
