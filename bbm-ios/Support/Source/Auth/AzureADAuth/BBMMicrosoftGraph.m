// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMMicrosoftGraph.h"
#import "BBMAzureTokenManager.h"


static NSString *const kBBMEUserExtensionId     = @"com.bbm.sdk.example.azure.userValues";
static NSString *const kNoTokenErrorDomain      = @"com.bbmenterprise.azure.noToken";

//MS Graph REST API Paths.  If you require additional user object fields, add the field values to
//The select parameters here.
static NSString *const kPathPrefix              = @"https://graph.microsoft.com/v1.0";
static NSString *const kCurrentUserPath         = @"/me?$select=id,displayName,mail&$expand=extensions";
static NSString *const kAllUsersPath            = @"/users?$select=id,displayName,mail&$expand=extensions";
static NSString *const kUserPatchExtPathFmt     = @"/me/extensions/%@";
static NSString *const kUserPatchExtPost        = @"/me/extensions";

//Post Bodies
static NSString *const kUserExtBodyFmt          = @"{\"%@\":\"%@\","
                                                  @"\"@odata.type\":\"microsoft.graph.openTypeExtension\","
                                                  @"\"extensionName\":\"%@\" }";


@implementation BBMMicrosoftGraph

+ (instancetype)sharedInstance
{
    static BBMMicrosoftGraph *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BBMMicrosoftGraph alloc] init];
    });
    return sharedInstance;
}


#pragma mark - Public Convenience

- (void)getCurrentUser:(MSGraphApiResponse)callback
{
    [self getResource:kCurrentUserPath callback:callback];
}


- (void)getAllUsers:(MSGraphApiResponse)callback
{
    [self getResource:kAllUsersPath callback:callback];
}


- (void)updateLocalUserField:(NSString *)field
                       value:(NSString *)value
                    callback:(MSGraphApiResponse)callback
{
    NSString *path = [NSString stringWithFormat:kUserPatchExtPathFmt, kBBMEUserExtensionId];
    NSString *body = [NSString stringWithFormat:@"{\"%@\":\"%@\"}", field, value];
    [self updateResource:path method:@"PATCH" body:body callback:callback];
}


- (void)addLocalUserField:(NSString *)field
                    value:(NSString *)value
                 callback:(MSGraphApiResponse)callback
{
    NSString *body = [NSString stringWithFormat:kUserExtBodyFmt, field, value, kBBMEUserExtensionId];
    [self updateResource:kUserPatchExtPost method:@"POST" body:body callback:callback];
}


#pragma mark - Public General

- (void)updateResource:(NSString *)path
                method:(NSString *)method
                  body:(NSString *)body
              callback:(MSGraphApiResponse)callback
{
    [self.tokenManager getMSGraphToken:^(NSString *token) {
        if(nil == token) {
            [self sendTokenError:callback];
            return;
        }
        NSURLRequest *request = [self constructRequest:path method:method token:token body:body];
        [self sendRequest:request callback:callback];
    }];
}



- (void)getResource:(NSString *)path
           callback:(MSGraphApiResponse)callback
{
    [self.tokenManager getMSGraphToken:^(NSString *token) {
        if(nil == token) {
            [self sendTokenError:callback];
            return;
        }
        NSURLRequest *request = [self constructRequest:path method:@"GET" token:token body:nil];
        [self sendRequest:request callback:callback];
    }];
}


#pragma mark - Internal

- (NSMutableURLRequest *)constructRequest:(NSString *)path
                                   method:(NSString *)method
                                    token:(NSString *)token
                                     body:(NSString *)body
{
    NSString *tokenHeader = [NSString stringWithFormat:@"Bearer %@",token];
    NSString *fullPath = [NSString stringWithFormat:@"%@%@",kPathPrefix, path];

    NSURL *url = [NSURL URLWithString:fullPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:tokenHeader forHTTPHeaderField:@"Authorization"];
    [request setValue:@"graph.microsoft.com" forHTTPHeaderField:@"Host"];
    [request setHTTPMethod:method];

    if(body) {
        request.HTTPBody = [body dataUsingEncoding:NSUTF8StringEncoding];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    }

    return request;
}


- (void)sendRequest:(NSURLRequest *)request
           callback:(MSGraphApiResponse)callback
{
    void (^handler)(NSData*, NSURLResponse*, NSError*) = ^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = data ? [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] : nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(callback) {
                callback(response, json, error);
                
            }
        });
    };

    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                             completionHandler:handler];
    [task resume];
}


- (void)sendTokenError:(MSGraphApiResponse)callback
{
    if(callback) {
        callback(nil, nil, [NSError errorWithDomain:kNoTokenErrorDomain code:0 userInfo:nil]);
    }
}


#pragma mark - User Exensions

- (NSString *)regIdForUserData:(NSDictionary *)userData
{
    NSArray *exentions = userData[@"extensions"];
    for(NSDictionary *extension in exentions) {
        if([extension[@"id"] isEqualToString:kBBMEUserExtensionId]) {
            return extension[@"regId"];
        }
    }
    return nil;
}


- (BOOL)userDataHasExtension:(NSDictionary *)userData
{
    NSArray *exentions = userData[@"extensions"];
    for(NSDictionary *extension in exentions) {
        if([extension[@"id"] isEqualToString:kBBMEUserExtensionId]) {
            return YES;
        }
    }
    return NO;
}



@end
