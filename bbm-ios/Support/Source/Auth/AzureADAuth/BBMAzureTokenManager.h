// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import "BBMAuthenticationDelegate.h"

/*!
 @abstract
 BBMTokenManager implementation based on Azure Active Directory.
 */
@interface BBMAzureTokenManager : NSObject <BBMTokenManager>

/*!
 @details
 Sets the Azure client ID, tenantID and scope.  See ConfigSettings.h for the values required
 here.  This must be called before attempting to start the BBM Enterprise service.
 */
+ (void)setClientId:(NSString *)clientId
           tenantId:(NSString *)tenantId
           bbmScope:(NSString *)bbmscope;

/*! @brief Returns the oAuth account associated with the current session/user */
- (NSString *)accessToken;

/*! @brief Asynchronously get the MSGraph token for user related REST API calls */
- (void)getMSGraphToken:(void (^)(NSString*))callback;

/*! @brief Asynchronously get the Access token silently for user related REST API calls */
- (void)getAccessTokenSilent:(void (^)(NSString*))callback;


@end



