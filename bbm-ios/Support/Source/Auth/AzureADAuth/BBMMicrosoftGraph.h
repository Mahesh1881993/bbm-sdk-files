// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

@class BBMAzureTokenManager;

typedef void (^MSGraphApiResponse)(NSURLResponse *, NSDictionary *, NSError *);



/*!
 @abstract
 Wrapper for the required User operations on the Microsoft Graph API.   
 */
@interface BBMMicrosoftGraph : NSObject


/*! @brief Returns the shared instance of the MS Graph API */
+ (instancetype)sharedInstance;


/*! @brief Sets the token manager for authenticating requests */
@property (nonatomic, strong) BBMAzureTokenManager *tokenManager;


#pragma mark - General Graph API Access

/*!
 @details
 Gets the specific graph resource an returns the result asyncronously via the callback.
 The callback will include an NSDictionary representation of the resulting JSON.
 @param path The Graph API path (eg: /me or /users or /users?$select,id,displayName etc)
 @param callback Callback to be invoked on completion
 */
- (void)getResource:(NSString *)path callback:(MSGraphApiResponse)callback;

/*!
 @details
 Updates the specific graph resource an returns the result asyncronously via the callback.
 The callback will include an NSDictionary representation of the resulting JSON.
 @param path The Graph API path (eg: /me or /users or /users?$select,id,displayName etc)
 @param method POST or PATCH
 @param body The POST or PATCH body
 @param callback Callback to be invoked on completion
 */
- (void)updateResource:(NSString *)path
                method:(NSString *)method
                  body:(NSString *)body
              callback:(MSGraphApiResponse)callback;



#pragma mark - Convenience Methods

/*!
 @details
 Gets the current active directory details
 @param callback Callback to be invoked on completion
 */
- (void)getCurrentUser:(MSGraphApiResponse)callback;


/*!
 @details
 Gets a list of users in the directory with a valid registation ID
 @param callback Callback to be invoked on completion
 */
- (void)getAllUsers:(MSGraphApiResponse)callback;


/*!
 @details
 Adds a custom field to the BBME extension on the local user data.  Use this method if
 userDataHasExtension returns NO.
 @param field The field Name
 @param value The field Value
 @param callback Callback to be invoked on completion
 */
- (void)addLocalUserField:(NSString *)field
                    value:(NSString *)value
                 callback:(MSGraphApiResponse)callback;


/*!
 @details
 Updates a custom field on the BBME extension for the local user.  Use this method if
 userDataHasExtension returns YES.
 @param field The field Name
 @param value The field Value
 @param callback Callback to be invoked on completion
 */
- (void)updateLocalUserField:(NSString *)field
                       value:(NSString *)value
                    callback:(MSGraphApiResponse)callback;


#pragma mark - Utilities

/*!
 @details
 Returns TRUE if the userData contains the bbme extension
 */
- (BOOL)userDataHasExtension:(NSDictionary *)userData;


/*!
 @details
 Returns the regId for the given user data by searching in the user data extensions
 @return NSString The users registration id or nil if it doesn't exist
 */
- (NSString *)regIdForUserData:(NSDictionary *)userData;

@end
