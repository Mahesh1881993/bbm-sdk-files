// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import "BBMAzureTokenManager.h"
#import "BBMAuthenticatedAccount.h"
#import "BBMMicrosoftGraph.h"
#import <MSAL/MSAL.h>


@interface BBMAzureTokenManager ()
@property (nonatomic, weak) id<BBMAuthenticationDelegate> delegate;
@property (nonatomic, readwrite) NSDictionary *localUserData;
@property (nonatomic, strong) MSALPublicClientApplication *alClient;

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *userIdentifier;
@property (nonatomic, strong) MSALUser *localUser;
@end


@implementation BBMAzureTokenManager

static NSString *s_clientId = nil;
static NSString *s_tenantId = nil;
static NSString *s_bbmScope = nil;

+ (void)setClientId:(NSString *)clientId
           tenantId:(NSString *)tenantId
           bbmScope:(NSString *)bbmscope
{
    s_clientId = clientId;
    s_tenantId = tenantId;
    s_bbmScope = bbmscope;
}


+ (instancetype)tokenManagerWithDelegate:(id<BBMAuthenticationDelegate>)delegate;
{
    NSAssert(nil != s_clientId, @"You have to specify an Azure client ID before starting the service");
    NSAssert(nil != s_tenantId, @"You have to specify an Tenant ID before starting the service");
    NSAssert(nil != s_bbmScope, @"You have to specify an the bbm API grant scope before starting the service");

    BBMAzureTokenManager *retVal = [[BBMAzureTokenManager alloc] init];
    if(retVal) {
        retVal.delegate = delegate;
    }

    NSError *error = nil;
    NSString *authority = [NSString stringWithFormat:@"https://login.microsoftonline.com/%@", s_tenantId];
    retVal.alClient = [[MSALPublicClientApplication alloc] initWithClientId:s_clientId
                                                                  authority:authority
                                                                      error:&error];

    if(error) {
        NSLog(@"CRITICAL ERROR: Could not construct auth client: %@", error.localizedDescription);
    }

    //Apply this token manager to the BBMMicrosoftGraph instance so that it can authorize requests
    [BBMMicrosoftGraph sharedInstance].tokenManager = retVal;

    return retVal;
}

- (void)startDataServices:(NSString *)accessToken
                  idToken:(NSString *)idToken
                 callback:(void (^)(NSString *, NSError *))callback
{
    //User registration is done automatically with the Azure implementation.  We simply need to
    //callback that the local user is registered.
    callback(self.userIdentifier, nil);
}



- (void)signOut
{
    NSError *error = nil;
    [self.alClient removeUser:self.localUser error:&error];
    self.localUser = nil;
    self.accessToken = nil;
    self.userIdentifier = nil;
    [self.delegate localAuthDataChanged:nil error:nil];
}


- (void)signInSilently
{
    //If the MSALClient has no users, we have no cached credentials.
    NSArray *users = [self.alClient users:nil];
    if(users.count == 0) {
        [self.delegate localAuthDataChanged:nil error:nil];
        return;
    }
    
    /*Here multiple scope is added in the array*/
    NSArray *scopes = @[@"https://graph.microsoft.com/User.Read", @"https://graph.microsoft.com/User.ReadWrite",
                        @"https://graph.microsoft.com/User.ReadBasic.All", @"https://graph.microsoft.com/Directory.Read.All", @"https://graph.microsoft.com/Group.Read.All", @"https://graph.microsoft.com/Group.ReadWrite.All", @"https://graph.microsoft.com/Directory.ReadWrite.All"];
    [self.alClient acquireTokenSilentForScopes:scopes
                                          user:users.firstObject
                               completionBlock:^(MSALResult *result, NSError *error)
    {
         [self handleAuthResult:result error:error];
    }];
}


- (void)signIn
{
  
    /*Here multiple scope is added in the array*/
    NSArray *scopes = @[@"https://graph.microsoft.com/User.Read", @"https://graph.microsoft.com/User.ReadWrite",
                        @"https://graph.microsoft.com/User.ReadBasic.All", @"https://graph.microsoft.com/Directory.Read.All", @"https://graph.microsoft.com/Group.Read.All", @"https://graph.microsoft.com/Group.ReadWrite.All", @"https://graph.microsoft.com/Directory.ReadWrite.All"];
    
    [self.alClient acquireTokenForScopes:scopes
                         completionBlock:^(MSALResult *result, NSError *error)
    {
        [self handleAuthResult:result error:error];
    }];
}


- (void)handleAuthResult:(MSALResult *)result error:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!error) {
            self.accessToken = result.accessToken;
            self.userIdentifier = result.user.uid;
            self.localUser = result.user;
            [[BBMMicrosoftGraph sharedInstance] getCurrentUser:^(NSURLResponse *response, NSDictionary *data, NSError *error) {
                if(nil == error) {
                    self.localUserData = data;
                    [self.delegate localAuthDataChanged:[self getAccount] error:error];
                }else{
                    NSLog(@"Error getting user data: %@", error.localizedDescription);
                    [self signOut];
                }
            }];
        }else{
            [self.delegate localAuthDataChanged:nil error:error];
            NSLog(@"Error getting token %@", error.localizedDescription);
        }
    });  

}

- (void)getAccessToken:(void (^)(NSString*))callback
{
   /*Here multiple scope is added in the array*/
    NSArray *scopes = @[@"https://graph.microsoft.com/User.Read", @"https://graph.microsoft.com/User.ReadWrite",
                        @"https://graph.microsoft.com/User.ReadBasic.All", @"https://graph.microsoft.com/Directory.Read.All", @"https://graph.microsoft.com/Group.Read.All", @"https://graph.microsoft.com/Group.ReadWrite.All", @"https://graph.microsoft.com/Directory.ReadWrite.All"];
    [self.alClient acquireTokenForScopes:scopes
                         completionBlock:^(MSALResult *result, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if(!error) {
                 callback(result.accessToken);
                 NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:result.accessToken forKey:@"token"];
             }else{
                 NSLog(@"Error getting access token %@", error.localizedDescription);
                 callback(nil);
             }
         });

     }];
}

- (void)getAccessTokenSilent:(void (^)(NSString*))callback
{
   /*Here multiple scope is added in the array*/
    NSArray *scopes = @[@"https://graph.microsoft.com/User.Read", @"https://graph.microsoft.com/User.ReadWrite",
                        @"https://graph.microsoft.com/User.ReadBasic.All", @"https://graph.microsoft.com/Directory.Read.All", @"https://graph.microsoft.com/Group.Read.All", @"https://graph.microsoft.com/Group.ReadWrite.All", @"https://graph.microsoft.com/Directory.ReadWrite.All"];
    
    [self.alClient acquireTokenSilentForScopes:scopes
                                          user:self.localUser
                         completionBlock:^(MSALResult *result, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if(!error) {
                 callback(result.accessToken);
                 NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:result.accessToken forKey:@"token"];
                 NSLog(@"SILENT ACCESS TOKEN --> %@", result.accessToken);
             }else{
                 NSLog(@"Error getting access token %@", error.localizedDescription);
                 callback(nil);
             }
         });

     }];
}

- (void)getMSGraphToken:(void (^)(NSString*))callback
{
    /*Here multiple scope is added in the array*/
    NSArray *scopes = @[@"https://graph.microsoft.com/User.Read", @"https://graph.microsoft.com/User.ReadWrite",
                        @"https://graph.microsoft.com/User.ReadBasic.All", @"https://graph.microsoft.com/Directory.Read.All", @"https://graph.microsoft.com/Group.Read.All", @"https://graph.microsoft.com/Group.ReadWrite.All", @"https://graph.microsoft.com/Directory.ReadWrite.All"];
    

    [self.alClient acquireTokenSilentForScopes:scopes
                                          user:self.localUser
                               completionBlock:^(MSALResult *result, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!error) {
                callback(result.accessToken);
                NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:result.accessToken forKey:@"token"];
            }else{
                NSLog(@"Error getting graph token %@", error.localizedDescription);
                callback(nil);
            }
        });
    }];
}


- (BBMAuthenticatedAccount *)getAccount
{
    if(nil == self.localUserData || nil == self.localUser) {
        return nil;
    }

    BBMAuthenticatedAccount *bbmAcct  = [BBMAuthenticatedAccount accountDataWithId:self.localUser.uid
                                                                           idToken:nil
                                                                       accessToken:self.accessToken
                                                                              name:self.localUserData[@"displayName"]
                                                                             email:nil
                                                                         avatarURL:nil];
    return bbmAcct;
}




@end
