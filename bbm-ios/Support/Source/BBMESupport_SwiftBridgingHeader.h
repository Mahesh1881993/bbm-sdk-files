// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "ConfigSettings.h"

#import "BBMAuthController.h"
#import "BBMAuthenticatedAccount.h"
#import "BBMAuthenticationDelegate.h"

#if USE_GOOGLEID || !USE_AZUREAD
#import "BBMGoogleTokenManager.h"
#import "BBMFirebaseKeyStorageProvider.h"
#import "BBMFirebaseUserManager.h"
#elif USE_AZUREAD
#import "BBMAzureTokenManager.h"
#import "BBMAzureCosmosKeyStorageProvider.h"
#import "BBMAzureUserManager.h"
#import "BBMMicrosoftGraph.h"
#endif

#import "BBMAccess.h"
#import "BBMChatCreator.h"
#import "BBMEndpointManager.h"

#import "BBMUIUtilities.h"
#import "BBMUIWidgetsConfig.h"
#import "BBMChatMessageDataSource.h"
#import "BBMChatViewController.h"


#import "BBMMessageCell.h"
#import "BBMFileMessageCell.h"
#import "BBMPictureMessagCell.h"
#import "BBMTextMessageCell.h"
#import "BBMQuotedMessageCell.h"

#import "BBMMessageInputView.h"
#import "BBMTimedMessageViewController.h"
#import "BBMMediaCallHelper.h"
#import "BBMMessageLoader.h"
#import "BBMTimedMessageManager.h"
#import "BBMGroupChatMessageDetailCell.h"
#import "BBMGroupChatMessageDetailView.h"
#import "BBMChatParticipantsViewController.h"

#import "BBMChatMessage+Extensions.h"
#import "BBMUser+Extensions.h"

#import "BBMLocalNotificationManager.h"
#import "BBMMessageNotificationView.h"
#import "UIView+Extra.h"

#import "BBMImageCache.h"
#import "BBMImageLoader.h"
#import "BBMImageProvider.h"

#import "BBMAppUser.h"
#import "BBMAppUserListener.h"
#import "BBMAppUserSource.h"
#import "BBMUserManager.h"

#import "BBMUtilities.h"
#import "NSString+Base64.h"
#import "NSURL+Archiving.h"
#import "NSMutableAttributedString+Util.h"

#import "BBMKeyManager.h"
#import "BBMKeyPair.h"
#import "BBMKeyStorageProvider.h"
#import "BBMProfileKeys.h"
#import "BBMEndpoint.h"


//If you wish to use firebase for key storage, include the following headers
//These will require the Firebase library to be imported


