// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

#import <BBMEnterprise/BBMEnterprise.h>

@interface BBMEndpoint : NSObject

@property (nonatomic, strong, readonly) NSString *endpointId;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *endpointDescription;
@property (nonatomic, assign, readonly) BOOL isLocalEndpoint;

/*!
 @details
 Creates an array of BBMEndpoint objects from a BBMEndpointsMessage object.
 @param message The message received containing the list of endpoints.
 @return NSArray The BBMEndpoint objects created from the message.
 */
+ (NSArray *)endpointsFromMessage:(BBMEndpointsMessage *)message;

@end
