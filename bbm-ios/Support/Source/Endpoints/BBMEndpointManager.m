// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMEndpointManager.h"
#import "BBMAccess.h"
#import "BBMEndpoint.h"

@interface BBMEndpointManager () <BBMIncomingMessageListener>

@property (nonatomic, strong) NSMapTable *callbacks;
@property (nonatomic, assign) long long maxEndpoints;
@property (nonatomic, assign) BOOL retrySetupAfterDeregistration;
@property (nonatomic,   copy) NSString *lastSetupState;;

@end

@implementation BBMEndpointManager

- (id)init
{
    self = [super init];
    self.callbacks = [NSMapTable weakToStrongObjectsMapTable];
    // Listen for the list of endpoints or any endpoint update results.
    NSArray *messageNames = @[[BBMEndpointsMessage messageName],
                              [BBMEndpointUpdateResultMessage messageName],
                              [BBMEndpointDeregisterResultMessage messageName]];
    [[BBMEnterpriseService service] addListener:self
                                forMessageNames:messageNames];
    return self;
}

-(void)addEndpointListener:(id)listener callback:(EndpointsUpdatedCallback)callback;
{
    [self.callbacks setObject:[callback copy] forKey:listener];
}

- (void)removeEndpointListener:(id)listener
{
    [self.callbacks removeObjectForKey:listener];
}

- (NSArray*)endpointsWithLocalEndpointFirst:(NSArray *)endpoints
{
    return [endpoints sortedArrayUsingComparator:^NSComparisonResult(BBMEndpoint *a, BBMEndpoint *b)
            {
                return a.isLocalEndpoint ? NSOrderedAscending : NSOrderedDescending;
            }];
}

- (void)registerEndpoint:(NSString *)name description:(NSString *)description
{
    // Set the name and description of the endpoint. These values are returned in the endpoint list.
    // The endpoint id is nil because it's the local endpoint
    [BBMAccess updateEndpoint:nil name:name description:description];
}

- (void)deregisterEndpoint:(NSString *)endpointId
{
    // Once an endpoint is deregistered it won't show up in the endpoint list and it won't take up a
    // spot
    [BBMAccess sendEndpointDeregister:endpointId];
}

- (void)deregisterAnyEndpointAndContinueSetup
{
    typeof(self) __weak weakSelf = self;
    EndpointsUpdatedCallback callback = ^(NSArray *endPoints) {
        if(endPoints.count > 0) {
            weakSelf.retrySetupAfterDeregistration = YES;
            BBMEndpoint *endpoint = endPoints.lastObject;
            [weakSelf deregisterEndpoint:endpoint.endpointId];
        }
        // Now that we have removed and endpoint remove the listener
        [weakSelf removeEndpointListener:weakSelf];
    };
    [weakSelf addEndpointListener:self
                         callback:callback];
    [BBMAccess requestEndpoints];
}

- (void)deregisterAllNonLocalEndpoints
{
    typeof(self) __weak weakSelf = self;
    EndpointsUpdatedCallback callback = ^(NSArray *endPoints) {
        for(BBMEndpoint *endpoint in endPoints) {
            if(!endpoint.isLocalEndpoint) {
                [weakSelf deregisterEndpoint:endpoint.endpointId];
            }
        }
        // Now that we have removed and endpoint remove the listener
        [weakSelf removeEndpointListener:weakSelf];
    };
    [weakSelf addEndpointListener:self
                         callback:callback];
    [BBMAccess requestEndpoints];
}

#pragma mark - BBMIncomingMessageListener

- (void)receivedIncomingMessage:(BBMIncomingMessage *)incomingMessage
{
    if ([incomingMessage.messageName isEqualToString:[BBMEndpointsMessage messageName]]) {
        // This is the lsit of endpoints
        BBMEndpointsMessage *endpointsMessage = (BBMEndpointsMessage*)incomingMessage;
        self.maxEndpoints = endpointsMessage.maxEndpoints.longLongValue;
        // Create instances of BBMEndpoint from the contents of the BBMEndpointsMessage message.
        NSArray *endpoints = [BBMEndpoint endpointsFromMessage:endpointsMessage];
        endpoints = [self endpointsWithLocalEndpointFirst:endpoints];
        // Notify all listeners by executing the callbacks to send the updates list of endpoints
        for(EndpointsUpdatedCallback callback in [self.callbacks objectEnumerator].allObjects) {
            if(callback) {
                callback(endpoints);
            }
        }
    }
    else if ([incomingMessage.messageName isEqualToString:[BBMEndpointUpdateResultMessage messageName]]){
        // If there is an endpoint update request the list of endpoints since the list is not updated
        // automatically.
        [BBMAccess requestEndpoints];
    }
    else if([incomingMessage.messageName isEqualToString:[BBMEndpointDeregisterResultMessage messageName]]){
        // If there is an endpoint update request the list of endpoints since the list is not updated
        // automatically.
        [BBMAccess requestEndpoints];

        if(self.retrySetupAfterDeregistration) {
            [BBMAccess sendSetupRetry];
            self.retrySetupAfterDeregistration = NO;
        }
    }
}

#pragma mark - BBMAuthenticationDelegate

- (void)authStateChanged:(BBMAuthState *)authState
{
    if([authState.setupState isEqualToString:self.lastSetupState]) {
        return;
    }

    self.lastSetupState = authState.setupState;

    if([authState.setupState isEqualToString:kBBMSetupStateSuccess]) {
        // Once setup is ongoing we register the endpoint to ensure it has a name and description
        [self registerEndpoint:[UIDevice currentDevice].name  description:[UIDevice currentDevice].model];
    }

    if([authState.setupState isEqualToString:kBBMSetupStateSuccess] ||
       [authState.setupState isEqualToString:kBBMSetupStateFull]) {
        //Update the endpoint list if setup state is success or full
        [BBMAccess requestEndpoints];
    }
}

@end

