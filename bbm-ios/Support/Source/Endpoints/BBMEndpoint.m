// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMEndpoint.h"

@interface BBMEndpoint ()

@property (nonatomic, readwrite) NSString *endpointId;
@property (nonatomic, readwrite) NSString *name;
@property (nonatomic, readwrite) NSString *endpointDescription;
@property (nonatomic, readwrite) BOOL isLocalEndpoint;

@end

@implementation BBMEndpoint

+ (NSArray*)endpointsFromMessage:(BBMEndpointsMessage *)message
{
    NSMutableArray *endpoints = [NSMutableArray array];
    for (BBMEndpointsMessage_RegisteredEndpoints *endpoint in message.registeredEndpoints) {
        BBMEndpoint *newEndpoint = [BBMEndpoint endpointWithId:endpoint.endpointId
                                                          name:endpoint.nickname
                                                   description:endpoint.objectDescription
                                               isLocalEndpoint:endpoint.isCurrent];
        [endpoints addObject:newEndpoint];
    }
    return endpoints;
}

+ (BBMEndpoint*)endpointWithId:(NSString *)endpointId
                          name:(NSString *)name
                   description:(NSString *)description
                 isLocalEndpoint:(BOOL)isLocalEndpoint
{
    BBMEndpoint *endpoint = [[BBMEndpoint alloc] init];
    endpoint.endpointId = endpointId;
    endpoint.name = name;
    endpoint.endpointDescription = description;
    endpoint.isLocalEndpoint = isLocalEndpoint;
    return endpoint;
}


@end
