// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMAuthController.h"

typedef void (^EndpointsUpdatedCallback)(NSArray *endPoints);

/*!
 @details
 Convenience class for managing endpoints.   This class will automatically listen to the BBMDS interface
 for the incoming BBMEndpointsMessage message and call the supplied callbacks.
 */
@interface BBMEndpointManager : NSObject <BBMAuthControllerDelegate>

/*!
 @details
 Adds a listener that will get notified when the list of endpoints is received. Calling this results
 in a BBMEndpointsGetMessage message being sent. Once a response is received the \a callback will be called.
 @param listener This is used as a key to be able to remove a listener.
 @param callback Callback to be run when the list of endpoints is received.
 */
- (void)addEndpointListener:(id)listener callback:(EndpointsUpdatedCallback)callback;

/*!
 @details
 Removes an endpoint listener.
 @param listener This is the listener to be removed.
 */
- (void)removeEndpointListener:(id)listener;

/*!
 @details
 Deregisters the endpoint that matches the endpoint id.
 @param endpointId The endpoint id for the endpoint to deregister. This id can be obtained from the
 list of endpoints received when sending a BBMEndpointsGetMessage message.
 */
- (void)deregisterEndpoint:(NSString *)endpointId;

/*!
 @details
 Deregisters all endpoints except the current device
 */
- (void)deregisterAllNonLocalEndpoints;


/*!
 @details
 Returns the maximum number of endpoints that can be registered.
 @return long long The maximum number of endpoints that can be registered.
 */
- (long long)maxEndpoints;

/*!
 @details
 Deregisters a random endpoint and sends a BBMSetupRetryMessage once the endpoint has been removed.
 */
- (void)deregisterAnyEndpointAndContinueSetup;

@end
