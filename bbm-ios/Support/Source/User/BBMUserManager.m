// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMUserManager.h"
#import <BBMEnterprise/BBMEnterprise.h>

#import "BBMAuthenticatedAccount.h"
#import "BBMAccess.h"


@interface BBMUserManager ()


@property (nonatomic) NSHashTable *listeners;
@property (nonatomic) NSMutableArray *users;
@property (nonatomic) NSMutableDictionary *usersByRegId;
@property (nonatomic) NSMutableDictionary *appUsersByRegId;
@property (nonatomic) NSMutableDictionary *appUsersByUid;
@property (nonatomic) ObservableMonitor *localUserMonitor;


@property (nonatomic) id<BBMAppUserSource> userSource;
@end

@implementation BBMUserManager

- (id)initWithSource:(id<BBMAppUserSource>)source
{
    self = [super init];
    if(self) {
        self.listeners = [NSHashTable weakObjectsHashTable];
        self.userSource = source;
        typeof(self) __weak weakSelf = self;
        [self.userSource setUserListChangedCallback:^(NSArray *appUsers, NSDictionary *usersByRegId) {
            weakSelf.users = [appUsers mutableCopy];
            weakSelf.usersByRegId = [usersByRegId mutableCopy];
            weakSelf.appUsersByUid = [NSMutableDictionary dictionary];
            for(BBMAppUser *user in appUsers) {
                weakSelf.appUsersByUid[user.uid] = user;
            }
            [weakSelf filterLocalUserAndNotify];
        }];
        [self.userSource start];

        //This avoids the need to wait until we have our local regId to start the user manage.  We'll monitor
        //the regId and filter our lists once we know our own regId
        self.localUserMonitor = [ObservableMonitor monitorActivatedWithName:@"localUserFilter" block:^{
            [weakSelf filterLocalUserAndNotify];
        }];
    }
    return self;
}


- (void)filterLocalUserAndNotify
{
    NSNumber *localRegId = [BBMAccess currentUser].regId;
    if(localRegId) {
        BBMAppUser *user = self.usersByRegId[localRegId];
        [self.users removeObject:user];
        [self.usersByRegId removeObjectForKey:localRegId];
        [self.appUsersByRegId removeObjectForKey:localRegId];
    }
    [self notifyListeners];
}


- (void)registerLocalUserWithAccount:(BBMAuthenticatedAccount *)account
                               regId:(NSString *)regId
{
    if(account.accountId.length == 0 ||
       regId.length == 0 ||
       account.name.length == 0)
    {
        NSLog(@"Invalid user data");
        return;
    }

    BBMAppUser *user = [[BBMAppUser alloc] init:account.accountId
                                            regId:regId.longLongValue
                                             name:account.name
                                            email:account.email
                                       avatarUrl:account.avatarUrl.absoluteString];

    [self.userSource registerLocalUser:user];
    [self filterLocalUserAndNotify];
}

- (void)addUserListener:(id<BBMAppUserListener>)listener;
{
    if(listener) {
        [self.listeners addObject:listener];
        [listener usersChanged:self.users];
    }
}

- (void)removeUserListener:(id<BBMAppUserListener>)listener;
{
    if(listener) {
        [self.listeners removeObject:listener];
    }
}

- (void)notifyListeners
{
    for(id<BBMAppUserListener> listener in self.listeners)
    {
        [listener usersChanged:self.users];
    }
}

- (BBMAppUser *)userForRegId:(NSNumber*)regId
{
    if(regId) {
        return self.usersByRegId[regId];
    }
    return nil;
}

- (BBMAppUser *)userForUID:(NSString *)uid
{
    return self.appUsersByUid[uid];
}

- (void)refreshUserList
{
    if(![self.userSource respondsToSelector:@selector(refreshUserList:)]) {
        return;
    }

    typeof(self) __weak weakSelf = self;
    [self.userSource refreshUserList:^(NSArray *appUsers, NSDictionary *usersByRegId) {
        weakSelf.users = [appUsers mutableCopy];
        weakSelf.usersByRegId = [usersByRegId mutableCopy];
        weakSelf.appUsersByUid = [NSMutableDictionary dictionary];
        for(BBMAppUser *user in appUsers) {
            weakSelf.appUsersByUid[user.uid] = user;
        }
        [weakSelf filterLocalUserAndNotify];
    }];
}

@end
