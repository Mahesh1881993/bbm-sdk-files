// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

@interface BBMAppUser : NSObject

@property (nonatomic) NSString *uid;
@property (assign) long long regId;

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *avatarUrl;

- (id)init:(NSString*)uid
     regId:(long long)regId
      name:(NSString*)name
     email:(NSString*)email
 avatarUrl:(NSString*)avatarUrl;

- (id)initWithDictionary:(NSDictionary*)dictionary;

- (void)setValuesForKeysWithDictionary:(NSDictionary *)dictionary;

- (NSDictionary*)asDictionary;

@end
