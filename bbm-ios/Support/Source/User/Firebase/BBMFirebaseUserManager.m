// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMFirebaseUserManager.h"
#import "Firebase.h"
#import "BBMAppUserSource.h"
#import "BBMAccess.h"

@interface BBMFirebaseUserManager () <BBMAppUserSource>

@property (nonatomic) FIRDatabaseReference *usersDBReference;
@property (nonatomic) FIRDatabaseHandle usersDBHandle;

@property (nonatomic) NSHashTable *listeners;
@property (nonatomic) NSArray *users;
@property (nonatomic) NSMutableDictionary *usersByRegId;

@property (nonatomic, copy) BBMUserListChangedCallback callback;

@end

static NSString *const kDBUsersPath = @"bbmsdk/identity/users";


@implementation BBMFirebaseUserManager

+ (id<BBMAppUserSource>)instance
{
    return [[self alloc] init];
}

- (void)start
{
    self.usersDBReference = [[FIRDatabase database] referenceWithPath:kDBUsersPath];
    self.usersDBHandle = [self.usersDBReference observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *rawContacts = snapshot.value;
        if(rawContacts == (id)[NSNull null]) {
            return;
        }

        NSMutableDictionary *usersByRegId = [[NSMutableDictionary alloc] init];
        NSMutableArray *users = [[NSMutableArray alloc] init];
        for(NSString *key in rawContacts.allKeys) {

            NSMutableDictionary *contactDictionary = [rawContacts[key] mutableCopy];
            BBMAppUser *user = [[BBMAppUser alloc] initWithDictionary:contactDictionary];
            user.uid = key;
            contactDictionary[@"uid"] = key;
            //Key is regId. This will be used for lookups.
            if(user.regId > 0) {
                usersByRegId[[NSNumber numberWithLongLong:user.regId]] = user;
                [users addObject:user];
            }
        }
        
        //Note that all users of the app become contacts for other users. This is not scalable
        //and is a simplification of how contact management could be handled. In production usually
        //the user would be able to add a subset of all the users as contacts.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [users sortUsingComparator:^NSComparisonResult(BBMAppUser *u1, BBMAppUser *u2) {
                return [u1.name compare:u2.name];
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.users = users;
                self.usersByRegId = usersByRegId;
                if(self.callback) {
                    self.callback([self.users copy], [self.usersByRegId copy]);
                }
            });
        });
    }];
}


- (void)setUserListChangedCallback:(BBMUserListChangedCallback)callback
{
    self.callback = callback;
}

- (void)registerLocalUser:(BBMAppUser *)user
{
    NSString *firebaseUserId = [[FIRAuth auth] currentUser].uid;
    [[self.usersDBReference child:firebaseUserId] setValue:[user asDictionary]];
}


@end
