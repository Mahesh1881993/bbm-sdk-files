// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMAppUser.h"
#import <BBMEnterprise/BBMEnterprise.h>

@implementation BBMAppUser

- (id)init:(NSString *)uid
    regId:(long long)regId
     name:(NSString *)name
    email:(NSString *)email
avatarUrl:(NSString *)avatarUrl
{
    self = [super init];
    if (self) {
        self.uid = uid;
        self.regId = regId;
        self.name = name?:@"";
        self.email = email?:@"";
        self.avatarUrl = avatarUrl?:@"";
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    if( (self = [super init]) ) {
        [self setValuesForKeysWithDictionary:dictionary];
    }
    return self;
}

- (void)setValuesForKeysWithDictionary:(NSDictionary *)dictionary
{
    
    _uid = dictionary[@"uid"];
    _regId = [dictionary[@"regId"] longLongValue];
    _name = dictionary[@"name"]?:@"";
    _email = dictionary[@"email"]?:@"";
    _avatarUrl = dictionary[@"avatarUrl"]?:@"";
}

- (NSDictionary *)asDictionary
{
    //uid is not included because it is the key
    NSString *regIdStr = [NSString stringWithFormat:@"%lld",self.regId];
    return @{@"regId":regIdStr,
             @"name":self.name,
             @"email":self.email,
             @"avatarUrl":self.avatarUrl};
}


@end
