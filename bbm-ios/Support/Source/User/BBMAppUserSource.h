// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import "BBMAppUser.h"

/*!
 @details
 Returns all contacts minus the local user and all contacts keyed by registrationId
 */
typedef void (^BBMUserListChangedCallback)(NSArray *, NSDictionary *);

/*!
 @details
 Generalized interface for a user source.  To be used in conjunction with BBMUserManager
 to provide user data.
 */
@protocol BBMAppUserSource <NSObject>


/*!
 @details
 This should return a configured instance of your user source
 */
+ (id<BBMAppUserSource>)instance;

/*!
 @details
 Called when we have local user data to add/update in the database.  Thses methods should
 map the users's regId to their user data either by mapping an entry and/or updating
 the user field.
 */
- (void)registerLocalUser:(BBMAppUser *)user;

/*!
 @details
 Callback to be executed whenever the user data changes
 */
- (void)setUserListChangedCallback:(BBMUserListChangedCallback)callback;

/*!
 @details
 Called when we're ready to start syncing user data
 */
- (void)start;

@optional

/*!
 @details
 Call to manually refresh the user list.  
 */
- (void)refreshUserList:(BBMUserListChangedCallback)callback;

@end
