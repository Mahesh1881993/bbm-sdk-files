// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMAzureUserManager.h"
#import "BBMMicrosoftGraph.h"
#import "ApiCalls.h"
#import "SharedData.h"

static NSString *const kRegIdField = @"regId";

#define ValueOrNil(X) (id)X!=[NSNull null] && X ? X : nil
#define ValueOrEmptyString(X) (id)X!=[NSNull null] && X ? X : nil

@interface BBMAzureUserManager()
@property (nonatomic, strong) NSArray *users;
@property (nonatomic, strong) NSData *profileData;
@property (nonatomic, strong) NSString *profileUrl;
@property (nonatomic, strong) NSString *profile;

@property (nonatomic, strong) NSMutableArray *profileArray;
@property (nonatomic, strong) NSMutableArray *customArray;
@property (nonatomic, strong) NSMutableArray *sampleArray;

@property (nonatomic, strong) NSDictionary *userByRegId;
@property (nonatomic,   copy) BBMUserListChangedCallback callback;
@end


@implementation BBMAzureUserManager

+ (id<BBMAppUserSource>)instance
{
    return [[self alloc] init];
}

- (void)start
{

    [self refreshUserList:self.callback];
}


- (void)registerLocalUser:(BBMAppUser *)user
{
    void (^registerUser)(NSDictionary *) = ^(NSDictionary *localUserData){
        NSString *regIdStr = [NSString stringWithFormat:@"%lld", user.regId];

        NSString *currentRegIdStr = [[BBMMicrosoftGraph sharedInstance] regIdForUserData:localUserData];
        if(user.regId != 0 && [regIdStr isEqualToString:currentRegIdStr]) {
            NSLog(@"RegIds Match (%@).  No local user update required", regIdStr);
            return;
        }

        if([[BBMMicrosoftGraph sharedInstance] userDataHasExtension:localUserData]) {
            [[BBMMicrosoftGraph sharedInstance] updateLocalUserField:kRegIdField
                                                               value:regIdStr
                                                            callback:nil];
        }else{
            [[BBMMicrosoftGraph sharedInstance] addLocalUserField:@"regId"
                                                            value:regIdStr
                                                         callback:nil];
        }
        NSLog(@"Updated local user regId to %@", regIdStr);
    };

    [[BBMMicrosoftGraph sharedInstance] getCurrentUser:^(NSURLResponse *response, NSDictionary *data, NSError *error) {
        if(nil == error && nil != data) {
            registerUser(data);
        }else{
            NSLog(@"Unable to register local user.  Unable to get existing local user data: %@", error.localizedDescription);
        }
    }];
}


- (void)setUserListChangedCallback:(BBMUserListChangedCallback)callback
{
    self.callback = callback;
    if(callback) {
        self.callback(self.users, self.userByRegId);
    }
}


- (void)refreshUserList:(BBMUserListChangedCallback)callback
{

    [[BBMMicrosoftGraph sharedInstance] getAllUsers:^(NSURLResponse *response, NSDictionary *data, NSError *error) {
        if(nil == data || error) {
            NSLog(@"Error: Could not refresh user list");
            if(callback) {
                callback(nil, nil);
            }
            return;
        }
    
        NSMutableArray *appUsers = [NSMutableArray array];
        NSMutableDictionary *appUserDict = [NSMutableDictionary dictionary];
        NSMutableArray *users = data[@"value"];

        self.sampleArray = [NSMutableArray new];

        for(NSDictionary *user in users) {
            NSString *regId = [[BBMMicrosoftGraph sharedInstance] regIdForUserData:user];
            //We're only interested in Active Directory users with a regId
            if(nil == regId || (id)regId == [NSNull null]) {
                continue;
            }    
            
            NSString *userId = ValueOrNil(user[@"id"]);
            NSString *avatarUrl = ValueOrNil(user[@"avatarUrl"]);
            NSLog(@"avatarUrl--> %@", avatarUrl);
            NSArray *parts = [userId componentsSeparatedByString:@"."];
            userId = parts.count ? parts[0] : nil;
            NSAssert(nil != userId, @"The key manager requires a user ID");
     
            BBMAppUser *appUser = [[BBMAppUser alloc] init:userId
                                                     regId:[regId longLongValue]
                                                      name:ValueOrEmptyString(user[@"displayName"])
                                                     email:ValueOrEmptyString(user[@"mail"])
                                                 avatarUrl:nil];
            
            
            [appUsers addObject:appUser];
            [appUserDict setObject:appUser forKey:@([regId longLongValue])];
        }
        
            self.userByRegId = appUserDict;
            self.users = appUsers;
            
            
            if(callback){
                callback(appUsers, appUserDict);
            }

      
    }];
}



@end
