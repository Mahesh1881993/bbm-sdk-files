// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

#import "BBMAppUserSource.h"

/*!
 @abstract
 A BBMAppUserSource implemenation based on the MSGraphAPI and Azure Active Directory

 For Azure AD, we use the MSGraph API to add a "regId" field in a custom extension to
 active directory user objects.  This allows querys to the directory directly to discover
 any users with whom we can chat, call, etc via the BBM Enterprise SDK.  The local user's regId
 property is added automatically on login.  See BBMMicrosoftGraph for details on how the
 regId id added as a user object extension.

 BBMAzureUserManager does not automatically keep user records current.  refreshUserList should
 or called when it is critical that the data be current.
 
 Data is not cached.  The user list will be refreshed on each application launch and will not
 be available if the client is offline.
 */
@interface BBMAzureUserManager : NSObject <BBMAppUserSource>
@end
