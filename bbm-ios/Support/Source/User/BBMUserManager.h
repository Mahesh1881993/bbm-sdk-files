// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import "BBMAppUserListener.h"
#import "BBMAppUser.h"
#import "BBMAppUserSource.h"

@class BBMAuthenticatedAccount;

@interface BBMUserManager : NSObject

/*!
 @details
 Initialize the user manager with a given \a user source
 @param source The user data source to use
 */
- (id)initWithSource:(id<BBMAppUserSource>)source;

/*!
 @details
 Register the local user with the source.
 @param account The user account to register
 @param regId The regId for the \a account
 */
- (void)registerLocalUserWithAccount:(BBMAuthenticatedAccount *)account
                               regId:(NSString *)regId;


/*!
 @details
 Add a listener to be called when the user data changes
 @param listener The listener to add
 */
- (void)addUserListener:(id<BBMAppUserListener>)listener;


/*!
 @details
 Remove a listener for user data source changes
 @param listener The listener to remove
 */
- (void)removeUserListener:(id<BBMAppUserListener>)listener;


/*!
 @details
 Query the manager for a user with the given regId.  BBMAppUser objects are reused and observable.
 @param regId The regId of the user to query for
 @return BBMAppUser The user or nil if the user is not found
 */
- (BBMAppUser *)userForRegId:(NSNumber *)regId;


/*!
 @details
 Returns an app user object given the users oAuth identifier.
 @param uid The users oAuth identifier
 */
- (BBMAppUser *)userForUID:(NSString *)uid;

- (void)refreshUserList;


@end
