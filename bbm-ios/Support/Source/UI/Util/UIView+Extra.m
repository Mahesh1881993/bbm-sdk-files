// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "UIView+Extra.h"

@implementation UIView (Extra)

- (void)addSubviewAndContraintsWithSameFrame:(UIView*)view
{
    view.frame = self.frame;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:view];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"|[view]|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:[NSDictionary dictionaryWithObjectsAndKeys:view, @"view", nil]]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat: @"V:|[view]|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:[NSDictionary dictionaryWithObjectsAndKeys:view, @"view", nil]]];
}

@end
