// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>

@interface UIView (Extra)
/*!
 @details
 Adds a view to a subview and constrains it to fill it's parent.  This is the modern equivalent to
 adding a view to a superview and, setting the view's frame to the superview's bounds and
 using automatic resizing
 @param view Subview to add
 */
- (void)addSubviewAndContraintsWithSameFrame:(UIView *)view;

@end
