// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMEnterprise.h>


@class BBMChatMessage;
@class BBMChat;
@class BBMUserManager;
@class BBMAppUser;
@class BBMTimedMessageManager;
@class BBMChatMessageListener;


@interface BBMUIUtilities : NSObject

/*!
 @details
 Returns the default timed message manager.  This is the instance used internally
 by the BBMUIWidget library
 @return BBMTimedMessageManager
 */
+ (BBMTimedMessageManager *)defaultTimedMessageManager;

/*!
 @details
 Returns the default message listener.  This is the instance used internally
 by the BBMUIWidget library
 @return BBMMessageListener
 */
+ (BBMChatMessageListener *)defaultChatMessageListener;

/*!
 @details
 Call to set the user manager to be used internally by the BBMUIWidget library.  Many UI components
 will automatically render the user name, avatar, etc and for this, they need to be able to query
 your BBMUserManager instance for the name and avatar data.  If this is not set, you will see
 registration IDs instead of user names and some components may not work correctly.
 @param userManager Your configured user manager object
 */
+ (void)setDefaultUserManager:(BBMUserManager *)userManager;

/*!
 @details
 Returns the display name for the given user.  The defaultUserManager is used to do the lookup
 @return NSString The user name
 */
+ (NSString *)displayNameForUser:(BBMUser *)user;

/*!
 @details
 Returns the BBMAppUser object from the supplied user manager for the user with the given
 registration id (or nil if the user does not exist in the manager).
 @param regId the reg id for the user
 @return BBMAppUser the app user object corresponding to the given \a regId
 */
+ (BBMAppUser *)userForRegId:(NSNumber *)regId;


/*! @brief Returns the chat title for a global search match result */
+ (NSString *)chatTitleForSearchMatch:(NSString*)searchMatchUserUri inChat:(BBMChat *)chat;

/*! @brief Returns the user readable string for particular message in a conversation */
+ (NSString *)stringForMessageCellForChatMessage:(BBMChatMessage *)message;

/*! @brief Returns the user readable string for the chats list */
+ (NSString *)stringForChatCellForChatMessage:(BBMChatMessage *)message;

/*! @brief Returns the status image for a particular message */
+ (UIImage *)statusImageForChatMessage:(BBMChatMessage *)message;

/*! @brief Returns the status image for a given message state */
+ (UIImage *)statusImageForChatMessageState:(BBMChatMessageStateMessage_StatesState)status;

/*! @brief Returns the formatted date for a unix timestamp */
+ (NSString *)formattedChatMessageTimestamp:(unsigned long long)timestamp;

/*! @brief Adds a circular mask to a \a view */
+ (void)cirularizeView:(UIView *)view;

@end


/*!
 @abstract
 Automatically observes a chat updates the title based on the current participants, subject, etc

 @code
 //Typical Usage:
 self.titleObserver = [[BBMChatTitleObserver alloc] initWithChat:chat];
 self.chatObserver = [ObservableMonitor monitorActivatedWithName:@"mon" block:^{
     //The chatTitle property on titleObserver is observable and will change whenever
     //the list of chat participants changes.  That change, in turn, will trigger
     //this monitor and update the label below.
     weakSelf.chatTitleLabel.text = weakSelf.titleObserver.chatTitle;
 }];
 */
@interface BBMChatTitleObserver : NSObject

- (instancetype)initWithChat:(BBMChat *)chat;

/*! @brief The derived chat title.  Observable */
@property (nonatomic, readonly) NSString *chatTitle;

@end
