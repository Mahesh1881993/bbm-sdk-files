// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>

#import "BBMMessageNotificationView.h"
#import "BBMUIWidgets.h"
#import "BBMImageCache.h"

@interface BBMMessageNotificationView ()

@property (nonatomic, weak) IBOutlet UIView  *view;
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@end

@implementation BBMMessageNotificationView

- (id)init
{
    self = [super init];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"BBMMessageNotificationView" owner:self options:nil];
        [self addSubview:self.view];
        self.avatarImageView.image = [UIImage imageNamed:kStrDefautAvatarImage];
    }
    return self;
}

+ (void)showNotification:(BBMChatMessage *)message
{
    BBMMessageNotificationView *BBMMessageNotificationView = [[self alloc] init];
    [BBMMessageNotificationView show:message];
}

- (void)show:(BBMChatMessage *)message
{
    self.senderLabel.text = [BBMUIUtilities displayNameForUser:message.resolvedSenderUri];
    self.messageLabel.text = [BBMUIUtilities stringForChatCellForChatMessage:message];
    BBMAppUser *contact = [BBMUIUtilities userForRegId:message.resolvedSenderUri.regId];
    [self loadImageUrl:contact.avatarUrl];


    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [keyWindow addSubview:self];
    self.view.frame = CGRectMake(0.0f, -CGRectGetHeight(self.view.frame), CGRectGetWidth(keyWindow.bounds), CGRectGetHeight(self.view.frame));
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.view.frame = CGRectMake(0.0f, [UIApplication sharedApplication].statusBarFrame.size.height, CGRectGetWidth(keyWindow.bounds), CGRectGetHeight(self.view.frame));
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self hide];
        });
    }];
}

- (void)hide
{
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.view.frame = CGRectMake(0.0f, -CGRectGetHeight(self.view.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.frame));
    } completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
}

- (void)loadImageUrl:(NSString *)url
{
    UIImage *image = [[BBMImageCache sharedImageCache] imageForKey:url];
    if(image) {
        self.avatarImageView.image = image;
        return;
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage *avatar = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(avatar) {
                [[BBMImageCache sharedImageCache] cacheImage:avatar forKey:url];
                self.avatarImageView.image = avatar;
            }
        });
    });

}


@end
