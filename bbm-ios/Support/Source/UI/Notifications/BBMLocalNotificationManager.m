// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>

#import "BBMUIWidgets.h"

#import "BBMLocalNotificationManager.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface BBMLocalNotificationManager ()
@property (nonatomic) UILocalNotification *incomingCallNotification;
@end


@implementation BBMLocalNotificationManager

+ (BBMLocalNotificationManager *)sharedInstance
{
    static BBMLocalNotificationManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BBMLocalNotificationManager alloc] init];
    });
    return sharedInstance;
}

+ (void)registerNotifications
{
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

- (void)notifyForIncomingCall:(BBMECall *)call
{
    // shouldn't happen, but in case there's already one, get rid of it
    [self clearIncomingCallNotification];
    
    // No sound required.  System notifciations tones play when you're in the background
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    
    NSString *regId = [call peerRegId];
    notif.alertBody = [NSString stringWithFormat:kStrCallNotification, regId];
    notif.alertAction = kStrGenericOpen;
    
    self.incomingCallNotification = notif;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notif];
}

- (void)notifyForIncomingMessage:(BBMChatMessage *)message
{
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    notif.alertBody = [BBMUIUtilities stringForChatCellForChatMessage:message];
    notif.alertAction = kStrGenericOpen;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notif];
}

- (void)clearIncomingCallNotification
{
    if (self.incomingCallNotification) {
        [[UIApplication sharedApplication] cancelLocalNotification:self.incomingCallNotification];
        self.incomingCallNotification = nil;
    }
}

#pragma clang diagnostic pop

@end
