// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <UIKit/UIKit.h>

/*!
 @abstract
 Listens for new incoming messages and displays a notification. It allows the exclusion
 of conversations so that the user doesn't see notifications for a conversation that is
 displayed
 */
@class BBMChatMessage;

@interface BBMMessageNotificationView : UIView

/*!
 @details
 Show a notification for the given \a message
 @param message the message used to show the notification
 */
+ (void)showNotification:(BBMChatMessage *)message;

@end
