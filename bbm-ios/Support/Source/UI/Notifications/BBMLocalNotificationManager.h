// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

#import <BBMEnterprise/BBMEnterprise.h>

/*!
 @abstract
 This class is responsible for registering for notifications and provides methods for posting
 local notifications.
 */
@interface BBMLocalNotificationManager : NSObject

+ (BBMLocalNotificationManager *)sharedInstance;

/*!
 @details
 Registers notification categories and settings. This should be called when the app is launched.
 This will prompt the user if they want to allow notifications for this app the first time it is
 called.
 */
+ (void)registerNotifications;

/*!
 @details
 Posts a local notification for an incoming call. This should be called when a call is received
 when the app is in the background
 */
- (void)notifyForIncomingCall:(BBMECall *)call;

/*!
 @details
 Posts a local notification for an incoming message. This should be called when a message is received
 when the app is in the background
 */
- (void)notifyForIncomingMessage:(BBMChatMessage *)message;

/*! @brief Clears the previous incoming call notification from the device's notification center. */
- (void)clearIncomingCallNotification;

@end
