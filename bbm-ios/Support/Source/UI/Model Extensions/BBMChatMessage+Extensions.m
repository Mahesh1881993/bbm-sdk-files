// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMChatMessage+Extensions.h"
#import "BBMUIWidgets.h"

@implementation BBMChatMessage (Extensions)

-(BOOL)isTagUnhandled
{
    static dispatch_once_t onceToken;
    static NSSet *s_tags;
    dispatch_once(&onceToken, ^{
        s_tags = [NSSet setWithArray:kKnownMessageTags];
    });

    return ![s_tags containsObject:self.tag];
}

- (BBMChat *)chat
{
    return [BBMAccess getChatForId:self.chatId];
}

- (BOOL)isHighPriority
{
    return [[self.rawData objectForKey:kMessageDataKey_Priority] isEqualToString:kMessageDataValue_PriorityHigh];
}

- (BOOL)isTimedMessage
{
    return [self.tag isEqualToString:kMessageTag_TimedMessage];
}

- (BOOL)isExpiredTimedMessage
{
    CGFloat secondsLeft = [[BBMUIUtilities defaultTimedMessageManager] secondsLeftForMessage:self];
    return self.isTimedMessage && (self.isDeletedFlagSet && secondsLeft <= 0.0f);
}

- (BOOL)canBeRetracted
{
    if (self.isIncomingFlagSet ||
        self.recall == kBBMChatMessage_RecallRecalled ||
        self.recall == kBBMChatMessage_RecallRecalling ||
        [self.tag isEqualToString:kBBMChatMessage_TagJoin] ||
        [self.tag isEqualToString:kBBMChatMessage_TagLeave]) {
        return NO;
    }
    return YES;
}

- (BOOL)canBeQuoted
{
    return ([self.tag isEqualToString:kBBMChatMessage_TagText] &&
            self.recall != kBBMChatMessage_RecallRecalled &&
            self.recall != kBBMChatMessage_RecallRecalling);
}

- (BOOL)canBeEdited
{
    if (self.isIncomingFlagSet ||
        self.recall == kBBMChatMessage_RecallRecalled ||
        self.recall == kBBMChatMessage_RecallRecalling ||
        ![self.tag isEqualToString:kBBMChatMessage_TagText]) {
        return NO;
    }
    return YES;
}

@end
