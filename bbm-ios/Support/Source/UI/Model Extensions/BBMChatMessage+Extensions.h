// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMChatMessage.h>

@interface BBMChatMessage (Extensions)

/*! @brief Returns true if the message tag is unknown.  Add your custom tags to this list.  */
- (BOOL)isTagUnhandled;

/*! @brief Returns the chat to which this message belongs */
- (BBMChat *)chat;

/*! @brief Returns true if the message is high priority */
- (BOOL)isHighPriority;

/*! @brief Returns true if this message is a timed message */
- (BOOL)isTimedMessage;

/*! @brief Returns true if this message is an expired timed message */
- (BOOL)isExpiredTimedMessage;

/*! @brief Returns true if this message can be retracted */
- (BOOL)canBeRetracted;

/*! @brief Returns true if this message can be quoted */
- (BOOL)canBeQuoted;

/*! @brief Returns true if this message can be edited */
- (BOOL)canBeEdited;

@end
