// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <Foundation/Foundation.h>

@class BBMKeyManager;
@class BBMAuthController;

typedef BOOL (^PasswordValidator)(NSString *password);
typedef void (^PasswordEntryCancelledCallback)(void);

typedef enum {
    kBBMPasswordChangeSuccess,
    kBBMPasswordChangeFailure,
    kBBMPasswordChangeInputMismatch,
    kBBMPasswordChangeInvalidPassword,
    kBBMPasswordChangeCancelled
}BBMPasswordChangeResult;

/*!
 @abstract
 A simple password prompt for getting, submitting and managing the application password
 */
@interface BBMUserPasswordManager : NSObject

/*!
 @details
 Initialize the password manager with the given \a keyManager instance.  The password
 manager will automatically monitor the management key state
*/
- (id)initWithKeyManager:(BBMKeyManager *)keyManager;

/*! @breif The local user's regId.  Must be set before we attempt to do any kind of password validation */
@property (nonatomic, strong) NSString *localUserRegId;

/*! @brief If set, this will be called to validate the password the user enters.  A simple default
           validator is included that only checks if the password is 8 characters or longer
 */
@property (nonatomic, copy) PasswordValidator passwordValidationBlock;

/*! @brief Called if the user cancels password entry.  This should exit the login flow */
@property (nonatomic, copy) PasswordEntryCancelledCallback passwordEntryCancelledCallback;

/*!
 @details
 Calling this will open a prompt allowing the user to change their password
 */
- (void)promptToChangePassword:(void (^)(BBMPasswordChangeResult result))callback;

- (void)stop;
@end
