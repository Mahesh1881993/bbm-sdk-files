// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMUserPasswordManager.h"
#import "BBMKeyManager.h"
#import "BBMAuthController.h"
#import <BBMEnterprise/BBMEnterprise.h>

//Localized Strings for all of the password prompts can be found here.
#import "BBMUIWidgetsConfig.h"


@interface BBMUserPasswordManager()
@property (nonatomic, weak) BBMKeyManager *keyManager;
@property (nonatomic, strong) ObservableMonitor *managementKeyMonitor;
@end


@implementation BBMUserPasswordManager

- (id)initWithKeyManager:(BBMKeyManager *)keyManager
{
    if( (self = [super init]) ) {
        self.keyManager = keyManager;
        [self monitorManagmentKeyState];

        //The default password validator only ensures that your password is 8 characters or longer.
        //Supply a more interesting implementation to ensure passwords are strong
        self.passwordValidationBlock = ^BOOL(NSString *password) {
            return password.length > 7;
        };
    }
    return self;
}


- (void)monitorManagmentKeyState
{
    typeof(self) __weak weakSelf = self;
    self.managementKeyMonitor = [ObservableMonitor monitorActivatedWithName:@"mgmtKeyMon" block:^{
        BBMManagementKeyState state = weakSelf.keyManager.managementKeyState;

        if(state == kBBMMgmtKeyPasswordRequired) {
            [weakSelf promptForPassword:^(NSString *password) {
                [weakSelf.keyManager synchronizeManagementKeys:password];
            } verify:NO];
        }else if(state == kBBMMgmtKeyInitialPasswordRequired) {
            [weakSelf promptForPassword:^(NSString *password) {
                [weakSelf.keyManager synchronizeManagementKeys:password];
            } verify:YES];
        }else if(state == kBBMMgmtKeyIncorrectPassword) {
            [weakSelf repromptForPassword:^(NSString *password) {
                [weakSelf.keyManager synchronizeManagementKeys:password];
            } message:kStrPasswordRePromptMsg verify:NO];
        }
    }];
}

- (void)stop
{
    [self.managementKeyMonitor deActivate];
    self.managementKeyMonitor = nil;
}

#pragma mark - Password Prompt

- (void)promptForPassword:(void (^)(NSString *password))callback verify:(BOOL)verify
{

    UIAlertController *ac = [UIAlertController alertControllerWithTitle:kStrPasswordPromptTitle
                                                                message:(verify ? kStrPasswordMessageInit : kStrPasswordMessage)
                                                         preferredStyle:UIAlertControllerStyleAlert];

    [ac addAction:[self passwordAction:callback forAlertController:ac verify:(BOOL)verify]];
    if(!verify) {
        [ac addAction:[self forgotPasswordAction]];
        [ac addAction:[self cancelAction]];
    }


    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [vc presentViewController:ac animated:YES completion:nil];
}


- (void)repromptForPassword:(void (^)(NSString *password))callback message:(NSString *)message verify:(BOOL)verify
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:kStrPasswordRePromptTitle
                                                                message:message
                                                         preferredStyle:UIAlertControllerStyleAlert];

    [ac addAction:[self passwordAction:callback forAlertController:ac verify:verify]];
    if(!verify) {
        [ac addAction:[self forgotPasswordAction]];
        [ac addAction:[self cancelAction]];
    }

    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [vc presentViewController:ac animated:YES completion:nil];
}


#pragma mark - Actions

- (UIAlertAction *)passwordAction:(void (^)(NSString *password))callback forAlertController:(UIAlertController *)ac verify:(BOOL)verify
{
    [ac addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = kStrPassword;
        textField.secureTextEntry = YES;
    }];

    if(verify) {
        [ac addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = kStrVerifyPassword;
            textField.secureTextEntry = YES;
        }];
    }

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:kStrSubmitPassword
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action)
       {
           UITextField *passwordField = ac.textFields[0];
           NSString *password = passwordField.text;
           BOOL isValid = self.passwordValidationBlock(password);
           BOOL matches = true;
           if(verify) {
               NSString *passwordVer = ac.textFields[1].text;
               matches= [password isEqualToString:passwordVer];
           }
           if(isValid && matches) {
               callback(password);
           }else{
               NSString *msg = !isValid ? kStrPasswordInvalidMsg : kStrPasswordsDontMatch;
               [self repromptForPassword:callback message:msg verify:verify];
           }
       }];
    return okAction;
}

- (UIAlertAction *)cancelAction
{
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       if(self.passwordEntryCancelledCallback) {
                                                           self.passwordEntryCancelledCallback();
                                                       }
                                                   }];
    return cancel;
}

- (UIAlertAction *)forgotPasswordAction
{
    UIAlertAction *forgotAction = [UIAlertAction actionWithTitle:kStrPasswordForgot
                                                     style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       [self promptForForgotPassword];
                                                   }];

    return forgotAction;
}


#pragma mark - Forgot Password

- (void)promptForForgotPassword
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:kStrPasswordForgot
                                                                message:kStrPasswordForgotMsg
                                                         preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *resetAction = [UIAlertAction actionWithTitle:kStrGenericOK
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [self.keyManager resetKeyManagementPassword];
                                                    }];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       [self promptForPassword:^(NSString *password) {
                                                           [self.keyManager synchronizeManagementKeys:password];
                                                       } verify:NO];
                                                   }];

    [ac addAction:resetAction];
    [ac addAction:cancel];

    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [vc presentViewController:ac animated:YES completion:nil];

}


#pragma mark - Change Password

- (void)promptToChangePassword:(void (^)(BBMPasswordChangeResult result))callback
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:kStrChangePassword
                                                                message:nil
                                                         preferredStyle:UIAlertControllerStyleAlert];

    [ac addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = kStrPassword;
        textField.secureTextEntry = YES;
    }];

    [ac addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = kStrPassword;
        textField.secureTextEntry = YES;
    }];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:kStrSubmitPassword
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action)
   {
       UITextField *passwordFieldA = ac.textFields[0];
       UITextField *passwordFieldB  = ac.textFields[1];

       NSString *password = passwordFieldA.text;
       NSString *passwordValidate = passwordFieldB.text;

       //Validate the two passwords
       if(![password isEqualToString:passwordValidate]) {
           callback(kBBMPasswordChangeInputMismatch);
           return;
       }

       //Make sure the password we entered is valid
       BOOL isValid = self.passwordValidationBlock(password);
       if(!isValid) {
           callback(kBBMPasswordChangeInvalidPassword);
           return;
       }

       //Attempt to change the password
       [self.keyManager changePassword:password regId:self.localUserRegId callback:^(BOOL success) {
           if(success) {
               callback(kBBMPasswordChangeSuccess);
           }else{
               callback(kBBMPasswordChangeFailure);
           }
       }];
    }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             callback(kBBMPasswordChangeCancelled);
                                                         }];

    [ac addAction:okAction];
    [ac addAction:cancelAction];

    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [vc presentViewController:ac animated:YES completion:nil];
}

@end
