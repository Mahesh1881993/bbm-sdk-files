// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#ifndef BBMUISupport_h
#define BBMUISupport_h

//Config & Constants
#import "BBMMessageTypes.h"
#import "BBMUIWidgetsConfig.h"

//Core Support
#import "BBMUIUtilities.h"
#import "BBMAccess.h"

//Controllers
#import "BBMUserManager.h"
#import "BBMTimedMessageManager.h"
#import "BBMChatMessageListener.h"
#import "BBMMessageLoader.h"

//Extensions
#import "BBMChatMessage+Extensions.h"

#endif /* BBMUISupport_h */
