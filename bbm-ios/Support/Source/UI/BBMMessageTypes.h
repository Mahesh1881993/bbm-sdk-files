// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#ifndef MESSAGE_TYPES
#define MESSAGE_TYPES

/*!
 Predefined message tags.  These can be changed here if they conflict with
 tags you already have in use.
 */
#define kMessageTag_Picture             @"Picture"
#define kMessageTag_Quote               @"Quote"
#define kMessageTag_TimedMessage        @"TimedMessage"

//This is the list of message tags which are known to the app.  Add your custom tags here.
//This allows the UI to ignore messages which it cannot render.
#define kKnownMessageTags           @[\
                                      kBBMChatMessage_TagAdmin, \
                                      kBBMChatMessage_TagRemove, \
                                      kMessageTag_TimedMessage, \
                                      kBBMChatMessage_TagText, \
                                      kMessageTag_Picture, \
                                      kMessageTag_Quote, \
                                      kBBMChatMessage_TagJoin, \
                                      kBBMChatMessage_TagLeave, \
                                      kBBMChatMessage_TagShred]

/*!
 Predefined message data keys.  These can be changed here if they conflict with
 keys you already have in use.
 */
#define kMessageDataKey_Priority        @"priority"         //Message priority.
#define kMessageDataKey_TimedMessage    @"expiryInSeconds"  //Message expiry time for timed messages
#define kMessageDataKey_QuoteSource     @"source"           //Quoted message source
#define kMessageDataKey_QuoteText       @"text"             //Quoted message text
#define kMessageDataKey_QuoteTime       @"timestamp"        //Quoted message send time

#define kMessageDataKey_FileExtension   @"fileExtension"    //The file extension for shared files
#define kMessageDataKey_FileUTIType     @"fileUTIType"      //The file UTI Type for shared files
#define kMessageDataKey_FileName        @"fileName"         //The file name for shared files

/*!
 Predefined message data values.  These can be changed here if they conflict with
 keys you already have in use.
 */
#define kMessageDataValue_PriorityHigh  @"High"


typedef enum : NSUInteger {
    kMessagePriority_Default,
    kMessagePriority_High
} MessagePriority;


typedef enum : NSInteger {
    BBMTimedMessageValue_0Second = 0,
    BBMTimedMessageValue_5Seconds = 5,
    BBMTimedMessageValue_10Seconds = 10,
    BBMTimedMessageValue_30Seconds = 30,
    BBMTimedMessageValue_60Seconds = 60
} BBMTimedMessageValue;

#endif
