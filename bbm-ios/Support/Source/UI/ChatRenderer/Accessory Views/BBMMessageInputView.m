// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMMessageInputView.h"

#import <Photos/PHAsset.h>
#import <Photos/PHImageManager.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/UTCoreTypes.h>

#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMUIWidgets.h"
#import "UIView+Extra.h"
#import "SharedData.h"
#import <Photos/Photos.h>



@interface BBMMessageInputView ()  <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *referencedTextHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageInputViewTallHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageInputViewShortHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thumbnailImageViewWidthConstraint;

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *priorityButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteAttachmentButton;

@property (weak, nonatomic) IBOutlet UILabel *referencedTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *timedMessageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;

@property (strong, nonatomic) ObservableMonitor *referencedMessageMonitor;

@property (strong, nonatomic) NSString *picturePath;
@property (strong, nonatomic) NSString *pictureThumbnailPath;

@property (strong, nonatomic) BBMChatMessage *quotedMessage;
@property (strong, nonatomic) BBMChatMessage *editedMessage;

@property (assign, nonatomic) BBMTimedMessageValue timedMessageValue;


@end

@implementation BBMMessageInputView

- (id)initInView:(UIView*)view heightConstraint:(NSLayoutConstraint*)heightConstraint
{
    if ( (self = [super init]) ) {
        [[NSBundle mainBundle] loadNibNamed:kMessageInputNib owner:self options:nil];
        self.view.frame = view.frame;
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        [view addSubviewAndContraintsWithSameFrame:self.view];

        self.addButton.layer.cornerRadius =  CGRectGetWidth(self.addButton.bounds)/2.0f;
        self.thumbnailImageView.layer.cornerRadius =  8.0f;

        self.textFieldContainerHeightConstraint = heightConstraint;
        self.deleteAttachmentButton.hidden = YES;
        self.thumbnailImageViewWidthConstraint.constant = 0;
        [self layoutIfNeeded];
    }
    return self;
}


#pragma mark - Actions

- (BBMMessageSendParameters *)getMessageSendParameters
{
    BBMMessageSendParameters *param = [[BBMMessageSendParameters alloc] init];
    param.chatId = self.chat.chatId;
    param.textMessage = self.messageTextField.text;
    param.picturePath = self.picturePath;
    param.pictureThumbnailPath = self.pictureThumbnailPath;
    param.quotedMessage = self.quotedMessage;
    param.editedMessage = self.editedMessage;
    param.priority = self.priorityButton.selected ? kMessagePriority_High : kMessagePriority_Default;
    param.duration = self.timedMessageValue;
    return param;
}

- (void)resetMessageParameters
{
    self.messageTextField.text = nil;
    self.picturePath = nil;
    self.thumbnailImageView.image = nil;
    self.thumbnailImageViewWidthConstraint.constant = 0;
    [self cancelReferencedMessage];
    self.sendButton.enabled = NO;
    self.priorityButton.selected = NO;
    self.timedMessageValue = BBMTimedMessageValue_0Second;
    self.timedMessageLabel.text = @"";
    self.deleteAttachmentButton.hidden = YES;
}

- (IBAction)sendTapped:(id)sender
{
    if(self.sendTextMessageBlock) {
        self.sendTextMessageBlock([self getMessageSendParameters]);
    }
    [self resetMessageParameters];
}


- (IBAction)togglePriority:(id)sender
{
    self.priorityButton.selected = !self.priorityButton.selected;
}


- (IBAction)cancelPressed:(id)sender
{
    [self cancelReferencedMessage];
}

- (IBAction)deleteAttachmentPressed:(id)sender
{
    self.picturePath = nil;
    self.pictureThumbnailPath = nil;
    self.timedMessageValue = 0;
    self.deleteAttachmentButton.hidden = YES;
    self.thumbnailImageViewWidthConstraint.constant = 0;
    self.sendButton.enabled = self.messageTextField.text.length > 0;
    [self layoutIfNeeded];
}


- (void)cancelReferencedMessage
{
    self.referencedMessageMonitor = nil;
    
    self.quotedMessage = nil;
    self.editedMessage = nil;
    
    self.referencedTextLabel.text = nil;
    self.referencedTextHeightConstraint.active = NO;
    self.textFieldContainerHeightConstraint.constant = self.messageInputViewShortHeightConstraint.constant;
}


- (void)showPictureThumbnail:(NSString*)picturePath
{
    self.thumbnailImageViewWidthConstraint.constant = 50;
    self.thumbnailImageView.image = [UIImage imageWithContentsOfFile:self.pictureThumbnailPath];
    self.sendButton.enabled = self.messageTextField.text.length > 0 || self.pictureThumbnailPath.length > 0;
    self.deleteAttachmentButton.hidden = NO;
}

-(void)showTimedMessageValue:(BBMTimedMessageValue)timeValue
{
    self.thumbnailImageViewWidthConstraint.constant = 50;
    self.thumbnailImageView.image = [UIImage imageNamed:@"timebomb_icon"];
    self.sendButton.enabled = self.messageTextField.text.length > 0;
    self.deleteAttachmentButton.hidden = NO;
    self.timedMessageValue = timeValue;
}


- (IBAction)showMenu:(UIButton*)sender
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil
                                                                        message:nil
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIPopoverPresentationController *popoverPresentationController = [controller popoverPresentationController];
    popoverPresentationController.sourceRect = sender.bounds;
    popoverPresentationController.sourceView = sender;

    //Timed photos are not supported so don't show this option if timed message value has been selected.
    if(self.timedMessageValue == BBMTimedMessageValue_0Second){
        UIAlertAction *photoAction = [UIAlertAction actionWithTitle:kStrPhoto
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                [self showPhotoPicker];
                                                            }];
        
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {
                                                                [self showCameraPicker];
                                                            }];
        
        [controller addAction:cameraAction];
        [controller addAction:photoAction];
    }

    //Timed photos are not supported so only show this option if no photo has been selected.
    if(self.picturePath == nil){
        UIAlertAction *timedMessageAction = [UIAlertAction actionWithTitle:kStrTimedMessage
                                                                     style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {
                                                                       [self showTimedMessagePicker];
                                                                   }];
        /* Timer message is removed from UIAlert Controller since this is not required for the functionality */
        
//        [controller addAction:timedMessageAction];
    }

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                         }];
    [controller addAction:cancelAction];

    [self.parentViewController presentViewController:controller animated:YES completion:nil];
}


- (void)showPhotoPicker
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = NO;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.parentViewController presentViewController:imagePickerController animated:YES completion:nil];
}

/* There is no option to take the image directly from camera and upload to chat conversation, So Seperate method is written to capture image from the camera */

- (void)showCameraPicker
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = NO;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self.parentViewController presentViewController:imagePickerController animated:YES completion:nil];
    
}

-(void)showTimedMessagePicker
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:kStrTimedMessageDuration
                                                                        message:nil
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *fiveSecondsAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"5 %@", kStrSeconds]
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [self showTimedMessageValue:BBMTimedMessageValue_5Seconds];
                                                              }];
    [controller addAction:fiveSecondsAction];

    UIAlertAction *tenSecondsAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"10 %@", kStrSeconds]
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 [self showTimedMessageValue:BBMTimedMessageValue_10Seconds];
                                                             }];
    [controller addAction:tenSecondsAction];

    UIAlertAction *twentyFiveSecondsAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"30 %@", kStrSeconds]
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {
                                                                        [self showTimedMessageValue:BBMTimedMessageValue_30Seconds];
                                                                    }];
    [controller addAction:twentyFiveSecondsAction];

    UIAlertAction *sixtySecondsAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"60 %@", kStrSeconds]
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   [self showTimedMessageValue:BBMTimedMessageValue_60Seconds];
                                                               }];
    [controller addAction:sixtySecondsAction];


    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [self deleteAttachmentPressed:self];
                                                         }];
    [controller addAction:cancelAction];

    [self.parentViewController presentViewController:controller animated:YES completion:nil];
    
}


- (void)showQuotedMessage:(BBMChatMessage*)quotedMessage
{
    typeof(self) __weak weakSelf = self;
    [self showReferencedMessage:quotedMessage withSetupBlock:^{
        weakSelf.quotedMessage = quotedMessage;
        NSString *quoteText = [BBMUIUtilities stringForMessageCellForChatMessage:quotedMessage];
        weakSelf.referencedTextLabel.text = [NSString stringWithFormat:kStrQuoting, quoteText];
    }];
}


- (void)showEditedMessage:(BBMChatMessage*)editedMessage
{
    typeof(self) __weak weakSelf = self;
    [self showReferencedMessage:editedMessage withSetupBlock:^{
        weakSelf.editedMessage = editedMessage;
        NSString *message = [BBMUIUtilities stringForMessageCellForChatMessage:editedMessage];
        weakSelf.referencedTextLabel.text = [NSString stringWithFormat:kStrEditing, message];
        weakSelf.messageTextField.text = message;
    }];
}


- (void)showReferencedMessage:(BBMChatMessage *)message withSetupBlock:(void (^)(void))setupBlock
{
    [self cancelReferencedMessage];
    
    typeof(self) __weak weakSelf = self;
    self.referencedMessageMonitor = [ObservableMonitor monitorActivatedWithName:@"referencedMessageMonitor" block:^{
        //If the message is recalled or deleted there will be a problem when we try to send
        //so we have to monitor that and cancel the referenced message
        if (message.recall == kBBMChatMessage_RecallRecalled || [message isDeletedFlagSet]) {
            [weakSelf cancelReferencedMessage];
        }
        else {
            if (setupBlock) {
                setupBlock();
            }
            weakSelf.referencedTextHeightConstraint.active = YES;
            weakSelf.messageInputViewTallHeightConstraint.active = YES;
            weakSelf.textFieldContainerHeightConstraint.constant = weakSelf.messageInputViewTallHeightConstraint.constant;
        }
    }];
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    if ([info objectForKey:UIImagePickerControllerReferenceURL]){
        [SharedData sharedData].isCamera = NO;
        NSURL *imageAssetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
        PHFetchResult *imageAsset = [PHAsset fetchAssetsWithALAssetURLs:@[imageAssetURL] options:nil];
        if (imageAsset.count > 0) {
            PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
            options.networkAccessAllowed = YES;
            options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
            };
            options.version = PHImageRequestOptionsVersionCurrent;
            [[PHImageManager defaultManager] requestImageDataForAsset:[imageAsset firstObject]
                                                              options:options
                                                        resultHandler:^(NSData *imageData,
                                                                        NSString *dataUTI,
                                                                        UIImageOrientation orientation,
                                                                        NSDictionary *info)
            {
                //Our picture sending assumes we're sharing images as JPEGs.  If the returned UTI is something else
                //heif, png, etc, convert it to a jpeg first
                if(![dataUTI isEqualToString:@"public.jpeg"]) {
                    UIImage *picture = [UIImage imageWithData:imageData];
                    imageData = UIImageJPEGRepresentation(picture, 0.9);
                }
                //Use the original image filename or a random one if it doesn't exist
                NSURL *fileurl = [info objectForKey:@"PHImageFileURLKey"];
                NSString *filename = fileurl.absoluteString.lastPathComponent ?: [NSString stringWithFormat:@"%@.jpg", [NSUUID UUID].UUIDString];
                self.picturePath = [BBMAccess saveDataToTmpTransferDir:imageData filename:filename];
            }];

            options.resizeMode = PHImageRequestOptionsResizeModeFast;
            CGSize targetSize = CGSizeMake(kTargetThumbnailSize, kTargetThumbnailSize);
            [[PHImageManager defaultManager] requestImageForAsset:[imageAsset firstObject]
                                                       targetSize:targetSize
                                                      contentMode:PHImageContentModeDefault
                                                          options:options
                                                    resultHandler:^(UIImage * _Nullable result,
                                                                    NSDictionary * _Nullable info)
            {
                NSData *imageData = UIImageJPEGRepresentation(result, 0.7f);
                NSString *thumbfilename = [NSString stringWithFormat:@"%@.jpg", [NSUUID UUID].UUIDString];
                NSString *savedThumbPath = [BBMAccess saveDataToTmpTransferDir:imageData filename:thumbfilename];
                self.pictureThumbnailPath = savedThumbPath;
                [self showPictureThumbnail:savedThumbPath];
            }];
        }
    }
    else {
        
        /* If the image does not contain reference URL, this else block will be triggered to save the captured image to device */
        
        [SharedData sharedData].isCamera = YES;
        NSLog(@"ReferenceURL--> %@", [info objectForKey:UIImagePickerControllerReferenceURL]);
        
        UIImage * pickedImage = info[UIImagePickerControllerOriginalImage];
        
        UIImageWriteToSavedPhotosAlbum(pickedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
        /* Once the image is saved to the device didFinishSavingWithError delegate will be called */

    }

    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

/*This delegate will take the last image saved in photo library and convert into reference URL to upload in the chat conversation*/

- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo{
    
    if (!error) {
        NSLog(@"SAVED SUCCESS");
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.networkAccessAllowed = YES;
        options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
        };
        options.version = PHImageRequestOptionsVersionCurrent;
        
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
        PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
        PHAsset *lastAsset = [fetchResult lastObject];
        options.resizeMode = PHImageRequestOptionsResizeModeFast;
        //        CGSize targetSize = CGSizeMake(kTargetThumbnailSize, kTargetThumbnailSize);
        
        
        if (fetchResult.count > 0) {
            PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
            options.networkAccessAllowed = YES;
            options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
            };
            options.version = PHImageRequestOptionsVersionCurrent;
            [[PHImageManager defaultManager] requestImageDataForAsset:lastAsset
                                                              options:options
                                                        resultHandler:^(NSData *imageData,
                                                                        NSString *dataUTI,
                                                                        UIImageOrientation orientation,
                                                                        NSDictionary *info)
             {
                 //Our picture sending assumes we're sharing images as JPEGs.  If the returned UTI is something else
                 //heif, png, etc, convert it to a jpeg first
                 if(![dataUTI isEqualToString:@"public.jpeg"]) {
                     UIImage *picture = [UIImage imageWithData:imageData];
                     imageData = UIImageJPEGRepresentation(picture, 0.9);
                 }
                 //Use the original image filename or a random one if it doesn't exist
                 NSURL *fileurl = [info objectForKey:@"PHImageFileURLKey"];
                 NSString *filename = fileurl.absoluteString.lastPathComponent ?: [NSString stringWithFormat:@"%@.jpg", [NSUUID UUID].UUIDString];
                 self.picturePath = [BBMAccess saveDataToTmpTransferDir:imageData filename:filename];
             }];
            
            options.resizeMode = PHImageRequestOptionsResizeModeFast;
            CGSize targetSize = CGSizeMake(kTargetThumbnailSize, kTargetThumbnailSize);
            [[PHImageManager defaultManager] requestImageForAsset:lastAsset
                                                       targetSize:targetSize
                                                      contentMode:PHImageContentModeDefault
                                                          options:options
                                                    resultHandler:^(UIImage * _Nullable result,
                                                                    NSDictionary * _Nullable info)
             {
                 NSData *imageData = UIImageJPEGRepresentation(result, 0.7f);
                 NSString *thumbfilename = [NSString stringWithFormat:@"%@.jpg", [NSUUID UUID].UUIDString];
                 NSString *savedThumbPath = [BBMAccess saveDataToTmpTransferDir:imageData filename:thumbfilename];
                 self.pictureThumbnailPath = savedThumbPath;
                 [self showPictureThumbnail:savedThumbPath];
                 
             }];
        }
    }
    else {
        UIAlertController * controller = [UIAlertController alertControllerWithTitle:@"Something went wrong" message:@"Please try again later" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil];
        [controller addAction:action];
        [self.parentViewController presentViewController:controller animated:YES completion:nil];
    }
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [BBMAccess sendTypingNotification:self.chat];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (self.isTypingBlock) {
        self.isTypingBlock(NO);
    }
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger messageTextLength = textField.text.length + string.length - range.length;
    BOOL shouldChange = messageTextLength <= 2000;
    if (shouldChange) {
        self.sendButton.enabled = messageTextLength > 0 || self.picturePath.length > 0;
        if (self.isTypingBlock) {
            self.isTypingBlock(YES);
        }
    }
    return shouldChange;
}

@end





