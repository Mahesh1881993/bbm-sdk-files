// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMTimedMessageViewController.h"
#import "BBMTimedMessageManager.h"
#import "BBMUIWidgets.h"

@interface BBMTimedMessageViewController ()

@property (nonatomic) BBMChatMessage *message;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIView *progressContainer;
@property (weak, nonatomic) IBOutlet UILabel *timedMessageLabel;
@property (weak, nonatomic) IBOutlet UIView *captionContainerView;
@property (weak, nonatomic) IBOutlet UIView *timedMessageContainer;

@property (strong, nonatomic) ObservableMonitor *timeLeftMonitor;

@end

@implementation BBMTimedMessageViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = kStrTimedMessageTitle;
    [[BBMUIUtilities defaultTimedMessageManager] startViewingMessage:self.message];
    
    [self showTextMessage];

}
-(void)showMessage:(BBMChatMessage *)message {
    self.message = message;
}

-(void)showTextMessage
{
    self.timedMessageContainer.layer.cornerRadius = 8.0f;
    self.photoView.hidden = YES;
    self.captionContainerView.hidden = YES;

    __weak typeof (self) weakSelf = self;
    self.timeLeftMonitor = [ObservableMonitor monitorActivatedWithName:@"timeLeftMonitor" block:^{
        NSString *text = [[BBMUIUtilities defaultTimedMessageManager] timedStringForMessage:weakSelf.message];
        double remainingTime = [[BBMUIUtilities defaultTimedMessageManager] secondsLeftForMessage:weakSelf.message];
        NSNumber *duration = weakSelf.message.rawData[kMessageDataKey_TimedMessage];
        double viewTime = duration.doubleValue;

        weakSelf.timedMessageLabel.text = text;

        if (remainingTime > 0) {
            [weakSelf.progressView setProgress:(remainingTime / viewTime) animated:NO];
            weakSelf.progressContainer.hidden = NO;
            [weakSelf.view layoutIfNeeded];
        }
        else{
            weakSelf.progressContainer.hidden = YES;
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}
@end
