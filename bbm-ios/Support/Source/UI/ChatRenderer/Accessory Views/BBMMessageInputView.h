// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <UIKit/UIKit.h>
#import <BBMEnterprise/BBMChatMessage.h>
#import "BBMMessageTypes.h"

@class BBMChatTextField;
@class BBMMessageSendParameters;

typedef void (^SendTextMessageBlock)(BBMMessageSendParameters *) ;
typedef void (^IsTypingBlock)(BOOL isTyping);

/*!
 @abstract
 A basic input field used by the BBMChatViewController to send new messages
 This class allows the user to input text, select a photo, and send messages.
 */
@interface BBMMessageInputView : UIView

/*! @brief The text input field */
@property (nonatomic,  weak) IBOutlet UITextField *messageTextField;

/*! @brief The parent view controller in which this control is embedded.  Set this. */
@property (nonatomic,  weak) UIViewController *parentViewController;

/*! @brief This block is called when a message is sent by the user */
@property (nonatomic,  copy) SendTextMessageBlock sendTextMessageBlock;

/*! @brief This block is called when a user starts typing */
@property (nonatomic,  copy) IsTypingBlock isTypingBlock;

/*! @brief The chat associated with this input field */
@property (nonatomic,strong) BBMChat *chat;

/*!
 @details
 Initialize the input field in the given \a view.  The supplied height constraint will be updated
 with the input views final height
 @param view The container for the input field
 @param heightConstraint The height of the container
 */
- (id)initInView:(UIView *)view heightConstraint:(NSLayoutConstraint*)heightConstraint;

/*! @brief show the \a quotedMessage */
- (void)showQuotedMessage:(BBMChatMessage*)quotedMessage;

/*! @brief Show the \a editedMessage */
- (void)showEditedMessage:(BBMChatMessage*)editedMessage;

@end



