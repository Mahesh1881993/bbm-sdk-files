// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMTimedMessageManager.h"
#import "BBMAccess.h"

@interface TimedMessageInfo : NSObject
@property (nonatomic, strong) BBMChatMessage *message;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, assign) double secondsLeft;
@end

@implementation TimedMessageInfo

- (double)secondsLeft
{
    [ObservableTracker getterCalledForObject:self propertyName:@"secondsLeft"];
    return _secondsLeft;
}

@end

@interface BBMTimedMessageManager ()
@property (nonatomic) NSMutableDictionary *timedMessageInfoDictionary;
@end

@implementation BBMTimedMessageManager

- (id)init
{
    self = [super init];
    if (self) {
        self.timedMessageInfoDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSString *)keyForMessage:(BBMChatMessage *)message
{
    return [NSString stringWithFormat:@"%@-%llu",message.chatId,message.messageId];
}

- (TimedMessageInfo *)timedMessageInfoForMessage:(BBMChatMessage *)message
{
    NSString *messageKey = [self keyForMessage:message];
    return self.timedMessageInfoDictionary[messageKey];
}

- (void)startViewingMessage:(BBMChatMessage *)message
{
    NSString *messageKey = [self keyForMessage:message];
    if(self.timedMessageInfoDictionary[messageKey] || message.isDeletedFlagSet){
        return;
    }
    TimedMessageInfo *messageInfo = [[TimedMessageInfo alloc] init];
    NSNumber *duration = message.rawData[kMessageDataKey_TimedMessage];
    messageInfo.secondsLeft = duration.floatValue;
    messageInfo.message = message;
    messageInfo.content = message.content;
    [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(decrementTimer:) userInfo:messageInfo repeats:YES];
    self.timedMessageInfoDictionary[messageKey] = messageInfo;

    //Once the message has been viewed, delete it so that it is displayed as expired once the timer expires.
    [BBMAccess deleteMessageWithChatId:[messageInfo.message getChatId] andID:messageInfo.message.messageId];
}

- (double)secondsLeftForMessage:(BBMChatMessage *)message
{
    TimedMessageInfo *messageInfo = [self timedMessageInfoForMessage:message];
    return messageInfo.secondsLeft;
}

- (NSString *)timedStringForMessage:(BBMChatMessage *)message
{
    TimedMessageInfo *messageInfo = [self timedMessageInfoForMessage:message];
    return messageInfo.content;
}

- (void)decrementTimer:(NSTimer *)timer {
    TimedMessageInfo *messageInfo = (TimedMessageInfo*)timer.userInfo;
    messageInfo.secondsLeft -= 0.01f;
    if (messageInfo.secondsLeft <= 0.0) {
        [timer invalidate];
        messageInfo.content = nil;
    }
}

@end
