// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <Foundation/Foundation.h>


@class BBMUser;

/*!
 @details
 Callback for the message status. \a results is an array of BBMDeliveryStatus objects
 keyed by the userURI
 */
typedef void (^MessageStatusCallback)(NSDictionary *results);

/*!
 @details
 Fetches the delivery status of a message for each participant in the chat
 */
@interface BBMMessageStatusLoader : NSObject

/*!
 @details
 Get the current delivery status for a message in a multi-person chat.
 @param message The message to get the status for
 @param callback Invoked when the message status has been fetched
 */
- (void)getMessageStatusForMessage:(BBMChatMessage *)message
                          callback:(MessageStatusCallback)callback;

@end

/*!
 @abstract
 Encapsulates the delivery status for single user for a message in a multi-person chat
 */
@interface BBMDeliveryStatus : NSObject

/*! @brief Instantiate an instance directly from the JSON payload */
+ (instancetype)statusFromDetails:(BBMChatMessageStateMessage_States *)details;

@property (nonatomic, strong) NSString  *userUri;
@property (nonatomic, assign) BBMChatMessageStateMessage_StatesState state;

@end
