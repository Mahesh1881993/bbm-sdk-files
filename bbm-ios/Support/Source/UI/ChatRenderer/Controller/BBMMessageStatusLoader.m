// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMMessageStatusLoader.h"
#import "BBMUIWidgets.h"

@interface BBMMessageStatusLoader () <BBMIncomingMessageListener>
@property (nonatomic,copy) MessageStatusCallback messageStatusCallback;
@property (nonatomic,copy) NSString              *messageStatusCookie;
@end

@implementation BBMMessageStatusLoader

- (instancetype)init
{
    if(( self = [super init]) ) {
        [[BBMEnterpriseService service] addListener:self forMessageNames:@[[BBMChatMessageStateMessage messageName]]];
    }
    return self;
}

- (void)dealloc
{
    [[BBMEnterpriseService service] removeListener:self forMessageNames:@[[BBMChatMessageStateMessage messageName]]];
}

- (void)getMessageStatusForMessage:(BBMChatMessage *)message
                          callback:(MessageStatusCallback)callback
{
    if(message.resolvedChatId.isOneToOneFlagSet) {
        NSLog(@"Error - you requesting the message status for a message in a 1-1 chat is not needed");
        return;
    }

    self.messageStatusCookie = [BBMAccess getMessageStatusForMessage:message];
    self.messageStatusCallback = callback;
}

- (void)receivedIncomingMessage:(BBMIncomingMessage *)incomingMessage
{
    if([incomingMessage.messageName isEqualToString:[BBMChatMessageStateMessage messageName]])
    {
        BBMChatMessageStateMessage *messageStatusResult = (BBMChatMessageStateMessage *)incomingMessage;
        BOOL isGroupChat = NO;
        if (messageStatusResult != nil) {
            isGroupChat = YES;
        }

        if (messageStatusResult == nil || ![self.messageStatusCookie isEqualToString:messageStatusResult.cookie]) {
            //Cookie is stale.
            return;
        }

        NSDictionary *statusResults;
        if (isGroupChat) {
            statusResults = [self getGroupChatStatusResults:messageStatusResult.states];
        }

        if (self.messageStatusCallback) {
            self.messageStatusCallback(statusResults);
        }
    }
}

- (NSDictionary *)getGroupChatStatusResults:(NSArray *)details
{
    NSMutableDictionary *statusResults = [NSMutableDictionary dictionary];
    for (BBMChatMessageStateMessage_States *result in details) {
        BBMDeliveryStatus *ds = [BBMDeliveryStatus statusFromDetails:result];
        statusResults[ds.userUri] = ds;
    }

    return statusResults;
}

@end



@implementation BBMDeliveryStatus

+ (instancetype)statusFromDetails:(BBMChatMessageStateMessage_States *)details
{
    BBMDeliveryStatus *retVal = [[self alloc] init];
    retVal.state  = details.state;
    retVal.userUri = details.userUri;
    return retVal;
}

@end

