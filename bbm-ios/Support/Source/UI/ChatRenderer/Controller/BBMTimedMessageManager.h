// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMEnterprise.h>

@interface BBMTimedMessageManager : NSObject

/*!
 @details
 This should be called when a timed message is viewed. When calling this a timer is started for that
 timed message and it will be marked as deleted. If the message is not a timed message nothing will
 happen.
 @param message The timed message that will be viewed.
 */
- (void)startViewingMessage:(BBMChatMessage *)message;

/*!
 @details
 Returns the seconds left to view an incoming timed message. This value is an observable.
 @param message The timed message of interest.
 @return double The time in seconds before the timed message expires.
 */
- (double)secondsLeftForMessage:(BBMChatMessage *)message;

/*!
 @details
 Returns the string content for a timed message. If the message has expired this will return nil.
 @param message The timed message of interest.
 @return NSString The string for the timed message.
 */
- (NSString *)timedStringForMessage:(BBMChatMessage *)message;

@end
