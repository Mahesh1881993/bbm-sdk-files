// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

/*!
 This class loads the messages in a conversation and keeps track of new messages. It also allows
 for loading of more messages if needed. Typically only the newest messages are loaded and older
 messages are loaded if there is a request from the user.
 */

#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMChat.h>

typedef void (^MessageLoaderCallback)(NSArray *messageList);

@interface BBMMessageLoader : NSObject

/*!
 @details
 Initialize a new message loader for the \a chat.  This will automatically load the first
 \a pageSize messages.  The supplied \a callback is invoked whenever there are new messages to
 render.
 @param chat The chat to load messages from
 @param pageSize The number of message to load in each batch
 @param callback Invoked whenever the message contains new data to render
 */
+ (BBMMessageLoader*)messageLoaderForConversation:(BBMChat *)chat
                                      pageSize:(NSUInteger)pageSize
                                      callback:(MessageLoaderCallback)callback;

/*! @brief Load the next batch of messages */
- (void)loadNextPage;

/*! @brief True if there are more messages in the chat to load */
- (BOOL)canLoadMoreMessages;

/*! @brief Marks all loaded but unread messages as read */
- (void)markUnreadMessagesAsRead;

@end
