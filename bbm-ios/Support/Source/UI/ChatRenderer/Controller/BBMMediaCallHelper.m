// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMMediaCallHelper.h"
#import "BBMUIWidgets.h"
#import "SharedData.h"

@implementation BBMMediaCallHelper

/* There is no seperate button for Audio and Video Call, So I have commented this function and rewritten for seperate Audio and video call below */

/* + (void)callRequestedForChat:(BBMChat *)chat
              viewController:(UIViewController *)viewController
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:kStrMediaModeSelect
                                                                        message:@""
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *voiceAction = [UIAlertAction actionWithTitle:kStrMediaModeVoice
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            [self startCall:kVoice inChat:chat];
                                                        }];
    [controller addAction:voiceAction];
    
    UIAlertAction *videoAction = [UIAlertAction actionWithTitle:kStrMediaModeVideo
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            [self startCall:kVideo inChat:chat];
                                                        }];
    [controller addAction:videoAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrMediaModeCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [controller addAction:cancelAction];
    
    [viewController.parentViewController presentViewController:controller
                                                      animated:YES
                                                    completion:nil];
}  */


/* This method is for Audio Call */
+ (void)callRequestedForChat:(BBMChat *)chat
           viewController:(UIViewController *)viewController {
    [self startCall:kVoice inChat:chat];
}

/* This method is for Video Call */
+ (void) voiceCallRequestedForChat:(BBMChat *)chat
                    viewController:(UIViewController *)viewController {
    [self startCall:kVideo inChat:chat];
}

+ (void)startCall:(MediaMode)mode inChat:(BBMChat *)chat
{
    BBMEMediaManager *mediaManager = [[BBMEnterpriseService service] mediaManager];
    //Request permission for the microphone
    MediaPermissionsCallback callback = ^(BOOL granted){
        if(!granted) {
            return;
        }
        NSArray *participants = [BBMAccess participantsForChat:chat].array;
        BBMChatParticipant *participant = participants.firstObject;
        BBMUser *user = participant.resolvedUserUri;

        [mediaManager callRegId:user.regId mediaMode:mode callback:^(BBMMediaError err) {
            if(err != kMediaErrorNoError) {
                
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:kStrGenericError
                                                                                    message:kStrCallError
                                                                             preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *okAction = [UIAlertAction actionWithTitle:kStrGenericOK
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
                [controller addAction:okAction];

                [[UIApplication sharedApplication].keyWindow.rootViewController  presentViewController:controller animated:YES completion:nil];
            }
        }];
    };
    [mediaManager requestMediaPermissionsWithCallback:callback mode:mode];
}

@end
