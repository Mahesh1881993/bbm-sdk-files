// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMMessageLoader.h"

#import <BBMEnterprise/BBMEnterprise.h>

#import "BBMUtilities.h"
#import "BBMUIWidgets.h"


@interface BBMMessageLoader ()

@property (nonatomic, strong) ObservableMonitor *messageMonitor;
@property (nonatomic, strong) NSArray *messages;
@property (nonatomic, strong) BBMChat *chat;
@property (nonatomic,   copy) MessageLoaderCallback callback;

@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) unsigned long long oldestMesageIdToLoad;
@property (nonatomic, assign) NSUInteger numberOfPagesToLoad;
@property (nonatomic, assign) BOOL hasUnreadMessages;

@end


@implementation BBMMessageLoader

+ (BBMMessageLoader*)messageLoaderForConversation:(BBMChat *)chat
                                      pageSize:(NSUInteger)pageSize
                                      callback:(MessageLoaderCallback)callback {

    return [[self alloc] initWithConversation:(BBMChat *)chat
                                     pageSize:pageSize
                                     callback:callback];
}

- (id)initWithConversation:(BBMChat*)chat
                  pageSize:(NSUInteger)pageSize
                  callback:(MessageLoaderCallback)callback
{
    self = [super init];
    if (self) {
        self.chat = chat;
        self.pageSize = pageSize;
        self.callback = callback;
        [self loadNextPage];
    }
    return self;
}


- (BOOL)canLoadMoreMessages
{
    return self.oldestMesageIdToLoad  > 1;
}


- (void)loadNextPage
{
    if(self.chat.numMessages != 0) {
        self.numberOfPagesToLoad++;
        NSUInteger numberOfMessagesToLoad = self.pageSize * self.numberOfPagesToLoad;
        if(numberOfMessagesToLoad > self.chat.numMessages) {
            self.oldestMesageIdToLoad = 1;
        }
        else {
            self.oldestMesageIdToLoad = self.chat.numMessages - numberOfMessagesToLoad;
        }
    }
    [self observeMessages];
}


- (void)observeMessages
{
    typeof(self) __weak weakSelf = self;
    self.messageMonitor = [ObservableMonitor monitorActivatedWithName:@"messageMonitor" block:^{
        NSMutableArray *identifiers = [[NSMutableArray alloc] init];
        for (long long ident = weakSelf.oldestMesageIdToLoad; ident <= weakSelf.chat.lastMessage; ident++) {
            BBMChatMessageKey *key = [BBMChatMessageKey keyWithChatId:weakSelf.chat.chatId messageId:ident];
            if(key) {
                [identifiers addObject:key];
            }
        }
        NSArray *messages = [[[[BBMEnterpriseService service] model] chatMessage] valuesForKeys:identifiers];
        NSMutableArray *filteredMessages = [[NSMutableArray alloc] initWithCapacity:messages.count];
        for(BBMChatMessage *message in [messages reverseObjectEnumerator]) {
            if(message.bbmState == kBBMStatePending) {
                continue;
            }
            //Timed message that have expired are marked as deleted so they are still displayed as expired messages.
            if((!message.isDeletedFlagSet && message.bbmState != kBBMStateMissing) || (message.isTimedMessage && message.isDeletedFlagSet) ) {
                [filteredMessages addObject:message];
                [ObservableTracker setTrackingDisable:YES];
                //Don't reload the whole list every time a state changes...
                if(message.isIncomingFlagSet && message.state != kBBMChatMessage_StateRead) {
                    weakSelf.hasUnreadMessages = YES;
                }
                [ObservableTracker setTrackingDisable:NO];
            }
        }
        weakSelf.messages = filteredMessages;
        if(weakSelf.callback) {
            weakSelf.callback(filteredMessages);
        }
    }];
}

- (void)markUnreadMessagesAsRead
{
    if(!self.hasUnreadMessages) {
        return;
    }
    //To mark all messages as read, we only need to mark the last received message as read
    for(BBMChatMessage *message in self.messages) {
        if(message.isIncomingFlagSet) {
            [BBMAccess markMessagesRead:@[message]];
            self.hasUnreadMessages = NO;
            break;
        }
    }
}

@end
