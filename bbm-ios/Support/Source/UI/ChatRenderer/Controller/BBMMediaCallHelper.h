// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <Foundation/Foundation.h>

@interface BBMMediaCallHelper : NSObject

/*!
 @details
 Show the call selector UI for the give chat.  This will handle starting a call automatically.
 This will work for a multi-party chat, but it will call the first user it finds in the chat.  This
 should only be used for 1-1 chats.
 @param chat The chat associated with the call
 @param viewController The controller from which this was invoked for presenting the resulting UI
 */
+ (void)callRequestedForChat:(BBMChat *)chat
              viewController:(UIViewController *)viewController;

+ (void)voiceCallRequestedForChat:(BBMChat *)chat
              viewController:(UIViewController *)viewController;

/*!
 Start a call for the given chat.  The callee will be the first participant found for the
 chat.  This should be used only for 1-1 chats.  A generic error will be displayed via an
 AlertView if the call fails.
 @param mode Voice or Video
 @param chat The chat from which to start the call
 */
+ (void)startCall:(MediaMode)mode inChat:(BBMChat *)chat;

@end
