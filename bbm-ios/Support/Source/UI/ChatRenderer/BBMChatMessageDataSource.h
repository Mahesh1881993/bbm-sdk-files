// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>
#import "BBMMessageCell.h"
#import "BBMImageProvider.h"

@class BBMChatMessage;
@class BBMMessageLoader;


/*!
 @abstract
 The dataSource for the BBMChatViewController.  This may be customized to add message types, or
 customize the list refresh behavior
 */
@interface BBMChatMessageDataSource : NSObject <UITableViewDataSource>

/*! @brief The message loader instance used to monitor the messages in the chat and to load additional pages */
@property (nonatomic, readonly) BBMMessageLoader *messageLoader;

/*! @brief The list of messages currently being rendered */
@property (nonatomic, readonly) NSArray *messages;

/*! @brief Used to load the avatars.  To change this, set an instance on the BBMChatViewController */
@property (nonatomic, strong) id<BBMImageProvider> avatarProvider;

/*!
 @details
 Create an instance of the dataSource.  This is used internally in BBMChatViewController but
 may also be used in a custom chat renderer.
 @param chat The chat to render
 @param tapCallback A callback that will be invoked for message cell long-press events
 @param table The associated tableView
 @return BBMChatMessageDataSource
 */
+ (instancetype)dataSourceWithChat:(BBMChat *)chat
                 longPressCallback:(MessageActionCallback)longPressCallback
                       tapCallback:(MessageActionCallback)tapCallback
                         tableView:(UITableView *)table;

/*! @brief Load the next batch of messages. */
- (void)loadNextPage;


@end
