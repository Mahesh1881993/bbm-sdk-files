// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMGroupChatMessageDetailCell.h"
#import "BBMUIWidgets.h"
#import "BBMMessageStatusLoader.h"

@interface BBMGroupChatMessageDetailCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;

@property (strong, nonatomic) BBMUser *user;
@property (strong, nonatomic) BBMChatMessage *message;
@property (strong, nonatomic) BBMDeliveryStatus *status;
@property (strong, nonatomic) NSString *avatarUrl;

@property (strong, nonatomic) ObservableMonitor *participantMonitor;


@end


@implementation BBMGroupChatMessageDetailCell


- (void)prepareForReuse
{
    [super prepareForReuse];
    [self setMessage:nil status:nil avatarProvider:nil];
}


- (void)setMessage:(BBMChatMessage *)message
            status:(BBMDeliveryStatus *)status
    avatarProvider:(id<BBMImageProvider>)avatarProvider

{
    self.user = [BBMAccess getUserForUserUri:status.userUri];
    self.message = message;
    self.status  = status;
    self.avatarImage.image = [UIImage imageNamed:kStrDefautAvatarImage];
    [BBMUIUtilities cirularizeView:self.avatarImage];

   BBMGroupChatMessageDetailCell *__weak weakSelf = self;
   self.participantMonitor = [ObservableMonitor monitorActivatedWithName:@"statusMonitor" block:^{
       BBMAppUser *user = [BBMUIUtilities userForRegId:weakSelf.user.regId];
       [weakSelf.nameLabel setText:user.name];
       NSString *url = user.avatarUrl;
       weakSelf.avatarUrl = url;
       [avatarProvider loadImage:url callback:^(UIImage *image, NSString *source, NSError *error) {
           if([source isEqualToString:weakSelf.avatarUrl] && image && nil == error) {
               weakSelf.avatarImage.image = image;
           }
       }];

       UIImage *statusImage = [BBMUIUtilities statusImageForChatMessageState:weakSelf.status.state];
       [weakSelf.statusImage setImage:statusImage];
   }];

}



@end
