//
// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMImageProvider.h"

@class BBMDeliveryStatus;

/*!
 @abstract
 Cell for displaying the individual participant's delivery status
 */
@interface BBMGroupChatMessageDetailCell : UITableViewCell


/*!
 @details
 Set up the cell
 @param message The associate message
 @param status The delivery status for the message
 @param avatarProvider The image provider for getting the user avatar images
 */
- (void)setMessage:(BBMChatMessage *)message
            status:(BBMDeliveryStatus *)status
    avatarProvider:(id<BBMImageProvider>)avatarProvider;


@end
