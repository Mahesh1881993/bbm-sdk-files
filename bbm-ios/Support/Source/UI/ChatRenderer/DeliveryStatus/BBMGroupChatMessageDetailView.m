// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMGroupChatMessageDetailView.h"
#import "BBMGroupChatMessageDetailCell.h"
#import "UIView+Extra.h"
#import "BBMUIWidgets.h"
#import "BBMMessageStatusLoader.h"

static NSString *const kDefaultNibName      = @"BBMGroupChatMessageDetailView";
static NSString *const kParticipantCellNib  = @"BBMGroupChatMessageDetailCell";
static NSString *const kUserCellRUID        = @"UserCellReuseId";

#define kParticipantCellHeight 50.0f    //Cell height from the Nib
#define kRevealDuration 0.15f           //Animation Duration

@interface BBMGroupChatMessageDetailView () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UITableView *participantsTable;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (strong, nonatomic) BBMChatMessage *message;
@property (strong, nonatomic) NSArray *deliveryStatus;
@property (strong, nonatomic) BBMMessageStatusLoader *statusLoader;
@property (copy,   nonatomic) DismissCallback dismissCallback;
@property (strong, nonatomic) id<BBMImageProvider> avatarProvider;

@end


@implementation BBMGroupChatMessageDetailView


+ (instancetype)viewWithMessage:(BBMChatMessage *)message
                 avatarProvider:(id<BBMImageProvider>)avatarProvider
{
    UINib *nib = [UINib nibWithNibName:kMessageStatusViewNib bundle:nil];
    BBMGroupChatMessageDetailView *retVal = [nib instantiateWithOwner:nil options:nil][0];

    NSAssert(retVal != nil, @"Unable to expand BBMConfMessageDetailView nib");
    NSAssert([retVal isKindOfClass:[BBMGroupChatMessageDetailView class]], @"Incorrect class");

    retVal.message = message;

    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:retVal
                                                                         action:@selector(dismiss:)];
    [retVal addGestureRecognizer:gr];

    retVal.containerView.layer.borderWidth = .5;
    retVal.containerView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    retVal.containerView.layer.cornerRadius = 10.0f;
    retVal.containerView.clipsToBounds = YES;

    [retVal.okButton setTitle:kStrGenericOK forState:UIControlStateNormal];

    retVal.avatarProvider = avatarProvider;

    return retVal;
}


- (void)awakeFromNib
{
    [super awakeFromNib];

    UINib *userCellNib = [UINib nibWithNibName:kMessageStatusCellNib bundle:nil];
    [self.participantsTable registerNib:userCellNib forCellReuseIdentifier:kUserCellRUID];
    self.statusLoader = [[BBMMessageStatusLoader alloc] init];
}


#pragma mark - Public

- (void)presentInView:(UIView *)view dismissCallback:(DismissCallback)callback;
{
    self.layer.opacity = 0.0f;
    [view addSubviewAndContraintsWithSameFrame:self];
    [self layoutIfNeeded];

    NSString *messageText = self.message.content;

    [self.messageLabel setText:messageText];

    [UIView animateWithDuration:kRevealDuration animations:^{
        self.layer.opacity = 1.0f;
    }];

    [self getMessageStatus];
    self.dismissCallback = callback;
}


- (void)getMessageStatus
{
    //This will also work for hosted chats where state ~= status
    BBMGroupChatMessageDetailView *__weak weakSelf = self;
    MessageStatusCallback callback = ^(NSDictionary *results) {
        weakSelf.deliveryStatus = [results allValues];
        [weakSelf.participantsTable reloadData];
    };

    [self.statusLoader getMessageStatusForMessage:self.message
                                         callback:callback];
}


- (void)dismiss:(UITapGestureRecognizer *)gr
{
    [UIView animateWithDuration:kRevealDuration animations:^{
        self.layer.opacity = 0.0f;
    } completion:^(BOOL finished){
        [self removeFromSuperview];
        if(self.dismissCallback) {
            self.dismissCallback();
            self.dismissCallback = nil;
        }
    }];
}


- (IBAction)okPressed:(id)sender {
    [self dismiss:nil];
}


#pragma mark - TableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self deliveryStatus] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BBMGroupChatMessageDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:kUserCellRUID];

    [cell setMessage:self.message
              status:self.deliveryStatus[indexPath.row]
      avatarProvider:self.avatarProvider];

    return cell;
}


@end
