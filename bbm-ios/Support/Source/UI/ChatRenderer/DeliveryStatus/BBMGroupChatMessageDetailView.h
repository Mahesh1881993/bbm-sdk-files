// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMImageProvider.h"

typedef void (^DismissCallback)(void);

@interface BBMGroupChatMessageDetailView : UIView

/*!
 Returns a message D-R detail view for the given \a message in the given \a conversation
 @param message The message we want to render read receipts for
 @param avatarProvider The avatar source
 @return The configured view
 */
+ (instancetype)viewWithMessage:(BBMChatMessage *)message
                 avatarProvider:(id<BBMImageProvider>)avatarProvider;

/*!
 Presents the view in the given \a view.  This will constrain the detail view to fully fill
 the container.  The detail view will remove itself from the container based on user
 interactions.
 @param view The container view
 @param callback Called when the view is dismissed
 */
- (void)presentInView:(UIView *)view dismissCallback:(DismissCallback)callback;


@end
