// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMChatViewController.h"

#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMUIWidgets.h"

#import "BBMTextMessageCell.h"
#import "BBMMessageInputView.h"
#import "BBMTimedMessageViewController.h"
#import "BBMGroupChatMessageDetailView.h"
#import "BBMChatParticipantsViewController.h"
#import "BBMMediaCallHelper.h"
#import "BBMChatMessageDataSource.h"
#import "BBMChatCreator.h"
#import "BBMImageLoader.h"
#import "SharedData.h"

@interface BBMChatViewController () <UITableViewDelegate>

@property (strong, nonatomic) BBMChat *chat;
@property (strong, nonatomic) BBMChatMessageDataSource *dataSource;

@property (weak,   nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *textFieldContainerView;
@property (weak,   nonatomic) IBOutlet NSLayoutConstraint *textFieldContainerBottomConstraint;
@property (weak,   nonatomic) IBOutlet NSLayoutConstraint *textFieldContainerHeightConstraint;
@property (weak,   nonatomic) IBOutlet UILabel *typingUsersLabel;
@property (weak,   nonatomic) IBOutlet NSLayoutConstraint *typingUsersHidingConstraint;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *callButton;
@property (weak, nonatomic) IBOutlet UILabel *loadMoreMessagesLabel;

@property (strong, nonatomic) BBMMessageInputView *messageInputView;
@property (strong, nonatomic) BBMChatCreator *chatCreator;

@property (strong, nonatomic) BBMChatTitleObserver* chatUtil;
@property (strong, nonatomic) ObservableMonitor *titleMonitor;
@property (strong, nonatomic) ObservableMonitor *typingUsersMonitor;
@property (strong, nonatomic) ObservableMonitor *userMonitor;

@property (strong, nonatomic) BBMChatMessage *actionedMessage;
@property (strong, nonatomic) BBMChatMessage *timedMessage;

@end

@implementation BBMChatViewController

+ (instancetype)viewControllerWithChat:(BBMChat *)chat
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kChatViewStoryboard bundle:[NSBundle mainBundle]];
    BBMChatViewController *chatViewController =(BBMChatViewController *)[storyboard instantiateInitialViewController];
    chatViewController.chat  = chat;
    chatViewController.avatarProvider = [BBMImageLoader sharedImageLoader];
    return chatViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //We want to scroll to the bottom of the table immediately to show the last message. The problem
    //is we need to know the height of all the cells to be able to scroll to the bottom but we
    //don't have that information yet. Fields that affect the height of each message are not
    //resolved yet. To avoid this problem we flip the table vertically and we flip each cell. This
    //means that the now the last message is shown at the first row but since the table is upside
    //down it looks like it's the last cell. Also, the array that contains the messages will hold
    //the messages in descending order.
    self.tableView.transform = CGAffineTransformMakeScale(1, -1);

    // To hide extra separators at end of the table
    self.tableView.tableFooterView = [UIView new];
    self.tableView.estimatedRowHeight = 50.0; //The height of cells varies a lot. There isn't really a good estimate.
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.dataSource = nil;
    self.tableView.delegate = self;

    //We want to get notified when the keyboard is displayed or hidden
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideOrShow:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideOrShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];


    //Create a label to use as a custom title view to allow us to add a tap recognizer to it.
    //Tapping the title will allow the user to change the subject of the chat.
    UILabel *titleView = [[UILabel alloc] init];
    titleView.font = [UIFont boldSystemFontOfSize:19];
    titleView.userInteractionEnabled = YES;
    [titleView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateChatSubject)]];
    self.navigationItem.titleView = titleView;
    
    
    /* Here titleview text color is changed based on community by using NSUser defaults */
    
    NSString * themeFont = [[NSUserDefaults standardUserDefaults] valueForKey:@"theme_font"];
    NSLog(@"themeFont--> %@", themeFont);
    if ([themeFont isEqualToString:@""] || [themeFont isKindOfClass:[NSNull class]] || themeFont == (id) [NSNull null]) {
        [titleView setTextColor:[UIColor blackColor]];
        
    }
    else {
        if ([themeFont hasPrefix:@"#"] && [themeFont length] > 1) {
            themeFont = [themeFont substringFromIndex:1];
        }
        
        [titleView setTextColor:[self colorWithHexString:themeFont]];
    }
    
    
    [self monitorTitle];

    [self monitorTypingNotifications];
    [self configureInputView];

    //Remove the call button if this is not a 1-1 chat
    if (!self.chat.isOneToOneFlagSet) {
        NSMutableArray *rightBarButtons = [self.navigationItem.rightBarButtonItems mutableCopy];
        [rightBarButtons removeObject:self.callButton];
        self.navigationItem.rightBarButtonItems = rightBarButtons;
    }
}


/*This method is used to get the color directly by passing color code as String */

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    /*If isInvitePolicy is 1, then its a group chat.
      If isInvitePolicy is 0, then its a direct chat.
      This BOOL value is checked to restrict Audio/Video call in group chat*/
    
    if ([SharedData sharedData].isInvitePoliicy) {
        UIBarButtonItem *btnMore = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"more"] style:UIBarButtonItemStylePlain target:self action:@selector(moreAction)];
        self.navigationItem.rightBarButtonItem = btnMore;
    }
    else {
        UIBarButtonItem *btnShare = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"phone"] style:UIBarButtonItemStylePlain target:self action:@selector(PhoneCallMethod)];
        UIBarButtonItem *btnRefresh = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"record"] style:UIBarButtonItemStylePlain target:self action:@selector(VideoCallMethod)];
        UIBarButtonItem *btnMore = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"more"] style:UIBarButtonItemStylePlain target:self action:@selector(moreAction)];
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnMore, btnRefresh, btnShare, nil]];
    }
    
    
    [self.messageInputView.messageTextField becomeFirstResponder];
    //This is so avoid displaying notifications for new messages for the conversation on screen
    [[BBMUIUtilities defaultChatMessageListener]  excludeConv:self.chat.chatId];
    [self monitorMessages];
    [self.dataSource.messageLoader markUnreadMessagesAsRead];
    
    
    /* Here Navigation bar color is changed based on community by using NSUser defaults */
    
    NSString * themeFont = [[NSUserDefaults standardUserDefaults] valueForKey:@"theme_font"];
    NSLog(@"themeFont--> %@", themeFont);
    if ([themeFont isEqualToString:@""] || [themeFont isKindOfClass:[NSNull class]] || themeFont == (id) [NSNull null]) {
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        
    }
    else {
        if ([themeFont hasPrefix:@"#"] && [themeFont length] > 1) {
            themeFont = [themeFont substringFromIndex:1];
        }
        
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [self colorWithHexString:themeFont]};
    }
    
    NSString * themeStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"theme_code"];
    NSLog(@"themeStr--> %@", themeStr);
    
    if ([themeStr isEqualToString:@""] || [themeStr isKindOfClass:[NSNull class]] || themeStr == (id) [NSNull null]) {
        [self.navigationController.navigationBar setBarTintColor: [self colorWithHexString:@"087099"]];
    }
    else {
        if ([themeStr hasPrefix:@"#"] && [themeStr length] > 1) {
            themeStr = [themeStr substringFromIndex:1];
        }
        
        [self.navigationController.navigationBar setBarTintColor: [self colorWithHexString:themeStr]];
    }
    
    self.navigationController.navigationBar.translucent = NO;
}
 

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //This is so that notifications for new incoming messages are displayed since the chat
    //is no longer on screen
    [[BBMUIUtilities defaultChatMessageListener] stopExcludingConv:self.chat.chatId];
}

-(void)dealloc
{
    [[BBMUIUtilities defaultChatMessageListener] stopExcludingConv:self.chat.chatId];
}

- (void)configureInputView
{
    //This is the view that contains the text field to enter messages, the send button
    self.messageInputView = [[BBMMessageInputView alloc] initInView:self.textFieldContainerView
                                                   heightConstraint:self.textFieldContainerHeightConstraint];
    self.messageInputView.chat = self.chat;
    //This reference to the parent view controller is used to display alerts and other view controllers
    self.messageInputView.parentViewController = self;

    typeof(self) __weak weakSelf = self;
    //This block will be invoked every time a message is sent by the messageInputView
    self.messageInputView.sendTextMessageBlock = ^(BBMMessageSendParameters *param)
    {
        [BBMAccess sendMessage:param];
        //After sending a message show the last message
        [weakSelf.tableView setContentOffset:CGPointMake(0.0f, 0.0f) animated:NO];
    };

    self.messageInputView.isTypingBlock = ^(BOOL isTyping) {
        if(isTyping) {
            BBMChatTypingMessage *msg = [[BBMChatTypingMessage alloc] initWithChatId:weakSelf.chat.chatId];
            [[BBMEnterpriseService sharedService] sendMessageToService:msg];
        }
    };
}

- (void)updateChatSubject
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:kStrChangeSubject
                                                                        message:kStrEnterSubjectDesc
                                                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:kStrGenericUpdate
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         NSString *subject = controller.textFields[0].text;
                                                         if (![subject isEqualToString:self.chat.subject]) {
                                                             [BBMAccess updateChatSubject:subject forChat:self.chat];
                                                         }
                                                     }];
    [controller addAction:okAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [controller addAction:cancelAction];
    
    [controller addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = kStrSubject;
        textField.text = self.chat.subject;
    }];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)keyboardWillHideOrShow:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];

    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = CGRectGetHeight(keyboardFrame);

    BOOL hiding = [notification.name isEqualToString:UIKeyboardWillHideNotification];
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        //If keyboard is showing set the constraint between the bottom of the container view
        //to the bottom of the view to the height of the keyboard. If the keyboard is hidden
        //set the constraint to 0.
        self.textFieldContainerBottomConstraint.constant = hiding ? 0 : keyboardHeight;
    } completion:^(BOOL finished) {
        [self.view layoutIfNeeded];
    }];
}

-(void)setChat:(BBMChat  *)chat
{
    if(chat != _chat) {
        _chat = chat;
        self.chatUtil = [[BBMChatTitleObserver alloc] initWithChat:chat];
        //Once we have a conversation we can start observing the messages
        [self monitorMessages];
    }
}

- (void)setShowMediaControls:(BOOL)showMediaControls
{
    NSMutableArray *buttons = [self.navigationController.navigationItem.rightBarButtonItems mutableCopy];
    [buttons removeObject:self.callButton];
    if(showMediaControls == YES) {
        [buttons addObject:self.callButton];
    }
    self.navigationController.navigationItem.rightBarButtonItems = buttons;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //Each time we scroll, we mark all incoming messages as read.  The messageLoader will no-op on
    //this if there are no unread messages in the list so it is safe to call often.
    [self.dataSource.messageLoader markUnreadMessagesAsRead];
}


#pragma mark - Observers

- (void)monitorTypingNotifications
{
    //This monitor will be triggered when the list of users typing in this conversation changes. We
    //can use this to show or hide our view that shows the user who is currently typing.
    typeof(self) __weak weakSelf = self;
    self.typingUsersMonitor = [ObservableMonitor monitorActivatedWithName:@"typingUsersMonitor" block:^{
        NSArray *typingUsers = [BBMAccess getTypingUsersForChat:weakSelf.chat];
        [weakSelf setTextForTypingUsers:typingUsers];

        // Show/hide the typing users view by setting an appropriate priority on the constraint that
        // hides the view
        [weakSelf.view layoutIfNeeded];
        weakSelf.typingUsersHidingConstraint.priority = (typingUsers.count > 0) ? UILayoutPriorityDefaultLow : UILayoutPriorityDefaultHigh;
        [UIView animateWithDuration:0.0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [weakSelf.view layoutIfNeeded];
        } completion:nil];
    }];
}

-(void)monitorTitle
{
    typeof(self) __weak weakSelf = self;
    //The method conversationTitle accesses a few getters that are observables so any change
    //in those values will trigger this monitor
    self.titleMonitor = [ObservableMonitor monitorActivatedWithName:@"chatTableTitleMonitor" block:^{
        //Set title
        UILabel *titleLabel = (UILabel *)weakSelf.navigationItem.titleView;
        titleLabel.text = [weakSelf.chatUtil chatTitle];
        [titleLabel sizeToFit];
    }];
}

-(void)monitorMessages
{
    typeof(self) __weak weakSelf = self;

    //The long press callback is invoked when the user long presses the message bubble.
    //Here we invoke
    MessageActionCallback longPressCallback = ^(BBMMessageCell *cell) {
        [weakSelf handleMessageLongPressForCell:cell];
    };

    //The tap callback is invoked when the user taps the message bubble
    MessageActionCallback tapCallback = ^(BBMMessageCell *cell) {
        [weakSelf handleMessageTapForCell:cell];
    };

    self.dataSource = [BBMChatMessageDataSource dataSourceWithChat:self.chat
                                                 longPressCallback:longPressCallback
                                                       tapCallback:tapCallback
                                                         tableView:self.tableView];
    self.dataSource.avatarProvider = self.avatarProvider;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"TimedMessageSegue"]) {
        //Show screen for timed messages
        BBMTimedMessageViewController *dest = (BBMTimedMessageViewController*)segue.destinationViewController;
        [dest showMessage:self.timedMessage];
        self.timedMessage = nil;
    }else if([segue.identifier isEqualToString:@"ShowParticipantDetails"]) {
        BBMChatParticipantsViewController *dest = (BBMChatParticipantsViewController *)segue.destinationViewController;
        [dest setChat:self.chat avatarProvider:self.avatarProvider];
        dest.title = kStrChatParticipants;
    }
}


#pragma mark - Table view data delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //The "load more messages" cell
    if(indexPath.row == self.dataSource.messages.count) {
        [self.dataSource loadNextPage];
        return;
    }
}

#pragma mark - Actions

- (void) moreAction {
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:kStrSelectOption
                                                                        message:@""
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *endChatAction = [UIAlertAction actionWithTitle:kStrEndChat
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [BBMAccess leaveChat:self.chat];
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                          }];
    
    UIAlertAction *chatDetails = [UIAlertAction actionWithTitle:kStrChatDetails
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            [self performSegueWithIdentifier:@"ShowParticipantDetails" sender:self];
                                                        }];
    
    UIAlertAction *retractChatAction = [UIAlertAction actionWithTitle:kStrRetractChat
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [BBMAccess retractChat:self.chat];
                                                              }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    
    if(!self.chat.isOneToOneFlagSet){
        [controller addAction:chatDetails];
    }
    [controller addAction:endChatAction];
    [controller addAction:retractChatAction];
    [controller addAction:cancelAction];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)moreTapped:(id)sender
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:kStrSelectOption
                                                                        message:@""
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *endChatAction = [UIAlertAction actionWithTitle:kStrEndChat
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [BBMAccess leaveChat:self.chat];
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                          }];

    UIAlertAction *chatDetails = [UIAlertAction actionWithTitle:kStrChatDetails
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self performSegueWithIdentifier:@"ShowParticipantDetails" sender:self];
                                                                }];

    UIAlertAction *retractChatAction = [UIAlertAction actionWithTitle:kStrRetractChat
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                    [BBMAccess retractChat:self.chat];
                                                                }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];

    if(!self.chat.isOneToOneFlagSet){
        [controller addAction:chatDetails];
    }
    [controller addAction:endChatAction];
    [controller addAction:retractChatAction];
    [controller addAction:cancelAction];

    [self presentViewController:controller animated:YES completion:nil];
}

/* Since we have changed audio and video call seperate button in chat conversation two different method has been called, One for audio and another for video*/

/*This method is called for Audio calls */
- (void) PhoneCallMethod {
    [BBMMediaCallHelper callRequestedForChat:self.chat viewController:self];
}

/*This method is called for Video calls */
- (void) VideoCallMethod {
    [BBMMediaCallHelper voiceCallRequestedForChat:self.chat viewController:self];

}

/*Previous method for audio and video call has been commented*/

//- (IBAction)callTapped:(UIBarButtonItem*)sender
//{
//    [BBMMediaCallHelper callRequestedForChat:self.chat viewController:self];
//}

#pragma mark - Typing Users

- (void)setTextForTypingUsers:(NSArray *)typingUsers
{
    if (typingUsers.count > 0) {
        //There is at least one user typing so build the string to display to the user.
        //The first user will be displayed by name, any additional users will simply be displayed as
        //the number of other users typing.
        NSString *otherUsers = @"";
        if (typingUsers.count > 1) {
            otherUsers = [NSString stringWithFormat:kStrTypingOthers, @(typingUsers.count - 1)];
        }
        
        BBMTyping *typingUser = typingUsers.firstObject;
        NSString *name = [BBMUIUtilities displayNameForUser:typingUser.resolvedUserUri];
        self.typingUsersLabel.text = [NSString stringWithFormat:kStrTypingNotification, name, otherUsers];
    }
    else {
        self.typingUsersLabel.text = nil;
    }
}


#pragma mark - Message Actions

- (void)handleMessageTapForCell:(BBMMessageCell *)cell
{
    BBMChatMessage *message = cell.message;
    //This will show the message delivery status.  You can customize other actions here as
    //needed.  To show delivery status, the message must be outgoing, the chat must be a multi
    //person chat and the message shouldn't be recalled
    if(!message.isIncomingFlagSet &&
       !message.resolvedChatId.isOneToOneFlagSet &&
       !(message.recall == kBBMChatMessage_RecallRecalled))
    {
        [self.messageInputView.messageTextField resignFirstResponder];
        BBMGroupChatMessageDetailView *msgDetials = [BBMGroupChatMessageDetailView viewWithMessage:message
                                                                                    avatarProvider:self.avatarProvider];
        [msgDetials presentInView:self.view dismissCallback:nil];
    }
    else if(message.isIncomingFlagSet && message.isTimedMessage && !message.isExpiredTimedMessage)
    {
        self.timedMessage = message;
        [self performSegueWithIdentifier:@"TimedMessageSegue" sender:self];
    }
}


-(void)handleMessageLongPressForCell:(BBMMessageCell *)cell
{
    BBMChatMessage *message = cell.message;
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:kStrMessageActions
                                                                message:message.content
                                                         preferredStyle:UIAlertControllerStyleActionSheet];



    if([message canBeQuoted]) {
        
    /*Here quote message functionality is not added with the AlertController since it does not required for the functionality */
      /*  UIAlertAction *quoteAction = [UIAlertAction actionWithTitle:kStrQuoteAction
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  [self.messageInputView showQuotedMessage:message];
                                                                  [self.messageInputView becomeFirstResponder];
                                                              }];
        
        [ac addAction:quoteAction]; */
    }

    if([message canBeEdited]) {
        UIAlertAction *editAction = [UIAlertAction actionWithTitle:kStrEditAction
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  [self.messageInputView showEditedMessage:message];
                                                                  [self.messageInputView becomeFirstResponder];
                                                              }];
        [ac addAction:editAction];
    }

    UIAlertAction *copyAction = [UIAlertAction actionWithTitle:kStrCopyAction
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              [[UIPasteboard generalPasteboard] setString:message.content];
                                                          }];
    [ac addAction:copyAction];

    if([message canBeRetracted]) {
        UIAlertAction *retractAction = [UIAlertAction actionWithTitle:kStrRetractAction
                                                                style:UIAlertActionStyleDestructive
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  [BBMAccess recallMessage:message];
                                                              }];
        [ac addAction:retractAction];
    }

    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:kStrDeleteAction
                                                            style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              [BBMAccess deleteMessage:message];
                                                          }];
    [ac addAction:deleteAction];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [ac addAction:cancelAction];

    [self presentViewController:ac animated:YES completion:nil];
}

@end

@implementation BBMLoadMoreMessagesCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.messageLabel.text = kStrLoadMore;
}

@end
