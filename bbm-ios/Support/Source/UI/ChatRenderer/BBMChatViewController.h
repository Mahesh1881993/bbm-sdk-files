// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>
#import "BBMImageProvider.h"

@class BBMChat;

/*!
 @details
 Displays a list of messages in a conversation.  You may customize the look and feel in the
 BBMChatView.storyboard file
 */
@interface BBMChatViewController : UIViewController

/*!
 @details
 The avatarProvider used to load avatars.  You may provide a custom implementation here.  If
 unspecified the shared instance of BBMImageLoader will be used
 */
@property (nonatomic, strong) id<BBMImageProvider> avatarProvider;

/*!
 @details
 Create an instance of the chat view with the given chat
 @param chat The chat to render
 */
+ (instancetype)viewControllerWithChat:(BBMChat *)chat;

/*!
 @details
 Set to false to hide the controls to place a voice or video call.  These are shown by default.
 */
@property (nonatomic) BOOL showMediaControls;

@end


@interface BBMLoadMoreMessagesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@end
