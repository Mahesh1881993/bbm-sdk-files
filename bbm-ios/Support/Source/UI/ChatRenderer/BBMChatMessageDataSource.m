// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMChatMessageDataSource.h"
#import "BBMMessageCell.h"
#import "BBMUIWidgets.h"

@interface BBMChatMessageDataSource ()

@property (nonatomic,   copy) MessageActionCallback longPressCallback;
@property (nonatomic,   copy) MessageActionCallback tapCallback;

@property (nonatomic, strong) BBMChat *chat;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, readwrite) NSArray *messages;
@property (nonatomic, readwrite) BBMMessageLoader *messageLoader;

@end



@implementation BBMChatMessageDataSource

+ (instancetype)dataSourceWithChat:(BBMChat *)chat
                 longPressCallback:(MessageActionCallback)longPressCallback
                       tapCallback:(MessageActionCallback)tapCallback
                         tableView:(UITableView *)table
{
    BBMChatMessageDataSource *retVal = [[BBMChatMessageDataSource alloc] init];
    retVal.longPressCallback = longPressCallback;
    retVal.tapCallback = tapCallback;
    retVal.chat = chat;
    retVal.tableView = table;
    [retVal configureMessageLoader];
    return retVal;
}

- (void)configureMessageLoader
{
    typeof(self) __weak weakSelf = self;

    MessageLoaderCallback callback = ^(NSArray *messageList) {
        weakSelf.messages = messageList;
        [weakSelf.tableView reloadData];
    };

    self.tableView.dataSource = self;
    self.messageLoader = [BBMMessageLoader messageLoaderForConversation:self.chat
                                                                 pageSize:kChatViewMessageBatchSize
                                                                 callback:callback];
}

- (void)loadNextPage
{
    [self.messageLoader loadNextPage];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //If the messageLoader can load more messages we show an extra row to allos the user to load
    //more messages
    return self.messages.count + ((self.messages.count > 0 && [self.messageLoader canLoadMoreMessages]) ? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [self cellIdentifierForIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    //Flip each cell vertically because table is upside down.
    cell.transform = CGAffineTransformMakeScale(1, -1);
    if(indexPath.row == self.messages.count) {
        //This is the "loadMoreMessages" cell
        return cell;
    }

    BBMMessageCell *messageCell = (BBMMessageCell*)cell;
    BBMChatMessage *message = self.messages[indexPath.row];
    [messageCell showMessage:message];

    messageCell.longPressActionCallback = self.longPressCallback;
    messageCell.tapActionCallback = self.tapCallback;
    messageCell.avatarProvider = self.avatarProvider;
    return cell;
}

-(NSString *)cellIdentifierForIndexPath:(NSIndexPath*)indexPath
{
    //This is the identifier for the cell to allow the user to load more messages
    if(indexPath.row == self.messages.count) {
        return kCellIdLoadMore;
    }
    BBMChatMessage *message = self.messages[indexPath.row];

    NSString *cellIdentifier;
    //Recalled and timed messages are always displayed using a text message cell
    if ([message.tag isEqualToString:kBBMChatMessage_TagText] ||
        message.recall == kBBMChatMessage_RecallRecalled ||
        message.recall == kBBMChatMessage_RecallRecalling ||
        [message.tag isEqualToString:kMessageTag_TimedMessage])
    {
        cellIdentifier = kCellIdText;
    }
    else if ([message.tag isEqualToString:kMessageTag_Picture]) {
        cellIdentifier = kCellIdPhoto;
    }
    else if ([message.tag isEqualToString:kMessageTag_Quote]) {
        cellIdentifier = kCellIdQuoted;
    }
    else {
        cellIdentifier = kCellIdSystem;
    }
    NSString *direction = message.isIncomingFlagSet ? kCellDirectionIncoming : kCellDirectionOutgoing;
    return [NSString stringWithFormat:@"%@%@", cellIdentifier, direction];
}

@end
