// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>

#import "BBMFileMessageCell.h"
#import "BBMImageCache.h"
#import "BBMAccess.h"
#import "BBMUIWidgets.h"

@interface BBMFileMessageCell () <UIDocumentInteractionControllerDelegate>
@property (nonatomic, strong) ObservableMonitor *progressMonitor;
@property (nonatomic, strong) ObservableMonitor *fileMonitor;
@property (nonatomic, strong) UIDocumentInteractionController *ic;
@end

@implementation BBMFileMessageCell

- (void)showMessage:(BBMChatMessage *)message
{
    [super showMessage:message];

    typeof(self) __weak weakSelf = self;
    self.fileMonitor = [ObservableMonitor monitorActivatedWithName:@"messageTableViewCellMessageMonitor" block:^{
        if(message.fileState == kBBMChatMessage_FileStateAvailable ||
           message.fileState == kBBMChatMessage_FileStateFailedAvailable)
        {
            [weakSelf showDownloadButton];
        }
        else if(message.fileState == kBBMChatMessage_FileStateFailed) {
            [weakSelf showDownloadError];
        }
        else if(message.fileState == kBBMChatMessage_FileStateTransferring) {
            [weakSelf showTransferProgress];
        }
        else if(message.fileState == kBBMChatMessage_FileStateDone) {
            [weakSelf showFileAvailable];
        }

        //Show the thumbnail if it is available
        if(weakSelf.message.thumb) {
            [weakSelf loadThumbnail:weakSelf.message];
        }
    }];
}

- (void)showTransferProgress
{
    if(self.progressIndicator == nil) {
        return;
    }

    [self showTransferIndicator];

    typeof(self) __weak weakSelf = self;
    self.progressMonitor = [ObservableMonitor monitorActivatedWithName:@"file_transfer_progress" selfTerminatingBlock:^BOOL{
        BBMChatMessageKey *key = [BBMChatMessageKey keyWithChatId:weakSelf.message.chatId messageId:weakSelf.message.messageId];
        BBMLiveMap *progressMap = [[[BBMEnterpriseService sharedService] model] chatMessageFileProgress];
        BBMChatMessageFileProgress *progress = [progressMap valueForKey:key];

        if(progress.bbmState == kBBMStateCurrent) {
            double pct = ((double)progress.bytes / (double)progress.total);
            weakSelf.progressIndicator.progress = pct;
            if(pct > 0.99) {
                return YES;
            }
        }
        else if(progress.bbmState == kBBMStateMissing) {
            return YES;
        }

        return NO;
    }];
}

- (void)loadThumbnail:(BBMChatMessage *)message
{
    typeof(self) __weak weakSelf = self;
    NSString *imagePath = message.thumb;
    UIImage *cachedImage =  [[BBMImageCache sharedImageCache] imageForKey:message.thumb];

    if(cachedImage) {
        weakSelf.thumbnailView.image = cachedImage;
    }else{
        //The thumbnail is always a local file so loading it via the
        //a default queue is safe
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData *data = [NSData dataWithContentsOfFile:imagePath];
            UIImage *thumb = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(thumb && message.thumb) {
                    [[BBMImageCache sharedImageCache] cacheImage:thumb forKey:message.thumb];
                }
                weakSelf.thumbnailView.image = thumb;
            });
        });
    }

}

#pragma mark - Document Preview

- (NSString *)tempPreviewPath
{
    //All BBME files are saved with a generic name such as <sandbox>/Files/1/123
    //The UIDocumentInterationController requires us to add the correct extension
    //so we copy the file to the caches directory with the extension added
    //The UTI type is also require.  Both the extension and the UTI should be
    //provided by the sender in the message rawData.

    NSString *extension = self.message.rawData[kMessageDataKey_FileExtension];
    if(nil == extension) {
        extension = [self.message.rawData[kMessageDataKey_FileName] pathExtension];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    if(paths.count == 0 || extension == nil) {
        NSLog(@"Unable to generate temp file name");
        return nil;
    }

    NSString *tempDir = paths[0];

    //To ensure uniqueness, we'll use the chatId and messageId to generate
    //a unique filename.
    NSString *fileName = [NSString stringWithFormat:@"%@_%lld", self.message.chatId, self.message.messageId];
    NSString *tempFile = [fileName stringByAppendingPathExtension:extension];
    NSString *tempPath = [tempDir stringByAppendingPathComponent:tempFile];

    return tempPath;
}

- (void)openFile
{
    //Ensure that we were sent the UTI Type for the file
    NSString *uti = self.message.rawData[kMessageDataKey_FileUTIType];
    NSString *extension = [self.message.rawData[kMessageDataKey_FileName] pathExtension];
    if(nil == uti && nil == extension) {
        NSLog(@"Not type can be inferred from file.  Unable to preview");
        return;
    }

    //Save a temporary copy with the correct extension
    NSError *error = nil;
    NSString *tempPath = [self tempPreviewPath];
    if(nil == tempPath) {
        return;
    }

    [[NSFileManager defaultManager] copyItemAtPath:self.message.file
                                            toPath:tempPath
                                             error:&error];

    if(error) {
        NSLog(@"Error writing temporary file for preview. %@", error.localizedDescription);
        return;
    }

    NSURL *file = [NSURL fileURLWithPath:tempPath];
    UIDocumentInteractionController *ic = [[UIDocumentInteractionController alloc] init];
    ic.URL = file;
    ic.UTI = uti;
    ic.name = KStrFilePreviewTitle;
    ic.delegate = self;
    [ic presentPreviewAnimated:YES];
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return [UIApplication sharedApplication].keyWindow.rootViewController;
}

- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller
{
    NSString *tempPath = [self tempPreviewPath];

    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&error];

    if(error) {
        NSLog(@"Error deleting temporary file for preview. %@", error.localizedDescription);
        return;
    }
}

#pragma mark - Download controls

- (IBAction)actionButtonPressed:(id)sender
{
    if(self.message.fileState == kBBMChatMessage_FileStateAvailable) {
        BBMChatMessageFileDownloadMessage *msg = [[BBMChatMessageFileDownloadMessage alloc] initWithChatId:self.message.chatId
                                                                                                 messageId:self.message.messageId];

        [[BBMEnterpriseService sharedService] sendMessageToService:msg];
    }
    else if(self.message.fileState == kBBMChatMessage_FileStateDone) {
        [self openFile];
    }
}

#pragma mark - File transfer status indicators

- (void)showTransferIndicator
{
    self.statusContainer.hidden = NO;
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:self.message.file];
    if(!self.message.isIncomingFlagSet && exists) {
        self.statusLabel.text = KStrSendingFile;
    }else{
        self.statusLabel.text = kStrDownloadingFile;
    }
    self.progressIndicator.hidden = NO;
}

- (void)showDownloadButton
{
    self.statusContainer.hidden = YES;
}

- (void)showDownloadError
{
    self.actionButton.hidden = YES;
    self.statusLabel.text = kStrFileUnavailable;
}

- (void)showFileAvailable
{
    self.statusContainer.hidden = YES;
}

@end
