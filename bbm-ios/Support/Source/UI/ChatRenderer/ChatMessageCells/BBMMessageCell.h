// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMImageProvider.h"

@class BBMMessageCell;

typedef void (^MessageActionCallback)(BBMMessageCell *cell);

/*!
 @abstract
 Base class for all BBMUIWidget message cells
 */
@interface BBMMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *priorityView;
@property (weak, nonatomic) IBOutlet UIView *bubbleView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *senderAvatar;

/*! @brief an BBMImageProvider implementation used to load avatars */
@property (nonatomic) id<BBMImageProvider> avatarProvider;

/*! @brief Invoked on the long press of the message bubble  */
@property (nonatomic, copy) MessageActionCallback longPressActionCallback;
@property (nonatomic, copy) MessageActionCallback tapActionCallback;

/*! @brief The message associated wit this cell  */
@property (nonatomic, readonly) BBMChatMessage *message;

/*!
 @details
 Sets the message and sets up the observers.  Subclasses will override this to
 do custom setup.
 */
- (void)showMessage:(BBMChatMessage *)message;


@end
