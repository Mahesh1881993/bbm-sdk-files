// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMPictureMessagCell.h"
#import "BBMUIWidgets.h"
#import "BBMImageCache.h"

@interface BBMPictureMessageCell()

@property (nonatomic) ObservableMonitor *pictureMonitor;
@property (nonatomic) ObservableMonitor *captionMonitor;

@end

@implementation BBMPictureMessageCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.dateLabel.layer.shadowOpacity = 1.0;
    self.dateLabel.layer.shadowRadius = 3.0;
    self.dateLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.dateLabel.layer.shadowOffset = CGSizeMake(0.0, 0.0);

    self.thumbnailView.layer.cornerRadius = kChatBubbleCornerRadius;
    self.thumbnailView.clipsToBounds = YES;

    self.statusContainer.layer.cornerRadius = kChatBubbleCornerRadius;
    self.statusContainer.clipsToBounds = YES;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.thumbnailView.image = nil;
}

-(void)showMessage:(BBMChatMessage*)message
{
    [super showMessage:message];
    typeof(self) __weak weakSelf = self;
    self.captionMonitor = [ObservableMonitor monitorActivatedWithName:@"photoCaptionMonitor" block:^{
        BOOL hasCaption = [BBMUIUtilities stringForMessageCellForChatMessage:weakSelf.message].length > 0;
        weakSelf.statusImage.hidden = !hasCaption;
    }];
}




@end


@implementation BBMPictureMessageCellIncoming
@end

@implementation BBMPictureMessageCellOutgoing

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.bubbleView.backgroundColor = kChatBubbleOutgoingBGColor;
}

@end
