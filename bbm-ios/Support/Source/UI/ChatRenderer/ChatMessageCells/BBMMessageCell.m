// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMMessageCell.h"

#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMUIWidgets.h"
#import "BBMImageCache.h"
#import "SharedData.h"
#import "Reachability.h"


@interface BBMMessageCell ()
@property (nonatomic) ObservableMonitor *textMonitor;
@property (nonatomic) ObservableMonitor *statusMonitor;
@property (nonatomic) ObservableMonitor *avatarMonitor;

@property (nonatomic, readwrite) BBMChatMessage *message;
@property (nonatomic) NSString *avatarUrl;
@property (nonatomic) NSString *profileUrl;


@end


@implementation BBMMessageCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.bubbleView.layer.cornerRadius = kChatBubbleCornerRadius;
    self.bubbleView.layer.masksToBounds = YES;
    //Add a gesture recognizer to show the menu controller
    UILongPressGestureRecognizer *lpGr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(longPress:)];
    [self addGestureRecognizer:lpGr];

    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(bubbleTapped:)];
    [self.bubbleView addGestureRecognizer:tapGr];
    self.senderAvatar.image = [UIImage imageNamed:kStrDefautAvatarImage];
    
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.senderAvatar.image = [UIImage imageNamed:kStrDefautAvatarImage];
}

-(void)showMessage:(BBMChatMessage*)message
{
    if(message == self.message) { return; }
    
    self.message = message;
    //Monitor the text because it can change any time.
    typeof(self) __weak weakSelf = self;
    self.textMonitor = [ObservableMonitor monitorActivatedWithName:@"messageTableViewCellMessageMonitor" block:^{
        if ([message isHighPriority]) {
            weakSelf.priorityView.hidden = NO;
        }
        else {
            weakSelf.priorityView.hidden = YES;
        }
        weakSelf.messageLabel.text = [BBMUIUtilities stringForMessageCellForChatMessage:message];
        weakSelf.senderLabel.text = message.isIncomingFlagSet ? [BBMUIUtilities displayNameForUser:message.resolvedSenderUri] : nil;
        weakSelf.dateLabel.text = [BBMUIUtilities formattedChatMessageTimestamp:weakSelf.message.timestamp];
    }];

    self.statusMonitor = [ObservableMonitor monitorActivatedWithName:@"messageTableViewCellMessageMonitor" block:^{
        weakSelf.statusImage.image = [BBMUIUtilities statusImageForChatMessage:weakSelf.message];
    }];

    self.avatarMonitor = [ObservableMonitor monitorActivatedWithName:@"avatarMonitor" block:^{
        if(message.isIncomingFlagSet) {
            BBMUser *user = message.resolvedSenderUri;
            BBMAppUser *appUser = [BBMUIUtilities userForRegId:user.regId];
            
           /*This Function is used to update the profile picture for the user by passing userID as parameter*/
            [self getUserImage:appUser.uid];
            
           /*Default profile picture update has been commented*/
            
//            if(appUser && appUser.avatarUrl ) {
//                [weakSelf loadAvatarUrl:appUser.avatarUrl];
//            }else{
//                weakSelf.senderAvatar.image = [UIImage imageNamed:kStrDefautAvatarImage];
//            }
        }
    }];

}


/*This method is used to check the internet availabilty by using Reachability class*/
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


/*This method is used to get the profile picture for the user by using NSURL Session(API HIT)*/
- (void) getUserImage: (NSString *) uid {
    
    if ([self connected]) {
        [SharedData sharedData].isThemeColor = NO;
        typeof(self) __weak weakSelf = self;

        
        NSString * token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
        NSString * appendToken = [NSString stringWithFormat:@"%@ %@",@"Bearer",token];
        
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",@"https://graph.microsoft.com/beta/users/",uid,@"/Photo/$value"];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        request.HTTPMethod = @"GET";
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:appendToken forHTTPHeaderField:@"Authorization"];
        
        NSError *error = nil;

        if (!error) {
            
            NSURLSessionDataTask *downloadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!error) {
                        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                        if (httpResp.statusCode == 200) {
                            weakSelf.senderAvatar.image = [UIImage imageWithData:data];
                           
                        }
                        else {
                            weakSelf.senderAvatar.image = [UIImage imageNamed:kStrDefautAvatarImage];
                   
                        }
                        
                    }
                });
                
            }];
            
            
            [downloadTask resume];
            
        }
    }
    
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    [BBMUIUtilities cirularizeView:self.senderAvatar];
}

#pragma mark - Cell Action

-(void)longPress:(UILongPressGestureRecognizer*) gestureRecognizer
{
    if(gestureRecognizer.state ==  UIGestureRecognizerStateBegan) {
        if(self.longPressActionCallback) {
            self.longPressActionCallback(self);
        }
    }
}

- (void)bubbleTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    if(gestureRecognizer.state ==  UIGestureRecognizerStateEnded) {
        if(self.tapActionCallback) {
            self.tapActionCallback(self);
        }
    }
}

- (void)loadAvatarUrl:(NSString *)url
{
    self.avatarUrl = url;
    UIImage *avatar = [[BBMImageCache sharedImageCache] imageForKey:url];
    if(avatar) {
        self.senderAvatar.image = avatar;
        return;
    }

    if(url && self.avatarProvider) {
        //The image loader can have a long timeout.  Use a weak ref to ensure we aren't retaining
        //cells, tables, etc if the callback is badly delayed
        typeof(self) __weak weakSelf = self;
        [self.avatarProvider loadImage:url callback:^(UIImage *image, NSString *source, NSError *error) {
            if(nil == error && image && [source isEqualToString:weakSelf.avatarUrl]) {
                weakSelf.senderAvatar.image = image;
                [[BBMImageCache sharedImageCache] cacheImage:image forKey:url];
            }
        }];
    }else{
        self.senderAvatar.image = [UIImage imageNamed:kStrDefautAvatarImage];
    }
}


@end
