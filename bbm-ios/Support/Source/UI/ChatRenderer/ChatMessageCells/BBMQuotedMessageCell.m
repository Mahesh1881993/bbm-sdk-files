// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMQuotedMessageCell.h"
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMMessageTypes.h"
#import "BBMUIWidgets.h"

@interface BBMQuotedMessageCell ()

@property (nonatomic) ObservableMonitor *quoteMonitor;
@property (weak, nonatomic) IBOutlet UILabel *quoteLabel;

@end

@implementation BBMQuotedMessageCell

- (void)showMessage:(BBMChatMessage *)message
{
    [super showMessage:message];
    
    typeof(self) __weak weakSelf = self;
    self.quoteMonitor = [ObservableMonitor monitorActivatedWithName:@"quoteMonitor" block:^{
        weakSelf.quoteLabel.text = [NSString stringWithFormat:@"%@ wrote: \"%@\"",
                                    message.rawData[kMessageDataKey_QuoteSource],
                                    message.rawData[kMessageDataKey_QuoteText]];
    }];
}

@end

@implementation BBMQuotedMessageCellIncoming
@end

@implementation BBMQuotedMessageCellOutgoing

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.bubbleView.backgroundColor = kChatBubbleOutgoingBGColor;
}

@end


