// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>

#import "BBMChatParticipantsViewController.h"
#import "BBMChatParticipantDetailCell.h"

#import "BBMUIWidgets.h"

static NSString *const kParticipantCellIdentifier = @"BBMChatParticipantDetailCell";

@interface BBMChatParticipantsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView             *tableView;

@property (nonatomic, strong) ObservableMonitor              *participantsMonitor;
@property (nonatomic, strong) BBMChat                        *chat;
@property (nonatomic, strong) NSArray                        *participants;
@property (nonatomic, strong) id<BBMImageProvider>           avatarProvider;

@end

@implementation BBMChatParticipantsViewController

- (void)setChat:(BBMChat *)chat avatarProvider:(id<BBMImageProvider>)avatarProvider
{
    self.chat = chat;
    self.avatarProvider = avatarProvider;

    typeof(self) __weak weakSelf = self;
    void (^participantsMonitorBlock)(void) = ^{
        BBMLiveList *participants = [BBMAccess participantsForChat:weakSelf.chat];
        if(participants.bbmState == kBBMStateCurrent)
        {
            NSMutableArray *participantArray = [participants.observableArray mutableCopy];
            NSPredicate *statePredicate = [NSPredicate predicateWithBlock:^BOOL(BBMChatParticipant  *participant, NSDictionary<NSString *,id> * _Nullable bindings) {
                return (participant.state == kBBMChatParticipant_StateActive || participant.state == kBBMChatParticipant_StatePending);
            }];
            [participantArray filterUsingPredicate:statePredicate];
            [participantArray sortUsingComparator:^NSComparisonResult(BBMChatParticipant *p1, BBMChatParticipant *p2) {
                BBMAppUser *user1 = [BBMUIUtilities userForRegId:p1.resolvedUserUri.regId];
                BBMAppUser *user2 = [BBMUIUtilities userForRegId:p2.resolvedUserUri.regId];
                return [user1.name compare:user2.name];
            }];
            weakSelf.participants = participantArray;
            [weakSelf.tableView reloadData];
        }

    };
    self.participantsMonitor = [ObservableMonitor monitorActivatedWithName:@"ParticipantsMonitor"
                                                                     block:participantsMonitorBlock];
}


#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.participants.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BBMChatParticipantDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:kParticipantCellIdentifier
                                                                             forIndexPath:indexPath];
    BBMChatParticipant *participant = self.participants[indexPath.row];
    [cell setParticipant:participant avatarProvider:self.avatarProvider];
    return cell;
}

#pragma mark - UITableViewDelegate

/* didSelectRowAtIndexPath is commented since we have restricted the add or remove admin and partcipiants from the group and normal chat respectively */

/* - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BBMChatParticipant *participant = self.participants[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self showActionMenu:participant];
}  */

- (void)showActionMenu:(BBMChatParticipant *)participant
{
    BBMChat *chat  = [BBMAccess getChatForId:participant.chatId];
    if(!chat.isAdminFlagSet) {
        return;
    }

    UIAlertAction *promoteDemoteAction = nil;
    if(participant.isAdminFlagSet) {
        promoteDemoteAction = [UIAlertAction actionWithTitle:kStrRemoveAdmin
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                         [BBMAccess demoteParticipant:participant];
                                                     }];
    }else{
        promoteDemoteAction = [UIAlertAction actionWithTitle:kStrAddAdmin
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                         [BBMAccess promoteParticipant:participant];
                                                     }];
    }

    UIAlertAction *removeAction = [UIAlertAction actionWithTitle:kStrRemoveParticipant
                                            style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * _Nonnull action) {
                                              [BBMAccess removeParticipantFromChat:participant];
                                          }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kStrGenericCancel
                                            style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * _Nonnull action) {
                                          }];

    UIAlertController *ac = [UIAlertController alertControllerWithTitle:kStrUserAdmin
                                                                message:@""
                                                         preferredStyle:UIAlertControllerStyleActionSheet];


    [ac addAction:promoteDemoteAction];
    [ac addAction:removeAction];
    [ac addAction:cancelAction];

    [self presentViewController:ac animated:YES completion:nil];
}

@end
