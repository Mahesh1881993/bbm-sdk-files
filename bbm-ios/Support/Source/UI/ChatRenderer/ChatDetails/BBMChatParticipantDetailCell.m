// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMChatParticipantDetailCell.h"

#import "BBMUIWidgets.h"
#import "BBMAccess.h"
#import "BBMUIUtilities.h"
#import "BBMAppUser.h"
#import "BBMImageProvider.h"

@interface BBMChatParticipantDetailCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UIImageView *chatOwnerOverlayIcon;


@property (strong, readwrite) BBMChatParticipant *participant;
@property (strong, nonatomic) NSString *chatId;
@property (strong, nonatomic) NSString *avatarUrl;
@property (strong, nonatomic) ObservableMonitor *participantMonitor;

@end



@implementation BBMChatParticipantDetailCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.nameLabel.text = @"";
    [self.avatarImage setImage:[UIImage imageNamed:kStrDefautAvatarImage]];
    [self.statusLabel setText:@""];
    self.chatOwnerOverlayIcon.hidden = YES;
    self.participant = nil;
}


- (void)setParticipant:(BBMChatParticipant *)participant
        avatarProvider:(id<BBMImageProvider>)avatarProvider
{
    self.participant = participant;
    self.chatId = participant.chatId;
    BBMUser *user = [BBMAccess getUserForUserUri:participant.userUri];
    BBMAppUser *appUser = [BBMUIUtilities userForRegId:user.regId];
    [BBMUIUtilities cirularizeView:self.avatarImage];

    BBMChatParticipantDetailCell *__weak weakSelf = self;
    self.participantMonitor = [ObservableMonitor monitorActivatedWithName:@"participantMonitor" block:^{
        weakSelf.nameLabel.text = [BBMUIUtilities userForRegId:participant.resolvedUserUri.regId].name;
        weakSelf.chatOwnerOverlayIcon.hidden = !weakSelf.participant.isAdminFlagSet;
        weakSelf.statusLabel.text = [weakSelf participantStateString:participant.state];
        NSString *url = appUser.avatarUrl;
        weakSelf.avatarUrl = url;
        [avatarProvider loadImage:url callback:^(UIImage *image, NSString *source, NSError *error) {
            if([source isEqualToString:weakSelf.avatarUrl] && image && nil == error) {
                weakSelf.avatarImage.image = [UIImage imageNamed:@"call_decline"];
            }
        }];
    }];
}

- (NSString *)participantStateString:(BBMChatParticipantState)state
{
    NSString *retVal = @"";
    switch (state) {
        case kBBMChatParticipant_StateActive:
            retVal = kStrParticipantActive;
            break;
        case kBBMChatParticipant_StatePending:
            retVal = kStrParticipantPending;
            break;
        case kBBMChatParticipant_StateLeft:
            retVal = kStrParticipantLeftChat;
            break;
        default:
            break;
    }
    return retVal;
}



@end
