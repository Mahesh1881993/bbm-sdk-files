// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <UIKit/UIKit.h>

@protocol BBMImageProvider;

/*!
 @abstract
 Cell for rendering chat participant details
 */
@interface BBMChatParticipantDetailCell : UITableViewCell

/*!
 @details
 Set the \a participant and \a avatarProvider.
 @param participant The chat participant to render
 @param avatarProvider An image provider instance to use for loading the user avatars
 */
- (void)setParticipant:(BBMChatParticipant *)participant
        avatarProvider:(id<BBMImageProvider>)avatarProvider;

/* @brief The participant associated with this cell */
@property (nonatomic, readonly) BBMChatParticipant *participant;

@end
