// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <UIKit/UIKit.h>

@class BBMChat;
@protocol BBMImageProvider;

/*!
 @abstract
 Renders a list of participants in a group chat.
 */
@interface BBMChatParticipantsViewController : UIViewController

/*!
 @details
 Set the chat and avatarProvider.  Call prior to presentation.
 @param chat The chat to render the participants for
 @param avatarProvider A BBMImageProvider instance to use to load the user avatars
 */
- (void)setChat:(BBMChat *)chat
 avatarProvider:(id<BBMImageProvider>)avatarProvider;

@end
