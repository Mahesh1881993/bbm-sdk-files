// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>

#import "BBMUIUtilities.h"
#import "BBMUIWidgets.h"


static BBMUserManager *s_userManager;
static BBMChatMessageListener *s_messageListener;
static BBMTimedMessageManager *s_defaultTimedMessageManager;


@implementation BBMUIUtilities

+ (BBMTimedMessageManager *)defaultTimedMessageManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_defaultTimedMessageManager = [[BBMTimedMessageManager alloc] init];
    });
    return s_defaultTimedMessageManager;
}

+ (BBMChatMessageListener *)defaultChatMessageListener
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_messageListener = [[BBMChatMessageListener alloc] init];
    });
    return s_messageListener;
}

+ (void)setDefaultUserManager:(BBMUserManager *)userManager
{
    s_userManager = userManager;
}

+ (NSString *)displayNameForUser:(BBMUser *)user
{
    if(s_userManager) {
        return [s_userManager userForRegId:user.regId].name;
    }else{
        return [user.regId stringValue];
    }
}

+ (BBMAppUser *)userForRegId:(NSNumber *)regId
{
    return [s_userManager userForRegId:regId];
}

+ (NSString *)chatTitleForSearchMatch:(NSString*)searchMatchUserUri inChat:(BBMChat *)chat;
{
    NSString *chatTitle = nil;

    NSArray *participants = [BBMAccess participantsForChat:chat].array;
    if (participants.count != 0) {
        NSString *matchedUserDisplayName;
        NSMutableArray *otherDisplayNames = [NSMutableArray array];
        for (BBMChatParticipant *participant in participants) {
            BBMUser *user = participant.resolvedUserUri;
            NSString *userDisplayName = [self displayNameForUser:user];
            if (userDisplayName != nil) {
                if ([user.uri isEqualToString:searchMatchUserUri]) {
                    matchedUserDisplayName = userDisplayName;
                } else {
                    [otherDisplayNames addObject:userDisplayName];
                }
            }
        }
        NSMutableArray *allNames = (matchedUserDisplayName == nil) ? [NSMutableArray array] : [NSMutableArray arrayWithObject:matchedUserDisplayName];
        [allNames addObjectsFromArray:otherDisplayNames];

        chatTitle = [allNames componentsJoinedByString:@", "];
    }
    return chatTitle;
}


+ (NSString *)stringForMessageCellForChatMessage:(BBMChatMessage *)msg;
{
    /*Here for name null condition is not checked and if user is in offline it shows (null)joined the chat/ (null)left the chat. To overcome this issue in offline, for name null condition is checked.
     If name is null return value is set to empty */
    
    NSString *name = [self displayNameForUser:msg.resolvedSenderUri];
    if([msg.resolvedSenderUri.uri isEqualToString:[BBMAccess currentUser].uri]) {
        name = kStrYou;
    }

    switch (msg.recall) {
        case kBBMChatMessage_Recall_Unspecified:
        case kBBMChatMessage_RecallFailed:
        case kBBMChatMessage_RecallNone: {
            if ([msg.tag isEqualToString:kBBMChatMessage_TagJoin]) {
                if ([name isEqualToString:@""] || [name isKindOfClass:[NSNull class]] || name == nil || name == (id) [NSNull null]) {
                    return @"";
                }else{
                    return [NSString stringWithFormat:kStrJoinedChat, name];
                }
            }
            else if ([msg.tag isEqualToString:kBBMChatMessage_TagLeave]) {
                if ([name isEqualToString:@""] || [name isKindOfClass:[NSNull class]] || name == nil || name == (id) [NSNull null]) {
                    return @"";
                }
                else {
                    return [NSString stringWithFormat:kStrLeftChat, name];
                }
            }
            else if ([msg.tag isEqualToString:kBBMChatMessage_TagShred]) {
                if(msg.isIncomingFlagSet) {
                    
                    if ([name isEqualToString:@""] || [name isKindOfClass:[NSNull class]] || name == nil || name == (id) [NSNull null]) {
                        return @"";
                    }
                    else {
                        return [NSString stringWithFormat:kStrRetractedChat, name];
                    }
                }
                else {
                    return [NSString stringWithFormat:kStrRetractChatSelf];
                }

            }
            else if ([msg.tag isEqualToString:kMessageTag_TimedMessage]) {
                if(msg.isIncomingFlagSet) {
                    return msg.isExpiredTimedMessage? kStrTimeExpired : kStrTimedReceived;
                }
                else {
                    NSNumber *duration = msg.rawData[kMessageDataKey_TimedMessage];
                    return [NSString stringWithFormat:kStrTimedDetails, (long)duration.integerValue];

                }
            }else if ([msg.tag isEqualToString:kBBMChatMessage_TagSubject]) {
//                return [NSString stringWithFormat:kStrSubjectChanged, msg.resolvedChatId.subject];
                return @"";
            }
            else if([msg.tag isEqualToString:kBBMChatMessage_TagAdmin]) {
                return kStrChatAdminChanged;
            }
            else if ([msg.tag isEqualToString:kBBMChatMessage_TagRemove] || [msg.tag isEqualToString:kBBMChatMessage_TagLeave]) {
                if ([name isEqualToString:@""] || [name isKindOfClass:[NSNull class]] || name == nil || name == (id) [NSNull null]) {
                    return @"";
                }
                else {
                    return [NSString stringWithFormat:kStrLeftChat, name];

                }
            }
            else if(msg.content.length == 0 && [msg isTagUnhandled]) {
                return msg.tag.length > 0 ? [NSString stringWithFormat:kStrUnhandledTag, msg.tag] : @"";;
            }
            return msg.content;
        }
        case kBBMChatMessage_RecallRecalled: {
            return kStrRetracted;
        }
        case kBBMChatMessage_RecallRecalling: {
            return kStrRetracting;
        }
        default: {
            return @"";
        }
    }
}


+ (NSString *)stringForChatCellForChatMessage:(BBMChatMessage *)msg;
{
    
    /*Here for name null condition is not checked and if user is in offline it shows (null)joined the chat/ (null)left the chat. To overcome this issue in offline, for name null condition is checked.
     If name is null return value is set to empty */
    
    NSString *name = [self displayNameForUser:msg.resolvedSenderUri];
 
    if([msg.resolvedSenderUri.uri isEqualToString:[BBMAccess currentUser].uri]) {
        name = kStrYou;
    }

    switch (msg.recall) {
        case kBBMChatMessage_RecallRecalled: {
            return kStrRetracted;
        }
        case kBBMChatMessage_RecallRecalling: {
            return kStrRetracting;
        }
        case kBBMChatMessage_Recall_Unspecified:
        case kBBMChatMessage_RecallFailed:
        case kBBMChatMessage_RecallNone:
        default: {
            break;
        }
    }

    if ([msg.tag isEqualToString:kMessageTag_Picture]) {
        return msg.isIncomingFlagSet ? kStrReceivedPicture : kStrSentPicture;
    }
    else if ([msg.tag isEqualToString:kBBMChatMessage_TagJoin]) {
        if ([name isEqualToString:@""] || [name isKindOfClass:[NSNull class]] || name == nil || name == (id) [NSNull null]) {
            return @"";
        }
        else {
            return [NSString stringWithFormat:kStrJoinedChat, name];
        }
    }
    else if ([msg.tag isEqualToString:kBBMChatMessage_TagRemove] || [msg.tag isEqualToString:kBBMChatMessage_TagLeave]) {
        if ([name isEqualToString:@""] || [name isKindOfClass:[NSNull class]] || name == nil || name == (id) [NSNull null]) {
            return @"";
        }
        else {
            return [NSString stringWithFormat:kStrLeftChat, name];
        }
    }
    else if([msg.tag isEqualToString:kBBMChatMessage_TagAdmin]) {
        return kStrChatAdminChanged;
    }
    else if ([msg.tag isEqualToString:kBBMChatMessage_TagShred]) {
        if(msg.isIncomingFlagSet) {
            if ([name isEqualToString:@""] || [name isKindOfClass:[NSNull class]] || name == nil || name == (id) [NSNull null]) {
                return @"";
            }
            else {
                return [NSString stringWithFormat:kStrRetractedChat, name];
            }
        }
        else {
            return [NSString stringWithFormat:kStrRetractChatSelf];
        }

    }
    else if ([msg.tag isEqualToString:kMessageTag_TimedMessage]) {
        if(msg.isIncomingFlagSet) {
            return msg.isExpiredTimedMessage? kStrTimeExpired : kStrTimedReceived;
        }
        else {
            NSNumber *duration = msg.rawData[kMessageDataKey_TimedMessage];
            return [NSString stringWithFormat:kStrTimedDetails, (long)duration.integerValue];
        }
    }else if ([msg.tag isEqualToString:kBBMChatMessage_TagSubject]) {
//        return [NSString stringWithFormat:kStrSubjectChanged, msg.resolvedChatId.subject];
        return @"";
    }
    else if(msg.content.length == 0 && [msg isTagUnhandled]) {
        return msg.tag.length > 0 ? [NSString stringWithFormat:kStrUnhandledTag, msg.tag] : @"";
    }
    return msg.content;
}


+ (UIImage *)statusImageForChatMessageState:(BBMChatMessageStateMessage_StatesState)status
{
    NSString *imageName = nil;
    switch(status)
    {
        case  kBBMChatMessageStateMessage_States_State_Unspecified:
            imageName = kPendingStateImage;
            break;
        case kBBMChatMessageStateMessage_States_StateDelivered:
            imageName = kDeliveredImage;
            break;
        case kBBMChatMessageStateMessage_States_StateRead:
            imageName = kReadStateImage;
            break;
    }
    return [UIImage imageNamed:imageName];
}


+ (UIImage *)statusImageForChatMessage:(BBMChatMessage *)message;
{
    BBMChat *chat = message.resolvedChatId;
    NSString *imageName = nil;
    if (message.recall == kBBMChatMessage_RecallRecalled || message.recall == kBBMChatMessage_RecallRecalling) {
        //For recalled messages we don't show the state
        imageName = kRetractStateImage;
    }
    else {
        switch (message.state) {
            case kBBMChatMessage_State_Unspecified: {
                imageName = kIncomingReadStateImage;
                break;
            }
            case kBBMChatMessage_StateDelivered: {
                imageName = [self getDeliveredStatusImageNameFor:message isGroupChat:!chat.isOneToOneFlagSet];
                break;
            }
            case kBBMChatMessage_StateFailed: {
                imageName = kFailedStateImage;
                break;
            }
            case kBBMChatMessage_StateRead: {
                imageName = [self getReadStatusImageNameFor:message isGroupChat:!chat.isOneToOneFlagSet];
                break;
            }
            case kBBMChatMessage_StateSending: {
                imageName = kSendingStateImage;
                break;
            }
            case kBBMChatMessage_StateSent: {
                imageName = kSentStateImage;
                break;
            }
        }
    }

    return [UIImage imageNamed:imageName];
}

+ (NSString *)getDeliveredStatusImageNameFor:(BBMChatMessage *)message isGroupChat:(BOOL)isGroupChat
{
    NSString *imageName = nil;
    if (message.isIncomingFlagSet) {
        imageName = kIncomingUnreadStateImage;
    } else { //Outgoing
        if (isGroupChat) {
            imageName = message.isStateIsPartial ? kDeliveredPartialStateImage : kDeliveredImage;
        } else {//1-1 Chat
            imageName = kDeliveredImage;
        }
    }
    return imageName;
}


+ (NSString *)getReadStatusImageNameFor:(BBMChatMessage *)message isGroupChat:(BOOL)isGroupChat
{
    NSString *imageName = nil;
    if (message.isIncomingFlagSet) {  //Grey pellet for incoming
        imageName = kIncomingReadStateImage;
    } else { //Outgoing
        if (isGroupChat) { // Group Chat
            imageName = message.isStateIsPartial ? kReadPartialStateImage : kReadStateImage;
        } else { //1-1 Chat
            imageName = kReadStateImage;
        }
    }
    return imageName;
}


+ (NSString *)formattedChatMessageTimestamp:(unsigned long long)timestamp {
    if(timestamp == 0) {
        return @"";
    }
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)timestamp];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    //If the timestamp is from today only show the time, otherwise show the full date.
    if ([[NSCalendar currentCalendar] isDateInToday:date]) {
        formatter.timeStyle = NSDateFormatterShortStyle;
    }
    else {
        formatter.dateStyle = NSDateFormatterShortStyle;
    }

    return [formatter stringFromDate:date];
}

+ (void)cirularizeView:(UIView *)view
{
    CAShapeLayer *shape = [CAShapeLayer layer];
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:view.bounds];
    shape.path = path.CGPath;
    view.layer.mask = shape;
    view.layer.masksToBounds = YES;
}

@end


#pragma mark - Title Observer

@interface BBMChatTitleObserver ()
@property (nonatomic) BBMChat *chat;
@property (nonatomic, readwrite) NSString *chatTitle;
@property (nonatomic) ObservableMonitor *titleMonitor;
@end

@implementation BBMChatTitleObserver


- (instancetype)initWithChat:(BBMChat *)chat;
{
    self = [super init];
    if(self) {
        self.chat = chat;
    }
    return self;
}


- (NSString *)chatTitle
{
    [ObservableTracker getterCalledForObject:self propertyName:@"chatTitle"];

    if (nil == self.titleMonitor) {
        typeof(self) __weak weakSelf = self;
        self.titleMonitor = [ObservableMonitor monitorActivatedWithName:@"updateChatTitleMonitor" block:^{
            //If the chat has a subject display it, otherwise display a list of the participants.
            if (weakSelf.chat.subject.length > 0) {
                weakSelf.chatTitle = weakSelf.chat.subject;
            }
            else {
                NSString *title = [weakSelf participantsDisplayNamesForTitle];
                if(title == nil || title.length ==0) {
                    title = kStrEmptyChat;
                }
                weakSelf.chatTitle = title;
            }
        }];
    }

    return _chatTitle;
}


- (NSString *)participantsDisplayNamesForTitle
{
    NSArray *participants = [BBMAccess participantsForChat:self.chat].array;
    NSMutableArray *displayNames = [[NSMutableArray alloc] init];
    for (BBMChatParticipant *participant in participants) {
        BBMUser *user = participant.resolvedUserUri;
        if(user.bbmState == kBBMStatePending) {
            return @"";
        }
        NSString *name = [s_userManager userForRegId:user.regId].name;
        if (name != nil) {
            [displayNames addObject:name];
        }
    }
    return [displayNames componentsJoinedByString:@", "];
}

@end
