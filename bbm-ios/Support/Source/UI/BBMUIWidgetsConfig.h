// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


/*!
 Configuration parameters for the UIWidget Library.  You may customize the look and feel of the
 UI widgets by modifying the constants here.
 */

#ifndef BBMUIConfig_h
#define BBMUIConfig_h

#pragma mark - General Parameters
/*! UI Parameters */
#define kChatBubbleCornerRadius     (8.0f)
#define kChatBubbleOutgoingBGColor  ([UIColor colorWithRed:54.0/255.0 green:137.0/255.0 blue:170.0/255.0 alpha:1.0])

#define kChatViewMessageBatchSize   25      //Number of messages to fetch in each batch
#define kTargetThumbnailSize        200.0f  //Thumbnail target size.  Thumbnails must be <70Kb.


#pragma mark - Layout Files
/*!
 These are the layout files used to configure your UI elements.  You may edit the provided
 layouts or substitute another layout by modifying the constants here.  Note that the layout
 must be properly KVO conforming to avoid runtime exceptions.
 */
#define kMessageInputNib            @"BBMMessageInputView"
#define kMessageStatusViewNib       @"BBMGroupChatMessageDetailView"
#define kMessageStatusCellNib       @"BBMGroupChatMessageDetailCell"
#define kChatViewStoryboard         @"BBMChatView"

/*!
 ChatViewController cell identifiers.
 There are "Incoming" and "Outgoing" variants of each of these.  For the outgoing cell, for example,
 the cell identifier would be BBMTextMessageCellOutgoing
 */
#define kCellIdText                 @"BBMTextMessageCell"
#define kCellIdPhoto                @"BBMPhotoMessageCell"
#define kCellIdQuoted               @"BBMQuotedMessageCell"
#define kCellIdSystem               @"BBMSystemMessageCell"
#define kCellIdLoadMore             @"LoadMoreMessagesTableViewCell"
#define kCellDirectionOutgoing      @"Outgoing"
#define kCellDirectionIncoming      @"Incoming"


#pragma mark - Images
/*!
 These constants define the message status images for the various states
 Include images conforming to these names in your assets bundle or change
 the names here to match your assets.  These assets are all included with
 the RichChat sample.
 */
#define kSentStateImage                   @"sent"
#define kSendingStateImage                @"sending"
#define kFailedStateImage                 @"failed"
#define kPendingStateImage                @"pending"
#define kDeliveredImage                   @"delivered"
#define kDeliveredPartialStateImage       @"delivered_partial"
#define kReadStateImage                   @"read"
#define kReadPartialStateImage            @"read_partial"
#define kRetractStateImage                @"retract_msg"
#define kIncomingReadStateImage           @"grey_pellet"
#define kIncomingUnreadStateImage         @"yellow_pellet"

/*! Default avatar images */
#define kStrDefautAvatarImage             @"default_avatar"


#pragma mark - Strings
/*!
 These are the string used to render various messages in the chat bubbles.  Change these
 here to modify the text that is rendered.
 */
#define kStrJoinedChat              NSLocalizedString(@"%@ joined the chat", nil)
#define kStrLeftChat                NSLocalizedString(@"%@ left the chat", nil)
#define kStrRetractedChat           NSLocalizedString(@"%@ retracted the chat",nil)
#define kStrRetractChatSelf         NSLocalizedString(@"You retracted the chat", nil)
#define kStrTimeExpired             NSLocalizedString(@"Timed message expired.", nil)
#define kStrTimedReceived           NSLocalizedString(@"Timed message received. Tap to view.",nil)
#define kStrTimedDetails            NSLocalizedString(@"Timed message sent. Timer set to %ld seconds.", nil)
#define kStrUnhandledTag            NSLocalizedString(@"Unhandled tag: %@", nil)
#define kStrRetracted               NSLocalizedString(@"Retracted", nil)
#define kStrRetracting              NSLocalizedString(@"Retracting", nil)
#define kStrReceivedPicture         NSLocalizedString(@"Received a picture", nil)
#define kStrSentPicture             NSLocalizedString(@"Sent a picture", nil)
#define kStrEmptyChat               NSLocalizedString(@"Empty Chat", nil)
#define kStrChatAdminChanged        NSLocalizedString(@"The chat administrators have changed", nil)
#define kStrSubjectChanged          NSLocalizedString(@"Subject Changed: %@", nil)
#define kStrYou                     NSLocalizedString(@"You", nil)

/*! Strings for the chat view controller */
#define kStrMediaModeSelect         NSLocalizedString(@"Select a media mode:", nil)
#define kStrMediaModeVoice          NSLocalizedString(@"Voice", nil)
#define kStrMediaModeVideo          NSLocalizedString(@"Video", nil)
#define kStrMediaModeCancel         NSLocalizedString(@"Cancel", nil)

#define kStrTypingOthers            NSLocalizedString(@" %@ other(s)", nil)
#define kStrTypingNotification      NSLocalizedString(@"%@%@ is typing a message", nil)

#define kStrMessageActions          NSLocalizedString(@"Message Actions", nil)
#define kStrDeleteAction            NSLocalizedString(@"Delete", nil)
#define kStrRetractAction           NSLocalizedString(@"Retract", nil)
#define kStrQuoteAction             NSLocalizedString(@"Quote", nil)
#define kStrEditAction              NSLocalizedString(@"Edit", nil)
#define kStrCopyAction              NSLocalizedString(@"Copy", nil)

#define kStrSelectOption            NSLocalizedString(@"Select an option", nil)
#define kStrRetractChat             NSLocalizedString(@"Retract Chat", nil)
#define kStrEndChat                 NSLocalizedString(@"Leave Chat", nil)
#define kStrChatDetails             NSLocalizedString(@"Show Participants",nil)
#define kStrChatParticipants        NSLocalizedString(@"Participants",nil)

#define kStrGenericOK               NSLocalizedString(@"OK", nil)
#define kStrGenericError            NSLocalizedString(@"Error", nil)
#define kStrGenericCancel           NSLocalizedString(@"Cancel", nil)
#define kStrGenericUpdate           NSLocalizedString(@"Update", nil)
#define kStrGenericOpen             NSLocalizedString(@"Open", nil)

#define kStrCallError               NSLocalizedString(@"Unable to place call.", nil)

#define kStrChangeSubject           NSLocalizedString(@"Change Subject", nil)
#define kStrEnterSubjectDesc        NSLocalizedString(@"Enter a subject for this chat", nil)
#define kStrSubject                 NSLocalizedString(@"Subject", nil)

#define kStrTimedMessageTitle       NSLocalizedString(@"Timed Message", nil)
#define kStrLoadMore                NSLocalizedString(@"Load More Messages", nil)

#define kStrUserAdmin               NSLocalizedString(@"User Actions", nil)
#define kStrRemoveAdmin             NSLocalizedString(@"Remove Chat Administrator", nil)
#define kStrAddAdmin                NSLocalizedString(@"Add Chat Administrator", nil)
#define kStrRemoveParticipant       NSLocalizedString(@"Remove Participant", nil)

#define kStrParticipantActive       NSLocalizedString(@"Active", nil)
#define kStrParticipantPending      NSLocalizedString(@"Pending", nil)
#define kStrParticipantLeftChat     NSLocalizedString(@"Left Chat", nil)


#define kStrUserAdmin               NSLocalizedString(@"User Actions", nil)
#define kStrRemoveAdmin             NSLocalizedString(@"Remove Chat Administrator", nil)
#define kStrAddAdmin                NSLocalizedString(@"Add Chat Administrator", nil)
#define kStrRemoveParticipant       NSLocalizedString(@"Remove Participant", nil)

//Call Notifications
#define kStrCallNotification        NSLocalizedString(@"Call from %@", nil)

//File Transfer Messages
#define KStrFilePreviewTitle        NSLocalizedString(@"Shared File", nil)
#define KStrSendingFile             NSLocalizedString(@"Sending",nil)
#define kStrDownloadingFile         NSLocalizedString(@"Downloading", nil)
#define kStrFileUnavailable         NSLocalizedString(@"File Unavailable", nil)

//Message Input Field Strings
#define kStrTimedMessageDuration    NSLocalizedString(@"Select Duration", nil)
#define kStrSeconds                 NSLocalizedString(@"Seconds", nil)
#define kStrQuoting                 NSLocalizedString(@"Quoting: %@", nil)
#define kStrEditing                 NSLocalizedString(@"Editing: %@", nil)

#define kStrPhoto                   NSLocalizedString(@"Photo", nil)
#define kStrTimedMessage            NSLocalizedString(@"Timed Message",nil)

//Password Entry Dialogs
#define kStrChangePassword          NSLocalizedString(@"Change Application Password", nil)
#define kStrPasswordPromptTitle     NSLocalizedString(@"Enter Application Password", nil)
#define kStrPasswordMessage         NSLocalizedString(@"Your application password is required", nil)
#define kStrPasswordMessageInit     NSLocalizedString(@"Create Application Password", nil)
#define kStrPasswordRePromptTitle   NSLocalizedString(@"Invalid Password", nil)
#define kStrPasswordRePromptMsg     NSLocalizedString(@"The entered password is incorrect. Please enter the correct password", nil)
#define kStrPasswordInvalidMsg      NSLocalizedString(@"Password was not a valid password", nil)
#define kStrPasswordsDontMatch      NSLocalizedString(@"The passwords you entered don't match.  Please try again.", nil)
#define kStrSubmitPassword          NSLocalizedString(@"Submit", nil)
#define kStrPassword                NSLocalizedString(@"Password", nil)
#define kStrVerifyPassword          NSLocalizedString(@"Verify Password", nil)
#define kStrPasswordInitialMsg      NSLocalizedString(@"Enter a password.  This will be use to secure your chats.", nil)
#define kStrPasswordForgot          NSLocalizedString(@"Forgot Password", nil)

#define kStrPasswordForgotMsg       NSLocalizedString(@"This will log you out of your account and remove you from all chats.  You may choose a new password the next time you sign in.  Are you sure you want to continue? ", nil)


#endif /* BBMUIConfig_h */
