// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "NSMutableAttributedString+Util.h"

@implementation NSMutableAttributedString (BBMUtil)

+ (NSMutableAttributedString *)attributedString:(NSString *)string
                                       withFont:(UIFont *)font
                                       andColor:(UIColor *)color
{
    NSMutableAttributedString *attributedString = nil;
    if (string) {
        UIFont *fontToApply = font ?: [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        UIColor *colorToApply = color ?: [UIColor blackColor];
        
        NSDictionary *attributes = @{NSFontAttributeName:fontToApply, NSForegroundColorAttributeName:colorToApply};
        attributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:attributes];
    }
    return attributedString;
}

- (NSMutableAttributedString *)applyFont:(UIFont *)font
                             toSubstring:(NSString *)substring
{
    NSMutableAttributedString *attributedString = self;
    
    if (substring.length > 0) {
        NSRange range = [[self string] rangeOfString:substring options:NSCaseInsensitiveSearch];
        if (range.location != NSNotFound) {
            attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:self];
            [attributedString addAttributes:@{NSFontAttributeName:font} range:range];
        }
    }
    
    return attributedString;
}

@end
