// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

typedef void  (^ImageProviderLoadedCallback)(UIImage *image, NSString *source, NSError *error);

/*!
 @details
 Cells require the ability to load avatar images.  An image provider instance must be set on
 the BBMChatViewController.  Cells will call loadImage:callback: if an image cannot be found
 in the shared cache.  The image provider instance should be implemented to de-duplicate
 requests for the same URL and to cache results to disk if applicable.  loadImage:callback
 will be called often and repeatedly.

 A simple default implementation for loading remote URLs is provided with BBMImageLoader.  This
 implementation uses built in URL caching.  Please review it for suitability in your application.
 */
@protocol BBMImageProvider
@required
- (void)loadImage:(NSString *)url callback:(ImageProviderLoadedCallback)callback;
@end
