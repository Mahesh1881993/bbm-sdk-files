// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>
#import "BBMImageProvider.h"

/*!
 @details
 This class provides a very simplistic loader for images based on NSURLSession.  It is suitable for loading remote
 image URLs that require no special URL handling.  Images are cached using the default protocol policy to the
 application cache directory.
 */
@interface BBMImageLoader : NSObject <BBMImageProvider>

+ (instancetype)sharedImageLoader;

/*!
 @details
 Load an image from a remote URL.  It is not recommended that this be used for local file system
 URLs.
 @param url The remote URL of the image
 @param callback Called when the image load is complete.  The block is not called if the load fails.
 */
- (void)loadImage:(NSString *)url callback:(ImageProviderLoadedCallback)callback;

@end
