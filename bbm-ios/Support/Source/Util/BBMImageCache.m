// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMImageCache.h"

#define kLogInfo NO

#define kCacheSize        50*1024*1024
#define kMaxCacheCount    200     //Limit the cache to 200 total items


@interface BBMImageCache () <NSCacheDelegate> {
    NSUInteger _calculatedTotalCost;
    
    //Least Recently Used Linked List... Orders the cache keys by
    //most recent access.  No need for an actual linked-list,
    //NSMutableArray is already using one internally
    NSMutableArray *_lruList;
    
    //To lock access to the lruLinked list to ensure we don't modify
    //it while doing other cache operations.
    NSRecursiveLock *_lruLock;
}

@property (strong) NSCache *cache;

@end


static BBMImageCache *sharedInstance;

@implementation BBMImageCache

+ (instancetype)sharedImageCache
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] _initPrivate];
        sharedInstance.cache.countLimit = 100;
        sharedInstance.cache.totalCostLimit = kCacheSize;
    });
    return sharedInstance;
}


- (id)init
{
    NSAssert(NO, @"Use sharedImageCache");
    self = nil;
    return nil;
}


- (id)_initPrivate
{
    if( (self = [super init])) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(memoryWarning:)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
        _lruList = [NSMutableArray array];
        self.cache = [[NSCache alloc] init];
        self.cache.delegate = self;
        _lruLock = [NSRecursiveLock new];
    }
    return self;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeAllImages];
}


#pragma mark - External

- (void)cacheImage:(UIImage *)img forKey:(NSString *)key
{
    //"isKind" is expensive.  Instead, we'll just assert here
    NSAssert(self.cache.totalCostLimit != 0, @"You must set the total cost limit");
    NSAssert(self.cache.countLimit !=0, @"You must set a non zero count limit");

    if(nil == key){
        NSLog(@"Attempted to cache image with nil key");
        return;
    }
    
    NSInteger cost = CGImageGetHeight(img.CGImage) * CGImageGetBytesPerRow(img.CGImage);
    [self.cache setObject:img forKey:key cost:cost];
    _calculatedTotalCost += cost;
    
    [_lruLock lock]; {
        //Add the key to the tail of LRU list
        [_lruList addObject:key];
    } [_lruLock unlock];
}


- (UIImage *)imageForKey:(NSString *)key
{
    key = [key copy];
    id retVal = [self.cache objectForKey:key];
    if(retVal && key) {
        //Move the key to the tail of the LRU list
        [_lruLock lock]; {
            [_lruList removeObject:key];
            [_lruList addObject:key];
        } [_lruLock unlock];
    }
    return retVal;
}


- (void)removeImageForKey:(NSString *)key
{
    if(key) {
        [self.cache removeObjectForKey:key];
    }
}


- (void)removeAllImages
{
    self.cache.delegate = nil;
    
    [self.cache removeAllObjects];
    
    [_lruLock lock]; {
        [_lruList removeAllObjects];
    } [_lruLock unlock];
    
    _calculatedTotalCost = 0;
    
    self.cache.delegate = self;
}


#pragma mark - Internal

- (void)memoryWarning:(NSNotification *)n
{
    //We don't want to get the eviction delegate callback during
    //this process...
    self.cache.delegate = nil;
    NSUInteger currentCost = _calculatedTotalCost;
    
    //Target cost is half the current total cache cost
    NSUInteger targetCost = currentCost * .5f;

    [_lruLock lock];
    while(_lruList.count && _calculatedTotalCost > targetCost) {
        id key = _lruList[0];
        id obj = [self.cache objectForKey:key];
        if(obj) {
            [self.cache removeObjectForKey:key];
            UIImage *img = (UIImage *)obj;
            NSUInteger size  = CGImageGetHeight(img.CGImage) * CGImageGetBytesPerRow(img.CGImage);
            _calculatedTotalCost -= size;
        }
        [_lruList removeObjectAtIndex:0];
    }
    [_lruLock unlock];
    
    self.cache.delegate = self;
}


#pragma mark - NSCacheDelegate

- (void)cache:(NSCache *)cache willEvictObject:(id)obj
{
    UIImage *img = obj;
    NSUInteger size  = CGImageGetHeight(img.CGImage) * CGImageGetBytesPerRow(img.CGImage);
    _calculatedTotalCost -= size;
}

@end
