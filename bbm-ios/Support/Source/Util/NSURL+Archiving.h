// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

@interface NSURL (BBMArchiving)

/*!
 Archives the contents of the source URL to the destURL.  The URL we're archiving
 must be a directory.
 @param destURL File URL.  Must not already exist
 @param error Error pointer 
 */
 - (void)archiveContentsToURL:(NSURL *)destURL error:(__autoreleasing NSError **)error;

@end



