// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

typedef void (^ArchiveCallback)(NSURL *url, NSError *error);

@interface BBMUtilities : NSObject

/*! @brief YES if the application is currently in the foreground */
+ (BOOL)isApplicationInForeground;

/*! @brief String representation of the given \a duration (in seconds). ie:  100 -> 1:40 */
+ (NSString *)durationAsString:(NSTimeInterval)duration;

/*!
 @details
 Archives the current logs to the library directory and returns the URL to the archive file.
 You can use the resulting file for bug reporting, etc.
 */
+ (void)archiveLogs:(ArchiveCallback)callback;

@end
