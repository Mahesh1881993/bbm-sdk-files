// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "NSURL+Archiving.h"

@implementation NSURL (BBMArchiving)

- (void)archiveContentsToURL:(NSURL *)destURL error:(NSError **)retError;
{
    NSFileManager *fm = [NSFileManager defaultManager];

    if(!(destURL.fileURL && ![fm fileExistsAtPath:destURL.path])) {
        NSLog(@"Error: File already exists at the destination path");
        *retError = [NSError errorWithDomain:@"com.blackberry.NSURL.archiveFailure.badDest" code:0 userInfo:nil];
        return;
    }

    BOOL isDir = NO;
    if(!(self.fileURL &&
        [fm fileExistsAtPath:self.path isDirectory:&isDir] &&
        isDir == YES))
    {
        NSLog(@"Only archiving of entire directories is supported");
        *retError = [NSError errorWithDomain:@"com.blackberry.NSURL.archiveFailure.badSource" code:0 userInfo:nil];
        return;
    }


    NSFileCoordinator *coord = [[NSFileCoordinator alloc] init];
    NSError *error;

    void (^accessor)(NSURL *) = ^(NSURL *inFile) {
        NSError *error;
        [fm copyItemAtURL:inFile toURL:destURL error:&error];
        if(error) {
            *retError = [NSError errorWithDomain:@"com.blackberry.NSURL.archiveFailure.errorDuringCopy" code:0 userInfo:nil];
        }
    };

    [coord coordinateReadingItemAtURL:self
                              options:NSFileCoordinatorReadingForUploading
                                error:&error
                           byAccessor:accessor];
}

@end


