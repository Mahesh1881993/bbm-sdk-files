// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "NSString+Base64.h"

#pragma mark - NSString BBMUtility

@implementation NSString (BBMUtility)

- (NSData *)utf8Bytes
{
    return [self dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)urlEncoded
{
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
}

@end


#pragma mark - NSString Base64

@implementation NSString (BBMBase64)

- (NSData *)base64DecodedData
{
    return [NSData dataWithBase64EncodedURLSafeString:self];
}

- (NSString*)base64URLSafeEncodedString
{
    NSData *strData = [self dataUsingEncoding:NSUTF8StringEncoding];
    return [strData base64URLSafeEncodedString];
}

- (NSString *)decodedUrlSafeBase64String
{
    NSData *strData = [NSData dataWithBase64EncodedURLSafeString:self];
    return [[NSString alloc] initWithData:strData encoding:NSUTF8StringEncoding];
}


@end


#pragma mark - NSData BBMUrlSafeBase64


@implementation NSData (BBMUrlSafeBase64)

- (NSString *)utf8String
{
    return [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
}

- (NSString *)base64URLSafeEncodedString
{
    NSMutableString *encodedStr = [[self base64EncodedStringWithOptions:0] mutableCopy];

    [encodedStr replaceOccurrencesOfString:@"=" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, encodedStr.length)];
    [encodedStr replaceOccurrencesOfString:@"+" withString:@"-" options:NSLiteralSearch range:NSMakeRange(0, encodedStr.length)];
    [encodedStr replaceOccurrencesOfString:@"/" withString:@"_" options:NSLiteralSearch range:NSMakeRange(0, encodedStr.length)];

    return encodedStr;
}

+ (NSData *)dataWithBase64EncodedURLSafeString:(NSString *)string
{
    NSMutableString *encodedStr = [string mutableCopy];
    [encodedStr replaceOccurrencesOfString:@"-" withString:@"+" options:NSLiteralSearch range:NSMakeRange(0, encodedStr.length)];
    [encodedStr replaceOccurrencesOfString:@"_" withString:@"/" options:NSLiteralSearch range:NSMakeRange(0, encodedStr.length)];

    //Pad the output until the size is a multiple of 4
    while(encodedStr.length % 4) {
        [encodedStr appendString:@"="];
    }

    //And now Apple can decode it....
    return [[NSData alloc] initWithBase64EncodedString:encodedStr options:0];
}


@end
