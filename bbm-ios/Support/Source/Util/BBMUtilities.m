// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMUtilities.h"
#import <UIKit/UIKit.h>
#import "NSURL+Archiving.h"
#import <BBMEnterprise/BBMEnterprise.h>

@implementation BBMUtilities

+ (BOOL)isApplicationInForeground
{
    return [UIApplication sharedApplication].applicationState != UIApplicationStateBackground;
}

+ (NSString *)durationAsString:(NSTimeInterval)duration
{
    NSNumberFormatter *timeFormatter = [[NSNumberFormatter alloc] init];
    timeFormatter.locale = [NSLocale currentLocale];
    timeFormatter.minimumIntegerDigits = 2;
    timeFormatter.maximumIntegerDigits = 2;
    
    NSUInteger hour = 0;
    NSTimeInterval remainingDuration = duration;
    if (remainingDuration > 3600) {
        hour = floor(remainingDuration/3600);
        remainingDuration = (remainingDuration-(hour*3600));
        NSUInteger minutes = floor(remainingDuration/60);
        NSUInteger seconds = trunc(remainingDuration - minutes * 60);
        return [NSString stringWithFormat:@"%lu:%@:%@",
                (unsigned long)hour,
                [timeFormatter stringFromNumber:@(minutes)],
                [timeFormatter stringFromNumber:@(seconds)]];
    }
    
    NSUInteger minutes = floor(remainingDuration/60);
    if (minutes > 0) {
        NSUInteger seconds = trunc(remainingDuration - minutes * 60);
        return [NSString stringWithFormat:@"%lu:%@",
                (unsigned long)minutes,
                [timeFormatter stringFromNumber:@(seconds)]];
    }
    NSUInteger seconds = trunc(remainingDuration - minutes * 60);
    return [NSString stringWithFormat:@"%d:%@",
            0,
            [timeFormatter stringFromNumber:@(seconds)]];
}

+ (void)archiveLogs:(ArchiveCallback)callback
{
    NSString *logsDir = [[BBMEnterpriseService service] logDirectory];
    NSURL *logsURL = [NSURL fileURLWithPath:logsDir];

    NSString *libraryFolder = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    if (paths) {
        libraryFolder = [paths objectAtIndex:0];
    }
    NSString *archiveFile = [libraryFolder stringByAppendingPathComponent:@"bbme_sdk_logs.zip"];

    NSFileManager *fm = [NSFileManager defaultManager];
    if([fm fileExistsAtPath:archiveFile]) {
        [fm removeItemAtPath:archiveFile error:nil];
    }

    NSError *error;
    NSURL *archiveURL = [NSURL fileURLWithPath:archiveFile];
    [logsURL archiveContentsToURL:archiveURL error:&error];
    if(error) {
        NSLog(@"Error Archiving Logs: %@", error.localizedDescription);
        if(callback)callback(nil, error);
        return;
    }
    if(callback)callback(archiveURL, error);
}


@end
