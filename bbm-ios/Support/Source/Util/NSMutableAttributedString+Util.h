// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSMutableAttributedString (BBMUtil)

/*!
 @details
 Creates a new mutable attributed string with the given font and color.
 */
+ (NSMutableAttributedString *)attributedString:(NSString *)string
                                       withFont:(UIFont *)font
                                       andColor:(UIColor *)color;

/*!
 @details
 Returns a new mutable attributed string based on the receiver with the given font applied to the given substring.
 */
- (NSMutableAttributedString *)applyFont:(UIFont *)font
                             toSubstring:(NSString *)substring;

@end
