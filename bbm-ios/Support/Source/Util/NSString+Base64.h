// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

@interface NSString (BBMUtility)

/*!
 @details
 Returns the string as a UTF8 encoded byte array
 */
- (NSData *)utf8Bytes;

/*!
 @details
 Returns the string with URL-safe percent encodings
 */
- (NSString *)urlEncoded;

@end

@interface NSString (BBMBase64)

/*!
 @details
 Creates an unpadded URL-safe base64 encoded string from another string
 Replaces: + with -, / with _ and removes all padding
 */
- (NSString*)base64URLSafeEncodedString;

/*!
 Returns the decoded string from a URLSafeBase64 encoded string
 */
- (NSString *)decodedUrlSafeBase64String;

/*!
 Treats the string as URL safe base64 encoded and returns the decoded data
 */
- (NSData *)base64DecodedData;

@end

@interface NSData (BBMUrlSafeBase64)

/*!
 @details
 Return the data object as a UTF8 encoded string
 */
- (NSString *)utf8String;

/*!
 @details
 Returns an unpadded URL-safe Base 64 encoded string from the given data
 @return A URL-safe base64 string representing the NSData
 */
- (NSString *)base64URLSafeEncodedString;

/*!
 @details
 Decodes a given padded or unpadded URL-Safe Base-64 encoded string from the given string
 @param string A URL-safe base64 string
 @return NSData derived from the
 */
+ (NSData *)dataWithBase64EncodedURLSafeString:(NSString *)string;

@end
