// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMImageLoader.h"

#define kMaximumConcurrentImageLoads 6      //Load 6 images at once
#define kDiskCacheSize 1024*1024*40         //40Mb
#define kTimeoutInterval 30                 //Timeout after 30 seconds

@interface BBMImageLoader ()
@property (nonatomic, strong) NSURLSessionConfiguration *config;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSMutableDictionary *callbacksForUrl;
@end

@implementation BBMImageLoader

+ (instancetype)sharedImageLoader
{
    static BBMImageLoader *s_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_sharedInstance = [[BBMImageLoader alloc] init];
    });
    return s_sharedInstance;
}

- (id)init
{
    if( (self = [super init] ) )
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cacheDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImageCache"];
        NSURLCache *urlCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:kDiskCacheSize diskPath:cacheDirectory];

        self.config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.config.URLCache= urlCache;
        self.config.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;

        //Use a relatively short timeout to prevent the queue from getting blocked.
        self.config.timeoutIntervalForRequest = kTimeoutInterval;

        self.queue  = [[NSOperationQueue alloc] init];
        self.queue.maxConcurrentOperationCount = kMaximumConcurrentImageLoads;

        self.callbacksForUrl = [NSMutableDictionary dictionary];
        self.session = [NSURLSession sessionWithConfiguration:self.config];
    }
    return self;
}

- (void)loadImage:(NSString *)url callback:(ImageProviderLoadedCallback)callback
{
    if(nil == url) {
        return;
    }

    typeof(self) __weak weakSelf = self;
    void (^loadBlock)(void) = ^{
        NSURL *urlToLoad = [NSURL URLWithString:url];
        [[weakSelf.session dataTaskWithURL:urlToLoad completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if(data && nil == error) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    //Create an image from the data.
                    UIImage *img =  [UIImage imageWithData:data];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSMutableArray *callbacks = weakSelf.callbacksForUrl[url];
                        for(ImageProviderLoadedCallback callback in callbacks) {
                            callback(img, url,nil);
                        }
                        [self.callbacksForUrl removeObjectForKey:url];
                    });
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableArray *callbacks = weakSelf.callbacksForUrl[url];
                    for(ImageProviderLoadedCallback callback in callbacks) {
                        callback(nil, url, error);
                    }
                    [weakSelf.callbacksForUrl removeObjectForKey:url];
                });
            }
        }] resume];
    };

    //Deduplicate requests for the same URL... If a request is currently in progress, use the existing
    //request and add our callback to the list of callbacks
    NSMutableArray *callbacks = self.callbacksForUrl[url];
    if(callbacks) {
        [callbacks addObject:callback];
    }else{
        callbacks = [NSMutableArray arrayWithObject:callback];
        self.callbacksForUrl[url] = callbacks;
    }

    //If there were no other callbacks in the queue, kick of the NSURLSession
    //If there are other callbacks, then there's a session already queued or running
    if(callbacks.count == 1) {
        [self.queue addOperationWithBlock:loadBlock];
    }
}


@end
