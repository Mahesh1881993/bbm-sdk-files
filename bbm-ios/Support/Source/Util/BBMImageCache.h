// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <UIKit/UIKit.h>

/*!
 @abstract
 In-memory LRU cache for optimizing image handling.  This will keep a specified number
 of UIImage objects in RAM, purging the least-recently-used as needed.  The cache size is
 configurable in BBMUIWidgetsConfig
 */
@interface BBMImageCache : NSObject 

/*!
 @details
 Returns the shared cache.
 */
+ (instancetype)sharedImageCache;

/*!
 @details
 Cache an image
 @param img The image to cache
 @param key The key to use to reference the image. 
 */
- (void)cacheImage:(UIImage *)img forKey:(NSString *)key;

/*!
 @details
 Returns the image for the given key.  Nil if the image does not exist
 @param key The key to use to reference the image.
 @return The cached image or nil
 */
- (UIImage *)imageForKey:(NSString *)key;

/*!
 @details
 Removes the image with the given key.
 @param key The key to use to reference the image. 
 */
- (void)removeImageForKey:(NSString *)key;

/*!
 @details
 Clears the cache
 */
- (void)removeAllImages;


@end
