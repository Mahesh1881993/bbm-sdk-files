// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import "BBMKeyStorageProvider.h"

@class BBMUserPasswordManager;
@class BBMUserManager;

typedef void (^ChatKeySyncCallback)(BBMChat *chat, KeySyncResult result);
typedef void (^UserKeySyncCallback)(NSString *regId, KeySyncResult result);

//Failure handler blocks types for import failures.
typedef void (^ChatKeyImportFailureHandler)(NSArray *mailboxIds);
typedef void (^UserKeyImportFailureHandler)(NSArray *regIds);
typedef void (^ProfileKeyImportFailureHandler)(void);

typedef void(^UserPasswordRequiredCallback)(void (^)(NSString *password, NSString *regId));
typedef void (^PasswordUpdateCallback)(BOOL);

/*!
 @enum
 State of the management keys.  Management keys are stored remotely and encrypted with the user
 password.  These must be
 */
typedef enum {
    kBBMMgmtKeyStateUnknown,            ///< Initial state
    kBBMMgmtKeysPending,                ///< Keys have been requested.  Values Pending
    kBBMMgmtKeysReady,                  ///< Management keys are valid and ready
    kBBMMgmtKeyPasswordRequired,        ///< Password required
    kBBMMgmtKeyInitialPasswordRequired, ///< Initial password required
    kBBMMgmtKeyIncorrectPassword,       ///< The supplied user password was incorrect.
    kBBMMgmtKeyRemoteWriteFailure,      ///< Write failure when uploading the management keys.
    kBBMMgmtKeyRemoteReadFailure,       ///< Read failure when downloading the management keys.
    kBBMMgmtKeyCorruptedKeyData,        ///< The management keys are corrupt.  Fatal failure.
    kBBMMgmtKeyReset                    ///< Called after resetting the management keys/password
}BBMManagementKeyState;


/*!
 @abstract
 BBMKeyManager automates the synchronization of keys between an instance of an id<BBMKeyStorageProvider>
 and the BBM service.
 */
@interface BBMKeyManager : NSObject <BBMIncomingMessageListener>

/*!
 @details
 Initialize the BBMKeyManager with a key storage provider.
 @param provider The key storage provider to use
 @param userManager A user manager instance.  The user manager maps regId->oAuth ID which is neccessary
                    for syncronizing user keys
 */
- (id)initWithKeyStorageProvider:(id<BBMKeyStorageProvider>)provider
                     userManager:(BBMUserManager *)userManager;

/*! @brief The SDK service domain.  Used for key generation.  This must be specified. */
@property (nonatomic, strong) NSString *sdkServiceDomain;

/*!
 Called automatically on logout to reset the internal state.  You can also recreate the key manager
 if desired
 */
- (void)resetKeyManager;

/*!
 @details
 Call this when the key manager is no longer need to avoid showing the password screen.
 @note Not calling this will prevent an instance from getting deallocated.
 */
- (void)stopKeyManager;


/*!
 @details
 Updates the users password and all associated encrypted private keys in external storage
 @param password The new password
 @param regId The users RegId
 @param callback Invoked when the password update has completed or failed
 */
- (void)changePassword:(NSString *)password
                 regId:(NSString *)regId
              callback:(PasswordUpdateCallback)callback;


/*!
 @details
 By default we will internally prompt the user for their password.  See BBMUserPasswordManager.  If
 you wish to control this yourself, call this method to disable the internal prompt.
 */
- (void)disableAutomaticPasswordManagement;


/*! @brief The internal password manager. nil if disableAutomaticPasswordManagement has been called */
@property (nonatomic, readonly) BBMUserPasswordManager *passwordManager;


/*!
 @details
 Resets the users password by removing all public and private keys and logging out all the users
 other endpoints
 */
- (void)resetKeyManagementPassword;


/*!
 @details
 Returns the current state of the management keys.  A single attempt on login will be made
 to sync the management keys.  On an existing endpoint, these will likely be in local storage.
 For new endpoints, these must be loaded from the database.  Failure indicates that a retry
 needs to be attempted via <method> once the error in the keyStorageProvider is resolved.
 @note Observable
 */
@property (nonatomic, readonly) BBMManagementKeyState managementKeyState;


/*!
 @details
 If the management key state is not kBBMMgmtKeysReady, this method should be called to re-attempt
 synchronization when appropriate.  A password must be supplied if the current managementKeyState
 is kBBMMgmtKeyPasswordRequired kBBMMgmtKeyPasswordIncorrect.  This can also be use to retry
 key synchronization in the case of a read or write failure.  A password should be supplied in these
 cases as well.
 @param password The password from which to derive the root keys.  This must match the password
                 used to encrypt the management keys or kBBMMgmtKeyPasswordIncorrect
                 will result.
 */
- (void)synchronizeManagementKeys:(NSString *)password;


//If set, these failure handlers are called when we encounter an error internally when importing
//keys provided by the KeyStorageProvider.  This can happen due to bad data, write failures at
//the file system level, etc.
//In most cases, you likely want to retry once then deal with the failure accordingly (re-export,
//end the chat, etc)
@property (nonatomic, copy) ChatKeyImportFailureHandler chatKeyImportFailureHandler;
@property (nonatomic, copy) UserKeyImportFailureHandler userKeyImportFailureHandler;
@property (nonatomic, copy) ProfileKeyImportFailureHandler profileKeyImportFailureHandler;


/*!
 @details
 Automatically monitor chats and chat participants for changes in their key state and automatically
 sync chat and user keys with the BBMKeyStorageProvider.  This will only sync the keys for chats
 and users who's keys are in the "import" state.
 */
- (void)startAutomaticKeySync;


/*!
 @details
 Stop monitoring chats and participants for changes in their key state
 */
 - (void)stopAutomaticKeySync;


/*!
 @details
 Forces an immediate sync of all chat and user keys with the BBMKeyStorageProvider.  This includes
 keys for users who are already in the synced state.  Use this with some caution as it will request
 keys for all chats, and all users currently in the model.
 */
- (void)syncAllChatAndUserKeys;


/*!
 @details
 Synchronize the user keys for a \a user.  This method does not check
 the user keyState.  The keys present in the keyStorageProvider will be applied
 regardless of the keyState.
 @param user The BBMUser for whom you wish to synchronize the keys
 @param callback Callback with the result of the key sync operation
 */
- (void)syncUserKey:(BBMUser*)user callback:(UserKeySyncCallback)callback;

/*!
 @details
 Equivalent to [self syncUserKey:user callback:nil]
 */
- (void)syncUserKey:(BBMUser*)user;


/*!
 @details
 Loads the user key from the storage provider (if it exists), adds it to the BBM Enterprise Service
 and calls back once the user object exists and the key is properly synced.  This is useful for
 handling incoming calls or outgoing calls without first creating a chat with the user.
 @param regId The user's regId as a string
 @param callback A optional callback to invoke once the user object exists
 */
- (void)readUserKey:(NSString *)regId callback:(UserKeySyncCallback)callback;


/*!
 @details
 Synchronize the chat keys for a particular \a chat. It is safe to call this at any time.  This
 will automatically check the key states in BBM and upload or download the chat keys from the
 external \a provider as required
 @param chat the chat for which you wish to synchronize the keys
 @param callback called on success or failure to sync the chat keys
 */
- (void)syncChatKey:(BBMChat*)chat callback:(ChatKeySyncCallback)callback;


/*!
 @details
 Equivalent to [self syncChatKey:myChat callback:nil]
 */
- (void)syncChatKey:(BBMChat*)chat;


/*!
 @details
 Synchronize the profile keys for the local user.  This will synchronize both the private and public
 keys for the current user.  It is safe to call this at any time.
 @param uid Implementation specific UID for the user if needed by your \a provider.  For example, the
            provided Firebase implementation uses the local user's Firebase UID and some schema
            rules to ensure only the user that writes a private key can read that key.
 @param regId Registration ID for the user
 @param callback A block to be called on on success or failure of the operation
 */
- (void)syncProfileKeysForLocalUser:(NSString*)uid
                              regId:(NSString*)regId
                           callback:(UserKeySyncCallback)callback;


/*!
 @details
 Equivalent to [self syncProfileKeysForLocalUser:userID regId:regId callback:nil]
 */
- (void)syncProfileKeysForLocalUser:(NSString*)userUID
                              regId:(NSString*)regId;

/*!
 @details
 Retries synchronization of the user's profile keys.  One of the above methods must have been
 called first with a valid uid and regid
 */
- (void)retrySyncProfileKeys;


/*!
 @details
 Requests and a new set of profile keys and uploads them via the KeyStoreProvider.
 @note Requesting new profile keys will update the current users encryption keys which will mean
       all of their current chat keys will be invalidated.  This should only typically be called if
       the profile keys fail to update (via the profileKeyImportFailureHandler)
 */
- (void)requestProfileKeys;

@end

