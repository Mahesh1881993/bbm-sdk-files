// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

/*!
 @details
 Represents a BBM key pair (encryption and signing key).  Supports both raw and base64 encoded as
 strings well as encrypted keys.
 */
@interface BBMKeyPair : NSObject

/*!
 Create a key pair with raw data. If encoded is set to true, the signingKey and encryptionKey
 properties will return the base64 encoded strings.  If encoded is set to false, those properties
 will return the string representation of the raw data which will probably not be suitable.
 Use encoded, for example when representing  keys generated from raw random data.  For keys
 that are already base64 encoded and can be represented as strings, encoding is not neccesary.
 @param encryptionKey The encryptionKey
 @param signingKey The signingKey
 @param encode YES if the keys should be base64 encoded.  NO if the keys are already base64.
 */
+ (instancetype)keyPairWithEncryptionKeyData:(NSData *)encryptionKey
                              signingKeyData:(NSData *)signingKey
                                      encode:(BOOL)encode;

/*!
 Create a key pair using a JSON dictionary.  This will take the dictionary, serialize it to JSON
 data then base64 encode that to store as the signingKey and/or encryptionKey.  This method doesn't
 care what dictionary you give it but it is typically used to create key pairs from BBMCipherText
 objects.
 @see BBMCipherText
 */
+ (instancetype)keyPairWithEncryptionKeyJson:(NSDictionary *)encryptionKey
                              signingKeyJson:(NSDictionary *)signingKey;

/* @brief Returns YES if the NSString key values are base64 encoded data */
@property (nonatomic, readonly) BOOL encoded;

/* @brief YES if the key was constructed with encrypted JSON. */
@property (nonatomic, readonly) BOOL encrypted;

@property (nonatomic, readonly) NSString *signingKey;       //Base64 Encoded if encoded == YES
@property (nonatomic, readonly) NSString *encryptionKey;    //Base64 Encoded if encoded == YES

@property (nonatomic, readonly) NSData *encryptionKeyData;  //Raw
@property (nonatomic, readonly) NSData *signingKeyData;     //Raw

@property (nonatomic, readonly) NSDictionary *encryptionKeyJson; //BBMCipherText JSON Object
@property (nonatomic, readonly) NSDictionary *signingKeyJson;    //BBMCipherText JSON Object

/*!
 Serialize the key pair to a JSON compliant dictionary
 */
- (NSDictionary *)toJson;
- (NSString *)toJsonString;

/*!
 Deserialze the key pair from a JSON String
 */
+ (BBMKeyPair *)keyPairWithJson:(NSDictionary *)json encrypted:(BOOL)encrypted;
+ (BBMKeyPair *)keyPairWithJson:(NSDictionary *)json encrypted:(BOOL)encrypted encoded:(BOOL)encoded;

@end




