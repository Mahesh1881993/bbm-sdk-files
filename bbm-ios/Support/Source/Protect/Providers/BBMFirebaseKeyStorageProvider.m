// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMFirebaseKeyStorageProvider.h"

#import <BBMEnterprise/BBMEnterprise.h>

#import "Firebase.h"
#import "NSString+Base64.h"
#import "BBMKeyPair.h"
#import "BBMProfileKeys.h"
#import "BBMSecUtil.h"

//Firebase path names
static NSString *const kDBKeyStorePath                  = @"keyStore/%@";
static NSString *const kDBPrivateKeyStorePath           = @"keyStore/%@/private";
static NSString *const kDBPublicKeyStorePath            = @"keyStore/%@/public";
static NSString *const kDBPublicKeyStoreMailboxPath     = @"keyStore/%@/private/mailboxes/%@";

static NSString *const kDBPrivateKeyStoreProfileKeysKey = @"profile";
static NSString *const kDBPrivateKeyStoreMgmtKeysKey    = @"manage";
static NSString *const kDBKeyStorePublicKeysKey         = @"public";
static NSString *const kDBKeyStorePrivateKeysKey        = @"private";

@implementation BBMFirebaseKeyStorageProvider

@synthesize localUserUid;
@synthesize tokenManager;

+ (id<BBMKeyStorageProvider>)instance
{
    return [[self alloc] init];
}

- (void)writeProfileKeys:(BBMProfileKeys *)keys
          managementKeys:(BBMKeyPair *)managementKeys
                callback:(KeyWriteCallback)callback
{
    NSString *path = [NSString stringWithFormat:kDBKeyStorePath, self.localUserUid];
    FIRDatabaseReference *privateKeyDb = [[FIRDatabase database] referenceWithPath:path];

    NSDictionary *privateKeys = @{kDBPrivateKeyStoreMgmtKeysKey : managementKeys.toJson,
                                  kDBPrivateKeyStoreProfileKeysKey : keys.privateKeyPair.toJson};

    NSDictionary *keyStoreJson = @{kDBKeyStorePublicKeysKey  : keys.publicKeyPair.toJson,
                                   kDBKeyStorePrivateKeysKey : privateKeys};

    [privateKeyDb updateChildValues:keyStoreJson withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        callback(nil == error);
    }];

}


- (void)writeChatKey:(BBMCipherText *)key
          forMailbox:(NSString *)mailboxId
            callback:(ChatKeyWriteCallback)callback
{
    NSDictionary *encodedKey = [key toDictionary];
    FIRDatabaseReference *chatKeyDBRef = [self getChatKeyDBReferenceForMailboxId:mailboxId];
    [chatKeyDBRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot)
     {
         if (snapshot.exists) {
             NSDictionary *existingKey = snapshot.value;
             if ([existingKey isEqualToDictionary:encodedKey]) {
                 NSLog(@"Received chatKey from bbmcore but identical key exists in Firebase, ignoring.");
                 callback(mailboxId, YES);
                 return;
             }
         }
         NSLog(@"Writing chatKey received from bbmcore to Firebase.");
         [chatKeyDBRef setValue:encodedKey];
         callback(mailboxId, YES);
     }];
}


- (void)readChatKey:(NSString *)mailboxId
           callback:(ChatKeyReadCallback)callback
{
    FIRDatabaseReference *chatKeyDBRef = [self getChatKeyDBReferenceForMailboxId:mailboxId];
    [chatKeyDBRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot)
     {
         if (snapshot.exists == NO) {
             //Specific to Firebase - this implies only that we could not read the data, not that
             //it doesn't exist.
             callback(mailboxId, nil, kKeySyncResultNoKey);
             return;
         }
         //Private keys will always be JSON cipherText objects ({payload:<>, nonce:<>, mac:<> }
         NSDictionary *key = snapshot.value;
         BBMCipherText *keyCT = [BBMCipherText withJSONDictionary:key];
         callback(mailboxId, keyCT, kKeySyncResultSuccess);
     }];
}


- (void)removeChatKey:(NSString *)mailboxId
             callback:(ChatKeyWriteCallback)callback
{
    FIRDatabaseReference *chatKeyDBRef = [self getChatKeyDBReferenceForMailboxId:mailboxId];
    [chatKeyDBRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        callback(mailboxId, error == nil);
    }];
}


- (void)readPrivateKeys:(KeyReadCallback)callback
{
    NSString *path = [NSString stringWithFormat:kDBPrivateKeyStorePath, self.localUserUid];
    FIRDatabaseReference *privateKeyDbRef = [[FIRDatabase database] referenceWithPath:path];
    [privateKeyDbRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot)
     {
         if (snapshot.exists) {
             NSDictionary *privateKeyDict = snapshot.value;
             NSDictionary *keyJson = [privateKeyDict objectForKey:kDBPrivateKeyStoreProfileKeysKey];
             if(keyJson) {
                 BBMKeyPair *keys = [BBMKeyPair keyPairWithJson:keyJson encrypted:YES];
                 callback(keys, kKeySyncResultSuccess);
             }else{
                 callback(nil , kKeySyncResultNoKey);

             }
         } else {
             callback(nil , kKeySyncResultNoKey);
         }
     }];
}


- (void)readPublicKeys:(NSString *)uid callback:(KeyReadCallback)callback;
{
    NSString *path = [NSString stringWithFormat:kDBPublicKeyStorePath, uid];
    FIRDatabaseReference *publicKeyDBReference = [[FIRDatabase database] referenceWithPath:path];
    [publicKeyDBReference observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot)
     {
         if (snapshot.exists) {
             NSDictionary *publicKeyData = snapshot.value;
             BBMKeyPair *keys = [BBMKeyPair keyPairWithJson:publicKeyData encrypted:NO encoded:NO];
             callback(keys, kKeySyncResultSuccess);
         }else{
             callback(nil, kKeySyncResultNoKey);
         }
     }];

}


- (FIRDatabaseReference*)getChatKeyDBReferenceForMailboxId:(NSString*)mailboxId
{
    NSString *encodedMailboxId = [mailboxId base64URLSafeEncodedString];
    NSString *chatKeyPath = [NSString stringWithFormat: kDBPublicKeyStoreMailboxPath, self.localUserUid, encodedMailboxId];
    return [[FIRDatabase database] referenceWithPath:chatKeyPath];
}


#pragma mark - Management Keys

- (void)readManagementKeys:(KeyReadCallback)callback
{
    NSString *localUserPrivateKeyPath = [NSString stringWithFormat:kDBPrivateKeyStorePath, self.localUserUid];
    FIRDatabaseReference *privateKeyDbRef = [[FIRDatabase database] referenceWithPath:localUserPrivateKeyPath];
    [privateKeyDbRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot)
     {
         if(snapshot.exists) {
             NSDictionary *privateKeyData = snapshot.value;
             NSDictionary *managmentKeys = privateKeyData[kDBPrivateKeyStoreMgmtKeysKey];
             if(managmentKeys) {
                 BBMKeyPair *keys = [BBMKeyPair keyPairWithJson:managmentKeys encrypted:YES];
                 callback(keys, kKeySyncResultSuccess);
             } else {
                 callback(nil , kKeySyncResultNoKey);
             }
         }else {
             callback(nil , kKeySyncResultNoKey);
         }
     }];
}


- (void)writeManagementKeys:(BBMKeyPair *)keys callback:(KeyWriteCallback)callback
{
    NSString *path = [NSString stringWithFormat:kDBPrivateKeyStorePath, self.localUserUid];
    FIRDatabaseReference *privateKeyDbRef = [[FIRDatabase database] referenceWithPath:path];

    //Per the schema rules, we must include the regId when writing the management keys.
    NSDictionary *keyDict = @{kDBPrivateKeyStoreMgmtKeysKey : [keys toJson] };
    
    [privateKeyDbRef setValue:keyDict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if(nil == error) {
            callback(YES);
        }else{
            callback(NO);
        }
    }];
}

- (void)removeAllLocalUserKeys:(KeyWriteCallback)callback
{
    NSString *path = [NSString stringWithFormat:kDBKeyStorePath, self.localUserUid];
    FIRDatabaseReference *keyStoreRef = [[FIRDatabase database] referenceWithPath:path];

    [keyStoreRef setValue:nil withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        callback(nil == error);
    }];
}



@end
