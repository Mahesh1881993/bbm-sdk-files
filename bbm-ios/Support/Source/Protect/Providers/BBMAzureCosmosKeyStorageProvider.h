// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import <Foundation/Foundation.h>
#import "BBMKeyStorageProvider.h"


/*!
 @abstract
 BBMKeyStorageProvider implementation based on the BBM Enterprise Web SDK's KeyProviderServer
 example web service which will in turn store keys in an Azure Cosmos Database.
 */
@interface BBMAzureCosmosKeyStorageProvider : NSObject <BBMKeyStorageProvider>
@end
