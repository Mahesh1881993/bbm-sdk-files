// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.


#import "BBMAzureCosmosKeyStorageProvider.h"
#import "ConfigSettings.h"
#import "BBMSecUtil.h"
#import "BBMKeyPair.h"
#import "BBMProfileKeys.h"
#import "BBMAzureTokenManager.h"
#import "BBMKeyStorageProvider.h"
#import "NSString+Base64.h"

/*
 Private keys <ct keypair> are of the form:
 {
     sign : {
         "mac":<string>,
         "nonce":<string>,
         "payload":<string>
     },
     encrypt: {
        "mac":<string>,
        "nonce":<string>,
        "payload":<string>
     }
 }

 Chat keys <ct key> are of the form:
 {
     "mac":<string>,
     "nonce":<string>,
     "payload":<string>
 }

 Public keys <pt keypair> are of the form:
 {
    "sign" : {"key" : <base 64 encoded key> },
    "encrypt" : { "key" : <base 64 encoded key>}
 }

 The users key store will be sent/received in this format:
 {
     "public": <pt keypair>,

     "private": {
        "manage": <ct keypair>,
        "profile":<ct keypair>.

        "chat.mailboxId1": <ct key>,
        "chat.mailboxId2": <ct key>,
         ...
     }
 }

 Request bodies are in the form:
 {
 "token" : <jwt token>,
 "keys" : <new keys>,
 "replace" : <true or false>
 }
 */

static NSString *const kDBPrivateKeyStorePath           = @"private";
static NSString *const kDBPublicKeyStorePath            = @"public";
static NSString *const kDBKeyStoreEncryptionName        = @"encrypt";
static NSString *const kDBKeyStoreSigningName           = @"sign";
static NSString *const kDBKeyStoreProfileKeysName       = @"profile";
static NSString *const kDBKeyStoreManagementeKeysName   = @"manage";

//The various keys in these templates must match the values above
static NSString *const kDBEmptyKeyStore                 = @"{\"public\": {}, \"private\": {} }";
static NSString *const kDBChatKeyJson                   = @"{\"private\":{\"mailboxes\" : { \"%@\" : %@ } } }";
static NSString *const kDBMgmtKeyJson                   = @"{\"private\":{ \"manage\": {\"encrypt\": %@ , \"sign\": %@ } } }";
static NSString *const kDBPublicKeyJson                 = @"\"public\":{ \"encrypt\": { \"key\" : \"%@\" } , \"sign\": { \"key\" : \"%@\" } }";
static NSString *const kDBPrivateKeyJson                = @"\"private\": { \"profile\" : %@, \"manage\" : %@, \"mailboxes\" : {} }";

//For posting new key data, the post body should be in this format.
static NSString *const kKeyUpdatePostBodyFmt            = @"{\"keys\":%@, \"replace\":%@ }";

@interface BBMAzureCosmosKeyStorageProvider ()
@property (nonatomic, strong) NSDictionary *cachedKeys;
@end


@implementation BBMAzureCosmosKeyStorageProvider

@synthesize localUserUid;
@synthesize tokenManager;

+ (id<BBMKeyStorageProvider>)instance
{
    return [[self alloc] init];
}

- (NSMutableURLRequest *)constructRequest:(NSString *)path
                                   method:(NSString *)method
                                     body:(NSString *)body
                                    token:(NSString *)token
{
    NSString *fullPath = [NSString stringWithFormat:@"%@%@", AZURE_KMS_PATH, path];
    NSURL *url = [NSURL URLWithString:fullPath];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:method];
    if(body) {
        [request setHTTPBody:[body utf8Bytes]];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    
    if(token) {
        NSString *tokenStr = [NSString stringWithFormat:@"Bearer %@", token];
        [request setValue:tokenStr forHTTPHeaderField:@"Authorization"];
    }

    return request;
}

- (void)getToken:(void (^)(NSString *))callback
{
    [(BBMAzureTokenManager *)self.tokenManager getAccessTokenSilent:^(NSString *token) {
        callback(token);
    }];
}


#pragma mark - Key Read


- (void)refreshCachedKeyStore:(void (^)(KeySyncResult))callback
{
    [self getToken:^(NSString *token) {
        if(nil == token) {
            callback(kKeySyncResultReadFailure);
            return;
        }

        NSLog(@"Refreshing Private Key Store");
        NSString *path = [NSString stringWithFormat:@"%@",self.localUserUid];
        NSMutableURLRequest *request = [self constructRequest:path method:@"GET" body:nil token:token];

        void (^httpResponseHandler)(NSData*, NSURLResponse*, NSError*) = ^(NSData *data, NSURLResponse *response, NSError *error) {
            NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;

            NSDictionary *json = data ? [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] : nil;
            dispatch_async(dispatch_get_main_queue(), ^{
                if(statusCode == 200) {
                    self.cachedKeys  = json;
                    NSLog(@"Refreshed Private Key Store");
                    callback(kKeySyncResultSuccess);
                }else if(statusCode == 404){
                    //If the keys aren't found, they don't exist yet.
                    self.cachedKeys = [NSDictionary dictionary];
                    callback(kKeySyncResultNoKey);
                }else if(statusCode == 400 || statusCode == 401 || statusCode == 500) {
                    callback(kKeySyncResultReadFailure);
                }else{
                    callback(kKeySyncResultUnknown);
                }
            });
        };

        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:httpResponseHandler];
        [task resume];
    }];
}

- (void)getPublicKeys:(NSString *)uid callback:(void (^)(NSDictionary *))callback
{
    [self getToken:^(NSString *token) {
        NSString *path = [NSString stringWithFormat:@"%@", uid];
        NSMutableURLRequest *request = [self constructRequest:path method:@"GET" body:nil token:token];

        void (^httpResponseHandler)(NSData*, NSURLResponse*, NSError*) = ^(NSData *data, NSURLResponse *response, NSError *error) {
            NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
            dispatch_async(dispatch_get_main_queue(), ^{
                if(statusCode == 200) {
                    NSDictionary *json = data ? [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] : nil;
                    callback(json);
                }else{
                    callback(nil);
                }
            });
        };

        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:httpResponseHandler];
        [task resume];
    }];
}


- (void)readChatKey:(NSString *)mailboxId callback:(ChatKeyReadCallback)callback
{
    // { private: { mailboxes : { <mailboxId> : { <chatKey> } } } }
    void (^readKey)(KeySyncResult) = ^(KeySyncResult result){
        NSDictionary *mailboxes = self.cachedKeys[kDBPrivateKeyStorePath][@"mailboxes"];
        NSDictionary *chatKey = mailboxes[[mailboxId base64URLSafeEncodedString]];
        if(chatKey) {
            BBMCipherText *ct = [BBMCipherText withJSONDictionary:chatKey];
            callback(mailboxId, ct, kKeySyncResultSuccess);
        }else{
            callback(mailboxId, nil, kKeySyncResultNoKey);
        }
    };

    if(nil == self.cachedKeys) {
        [self refreshCachedKeyStore:^(KeySyncResult result) {
            if(result != kKeySyncResultSuccess) {
                callback(mailboxId, nil, result);
                return;
            }
            readKey(result);
        }];
    }else{
        readKey(kKeySyncResultSuccess);
    }
}


- (void)readManagementKeys:(KeyReadCallback)callback
{
    void (^readKey)(KeySyncResult) = ^(KeySyncResult result){
        NSDictionary *managementKeys = self.cachedKeys[kDBPrivateKeyStorePath][kDBKeyStoreManagementeKeysName];

        if(managementKeys) {
            BBMKeyPair *keyPair = [BBMKeyPair keyPairWithJson:managementKeys encrypted:YES];
            callback(keyPair, kKeySyncResultSuccess);
        }else{
            callback(nil, kKeySyncResultNoKey);
        }
    };

    if(nil == self.cachedKeys) {
        [self refreshCachedKeyStore:^(KeySyncResult result) {
            if(result != kKeySyncResultSuccess) {
                callback(nil, result);
                return;
            }
            readKey(result);
        }];
    }else{
        readKey(kKeySyncResultSuccess);
    }
}


- (void)readPrivateKeys:(KeyReadCallback)callback
{
    void (^readKey)(KeySyncResult) = ^(KeySyncResult result){
        NSDictionary *profileKeys = self.cachedKeys[kDBPrivateKeyStorePath][kDBKeyStoreProfileKeysName];

        if(profileKeys) {
            BBMKeyPair *keys = [BBMKeyPair keyPairWithJson:profileKeys encrypted:YES];
            callback(keys, kKeySyncResultSuccess);
        }else{
            callback(nil, kKeySyncResultNoKey);
        }
    };

    if(nil == self.cachedKeys) {
        [self refreshCachedKeyStore:^(KeySyncResult result) {
            if(result != kKeySyncResultSuccess) {
                callback(nil, result);
                return;
            }
            readKey(result);
        }];
    }else{
        readKey(kKeySyncResultSuccess);
    }
}




- (void)readPublicKeys:(NSString *)regId callback:(KeyReadCallback)callback
{
    void (^readKey)(NSDictionary *) = ^(NSDictionary *json){
        //Public keys are not encrypted... They are just plaintext Base64 strings.  They are
        //already base64 encoded so there is not need to re-encode them.
        NSDictionary *publicKeys = json[kDBPublicKeyStorePath];

        if(publicKeys) {
            BBMKeyPair *keys = [BBMKeyPair keyPairWithJson:publicKeys encrypted:NO encoded:NO];
            if(nil == keys) {
                callback(nil, kKeySyncResultNoKey);
            }
            callback(keys, kKeySyncResultSuccess);
        }else{
            callback(nil, kKeySyncResultNoKey);
        }
    };

    [self getPublicKeys:regId callback:^(NSDictionary *result) {
        if(nil == result) {
            callback(nil, kKeySyncResultNoKey);
            return;
        }
        readKey(result);
    }];
}



#pragma mark - Write Keys

- (void)updateKeys:(NSString *)keyJson
           replace:(BOOL)replace
          callback:(void (^)(BOOL))callback
{
    [self getToken:^(NSString *token) {
        if(nil == token) {
            callback(NO);
            return;
        }

        NSString *requestURL = self.localUserUid;
        NSString *body = [NSString stringWithFormat:kKeyUpdatePostBodyFmt, keyJson, replace ? @"true" : @"false"];
        NSMutableURLRequest *request = [self constructRequest:requestURL method:@"PUT" body:body token:token];
        void (^httpResponseHandler)(NSData*, NSURLResponse*, NSError*) = ^(NSData *data, NSURLResponse *response, NSError *error) {
            NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(nil == error && statusCode == 200);
            });
        };

        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:httpResponseHandler];
        [task resume];
    }];
}


- (void)removeAllLocalUserKeys:(KeyWriteCallback)callback
{
    [self updateKeys:kDBEmptyKeyStore replace:YES callback:^(BOOL success) {
        callback(success);
    }];
}


- (void)removeChatKey:(NSString *)mailboxId callback:(ChatKeyWriteCallback)callback
{
    NSString *keyStoreJson = [NSString stringWithFormat:kDBChatKeyJson, [mailboxId base64URLSafeEncodedString], @"null"];

    [self updateKeys:keyStoreJson replace:NO callback:^(BOOL success) {
        callback(mailboxId, success);
    }];
}


- (void)writeChatKey:(BBMCipherText *)key forMailbox:(NSString *)mailboxId callback:(ChatKeyWriteCallback)callback
{
    NSData *keyJson = [NSJSONSerialization dataWithJSONObject:[key toDictionary]
                                                      options:0
                                                        error:nil];
    NSString *keyJsonString = [[NSString alloc] initWithData:keyJson
                                                    encoding:NSUTF8StringEncoding];

    NSString *keyStoreJson = [NSString stringWithFormat:kDBChatKeyJson, [mailboxId base64URLSafeEncodedString], keyJsonString];

    [self updateKeys:keyStoreJson replace:NO callback:^(BOOL success) {
        callback(mailboxId, success);
    }];
}


- (void)writeManagementKeys:(BBMKeyPair *)managementKeys callback:(KeyWriteCallback)callback
{
    NSData *encKeyJson = [NSJSONSerialization dataWithJSONObject:managementKeys.encryptionKeyJson
                                                         options:0
                                                           error:nil];
    NSString *encKeyJsonString = [[NSString alloc] initWithData:encKeyJson
                                                       encoding:NSUTF8StringEncoding];

    NSData *signKeyJson = [NSJSONSerialization dataWithJSONObject:managementKeys.signingKeyJson
                                                          options:0
                                                            error:nil];
    NSString *signKeyJsonString = [[NSString alloc] initWithData:signKeyJson
                                                        encoding:NSUTF8StringEncoding];

    NSString *keyStoreJson = [NSString stringWithFormat:kDBMgmtKeyJson, encKeyJsonString, signKeyJsonString];

    [self updateKeys:keyStoreJson replace:NO callback:^(BOOL success) {
        callback(success);
    }];
}


- (void)writeProfileKeys:(BBMProfileKeys *)keys managementKeys:(BBMKeyPair *)managementKeys callback:(KeyWriteCallback)callback
{
    NSString *keysPublic = [NSString stringWithFormat:kDBPublicKeyJson, keys.publicKeyPair.encryptionKey, keys.publicKeyPair.signingKey];
    NSString *keysPrivate = [NSString stringWithFormat:kDBPrivateKeyJson, keys.privateKeyPair.toJsonString, managementKeys.toJsonString];

    //Combine them
    NSString *keyStoreJson = [NSString stringWithFormat:@"{%@,%@}", keysPublic, keysPrivate];

    [self updateKeys:keyStoreJson replace:YES callback:^(BOOL success) {
        callback(success);
    }];
}


@end
