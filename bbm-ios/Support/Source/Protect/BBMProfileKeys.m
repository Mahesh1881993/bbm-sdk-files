// Copyright (c) 2017 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMProfileKeys.h"
#import "BBMKeyPair.h"
#import "BBMSecUtil.h"
#import "NSString+Base64.h"

@implementation BBMProfileKeys

- (BOOL)allKeysReady
{
    //We have all of the keys and they are of a valid length
    return self.privateKeyPair != nil &&
    self.publicKeyPair != nil &&
    self.privateKeyPair.encryptionKey.length != 0 &&
    self.privateKeyPair.signingKey.length != 0 &&
    self.publicKeyPair.encryptionKey.length != 0 &&
    self.publicKeyPair.signingKey.length != 0;
}


- (NSDictionary *)privateKeystoreDict
{
    id encKey = self.privateKeyPair.encryptionKey;
    id signKey = self.privateKeyPair.signingKey;

    if(self.isPrivateKeyEncrypted) {
        //Key is a base64 encoded cipherText object
        encKey = [[BBMCipherText withBase64JSONString:self.privateKeyPair.encryptionKey] toDictionary];
        signKey = [[BBMCipherText withBase64JSONString:self.privateKeyPair.signingKey] toDictionary];
    }

    return @{@"encryptionKey": encKey,
             @"signingKey": signKey};
}

- (NSDictionary *)publicKeystoreDict
{
    return @{@"encryptionKey": self.publicKeyPair.encryptionKey,
             @"signingKey": self.publicKeyPair.signingKey};
}

- (BBMProfileKeys *)encyptedProfileKeys:(BBMKeyPair *)encryptionKey
{
    NSError *error;

    BBMKeyPair *encryptedPrivateKeys = [BBMSecUtil encryptKeyPair:self.privateKeyPair
                                                          rootKey:encryptionKey
                                                            error:&error];

    if(error || encryptedPrivateKeys.signingKey == nil || encryptedPrivateKeys.encryptionKey == nil) {
        NSLog(@"Profile Key encryption error: %@", error.localizedDescription);
        return nil;
    }

    BBMProfileKeys *retVal = [[BBMProfileKeys alloc] init];
    retVal.publicKeyPair = self.publicKeyPair;
    retVal.privateKeyPair = encryptedPrivateKeys;
    retVal.isPrivateKeyEncrypted = YES;
    return retVal;
}

- (BBMProfileKeys *)plaintextProfileKeys:(BBMKeyPair *)decryptionKey
{
    NSError *error;
    BBMKeyPair *decryptedPrivateKeys = [BBMSecUtil decryptKeyPair:self.privateKeyPair
                                                          rootKey:decryptionKey
                                                           encode:NO
                                                            error:&error];
    if(error) {
        NSLog(@"Profile Key decryption error: %@", error.localizedDescription);
        return nil;
    }


    BBMProfileKeys *retVal = [[BBMProfileKeys alloc] init];
    retVal.publicKeyPair = self.publicKeyPair;
    retVal.privateKeyPair = decryptedPrivateKeys;
    retVal.isPrivateKeyEncrypted = NO;
    return retVal;
}

@end
