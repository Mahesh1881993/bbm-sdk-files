// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

@class BBMKeyPair;

/*!
 @details
 Represents a set of BBM profile keys, the public and private encryption and signing keys for
 the local user.
 */
@interface BBMProfileKeys : NSObject


/*!
 @details
 Returns a set of profile keys with the private keys encrypted giving the supplied \a encryptionKey.
 These profile keys are suitable for external storage.  Self in this case must have the private
 keys as plaintext.  This will return a BBMProfileKey object with the isPrivateKeyEncrypted flag
 set to YES.
 @param encryptionKey The key pair to be used to encrypt the privateKeyPair
 */
- (BBMProfileKeys *)encyptedProfileKeys:(BBMKeyPair *)encryptionKey;


/*!
 @details
 Returns a set of profile keys with the private keys in plain text, decrypted using the given
 \a decryptionKey.  Self in this case must have the private keys in encrypted form.
 @param decryptionKey The key pair to be used to decrypt the privateKeyPair
 */
- (BBMProfileKeys *)plaintextProfileKeys:(BBMKeyPair *)decryptionKey;


/*! @brief YES if both key pairs are set and valid */
- (BOOL)allKeysReady;

/*! @brief The public key pair */
@property (nonatomic, strong) BBMKeyPair *publicKeyPair;

/*! @brief The private key pair */
@property (nonatomic, strong) BBMKeyPair *privateKeyPair;

/*! @brief YES if we know the private key is encrypted */
@property (nonatomic, assign) BOOL isPrivateKeyEncrypted;

/*!
 @details
 Returns the dictionary value of the privateKey pair in the form:
 @{ @"signingKey" : <value>, "encryptionKey" : <value> }

 If the private keys are encrypted, the value will be a JSON dictionary that represents a
 BBMCipherText object.
 */
- (NSDictionary *)privateKeystoreDict;


/*!
 @details
 Returns the dictionary value of the publicKey pair in the form:
 @{"encryptionKey" : <value>, @"signingKey" : <value> }

 Public keys will not be encrypted.
 */
- (NSDictionary *)publicKeystoreDict;


@end
