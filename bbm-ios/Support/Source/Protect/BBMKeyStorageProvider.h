// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>
#import <BBMEnterprise/BBMEnterprise.h>
#import "BBMAuthenticationDelegate.h"

@class BBMPrivateKeyPair;
@class BBMKeyPair;
@class BBMProfileKeys;
@class BBMCipherText;
@class BBMPrivateKeyCollection;

typedef enum {
    kKeySyncResultUnknown = -1,    ///<We have not tried syncing these keys yet
    kKeySyncResultNoKey = 0,       ///<The key does not exist in external storage
    kKeySyncResultSuccess = 1,     ///<They key was read from external storage
    kKeySyncResultReadFailure = 2, ///<An error occurred when reading the key.  It may or may not exist
    kKeySyncResultNoUser = 3 ,     ///<An attempt was made to read and set the user keys for a user that does not exist
    kKeySyncResultWriteFailure = 4 ///<An error occurred when writing the key
}KeySyncResult;

typedef void (^KeyWriteCallback)(BOOL success);
typedef void (^KeyReadCallback)(BBMKeyPair * key, KeySyncResult result);

typedef void (^ChatKeyWriteCallback)(NSString * mailboxId, BOOL success);
typedef void (^ChatKeyReadCallback)(NSString * mailboxId, BBMCipherText * key, KeySyncResult result);

typedef void (^AllKeysReadCallback)(BBMPrivateKeyCollection *keys, BOOL success);


/*!
 @details
 Classes that implement this protocol can be used as a storage provide for the BBMKeyManager.
 The callbacks will always be non-nil.
 @see BBMFirebaseKeyStorageProvider
 @see BBMKeyManager
 */
@protocol BBMKeyStorageProvider <NSObject>

/*!
 @details
 This should return a configured instance of your key storage provider
 */
+ (id<BBMKeyStorageProvider>)instance;

/*! @brief An implementation specific userUid string */
@property (nonatomic, strong) NSString * localUserUid;

/*! @brief A tokenManager the storage provider can use to get the necessary auth tokens (if applicable) */
@property (nonatomic, strong) id<BBMTokenManager> tokenManager;

/*!
 When called, this should read the users management keys from the database.  The root key is
 derived from the users password and is required to decrypt all private keys.
 @param callback Called on completion
 */
- (void)readManagementKeys:(KeyReadCallback)callback;


 /*!
  When called, this should write the supplied key pair to the database.
  @param managementKeys The management key pair
  @param callback callback that must be called when the write operation is complete
  */
 - (void)writeManagementKeys:(BBMKeyPair *)managementKeys callback:(KeyWriteCallback)callback;


/*!
 @details
 When called, this should write out the supplied profile keys to your database.
 @param keys The profile keys to store
 @param callback callback that must be called when the write operation is complete
 */
- (void)writeProfileKeys:(BBMProfileKeys *)keys
          managementKeys:(BBMKeyPair *)managementKeys
                callback:(KeyWriteCallback)callback;


/*!
 @details
 When called, this should write out the supplied chat key to your database.
 @param key The chat keys to store.  As of R5, this must be an encrypted BBMCipherText object
 @param mailboxId The mailbox identifier for the chat to which the key belongs
 @param callback callback that must be called when the write operation is complete
 */
- (void)writeChatKey:(BBMCipherText *)key
          forMailbox:(NSString *)mailboxId
            callback:(ChatKeyWriteCallback)callback;


/*!
 @details
 When called, this read and return out the  chat key from your database.
 @param mailboxId The mailbox identifier for the chat to which the key belongs
 @param callback callback that must be called when the read operation is complete
 */
- (void)readChatKey:(NSString *)mailboxId
           callback:(ChatKeyReadCallback)callback;


/*!
 @details
 When called, this should remove the chat key for the \a mailbox id from your database.
 @param mailboxId The mailbox identifier for the chat to which the key belongs
 @param callback callback that must be called when the read operation is complete
 */
- (void)removeChatKey:(NSString *)mailboxId
             callback:(ChatKeyWriteCallback)callback;


/*!
 @details
 When called, this should read and return the private keys for the local user
 @param callback callback that must be called when the read operation is complete
 */
- (void)readPrivateKeys:(KeyReadCallback)callback;


/*!
 @details
 When called, this should read and return the public keys for the user with the supplied \a regId
 @param callback callback that must be called when the read operation is complete
 */
- (void)readPublicKeys:(NSString *)uid
              callback:(KeyReadCallback)callback;


/*!
 @details
 Removes all of the local keys for the user
 */
- (void)removeAllLocalUserKeys:(KeyWriteCallback)callback;

@end


