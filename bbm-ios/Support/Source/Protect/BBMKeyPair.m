// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMKeyPair.h"
#import "NSString+Base64.h"

@interface BBMKeyPair()
@property (nonatomic, readwrite) NSString *signingKey;
@property (nonatomic, readwrite) NSString *encryptionKey;

@property (nonatomic, readwrite) BOOL encoded;
@property (nonatomic, readwrite) BOOL encrypted;
@end


@implementation BBMKeyPair

+ (instancetype)keyPairWithEncryptionKeyData:(NSData *)encryptionKey
                              signingKeyData:(NSData *)signingKey
                                      encode:(BOOL)encode
{
    NSString *encKey, *signKey;
    if(encode) {
        encKey = [encryptionKey base64URLSafeEncodedString];
        signKey = [signingKey base64URLSafeEncodedString];
    }else{
        encKey =  [[NSString alloc] initWithData:encryptionKey encoding:NSUTF8StringEncoding];

        signKey = [[NSString alloc] initWithData:signingKey encoding:NSUTF8StringEncoding];
    }
    BBMKeyPair *retVal = [[self alloc] init];
    retVal.encryptionKey = encKey;
    retVal.signingKey = signKey;
    retVal.encoded = encode;
    return retVal;
}


+ (instancetype)keyPairWithEncryptionKeyJson:(NSDictionary *)encryptionKey
                              signingKeyJson:(NSDictionary *)signingKey
{
    NSError *error;
    NSData *encKey = [NSJSONSerialization dataWithJSONObject:encryptionKey options:0 error:&error];
    if(error) {
        NSAssert(NO, @"Error: Unable to serialize the encryption key");
        return nil;
    }

    NSData *signKey = [NSJSONSerialization dataWithJSONObject:signingKey options:0 error:&error];
    if(error) {
        NSAssert(NO, @"Error: Unable to serialize the signing key");
        return nil;
    }

    BBMKeyPair *retVal = [self keyPairWithEncryptionKeyData:encKey signingKeyData:signKey encode:YES];
    retVal.encrypted = YES;
    return retVal;
}


- (NSString *)encryptionKey
{
    return _encryptionKey;
}


- (NSString *)signingKey
{
    return _signingKey;
}


- (NSData *)encryptionKeyData
{
    NSData *retVal;
    if(self.encoded) {
        retVal = [NSData dataWithBase64EncodedURLSafeString:[self encryptionKey]];
    }else{
        retVal = [self.encryptionKey utf8Bytes];
    }
    return retVal;
}


- (NSData *)signingKeyData
{
    NSData *retVal;
    if(self.encoded) {
        retVal = [NSData dataWithBase64EncodedURLSafeString:[self signingKey]];
    }else{
        retVal = [self.signingKey utf8Bytes];
    }
    return retVal;
}


- (NSDictionary *)signingKeyJson
{
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[self signingKeyData]
                                                         options:0
                                                           error:&error];
    if(error) {
        NSAssert(NO, @"The signing key is not an encrypted JSON object");
        NSLog(@"The signing key is not an encrypted JSON object");
        return nil;
    }
    return dict;
}

#pragma mark - JSON Serialization

+ (BBMKeyPair *)keyPairWithJson:(NSDictionary *)json encrypted:(BOOL)encrypted{
    return [self keyPairWithJson:json encrypted:encrypted encoded:YES];
}


+ (BBMKeyPair *)keyPairWithJson:(NSDictionary *)json encrypted:(BOOL)encrypted encoded:(BOOL)encoded
{
    if(![json[@"encrypt"] isKindOfClass:[NSDictionary class]] ||
       ![json[@"sign"] isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"Invalid key pair json. %@", json);
        return nil;
    }

    if(encrypted) {
        BBMKeyPair *retVal = [self keyPairWithEncryptionKeyJson:json[@"encrypt"] signingKeyJson:json[@"sign"]];
        return retVal;
    }else{
        NSData *encKeyData = [json[@"encrypt"][@"key"] utf8Bytes];
        NSData *signKeyData = [json[@"sign"][@"key"] utf8Bytes];
        return [self keyPairWithEncryptionKeyData:encKeyData signingKeyData:signKeyData encode:encoded];
    }
}

- (NSDictionary *)toJson
{
    NSDictionary *retVal;
    if(self.encrypted){
            retVal = @{@"sign": self.signingKeyJson, @"encrypt" : self.encryptionKeyJson };
    }else{
        retVal = @{@"sign" : @{@"key" : self.signingKey },  @"encrypt" : @{@"key" : self.encryptionKey } };
    }
    return retVal;
}

- (NSString *)toJsonString
{
    NSDictionary *jsonDict = [self toJson];
    NSData *data = [NSJSONSerialization dataWithJSONObject:jsonDict options:0 error:nil];
    NSString *retVal = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return retVal;
}

- (NSDictionary *)encryptionKeyJson
{
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[self encryptionKeyData]
                                                         options:0
                                                           error:&error];
    if(error) {
        NSAssert(NO, @"The encryption key is not an encrypted JSON object");
        NSLog(@"The encryption key is not an encrypted JSON object");
        return nil;
    }
    return dict;
}



- (NSString *)description
{
    if(self.encrypted) {
        return [NSString stringWithFormat:@"%@, %@", self.encryptionKeyJson, self.signingKeyJson];
    }
    return [NSString stringWithFormat:@"Encryption Key:%@, Signing Key:%@", self.encryptionKeyData, self.signingKeyData];
}

@end





