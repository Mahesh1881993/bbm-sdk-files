// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>

#import "BBMKeyManager.h"
#import "BBMEndpointManager.h"
#import "BBMKeyPair.h"
#import "BBMProfileKeys.h"
#import "BBMAccess.h"
#import "BBMSecUtil.h"
#import "BBMUserPasswordManager.h"
#import "NSString+Base64.h"
#import "BBMUserManager.h"
#import "BBMAppUser.h"

#pragma mark -

/*!
 Holds the callback for a chat key request
 */
@interface BBMKeyRequest : NSObject

+ (instancetype)requestWithChatId:(NSString *)chatId
                         callback:(ChatKeySyncCallback)callback;

@property (nonatomic, strong) NSString *chatId;
@property (nonatomic,   copy) ChatKeySyncCallback callback;

@end


#pragma mark -

@interface BBMKeyManager () {
    BBMManagementKeyState _managementKeyState;
}

@property (nonatomic, strong) id<BBMKeyStorageProvider> storageProvider;

//Holds the profile keys for the local user
@property (nonatomic, strong) BBMProfileKeys *cachedProfileKeys;

//Holds the keys and the sync status for remote management keys
@property (nonatomic, strong) BBMKeyPair *cachedManagementKeys;
@property (nonatomic, assign) KeySyncResult managementKeySyncStatus;
@property (nonatomic, strong) BBMKeyPair *derivedRootKey;

@property (nonatomic, assign) BOOL didGenerateNewManagementKeys;

//Holds BBMKeyRequests objects keyed by the cookie of the request
@property (nonatomic, strong) NSMutableDictionary *chatKeyRequestsByCookie;
//Host chatIds for any pending chat key requests to avoid duplication
@property (nonatomic, strong) NSMutableSet *pendingChatKeyImports;
//Holds registration Ids (as strings) for any pending profile key requests to avoid duplication
@property (nonatomic, strong) NSMutableSet *pendingUserKeyImports;

@property (nonatomic, strong) ObservableMonitor *profileKeysMonitor;
@property (nonatomic, strong) ObservableMonitor *keySyncMonitor;
@property (nonatomic, strong) ObservableMonitorSet *userMonitors;

@property (nonatomic, strong) BBMEndpointManager *endpointManager;
@property (nonatomic, readwrite) BBMUserPasswordManager *passwordManager;
@property (nonatomic, strong) BBMUserManager *userManager;

@property (nonatomic, strong) NSString *localUserRegId;

@end

@implementation BBMKeyManager

- (id)initWithKeyStorageProvider:(id<BBMKeyStorageProvider>)provider
                     userManager:(BBMUserManager *)userManager
{
    self = [super init];
    if(self) {
        self.storageProvider = provider;
        self.userManager = userManager;
        [self resetKeyManager];
        [[BBMEnterpriseService service] addListener:self forMessageNames:@[[BBMChatKeyMessage messageName],
                                                                           [BBMProfileKeysMessage messageName],
                                                                           [BBMProfileKeysImportFailureMessage messageName],
                                                                           [BBMChatKeysImportFailureMessage messageName],
                                                                           [BBMUserKeysImportFailureMessage messageName]]];
    }
    return self;
}


- (void)dealloc
{
    [[BBMEnterpriseService service] removeListener:self
                                   forMessageNames:@[[BBMChatKeyMessage messageName],
                                                     [BBMProfileKeysMessage messageName],
                                                     [BBMProfileKeysImportFailureMessage messageName],
                                                     [BBMChatKeysImportFailureMessage messageName],
                                                     [BBMUserKeysImportFailureMessage messageName]]];
}


- (void)resetKeyManager
{
    self.cachedProfileKeys = [[BBMProfileKeys alloc] init];
    self.cachedProfileKeys.isPrivateKeyEncrypted = YES;
    self.localUserRegId = nil;
    self.storageProvider.localUserUid = nil;

    self.chatKeyRequestsByCookie = [NSMutableDictionary dictionary];
    self.pendingChatKeyImports = [NSMutableSet set];
    self.pendingUserKeyImports = [NSMutableSet set];
    self.managementKeySyncStatus = kKeySyncResultUnknown;

    self.managementKeyState = kBBMMgmtKeyStateUnknown;
    self.derivedRootKey = nil;

    if(nil == self.passwordManager) {
        self.passwordManager = [[BBMUserPasswordManager alloc] initWithKeyManager:self];
    }

    self.userMonitors = [[ObservableMonitorSet alloc] init];
    self.endpointManager = [[BBMEndpointManager alloc] init];
}

- (BBMKeyPair *)cachedManagementKeys
{
    [ObservableTracker getterCalledForObject:self propertyName:@"cachedManagementKeys"];
    return _cachedManagementKeys;
}

// Checks our in-RAM management keys after we've attempted to load them and takes the appropriate
// steps to create a fresh set and upload them if necessary
- (void)checkManagmentKeys
{
    //Write the remotely stored management keys to the keychain
    if(nil == self.cachedManagementKeys) {
        NSAssert(nil != self.derivedRootKey, @"We should have a derived root key");
        //We were unable to get keys from the cloud and there are no keys locally.  Now we'll
        //generate a new pair and upload them to the database.
        if(self.managementKeySyncStatus == kKeySyncResultNoKey){
            NSError *error;
            BBMKeyPair *keyPair = [BBMSecUtil generateKeyPair:&error];
            self.didGenerateNewManagementKeys = YES;
            self.cachedManagementKeys = keyPair;
        }else{
            NSAssert(NO, @"Attempt to reconcile keys without known key sync result");
        }
    }

}


//Reads the management keys from remote storage and validates them
- (void)readManagmentKeys
{
    [self.storageProvider readManagementKeys:^(BBMKeyPair *key, KeySyncResult result) {
        if(key) {
            NSError *error;
            BBMKeyPair *managementKeys = [BBMSecUtil decryptKeyPair:key
                                                            rootKey:self.derivedRootKey
                                                             encode:YES
                                                              error:&error];

            //We are unable to tell the difference between a decryption failure and a failure to
            //validate.  If the password and therefore the derived root key is incorrect, we
            //will not be able validate the management keys or decrypt them
            if(error &&
              (error.code == kBBMSecDecryptionFailure || error.code == kBBMSecCipherTextValidationFailure))
            {
                //We have a derived root key, but we couldn't decrypt what was in the database.
                //This implies that the users password is incorrect.
                self.managementKeyState = kBBMMgmtKeyIncorrectPassword;
                self.derivedRootKey = nil;
                return;
            }
            else if(error)
            {
                //We can't deserialize the keys.
                self.managementKeyState = kBBMMgmtKeyCorruptedKeyData;
                return;
            }

            self.cachedManagementKeys = managementKeys;
            [BBMSecUtil persistManagementKeys:managementKeys];
            self.managementKeyState = kBBMMgmtKeysReady;
            self.managementKeySyncStatus = kKeySyncResultSuccess;
        }

        if(result == kKeySyncResultNoKey) {
            self.managementKeySyncStatus = kKeySyncResultNoKey;
            [self checkManagmentKeys];
        }else{
            self.managementKeySyncStatus = kKeySyncResultReadFailure;
        }

        if(result == kKeySyncResultReadFailure) {
            self.managementKeyState = kKeySyncResultReadFailure;
        }
    }];
}


- (void)synchronizeManagementKeys:(NSString *)password
{
    //[self.storageProvider removeAllLocalUserKeys:nil];

    self.cachedManagementKeys = [BBMSecUtil getManagmentKeys];

    if(nil == self.cachedManagementKeys) {
        self.managementKeyState = kBBMMgmtKeysPending;

        if(nil == self.derivedRootKey) {
            //If we do not have a derived key, prompt the user for their password
            if(nil == password || nil == self.localUserRegId) {
                //Read the key-store... If there is no key in it, we should ask for a verified
                //password
                [self.storageProvider readManagementKeys:^(BBMKeyPair *key, KeySyncResult result) {
                    if(result == kKeySyncResultSuccess) {
                        self.managementKeyState = kBBMMgmtKeyPasswordRequired;
                    }else{
                        self.managementKeyState = kBBMMgmtKeyInitialPasswordRequired;
                    }
                }];
            }else{
                NSData *pwdData = [password utf8Bytes];
                NSData *prefix = [self.localUserRegId utf8Bytes];
                NSAssert(nil != self.sdkServiceDomain && nil != prefix, @"Missing parameters for key generation");
                BBMDerivedKey *derivedKey = [BBMDerivedKey deriveKey:pwdData
                                                              sdkDomain:self.sdkServiceDomain
                                                              prefix:prefix];
                self.derivedRootKey = derivedKey.keyPair;
                [self readManagmentKeys];
            }
        }else{
            //Otherwise, just attempt to read the keys from firebase
            [self readManagmentKeys];
        }
    }else{
        self.managementKeyState = kBBMMgmtKeysReady;
    }
}


- (void)setManagementKeyState:(BBMManagementKeyState)managementKeyState
{
    if(managementKeyState != _managementKeyState) {
        [self willChangeValueForKey:@"managementKeyState"];
        _managementKeyState = managementKeyState;
        [self didChangeValueForKey:@"managementKeyState"];
    }
}


- (BBMManagementKeyState)managementKeyState
{
    [ObservableTracker getterCalledForObject:self propertyName:@"managementKeyState"];
    return _managementKeyState;
}


- (void)disableAutomaticPasswordManagement
{
    self.passwordManager = nil;
}

- (void)resetKeyManagementPassword
{
    [self.endpointManager deregisterAllNonLocalEndpoints];
    typeof(self) __weak weakSelf = self;
    [self.storageProvider removeAllLocalUserKeys:^(BOOL success) {
        weakSelf.managementKeyState = kBBMMgmtKeyReset;
    }];
}

- (void)stopKeyManager
{
    [self.passwordManager stop];
    self.passwordManager = nil;
}

#pragma mark - Key Sync

- (void)retrySyncProfileKeys
{
    if(self.storageProvider.localUserUid && self.localUserRegId) {
        [self syncProfileKeysForLocalUser:self.storageProvider.localUserUid regId:self.localUserRegId];
    }else{
        NSLog(@"Error: the local user UID and regId's aren't set yet.  Call syncProfileKeysForLocaluser:uid:regId");
    }
}

- (void)syncProfileKeysForLocalUser:(NSString*)uid regId:(NSString*)regId
{
    [self syncProfileKeysForLocalUser:uid regId:regId callback:nil];
}


- (void)syncProfileKeysForLocalUser:(NSString*)uid
                              regId:(NSString*)regId
                           callback:(UserKeySyncCallback)callback
{
    self.storageProvider.localUserUid = uid;
    self.passwordManager.localUserRegId = regId;
    self.localUserRegId = regId;

    typeof(self) __weak weakSelf = self;

    __block UserKeySyncCallback callbackCopy = [callback copy];
    __block BOOL profileKeysRequested = NO;


    BOOL (^monitorBlock)(void) = ^BOOL(void) {
        if(weakSelf.cachedManagementKeys == nil) {
            return NO;
        }

        NSString *profileKeysState = [weakSelf getProfileKeysState];
        if (profileKeysState == nil) {
            return NO;
        }
        // If the key state is 'Synced' then we have nothing to do
        if ([profileKeysState isEqualToString:kBBMKeyStateNotSynced]) {
            // Check to see if we have keys stored in Firebase
            [weakSelf.storageProvider readPrivateKeys:^(BBMKeyPair *keyPair, KeySyncResult result) {
                BOOL runCallback = NO;
                if(result == kKeySyncResultSuccess) {
                    weakSelf.cachedProfileKeys.privateKeyPair = keyPair;
                    runCallback = [weakSelf setProfileKeysIfReady];
                }else if(result == kKeySyncResultNoKey){
                    if(!profileKeysRequested) {
                        profileKeysRequested = YES;
                        [weakSelf requestProfileKeys];
                        runCallback = YES;
                    }
                }

                if(callbackCopy && (result == kKeySyncResultReadFailure || runCallback)) {
                    callbackCopy(regId, result);
                    callbackCopy = nil;
                }
            }];

            [weakSelf.storageProvider readPublicKeys:uid callback:^(BBMKeyPair *keyPair, KeySyncResult result) {
                BOOL runCallback = NO;
                if(result == kKeySyncResultSuccess) {
                    weakSelf.cachedProfileKeys.publicKeyPair = keyPair;
                    runCallback = [weakSelf setProfileKeysIfReady];
                }else if(result == kKeySyncResultNoKey){
                    if(!profileKeysRequested) {
                        profileKeysRequested = YES;
                        [weakSelf requestProfileKeys];
                        runCallback = YES;
                    }
                }

                if(callbackCopy && (result == kKeySyncResultReadFailure || runCallback)) {
                    callbackCopy(regId, result);
                    callbackCopy = nil;
                }
            }];

        }else {
            if(callback) {
                callback(regId, kKeySyncResultSuccess);
            }
        }

        return YES;
    };

    //Attempt an initial management key sync.
    [self synchronizeManagementKeys:nil];
    self.profileKeysMonitor = [ObservableMonitor monitorActivatedWithName:@"ProfileKeysMonitor"
                                                     selfTerminatingBlock:monitorBlock];
}


- (BOOL)setProfileKeysIfReady
{
    if (nil == self.cachedProfileKeys || ![self.cachedProfileKeys allKeysReady]) {
        return NO;
    }

    NSString *profileKeysState = [self getProfileKeysState];
    if ([profileKeysState isEqualToString:kBBMKeyStateNotSynced])
    {
        [self setProfileKeys:self.cachedProfileKeys];
    }
    return YES;
}


- (void)syncUserKey:(BBMUser*)user
{
    [self syncUserKey:user callback:nil];
}


- (void)syncUserKey:(BBMUser*)user callback:(UserKeySyncCallback)callback;
{
    BBMAppUser *appUser = [self.userManager userForRegId:user.regId];

    [self.storageProvider readPublicKeys:appUser.uid callback:^(BBMKeyPair *key, KeySyncResult result) {
        if(result == kKeySyncResultSuccess) {
            [self setPublicEncryptionKey:key forUser:[user.regId stringValue]];
        }
        if(callback) {
            callback([user.regId stringValue], result);
        }
    }];
}


- (void)readUserKey:(NSString *)uid callback:(UserKeySyncCallback)callback
{
    callback = callback ?: ^(NSString *uid, KeySyncResult success){};
    [self.storageProvider readPublicKeys:uid callback:^(BBMKeyPair *key, KeySyncResult result) {
        if(result == kKeySyncResultSuccess) {
            NSLog(@"Sending public user keys to BBM Service");
            [self setPublicEncryptionKey:key forUser:uid ];

            //Wait until the User object is added to the model and callback
            typeof(self) __weak weakSelf = self;
            [self.userMonitors addMonitorActivatedWithName:@"userMonitor" selfTerminatingBlock:^{
                BBMAppUser *appUser = [weakSelf.userManager userForUID:uid];
                BBMUser *user = [BBMAccess getUserForUserRegId:@(appUser.regId)];
                if(user.bbmState == kBBMStateCurrent && user.keyState == kBBMUser_KeyStateSynced) {
                    callback(uid, kKeySyncResultSuccess);
                    return YES;
                }
                else if(user.bbmState == kBBMStateMissing) {
                    callback(uid, kKeySyncResultNoUser);
                    return NO;
                }
                //User is still pending
                return NO;
            }];
        }else {
            callback(uid, result);
        }
    }];
}


- (void)syncChatKey:(BBMChat*)chat
{
    [self syncChatKey:chat callback:nil];
}


- (void)syncChatKey:(BBMChat*)chat callback:(ChatKeySyncCallback)callback
{
    if(nil == self.cachedManagementKeys) {
        NSLog(@"Deferring chat key sync.  Management keys aren't ready");
        return;
    }

    if(chat.mailboxId == nil || chat.mailboxId.length == 0) {
        NSLog(@"Critical ERROR: You cannot sync a chat key before you know the mailbox ID");
        callback(chat, NO);
        return;
    }

    if (chat.keyState == kBBMChat_KeyStateExport) {
        // Export the local key, it will be returned in a 'chatKey' message
        NSString *cookie = [self requestChatKey:chat.chatId];

        //We need to map the request key cookie to the chatId we're requesting the key for
        //The chatKey message does not include the chatId.  The cookie can be used to
        //determine which response belongs to which request
        BBMKeyRequest *request = [BBMKeyRequest requestWithChatId:chat.chatId callback:callback];
        self.chatKeyRequestsByCookie[cookie] = request;

    } else if (chat.keyState == kBBMChat_KeyStateImport) {
        void (^readKeyCallback)(NSString*, BBMCipherText*, KeySyncResult) = ^(NSString *mailboxId, BBMCipherText *key, KeySyncResult result) {
            if(result == kKeySyncResultSuccess) {
                [self setChatKey:key forMailboxId:mailboxId];
            }
            if(callback) {
                callback(chat, result);
            }
        };
        [self.storageProvider readChatKey:chat.mailboxId
                                 callback:readKeyCallback];
    }
}


- (void)startAutomaticKeySync
{
    typeof (self) __weak weakSelf = self;
    if(nil == self.keySyncMonitor) {
        self.keySyncMonitor = [ObservableMonitor monitorActivatedWithName:@"keyMonitor" block:^{
            [weakSelf syncAllChatAndUserKeys:NO];
        }];
    }
}


- (void)stopAutomaticKeySync
{
    [self.keySyncMonitor deActivate];
    self.keySyncMonitor = nil;
}


- (void)syncAllChatAndUserKeys
{
    [self syncAllChatAndUserKeys:YES];
}


- (void)syncAllChatAndUserKeys:(BOOL)force
{
    if(self.managementKeyState != kBBMMgmtKeysReady) {
        return;
    }

    BBMUserCriteria *crit = [[BBMUserCriteria alloc] init];
    crit.keyState = kBBMUser_KeyStateImport;
    BBMLiveList *usersList = [[[BBMEnterpriseService service] model] userWithCriteria:crit];

    for(BBMUser *user in usersList.observableArray) {
        NSString *regId = user.regId.stringValue;
        if([self.pendingUserKeyImports containsObject:regId]) {
            continue;
        }
        BBMAppUser *appUser = [self.userManager userForRegId:user.regId];
        if(nil == appUser.uid) {
            NSLog(@"User UID not availble yet.  Postponing key sync");
            continue;
        }
        [self.pendingUserKeyImports addObject:regId];
        [self syncUserKey:user callback:^(NSString *regId, KeySyncResult success) {
            NSLog(@"User Key sycned for %@", regId);
            [self.pendingUserKeyImports removeObject:regId];
        }];
    }

    //Forcing a key sync will reload all user keys even if they're in the sync'd state.  This can
    //be helpful if you have many users whose profile keys have changed.
    if(force) {
        BBMUserCriteria *crit = [[BBMUserCriteria alloc] init];
        crit.keyState = kBBMUser_KeyStateSynced;
        BBMLiveList *usersList = [[[BBMEnterpriseService service] model] userWithCriteria:crit];

        for(BBMUser *user in usersList.observableArray) {
            NSString *regId = user.regId.stringValue;
            if([self.pendingUserKeyImports containsObject:regId]) {
                continue;
            }
            BBMAppUser *appUser = [self.userManager userForRegId:user.regId];
            if(nil == appUser.uid) {
                NSLog(@"User UID not availble yet.  Postponing key sync");
                continue;
            }
            [self.pendingUserKeyImports addObject:regId];
            [self syncUserKey:user callback:^(NSString *regId, KeySyncResult success) {
                NSLog(@"User Key force-sycned for %@", regId);
                [self.pendingUserKeyImports removeObject:regId];
            }];
        }
    }

    BBMLiveList *chatsList = [[[BBMEnterpriseService service] model] chat];
    for (BBMChat *chat in chatsList.observableArray) {
        if(chat.bbmState != kBBMStateCurrent) {
            continue;
        }

        if([self.pendingChatKeyImports containsObject:chat.mailboxId] && chat.keyState == kBBMChat_KeyStateSynced) {
            [self.pendingChatKeyImports removeObject:chat.mailboxId];
            continue;
        }

        //We have to wait until the chat has a mailbox ID
        if(chat.mailboxId != nil && chat.mailboxId.length &&
          (chat.keyState == kBBMChat_KeyStateImport || chat.keyState == kBBMChat_KeyStateExport))
        {
            if([self.pendingChatKeyImports containsObject:chat.mailboxId]) {
                continue;
            }
            [self.pendingChatKeyImports addObject:chat.mailboxId];
            [self syncChatKey:chat callback:^(BBMChat *inchat, KeySyncResult result) {
                if(result == kKeySyncResultNoKey) {
                    //We've deleted the private key store or the chat key.  We need to leave
                    //this chat.  There is no way to resolve this error - we no longer have the
                    //private chat key
                    NSLog(@"Error loading chat key.. Leaving chat %@", chat.chatId);
                    [BBMAccess leaveChat:inchat];
                }else if(result != kKeySyncResultSuccess) {
                    //This may be a recoverable error... No need to leave the chat
                    NSLog(@"Cannot load chat keys for chat %@", chat.chatId);
                }else{
                    NSLog(@"Sync'd chat keys for %@", chat.chatId);
                }
            }];
        }
    }
}

#pragma mark - BBMDSConsumer

- (void)receivedIncomingMessage:(BBMIncomingMessage *)incomingMessage
{
    if ([incomingMessage.messageName isEqualToString:[BBMChatKeyMessage messageName]]) {
        //Response to a request for new chat keys
        BBMChatKeyMessage *chatKeyMessage = (BBMChatKeyMessage *)incomingMessage;
        [self handleIncomingChatKeyMesssage:chatKeyMessage];
    }
    else if ([incomingMessage.messageName isEqualToString:[BBMProfileKeysMessage messageName]]) {
        //Response to a request for new profile keys
        BBMProfileKeysMessage *profileKeysMessage = (BBMProfileKeysMessage *)incomingMessage;
        [self handleIncomingProfileKeyMessage:profileKeysMessage];
    }
    else if ([incomingMessage.messageName isEqualToString:[BBMUserKeysImportFailureMessage messageName]]) {
        //Error returned if we cannot update the user keys with the supplied key data
        BBMUserKeysImportFailureMessage *userKeysMsg = (BBMUserKeysImportFailureMessage *)incomingMessage;
        [self handleUserKeyImportFaiulre:userKeysMsg];
    }
    else if ([incomingMessage.messageName isEqualToString:[BBMChatKeysImportFailureMessage messageName]]) {
        //Error returned if we cannot update the chat keys with the supplied key data
        BBMChatKeysImportFailureMessage *chatKeyMsg = (BBMChatKeysImportFailureMessage *)incomingMessage;
        [self handleChatKeyImportFailure:chatKeyMsg];
    }
    else if([incomingMessage.messageName isEqualToString:[BBMProfileKeysImportFailureMessage messageName]]) {
        //Error returned if we cannot update the local users profile keys with the supplied key data
        BBMProfileKeysImportFailureMessage *profileKeyMsg = (BBMProfileKeysImportFailureMessage *)incomingMessage;
        [self handleProfileKeyImportFailure:profileKeyMsg];
    }
}


#pragma mark - Incoming Message Handlers

- (void)handleProfileKeyImportFailure:(BBMProfileKeysImportFailureMessage *)userKeyFailureMessage
{
    NSLog(@"Failure to import profile keys");
    if(self.profileKeyImportFailureHandler) {
        self.profileKeyImportFailureHandler();
    }
}


- (void)handleChatKeyImportFailure:(BBMChatKeysImportFailureMessage *)chatKeyFailureMessage
{
    NSLog(@"Failure to import chat keys for %@", chatKeyFailureMessage.mailboxIds);

    for(NSString *mailboxId in chatKeyFailureMessage.mailboxIds) {
        [self.pendingChatKeyImports removeObject:mailboxId];
    }

    if(self.chatKeyImportFailureHandler) {
        self.chatKeyImportFailureHandler(chatKeyFailureMessage.mailboxIds);
    }
}


- (void)handleUserKeyImportFaiulre:(BBMUserKeysImportFailureMessage *)userKeyFailureMessage
{
    NSLog(@"Failure to import user keys for %@", userKeyFailureMessage.regIds);
    if(self.userKeyImportFailureHandler) {
        self.userKeyImportFailureHandler(userKeyFailureMessage.regIds);
    }
}


- (void)handleIncomingChatKeyMesssage:(BBMChatKeyMessage *)chatKeyMessage
{
    NSString *key = chatKeyMessage.key;
    NSAssert(key != nil, @"key cannot be nil");

    BBMCipherText *keyCT = [self cipherTextFromKey:key];
    if(nil == keyCT) {
        return;
    }

    NSString *cookie = chatKeyMessage.cookie;
    BBMKeyRequest *request = self.chatKeyRequestsByCookie[cookie];
    if(nil == request) {
        NSLog(@"Error - could not find request for chatKey message");
        return;
    }
    [self.chatKeyRequestsByCookie removeObjectForKey:cookie];
    BBMChat *chat = [BBMAccess getChatForId:request.chatId];
    NSString *mailboxId = chat.mailboxId;

    void (^chatKeyWriteCallback)(NSString *mailboxId, BOOL success)  = ^(NSString *mailboxId, BOOL success) {
        if(success) {
            NSLog(@"Wrote new chat key for %@", request.chatId);
            [self setChatKeySynced:request.chatId];
        }
        if(request.callback) {
            request.callback(chat, success);
        }
    };

    [self.storageProvider writeChatKey:keyCT
                            forMailbox:mailboxId
                              callback:chatKeyWriteCallback];
}


- (void)handleIncomingProfileKeyMessage:(BBMProfileKeysMessage *)profileKeysMessage
{
    BBMProfileKeysMessage_PrivateKeys *privateKeys = profileKeysMessage.privateKeys;
    BBMProfileKeysMessage_PublicKeys *publicKeys = profileKeysMessage.publicKeys;

    BBMProfileKeys *profileKeys = [[BBMProfileKeys alloc] init];

    //Keys from BBM are just an opaque string.  We need to treat then as a string of bytes.
    profileKeys.publicKeyPair = [BBMKeyPair keyPairWithEncryptionKeyData:[publicKeys.encryption utf8Bytes]
                                                          signingKeyData:[publicKeys.signing utf8Bytes]
                                                                  encode:NO];

    //The private keys will be encrypted so supply the raw data so we don't double encode the
    //strings that are already encoded...
    profileKeys.privateKeyPair = [BBMKeyPair keyPairWithEncryptionKeyData:[privateKeys.encryption utf8Bytes]
                                                           signingKeyData:[privateKeys.signing utf8Bytes]
                                                                   encode:NO];

    BBMProfileKeys *encryptedKeys = [profileKeys encyptedProfileKeys:self.cachedManagementKeys];
    if(nil == encryptedKeys) {
        NSLog(@"Critical error: Unable in encrypt private keys");
        return;
    }


    NSError *error = nil;
    BBMKeyPair *encKeyPair  = [BBMSecUtil encryptKeyPair:self.cachedManagementKeys
                                                 rootKey:self.derivedRootKey
                                                   error:&error];

    [self.storageProvider writeProfileKeys:encryptedKeys managementKeys:encKeyPair callback:^(BOOL success) {
        if(!success) {
            self.managementKeyState = kBBMMgmtKeyRemoteWriteFailure;
        }else{
            self.managementKeyState = kBBMMgmtKeysReady;
            [BBMSecUtil persistManagementKeys:self.cachedManagementKeys];

            //We just created new managment keys.  We need to be confident that there are
            //no other endpoints using stale keys so remove them all.
            if(self.didGenerateNewManagementKeys) {
                [self.endpointManager deregisterAllNonLocalEndpoints];
            }
            [self setProfileKeySynced];
        }

    }];
}


#pragma - BBMDS Interface

- (NSString *)getProfileKeysState
{
    return [[[BBMEnterpriseService service] model] globalProfileKeysState];
}


- (void)requestProfileKeys
{
    BBMProfileKeysExportMessage *keyExportMsg = [[BBMProfileKeysExportMessage alloc] init];
    [[BBMEnterpriseService service] sendMessageToService:keyExportMsg];
}


- (void)setProfileKeys:(BBMProfileKeys *)keys
{
    if(nil == keys) {
        NSLog(@"Error: Attempt to send nil profile keys");
        return;
    }

    if(nil == self.cachedManagementKeys) {
        NSLog(@"Error: Attempted to set profile keys before we could decrypt them");
        return;
    }

    keys = [keys plaintextProfileKeys:self.cachedManagementKeys];
    if(nil == keys) {
        return;
    }

    BBMProfileKeysImportMessage_PrivateKeys *privateKeys = [[BBMProfileKeysImportMessage_PrivateKeys alloc] initWithEncryption:keys.privateKeyPair.encryptionKey
                                                                                                                       signing:keys.privateKeyPair.signingKey];
    BBMProfileKeysImportMessage_PublicKeys *publicKeys = [[BBMProfileKeysImportMessage_PublicKeys alloc] initWithEncryption:keys.publicKeyPair.encryptionKey
                                                                                                                    signing:keys.publicKeyPair.signingKey];;

    BBMProfileKeysImportMessage *exportMsg = [[BBMProfileKeysImportMessage alloc] initWithPrivateKeys:privateKeys publicKeys:publicKeys];
    [[BBMEnterpriseService service] sendMessageToService:exportMsg];
}


- (NSString *)requestChatKey:(NSString *)chatId
{
    BBMChatKeyExportMessage *keyExportMsg = [[BBMChatKeyExportMessage alloc] initWithChatId:chatId];
    [[BBMEnterpriseService service] sendMessageToService:keyExportMsg];
    return keyExportMsg.cookie;
}


- (void)setChatKey:(BBMCipherText *)key forMailboxId:(NSString *)mailboxId
{
    NSString *keyString = [self keyFromCipherText:key];
    if(nil == key) {
        return;
    }

    BBMChatKeysImportMessage_Keys *keys = [[BBMChatKeysImportMessage_Keys alloc] initWithKey:keyString
                                                                                   mailboxId:mailboxId];
    BBMChatKeysImportMessage *keyImportMsg = [[BBMChatKeysImportMessage alloc] initWithKeys:@[keys]];
    [[BBMEnterpriseService service] sendMessageToService:keyImportMsg];
}


- (void)setPublicEncryptionKey:(BBMKeyPair *)key forUser:(NSString *)regId
{
    BBMUserKeysImportMessage_Keys *keys = [[BBMUserKeysImportMessage_Keys alloc] initWithEncryptionPublicKey:key.encryptionKey
                                                                                                       regId:regId
                                                                                            signingPublicKey:key.signingKey];

    BBMUserKeysImportMessage *importMsg = [[BBMUserKeysImportMessage alloc] initWithKeys:@[keys]];
    [[BBMEnterpriseService service] sendMessageToService:importMsg];
}


- (void)setChatKeySynced:(NSString *)chatId
{
    NSLog(@"Setting chat key for chat %@ to Synced", chatId);
    NSDictionary *element = @{@"chatId" : chatId, @"keyState" : @"Synced"};
    BBMRequestListChangeMessage *syncChatKey = [[BBMRequestListChangeMessage alloc] initWithElements:@[element] type:@"chat"];
    [[BBMEnterpriseService service] sendMessageToService:syncChatKey];
}


- (void)setProfileKeySynced
{
    NSLog(@"Setting profile key state to Synced");
    NSDictionary* element = @{@"name":@"profileKeysState", @"value": @"Synced" };
    BBMRequestListChangeMessage *syncProfileKey = [[BBMRequestListChangeMessage alloc] initWithElements:@[element] type:@"global"];
    [[BBMEnterpriseService service] sendMessageToService:syncProfileKey];
}


#pragma mark - Encryption

- (void)changePassword:(NSString *)password
                 regId:(NSString *)regId
              callback:(PasswordUpdateCallback)callback
{
    if(self.managementKeyState != kBBMMgmtKeysReady || self.cachedManagementKeys == nil) {
        NSLog(@"Attempted to update the password before we've sync'd the management keys");
        callback(NO);
        return;
    }

    //Decrypted management keys...
    BBMKeyPair *managmentKeys = self.cachedManagementKeys;
    
    BBMKeyPair *updatedRootKey = [[BBMDerivedKey deriveKey:[password utf8Bytes]
                                                 sdkDomain:self.sdkServiceDomain
                                                    prefix:[regId utf8Bytes]] keyPair];


    NSError *error;
    BBMKeyPair *encryptedManagementKeys = [BBMSecUtil encryptKeyPair:managmentKeys
                                                             rootKey:updatedRootKey
                                                               error:&error];

    [self.storageProvider writeManagementKeys:encryptedManagementKeys callback:^(BOOL success) {
        if(success) {
            NSLog(@"updated password");
        }else{
            NSLog(@"failed to update password.");
        }
        callback(success);
    }];
}


- (NSString *)keyFromEncodedCipherText:(NSString *)cipherText
{
    BBMCipherText *key = [BBMCipherText withBase64JSONString:cipherText];
    if(nil == key) {
        NSLog(@"Invalid cipher text for key");
        return nil;
    }
    return [self keyFromCipherText:key];
}


- (NSString *)keyFromCipherText:(BBMCipherText *)key
{
    if(nil == self.cachedManagementKeys) {
        NSLog(@"Error: Unable to decrypt data.  No management keys");
        return nil;
    }

    NSError *error;
    NSData *keyData = [BBMSecUtil decryptData:key
                                          key:self.cachedManagementKeys.encryptionKeyData
                                      hmacKey:self.cachedManagementKeys.signingKeyData
                                        error:&error];
    if(error) {
        NSLog(@"Error decrypting key %@", error.localizedDescription);
        return nil;
    }
    return [[NSString alloc] initWithData:keyData encoding:NSUTF8StringEncoding];
}


- (BBMCipherText *)cipherTextFromKey:(NSString *)key
{
    if(nil == self.cachedManagementKeys) {
        return nil;
    }

    NSError *error;
    NSData *keyData = [key utf8Bytes];
    BBMCipherText *cipherText = [BBMSecUtil encryptData:keyData
                                                    key:self.cachedManagementKeys.encryptionKeyData
                                                hmacKey:self.cachedManagementKeys.signingKeyData
                                                  error:&error];

    if(error) {
        NSLog(@"Error encrypting key %@", error.localizedDescription);
        return nil;
    }

    return cipherText;
}

@end


@implementation BBMKeyRequest

+ (instancetype)requestWithChatId:(NSString *)chatId callback:(ChatKeySyncCallback)callback
{
    BBMKeyRequest *retVal = [[BBMKeyRequest alloc] init];
    retVal.callback = callback;
    retVal.chatId = chatId;
    return retVal;
}

@end
