// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

@class BBMCipherText;
@class BBMKeyPair;
@class BBMDerivedKey;

/*! @typedef Encryption/Decryption failure codes */
enum {
    kBBMSecInvalidInput = 999,
    kBBMSecInvalidKey = 1000,
    kBBMSecNonceGenerationFailed = 1001,
    kBBMSecCryptorCreationFailure = 1002,
    kBBMSecCipherTextValidationFailure = 1003,
    kBBMSecEncryptionFailure = 1004,
    kBBMSecDecryptionFailure = 1005,
    kBBMSecKeyGenerationFailed = 1006
};


@interface BBMSecUtil : NSObject

/*!
 @details
 Encrypts an NSData object using AES256-CTR and returns the cipher-text result (including the nonce,
 SHA-256 hmac generated using the \a hmacKey and the cipher text).
 @param data The data to encrypt
 @param key  The 256 bit AES key
 @param hmacKey The 256 bit key used to generate the hmac
 @param error the error or nil
 @return The cipher text or nil if an error occurred
 */
+ (BBMCipherText *)encryptData:(NSData *)data
                           key:(NSData *)key
                       hmacKey:(NSData *)hmacKey
                         error:(NSError **)error;

/*!
 @details
 Decrypts an BBMCipherText object using AES256-CTR and returns the plaintext result
 @param cipherText The data to decrypt
 @param key The 256 bit AES key
 @param hmacKey The 256 bit key used to generate the hmac
 @param error the error or nil
 @return NSData The decrypted data or nil if an error occurred
 */
+ (NSData *)decryptData:(BBMCipherText *)cipherText
                    key:(NSData *)key
                hmacKey:(NSData *)hmacKey
                  error:(NSError **)error;


/*!
 @details
 Generates a set of keys
 @param length The key length in bytes.
 @return BBMKeyPair the generated key pair
 */
+ (BBMKeyPair *)generateKeyPair:(NSError **)error length:(size_t)length;


/*!
 @details
 Generates a set of 32 bit keys.
 @return BBMKeyPair the generated key pair
 */
+ (BBMKeyPair *)generateKeyPair:(NSError **)error;


/*!
 @details
 Removes any management keys from the keychain
 */
+ (void)removeCachedManagementKeys;


/*!
 @details
 Retrieves a set of management keys from the local keychain
 @return The plain text management keys.  nil if they do not exist
*/
+ (BBMKeyPair *)getManagmentKeys;


/*!
 @details
 Persists a management key in the local keychain
 */
+ (void)persistManagementKeys:(BBMKeyPair *)keys;


/*!
 @details
 Encrypts a key pair with the given rootKey
 */
+ (BBMKeyPair *)encryptKeyPair:(BBMKeyPair *)key
                       rootKey:(BBMKeyPair *)encryptionKey
                         error:(NSError **)error;


/*!
 @details
 Decrypts a key pair with the given decryptionKey
 */
+ (BBMKeyPair *)decryptKeyPair:(BBMKeyPair *)key
                       rootKey:(BBMKeyPair *)decryptionKey
                        encode:(BOOL)encode
                         error:(NSError **)error;

/*!
 @details
 Generates a sha256 HMAC given the \a data (secret) and key
 */
+ (NSData *)sha256Hmac:(NSData *)data key:(NSData *)key;


@end


#pragma mark - Cipher Text

/*!
 Encapsulates a block of cipher text, the nonce used to encrypt it and the corresponding hmac
 All base64 encoding/decoding is done using URL-safe Base64.
 */
@interface BBMCipherText : NSObject

+ (BBMCipherText *)payload:(NSData *)payload
                     nonce:(NSData *)nonce
                      hmac:(NSData *)hmac;

/*!
 Returns a BBMCipherText object from a URLSafe base64 encoded string. ie: Reconstructs the cipher
 text from the output of toBase64EncodedJSON.
 */
+ (BBMCipherText *)withBase64JSONString:(NSString *)input;

/*!
 Creates a BBMCipherText object from it's dictionary representation.  This dictionary representation
 is suitable for using as JSON to store remotely.
 */
+ (BBMCipherText *)withJSONDictionary:(NSDictionary *)input;

@property (nonatomic, strong) NSData *payload;
@property (nonatomic, strong) NSData *nonce;
@property (nonatomic, strong) NSData *hmac;

- (NSDictionary *)toDictionary;
- (NSString *)toBase64EncodedJSON;

@end


#pragma mark - Derived Keys


@interface BBMDerivedKey : NSObject

+ (BBMDerivedKey *)deriveKey:(NSData *)secret
                   sdkDomain:(NSString *)domain
                      prefix:(NSData *)prefix;

/*! @brief The derived key represented as a 32 byte key-pair object */
- (BBMKeyPair *)keyPair;

@property (nonatomic, readonly) NSData *key;        //Full 64 byte key

@property (nonatomic, readonly) NSData *aesKey;     //Lower 32 bytes
@property (nonatomic, readonly) NSData *hmacKey;    //Upper 32 bytes

@end
