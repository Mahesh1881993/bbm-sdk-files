// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <BBMEnterprise/BBMEnterprise.h>
#import <CommonCrypto/CommonCrypto.h>

#import "BBMSecUtil.h"
#import "BBMKeyPair.h"
#import "NSString+Base64.h"

//Set to NO to always prompt the user for their password.  Setting to YES will
//save the management keys to the keychain  BBME SDK Pre-KMS DRK
#define USE_KEYCHAIN YES

static NSString *const kDerivedKeyDiversifier = @"BBME SDK Pre-KMS DRK";

static NSString *const kCryptoErrorDomain = @"com.blackberry.bbm.crypto";

static size_t const kManagementKeyLengthBytes =  kCCKeySizeAES256;     //For AES256
static size_t const kManagementHmacKeyLengthBytes = 32;                //Per the KMS Spec

static NSString *const kManagmentKeyTag  = @"com.blackberry.bbme.sdk.managementKey-a";
static NSString *const kManagmentHmacKeyTag  = @"com.blackberry.bbme.sdk.managementHmacKey-a";


@implementation BBMSecUtil : NSObject

+ (BBMKeyPair *)getManagmentKeys
{
    if(!USE_KEYCHAIN) {
        NSLog(@"Management key load failed. Keychain disabled");
        return nil;
    }

    NSString *managementKey = [BBMKeyGenerator getKeyForTag:kManagmentKeyTag];
    NSString *managementHmacKey = [BBMKeyGenerator getKeyForTag:kManagmentHmacKeyTag];

    if(nil == managementKey || nil == managementHmacKey) {
        return nil;
    }

    BBMKeyPair *retVal = [BBMKeyPair keyPairWithEncryptionKeyData:[managementKey base64DecodedData]
                                                   signingKeyData:[managementHmacKey base64DecodedData]
                                                           encode:YES];
    return retVal;
}


+ (void)removeCachedManagementKeys
{
    [BBMKeyGenerator removeKeyForTag:kManagmentKeyTag];
    [BBMKeyGenerator removeKeyForTag:kManagmentHmacKeyTag];
}


+ (void)persistManagementKeys:(BBMKeyPair *)keys
{
    NSError *error;
    BOOL didSaveE = [BBMKeyGenerator persistKey:keys.encryptionKey
                                        forTag:kManagmentKeyTag
                                         error:&error];

    if(error || !didSaveE) {
        NSLog(@"Error saving encryption key to keychain: %@", error.localizedDescription);
        error = nil;
    }

    BOOL didSaveS =  [BBMKeyGenerator persistKey:keys.signingKey
                                         forTag:kManagmentHmacKeyTag
                                          error:&error];

    if(error || !didSaveS) {
        NSLog(@"Error saving signing key to keychain: %@", error.localizedDescription);
    }
}

+ (BBMKeyPair *)generateKeyPair:(NSError **)error
{
    return [self generateKeyPair:error length:(size_t)kManagementKeyLengthBytes];
}

+ (BBMKeyPair *)generateKeyPair:(NSError **)error length:(size_t)length
{
    NSData* (^generateKey)(void) = ^NSData *{
        NSMutableData *key = [NSMutableData dataWithLength:length];
        if (SecRandomCopyBytes(kSecRandomDefault, length, key.mutableBytes)) {
            return nil;
        }
        return key;
    };

    NSData *mgmtKey = generateKey();
    if(nil == mgmtKey) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain
                                         code:kBBMSecKeyGenerationFailed
                                     userInfo:@{@"keyType":@"managementKey"}];
        }
        return nil;
    }

    NSData *mgmtHmacKey = generateKey();
    if(nil == mgmtHmacKey) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain
                                         code:kBBMSecKeyGenerationFailed
                                     userInfo:@{@"keyType":@"managementHmacKey"}];
        }
        return nil;
    }

    BBMKeyPair *keyPair = [BBMKeyPair keyPairWithEncryptionKeyData:mgmtKey
                                                    signingKeyData:mgmtHmacKey
                                                            encode:YES];
    return keyPair;
}


+ (BBMKeyPair *)encryptKeyPair:(BBMKeyPair *)key
                       rootKey:(BBMKeyPair *)encryptionKey
                         error:(NSError **)error
{
    BBMCipherText *encKey = [self encryptData:key.encryptionKeyData
                                          key:encryptionKey.encryptionKeyData
                                      hmacKey:encryptionKey.signingKeyData
                                        error:error];

    if(*error) {
        return nil;
    }

    BBMCipherText *signKey = [self encryptData:key.signingKeyData
                                           key:encryptionKey.encryptionKeyData
                                       hmacKey:encryptionKey.signingKeyData
                                         error:error];

    if(*error) {
        return nil;
    }

    BBMKeyPair *keyPair = [BBMKeyPair keyPairWithEncryptionKeyJson:encKey.toDictionary
                                                    signingKeyJson:signKey.toDictionary];
    return keyPair;
}


+ (BBMKeyPair *)decryptKeyPair:(BBMKeyPair *)key
                       rootKey:(BBMKeyPair *)decryptionKey
                        encode:(BOOL)encode
                         error:(NSError **)error
{
    BBMCipherText *encKeyCT = [BBMCipherText withBase64JSONString:key.encryptionKey];
    BBMCipherText *signKeyCT = [BBMCipherText withBase64JSONString:key.signingKey];

    if(nil == encKeyCT || nil == signKeyCT) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain
                                        code:kBBMSecInvalidKey
                                    userInfo:@{@"reason" : @"Could not deserialze the input key"}];
        }
        return nil;
    }

    NSData *encKey = [self decryptData:encKeyCT
                                   key:decryptionKey.encryptionKeyData
                               hmacKey:decryptionKey.signingKeyData
                                 error:error];

    if(error && *error) {
        return nil;

    }

    NSData *signKey = [self decryptData:signKeyCT
                                    key:decryptionKey.encryptionKeyData
                                hmacKey:decryptionKey.signingKeyData
                                  error:error];

    if(error && *error) {
        return nil;
    }

    return [BBMKeyPair keyPairWithEncryptionKeyData:encKey signingKeyData:signKey encode:encode];
}


+ (BBMCipherText *)encryptData:(NSData *)data
                           key:(NSData *)key
                       hmacKey:(NSData *)hmacKey
                         error:(NSError **)error
{

    if(0 == (data && key && hmacKey)) {
        if(error){
            *error = [NSError errorWithDomain:kCryptoErrorDomain code:kBBMSecInvalidInput userInfo:nil];
        }
        return nil;
    }

    //For this implementation, both the encryption and signing keys must be 32 bytes
    if(key.length != kManagementKeyLengthBytes || hmacKey.length != kManagementHmacKeyLengthBytes) {
        if(error){
            *error = [NSError errorWithDomain:kCryptoErrorDomain code:kBBMSecInvalidKey userInfo:nil];
        }
        return nil;
    }

    NSMutableData *nonce = [NSMutableData dataWithLength:kCCBlockSizeAES128];
    if (SecRandomCopyBytes(kSecRandomDefault, kCCBlockSizeAES128, nonce.mutableBytes)) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain code:kBBMSecNonceGenerationFailed userInfo:nil];
        }
       return nil;
    }

    CCCryptorRef cryptor = NULL;;
    CCCryptorStatus result = CCCryptorCreateWithMode(kCCEncrypt,
                                                     kCCModeCTR,
                                                     kCCAlgorithmAES,
                                                     ccNoPadding,
                                                     nonce.bytes,
                                                     [key bytes],
                                                     [key length],
                                                     0,
                                                     0,
                                                     0,
                                                     kCCModeOptionCTR_BE, //Deprecated but required
                                                     &cryptor);

    if (result != kCCSuccess) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain code:kBBMSecCryptorCreationFailure userInfo:nil];
        }
        return nil;
    }

    size_t cipherTextLen = 0;
    size_t bufferSize = data.length;
    uint8_t *cipherTextBuffer = malloc(bufferSize * sizeof(uint8_t));

    result = CCCryptorUpdate(cryptor,
                             [data bytes],
                             [data length],
                             cipherTextBuffer,
                             bufferSize,
                             &cipherTextLen);

    BBMCipherText *retVal = nil;
    NSAssert(cipherTextLen == data.length, @"Cipher text length should match the plaintext");
    if (result == kCCSuccess) {
        NSData *cipherText = [NSData dataWithBytes:cipherTextBuffer length:cipherTextLen];
        NSData *hmac = [self sha256Hmac:cipherText key:hmacKey];
        retVal = [BBMCipherText payload:cipherText nonce:nonce hmac:hmac];

    }else{
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain code:kBBMSecEncryptionFailure userInfo:nil];
        }
    }

    free(cipherTextBuffer);
    CCCryptorRelease(cryptor);

    return retVal;
}


+ (NSData *)decryptData:(BBMCipherText *)cipherText
                    key:(NSData *)key
                hmacKey:(NSData *)hmacKey
                  error:(NSError **)error
{
    if(0 == (cipherText && key && hmacKey)) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain code:kBBMSecInvalidInput userInfo:nil];
        }
        return nil;
    }

    if(key.length != kManagementKeyLengthBytes || hmacKey.length != kManagementHmacKeyLengthBytes) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain code:kBBMSecInvalidKey userInfo:nil];
        }
        return nil;
    }

    BOOL valid = [self validateCipherText:cipherText hmacKey:hmacKey];
    if(!valid) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain
                                         code:kBBMSecCipherTextValidationFailure
                                     userInfo:nil];
        }
        return nil;
    }

    CCCryptorRef cryptor = NULL;;
    CCCryptorStatus result = CCCryptorCreateWithMode(kCCDecrypt,
                                                     kCCModeCTR,
                                                     kCCAlgorithmAES,
                                                     ccNoPadding,
                                                     [cipherText.nonce bytes],
                                                     [key bytes],
                                                     [key length],
                                                     0,
                                                     0,
                                                     0,
                                                     kCCModeOptionCTR_BE, //Deprecated but required
                                                     &cryptor);

    if (result != kCCSuccess) {
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain
                                         code:kBBMSecCryptorCreationFailure
                                     userInfo:nil];
        }
        return nil;
    }

    size_t plainTextLen;
    //The decrypted text should always have a length equal to the ciphertext length
    size_t plainTextBufferSize = cipherText.payload.length;
    uint8_t *plainTextBuffer = malloc(plainTextBufferSize * sizeof(uint8_t));
    result = CCCryptorUpdate(cryptor,
                             [cipherText.payload bytes],
                             [cipherText.payload length],
                             plainTextBuffer,
                             plainTextBufferSize,
                             &plainTextLen);


    NSAssert(plainTextLen == [cipherText.payload length], @"Cipher text length should match the plaintext");
    NSData *ret = nil;
    if (result == kCCSuccess) {
        ret = [NSData dataWithBytes:plainTextBuffer length:plainTextLen];
    }else{
        if(error) {
            *error = [NSError errorWithDomain:kCryptoErrorDomain
                                         code:kBBMSecDecryptionFailure
                                     userInfo:nil];
        }
    }

    free(plainTextBuffer);
    CCCryptorRelease(cryptor);

    return ret;
}


+ (NSData *)sha256Hmac:(NSData *)data key:(NSData *)key
{
    NSMutableData* hash = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, key.bytes, key.length, data.bytes, data.length, hash.mutableBytes);
    return hash;
}


+ (BOOL)validateCipherText:(BBMCipherText *)cipherText hmacKey:(NSData *)key
{
    NSData *hash = [self sha256Hmac:cipherText.payload key:key];
    return [hash isEqualToData:cipherText.hmac];
}


@end




@implementation BBMCipherText

static NSString *const kPayloadKey  = @"payload";
static NSString *const kNonceKey    = @"nonce";
static NSString *const kMacKey      = @"mac";

+ (BBMCipherText *)payload:(NSData *)payload nonce:(NSData *)nonce hmac:(NSData *)hmac
{
    BBMCipherText *retVal = [[BBMCipherText alloc] init];
    retVal.payload  = payload;
    retVal.nonce = nonce;
    retVal.hmac = hmac;
    return retVal;
}


+ (BBMCipherText *)withJSONDictionary:(NSDictionary *)input
{
    NSData *payload = [NSData dataWithBase64EncodedURLSafeString:input[kPayloadKey]];
    NSData *nonce = [NSData dataWithBase64EncodedURLSafeString:input[kNonceKey]];
    NSData *hmac = [NSData dataWithBase64EncodedURLSafeString:input[kMacKey]];

    if(nil == payload | nil == nonce || nil == hmac) {
        NSLog(@"Invalid cipher text JSON");
        return nil;
    }

    return [self payload:payload nonce:nonce hmac:hmac];
}


+ (BBMCipherText *)withBase64JSONString:(NSString *)input
{
    NSData *decoded = [NSData dataWithBase64EncodedURLSafeString:input];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:decoded options:0 error:nil];
    return [self withJSONDictionary:json];
}


- (NSDictionary *)toDictionary
{
    return @{kPayloadKey : [self.payload base64URLSafeEncodedString],
             kNonceKey   : [self.nonce base64URLSafeEncodedString],
             kMacKey     : [self.hmac base64URLSafeEncodedString]   };
}


- (NSString *)toBase64EncodedJSON
{
    NSDictionary *dict = [self toDictionary];
    NSData *json = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    return [json base64EncodedStringWithOptions:0];
}

@end



#pragma mark - Derived Keys

@interface BBMDerivedKey()
@property (nonatomic, readwrite) NSData *key;
@property (nonatomic, readwrite) NSData *aesKey;
@property (nonatomic, readwrite) NSData *hmacKey;
@end

@implementation BBMDerivedKey


+ (BBMDerivedKey *)deriveKey:(NSData *)secret
                   sdkDomain:(NSString *)domain
                      prefix:(NSData *)prefix
{
    static const Byte separator[] = {0,0,0,1};
    NSData *diversifier = [kDerivedKeyDiversifier utf8Bytes];
    NSLog(@"diversifier --> %@", diversifier);
    NSLog(@"kDerivedKeyDiversifier --> %@", kDerivedKeyDiversifier);

    NSMutableData *data = [NSMutableData data];
    [data appendData:prefix];
    [data appendData:domain.utf8Bytes];
    [data appendData:secret];
    [data appendBytes:separator length:sizeof(separator)];
    [data appendData:diversifier];
     NSLog(@"data --> %@", data);

    const size_t digestLen = CC_SHA512_DIGEST_LENGTH;

    unsigned char digest[digestLen];
    CC_SHA512(data.bytes, (CC_LONG)data.length, digest);

    NSData *keyData = [NSData dataWithBytes:digest length:digestLen];

    BBMDerivedKey *key = [[BBMDerivedKey alloc] init];
    key.aesKey = [keyData subdataWithRange:NSMakeRange(0, kManagementKeyLengthBytes)];
    key.hmacKey = [keyData subdataWithRange:NSMakeRange(kManagementKeyLengthBytes, kManagementKeyLengthBytes)];
    key.key = keyData;
    return key;
}

- (BBMKeyPair *)keyPair
{
    return [BBMKeyPair keyPairWithEncryptionKeyData:self.aesKey
                                     signingKeyData:self.hmacKey
                                             encode:YES];
}



@end
