// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import <Foundation/Foundation.h>

typedef void (^StatisticsLoaderCallback)(NSDictionary *statistics);

/*!
 @details
 Utility class to access the analytics available in the BBM Enterprise SDK.
 */
@interface BBMAnalyticsReporter : NSObject

/*!
 @details
 Makes the content of the stat list available in a dictionary that contains a simple JSON format.
 Example output:
 {
    "bus.bbmToken" =     {
        ok = 1;
        try = 1;
    };
    "bus.register" =     {
        ok = 2;
        try = 2;
    };
    "mailbox.lookup" =     {
        ok = 11;
        try = 12;
    };
    "mailbox.lookupAll" =     {
        ok = 1;
        try = 1;
    };
    msg =     {
        Text = 1;
    };
    setup =     {
        ok = 1;
        try = 1;
    };
 }

 @param callback will be called once the statistics are obtained.
 */
- (void)getJSONStatistics:(StatisticsLoaderCallback)callback;

/*!
 @details
 Makes the content of the stat list available in a dictionary that contains
 Example output:
 {
    "bus.bbmToken.ok" = 1;
    "bus.bbmToken.try" = 1;
    "bus.register.ok" = 2;
    "bus.register.try" = 2;
    "mailbox.lookup.ok" = 11;
    "mailbox.lookup.try" = 12;
    "mailbox.lookupAll.ok" = 1;
    "mailbox.lookupAll.try" = 1;
    "msg.Text" = 1;
    "setup.ok" = 1;
    "setup.try" = 1;
 }

 @param callback will be called once the statistics are obtained. The callback returns a dictionary
 whose key and values are strings.
 */
- (void)getStringStatistics:(StatisticsLoaderCallback)callback;

/*!
 @details
 Calling this method results in the SDK clearing the statistics. This is typically done after the
 existing data has been fetched by calling getJSONStatistics or getStringStatistics and the results
 have been stored off the device. 
 */
- (void)commitStatistics;

@end
