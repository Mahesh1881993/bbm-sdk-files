// Copyright (c) 2018 BlackBerry. All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this
// software, or any work that includes all or part of this software.
//
// This sample code was created by BlackBerry using SDKs from Apple Inc.
// and may contain code licensed for use only with Apple products.
// Please review your Apple SDK Agreement for additional details.
//
// This file may contain contributions from others. Please review this entire
// file for other proprietary rights or license notices.

#import "BBMAnalyticsReporter.h"
#import <BBMEnterprise/BBMEnterprise.h>

#import <BBMEnterprise/BBMStat.h>
#import <BBMEnterprise/ObservableMonitorSet.h>

@interface BBMAnalyticsReporter()
@property (nonatomic, strong) ObservableMonitorSet *monitorSet;
@property (nonatomic, copy) StatisticsLoaderCallback jsonStatisticsCallback;
@property (nonatomic, copy) StatisticsLoaderCallback stringStatisticsCallback;

@end

@implementation BBMAnalyticsReporter

- (id)init
{
    if ((self = [super init])) {
        self.monitorSet = [[ObservableMonitorSet alloc] init];
    }
    return self;
}
- (void)getJSONStatistics:(StatisticsLoaderCallback)callback
{
    self.jsonStatisticsCallback = callback;
    //Request the stats list and then look at the list content.
    [self requestStats];

    NSMutableDictionary *statsDictionary = [NSMutableDictionary dictionary];
    typeof(self) __weak weakSelf = self;
    BOOL (^monitorBlock)(void) = ^BOOL(){
        BBMLiveList *stats = [[BBMEnterpriseService service] model].stat;
        if(stats.bbmState == kBBMStatePending) {
            return NO;
        }
        for (BBMStat *stat in stats) {
            NSString *statId = stat.objectName;

            id statValue;
            //If part contains more than one object then we pair the count and part arrays. Otherwise
            //use the first and only value in count.
            if(stat.part.count > 0) {
                statValue = [NSDictionary dictionaryWithObjects:stat.count forKeys:stat.part];
            }
            else if(stat.count.firstObject != nil) {
                long long value = [stat.count.firstObject unsignedLongLongValue];
                statValue = @(value);
            }
            if (statId.length > 0 && statValue != nil) {
                statsDictionary[statId] = statValue;
            }
        }
        if(weakSelf.jsonStatisticsCallback) {
            weakSelf.jsonStatisticsCallback(statsDictionary);
        }
        return YES;
    };
    [self.monitorSet addMonitorActivatedWithName:@"getJSONStatisticsMonitor"
                            selfTerminatingBlock:monitorBlock];
}

- (void)getStringStatistics:(StatisticsLoaderCallback)callback
{
    self.stringStatisticsCallback = callback;
    //Request the stats list and then look at the list content.
    [self requestStats];

    NSMutableDictionary *statsDictionary = [NSMutableDictionary dictionary];
    typeof(self) __weak weakSelf = self;
    BOOL (^monitorBlock)(void) = ^BOOL(){
        BBMLiveList *stats = [[BBMEnterpriseService service] model].stat;
        if(stats.bbmState == kBBMStatePending) {
            return NO;
        }
        for (BBMStat *stat in stats) {
            NSString *statId = stat.objectName;

            id statValue;
            if(stat.part.count > 0) {
                statValue = [NSDictionary dictionaryWithObjects:stat.count forKeys:stat.part];
                for(int i =0; i < stat.part.count; i++) {
                    long long count = [stat.count[i] unsignedLongLongValue];
                    NSString *part = stat.part[i];
                    NSString *key = [NSString stringWithFormat:@"%@.%@", statId, part];
                    statsDictionary[key] = [NSString stringWithFormat:@"%lld",count];
                }
            }
            else if(stat.count.firstObject != nil) {
                long long value = [stat.count.firstObject unsignedLongLongValue];
                statsDictionary[statId] = [NSString stringWithFormat:@"%lld",value];
            }

        }
        if(weakSelf.stringStatisticsCallback) {
            weakSelf.stringStatisticsCallback(statsDictionary);
        }
        return YES;;
    };
    [self.monitorSet addMonitorActivatedWithName:@"getJSONStatisticsMonitor"
                            selfTerminatingBlock:monitorBlock];
}

- (void)commitStatistics
{
    BBMStatsCommittedMessage *msg = [[BBMStatsCommittedMessage alloc] init];
    [[BBMEnterpriseService sharedService] sendMessageToService:msg];
}

- (void)requestStats
{
    //Stats list doesn't support listChange so we send a requestListAll to get all stats
    BBMRequestListAllMessage *msg = [[BBMRequestListAllMessage alloc] initWithType:@"stat"];
    [[BBMEnterpriseService sharedService] sendMessageToService:msg];
}
@end
