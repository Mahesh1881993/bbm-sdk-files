# Support

The Support folder contains classes common to all sample applications as well as several useful utilities.  Refer to the sample appliction documentation as well as the headers for details on how to utilize these support classes in your application.
