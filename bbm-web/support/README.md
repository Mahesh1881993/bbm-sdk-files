# Support

The support module contains a suite of basic implementations you can use to fast track your application development.

## Features

- Identity management using OAuth and authentication.
- Authentication with [Google Sign-In](https://developers.google.com/identity/) or [Azure Active Directory v2.0 authentication API](https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-appmodel-v2-overview).
- Contact list management using Firebase database or Azure Active Directory with [Microsoft Graph API](https://developer.microsoft.com/en-us/graph/docs/concepts/overview).
- Key storage reference implementation using Firebase database.
- WebComponents:
  - bbmChat to conveniently display everything needed to interact with a chat.
  - bbmChatHeader to display metadata for a chat and access chat operations.
  - bbmChatInput to send messages and files to a chat.
  - bbmChatMessageList to display the messages in a chat, each in a custom bubble.
  - bbmRichChatMessageList to display the messages in a chat, each in a default rich bubble.
  - bbmChatList to display a list of chats to which the user is subscribed.
  - bbmChatMessageStateList to display which intended recipients have received or read a message.
  - bbmChatTypingNotification to display which users are currently typing a message in a chat.
- Utility classes:
  - TimeRangeFormatter to display timestamps on messages, in a variable format, with more recent messages showing a time, and older messages showing just a date.

## Identity Management

The reference identity management implementation in the support module uses OAuth authentication. The GenericAuthHelper class can be used to perform authentication against a specified OAuth 2.0 server. If the sign-in succeeds, an OAuth token will be requested and sent to the BBM Enterprise SDK. The GenericAuthHelper class can also be used to authenticate with [Azure Active Directory v2.0 authentication API](https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-appmodel-v2-overview) using JSON Web Tokens (JWT).

## User Management

The support module includes a simple user management solution using Firebase database. Users are stored using their Firebase UID as the primary key. All users who authenticate successfully with the BBM Enterprise SDK are added to the users table.

```JSON
"users" : {
  "UID" : {
    "avatarUrl" : "https://avatar.com/testavatar.png",
    "email" : "test@test.com",
    "name" : "John Smith",
    "regId" : "123456789000"
  }
}
```
*Firebase users schema*

The support module includes a simple user management solution using Azure Active Directory.  It will automatically store the BBM Enterprise SDK registration ID for each user in the Azure Active Directory using  [Microsoft Graph API](https://developer.microsoft.com/en-us/graph/docs/concepts/overview) custom extension.  Only other Azure Active Directory users that have a BBM Enterprise SDK registration ID will be returned to the app.

## User Key Storage

The support module includes a reference [Cloud Key Storage](../../../cloudKeyStorage.html) implementation using Firebase realtime database. The security rules and storage schema are described in the [Firebase Cloud Key Storage](../../../firebaseCloudKeyStorage.html) guide.

The BBM Enterprise SDK makes key requests to a [KeyStorageProvider](../../../../reference/javascript/BBMEnterprise.KeyProviderInterface.html). A sample FirebaseKeyStorageProvider is included. If your application uses a different cloud storage solution you can provide your own implementation of the KeyProviderInterface.

## WebComponents

The support module includes several web components to simplify display of some parts of the user interface. To display the list of chats, create a bbmChatList. Each chat will be displayed in a custom bubble controlled by a template applied to the chat list.

The bbmChat will display chat metadata, incoming messages, and an area to choose text or files for outgoing messages. The bbmChat component is a composition of other components. Using bbmChat is the quickest way to get started on displaying a chat. However the components that make up a bbmChat can also be instantiated individually, to increase flexibility. Visit the [RichChat](../RichChat/README.md) example for a guide on how to use the bbmChat component.

The bbmRichChatMessageList displays message bubbles for text messages, files and pictures and is the default bubble for bbmChat. To display different types of bubbles, or to display these types differently, a bbmChatMessageList can be used. A bbmChatMessageList requires an html template to describe a bubble and will instantiate this template for each message rather than using a default bubble.
