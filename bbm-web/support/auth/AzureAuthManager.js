//****************************************************************************
// Copyright 2018 BlackBerry.  All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this software, or
// any work that includes all or part of this software.
//

"use strict";

/**
 * Manages authentication using Azure Auth.
 * This will handle both authenticating to get an access token to use with the
 * BBM Enterprise server and another access token for calling the
 * Microsoft Graph API's.
 * This takes 2 separate auth configuration objects to be used when authenticating
 * for both access tokens.  In most cases the configuration will be the same
 * except for the scope since the permissions will be for different resources.
 * This will cache both access tokens and handle requesting new ones when the
 * cached ones have expired and are requested.
 *
 * {@link https://developer.blackberry.com/files/bbm-enterprise/documents/guide/html/azureIdentityManagement.html}
 *
 * @param {Support.Auth.GenericAuthHelper.JWTConfig} authForBbmSdkConfig
 *   The configuration required to authenticate with the Azure auth service
 *   with a scope for the BBM Enterprise server.
 *   {@link https://developer.blackberry.com/files/bbm-enterprise/documents/guide/html/identityManagement.html}
 *
 * @param {Support.Auth.GenericAuthHelper.JWTConfig} authForUserManagerConfig
 *   The configuration required to authenticate with the Azure auth service
 *   with a scope for the Microsoft Graph API permissions that are required by
 *   the application.
 *   This would normally contain at least
 *   "https://graph.microsoft.com/User.ReadWrite https://graph.microsoft.com/User.ReadBasic.All"
 *   {@link https://developer.blackberry.com/files/bbm-enterprise/documents/guide/html/azureUserManagement.html}
 *
 * @memberof Support.Auth
 * @class AzureAuthManager
 */
function AzureAuthManager(authForBbmSdkConfig, authForUserManagerConfig) {
  var m_authForBbmSdkConfig = authForBbmSdkConfig;
  var m_authForUserManagerConfig = authForUserManagerConfig;  
  var m_helperForBbmSdk = new GenericAuthHelper();
  var m_helperForUserManager = new GenericAuthHelper();

  this.authenticate = function() {
    return new Promise(function(resolve, reject) {
      //First get token for BBM Enterprise server
      m_helperForBbmSdk.authenticate(m_authForBbmSdkConfig).then(function(userInfoAndTokens1) {
        //See if the preferred_username is set in a token.
        //This is not mandatory, but can be used to prevent potentially prompting
        //the user to login twice if they are signed in with multiple accounts
        //in the same browser. This would happen if they have work and personal
        //accounts.
        var token = userInfoAndTokens1.idToken || userInfoAndTokens1.accessToken;
        if (token) {
          try {
            var jwtInfo = m_helperForBbmSdk.parseJWTToken(token);
            if (jwtInfo && jwtInfo.preferred_username) {
              //set it in the config for Azure. This will suggest to Azure auth service to use the
              //same user account that the user just selected instead of prompting again.
              m_authForUserManagerConfig.login_hint = jwtInfo.preferred_username;
            }
          } catch (err) {
            console.log("Error occured trying to find preferred_username, will continue without it. err="+err);
          }
        }
        
        //Get access token to access the Microsoft Graph API. This is separate from the one used to authenticate
        //the user and get access token for the SDK but we will validate they both use the same AD user ID.
        m_helperForUserManager.authenticate(m_authForUserManagerConfig).then(function(userInfoAndTokens2) {
          //validate that we have user ID's and the user ID in both match.
          //This is to confirm that the user didn't select different accounts for
          //the 2 auth requests (eg. work and personal account).
          if (userInfoAndTokens1.userInfo.userId && userInfoAndTokens1.userInfo.userId === userInfoAndTokens2.userInfo.userId) {
           
            // Skipping the User API call and Getting userInfo from the JWT Token.
            var jwtInfo = m_helperForBbmSdk.parseJWTToken(userInfoAndTokens1.accessToken); 
         
            resolve(jwtInfo);
         
          } else {
            //we don't want to allow using different accounts.
            reject(new Error("User used different account with id="+userInfoAndTokens2.userInfo.userId
              +" to login for MS Graph than for the BBM Enterprise SDK with userId="+userInfoAndTokens1.userInfo.userId));
          }
        }).catch(function(err) {
          reject(err);
        });
      }).catch(function(err) {
        reject(err);
      });
    });
  };

  /**
   * Get the access token from the Azure auth service that can be used when
   * creating the {BBMEnterprise}.
   * This will return a cached token if it is still valid or it will request a
   * new one if the cached one has expired.
   *
   * @returns {Promise<string>}
   *   The promise of an access token that can be sent to the BBM Enterprise server.
   */
  this.getBbmSdkToken = function() {
    return m_helperForBbmSdk.getOAuthAccessToken(m_authForBbmSdkConfig);
  };

  /**
   * Get the access token from the Azure auth service that can be used by
   * the {Support.AzureUserManager}.
   * This will return a cached token if it is still valid or it will request a
   * new one if the cached one has expired.
   *
   * @returns {Promise<string>}
   *   The promise of an access token for the user manager.
   */
  this.getUserManagerToken = function() {
    return m_helperForUserManager.getOAuthAccessToken(m_authForUserManagerConfig);
  };

  /**
   * Get the access token from the Azure auth service that can be used by
   * the {Support.CosmosDbKeyProvider} or other key provider.
   * This will return a cached token if it is still valid or it will request a
   * new one if the cached one has expired.
   *
   * @returns {Promise<string>}
   *   The promise of an access token for a key provider.
   */
  this.getKeyProviderToken = function() {
    return m_helperForBbmSdk.getOAuthAccessToken(m_authForBbmSdkConfig);
  };
};
