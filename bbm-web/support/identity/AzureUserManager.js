//****************************************************************************
// Copyright 2018 BlackBerry.  All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this software, or
// any work that includes all or part of this software.
//
"use strict";
// Url to store custom extensions to, used to store regId.
var MS_GRAPH_API_ME_EXTENSIONS_URL = 'https://graph.microsoft.com/v1.0/me/extensions';
// Url to get data for all user including extensions which could contain the regId.
var MS_GRAPH_API_USERS_URL = 'https://graph.microsoft.com/v1.0/users?$select=id,displayName,userPrincipalName&$expand=extensions';
var MS_GRAPH_API_ME_AVATAR_URL = 'https://graph.microsoft.com/beta/me/Photo/$value';
var MS_GRAPH_API_USER_AVATAR_URL_PREFIX = 'https://graph.microsoft.com/beta/users/';
var MS_MY_GROUPS = 'https://graph.microsoft.com/v1.0/me/memberOf'
var MS_GET_GROUPS_MEMBERS = "https://graph.microsoft.com/v1.0/groups/<group_id>/members"
var MS_GRAPH_API_USER_AVATAR_URL_POSTFIX = '/Photo/$value';
var GET_GROUP_THEME = 'https://fg-qa.mobistreamsolutions.com/api/communities/<group_id>/';
// The name of our custom Active Directory extension that the regId is stored with.
var CUSTOM_AD_EXTENSION_ID = 'com.bbm.sdk.example.azure.userValues';

// Url to get data for local user including extensions which could contain the regId.
var MS_GRAPH_API_ME_URL = 'https://graph.microsoft.com/v1.0/me?$select=id,displayName,userPrincipalName&$expand=extensions';

// Url to get User Profile
var MS_GRAPH_API_ME_PROFILE_URL = 'https://graph.microsoft.com/v1.0/me';



/**
 * Contains functions for storing and retrieving users from a Azure database.
 *
 * @memberof Support
 * @class AzureUserManager
 */
(function(AzureUserManager) {
    // This function will always be called with the correct global context to
    // which this module may export.
    var global = this;
    // Where do we place the module?  Do we have an exports object to use?
    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = AzureUserManager();
        }
        exports.AzureUserManager = AzureUserManager();
    } else {
        global.AzureUserManager = AzureUserManager();
    }
}).call(this, function() {
    // To be thrown when user failed to register in database
    function RegistrationFailedError(message) {
        this.name = 'RegistrationFailedError';
        this.message = (message || '');
    }
    RegistrationFailedError.prototype = Error.prototype;
    // To be thrown if AzureUserManager has invalid parameter
    function InvalidArgumentError(message) {
        this.name = 'InvalidArgumentError';
        this.message = (message || '');
    }
    InvalidArgumentError.prototype = Error.prototype;
    var log = text => console.log(`AzureUserManager - ${text}`);
    /**
     * Tracks user contact list changes and triggers following events:
     *  - user_added: when a new user is added
     *  - user_changed: when existing user is changed
     *  - user_removed: when existing user is removed - will not happen with Azure
     * Provides functions to get user user information:
     *  - getUserAvatar: gets user avatar image URL
     *  - getUserName: gets user name
     *  - getUser: gets user information (regId, name, avatar)
     *  - loadUserAvatar: loads avatar photo image for specified user
     * @param {function} getTokenFunction
     *   Function to get an access token with required permissions for
     *   Microsoft Graph API.  This will be called for each HTTP request made to
     *   Microsoft Graph API so it should cache the token and acquire a new one
     *   when the cached one has expired.
     * @throws {RegistrationFailedError}
     *  Thrown when user failed to register
     * @throws {InvalidArgumentError}
     *  Thrown when userInfo parameter is invalid 
     */
    var AzureUserManager = function(localUserRegId, getTokenFunction) {
        if (typeof localUserRegId !== 'string') {
            throw new InvalidArgumentError('localUserRegId must be a string, it is: ' + typeof localUserRegId);
        }
        if (typeof getTokenFunction !== 'function') {
            throw new InvalidArgumentError('getTokenFunction must be an function.');
        }
        var m_localUserRegId = localUserRegId;
        var m_userMap = {};
        var m_avatarBlobCache = new Map();
        var m_eventListeners = {
            user_added: [],
            user_changed: [],
            user_removed: []
        }
        // Get extension data from Active Directory user object.
        // Our extension is probably the first one, but if multiple apps use the same
        // AD and add their extensions then make sure we use ours
        var getCustomExtensionValues = adUser => {
            if (!adUser) {
                console.error("getCustomExtensionValues: missing adUser");
            } else if (!adUser.extensions) {
                console.log("getCustomExtensionValues: missing extensions in adUser " + adUser.id);
            } else if (adUser.extensions.length === 0) {
                console.log("getCustomExtensionValues: empty extensions in adUser");
            } else {
                return adUser.extensions.find(function(ext) {
                    return ext.id === CUSTOM_AD_EXTENSION_ID;
                });
            }
        };
        // Check that the regId for the local user is in their user object in Active
        // Directory and store it there if not.
        var checkLocalUser = () => httpReq(MS_GRAPH_API_ME_URL).then(data => {


            var customData = getCustomExtensionValues(data);
            if (!customData) {
                log('[checkLocalUser] Creating extension and setting local user regId to ' + m_localUserRegId);
                return storeRegIdAD(true);
            } else if (customData.regId !== m_localUserRegId) {
                log(`[checkLocalUser] Updating local user regId from ` + `${customData.regId} to ${m_localUserRegId}`);
                return storeRegIdAD(false);
            } else {
                log('[checkLocalUser] Local user regId already set in AD ' + m_localUserRegId);
                return Promise.resolve()
            }
        });
        // Store the regId for local user in the Active Directory
        var storeRegIdAD = createExtension => {
            var postBodyObj = {
                regId: m_localUserRegId
            };
            var httpMethod;
            var url = MS_GRAPH_API_ME_EXTENSIONS_URL;
            if (createExtension) {
                httpMethod = 'POST';
                postBodyObj['@odata.type'] = 'microsoft.graph.openTypeExtension';
                postBodyObj['extensionName'] = CUSTOM_AD_EXTENSION_ID;
            } else {
                httpMethod = 'PATCH';
                url += '/' + CUSTOM_AD_EXTENSION_ID;
            }
            var postBody = JSON.stringify(postBodyObj);
            log(`[storeRegIdAD] Setting extension.` + ` | Http Method: ${httpMethod}` + ` | Url: ${url}` + ` | PostBody: ${postBody}`);
            return httpReq(url, httpRequest => httpRequest.setRequestHeader('Content-Type', 'application/json'), httpMethod, postBody);
        };


        // Get all users in the AD and call event listeners with any users of the app.
        var getAllUsers = () => new Promise(function(resolve, reject) {
            
            // Azure Call to get All users

            httpReq(MS_GRAPH_API_USERS_URL).then(data => {

                // Get My group info 

                httpReq(MS_MY_GROUPS).then(MyGropdata => {
                    var firstGroupID = null;
                    var firstGroupInfo = {};
                    var MyRoles = [];
                    MyGropdata.value.forEach(data => {
                        if (data["@odata.type"] == "#microsoft.graph.group" && firstGroupID == null) {
                            firstGroupID = data.id
                            firstGroupInfo = data;
                        }
                        if (data["@odata.type"] == "#microsoft.graph.directoryRole") {
                            MyRoles.push(data)
                        }
                    });
                    
                    // Keep the MyRoles ( User Profile Info with out BBM Info ), MyCommunityInAzure 

                    localStorage.setItem("MyRoles", JSON.stringify(MyRoles));
                    localStorage.setItem("MyCommunityInAzure", JSON.stringify(firstGroupInfo));


                    var temp = MS_GET_GROUPS_MEMBERS.replace("<group_id>", firstGroupID)
                    httpReq(temp).then(MyGroupMembers => {
                        var MyMembers = [];
                        MyGroupMembers.value.forEach(data => {
                            MyMembers.push(data.id);
                        });
                        data.value.forEach(otherUser => {
                            if (MyMembers.indexOf(otherUser.id) != -1) {
                                var customData = getCustomExtensionValues(otherUser);
                                if (customData && customData.regId) {
                                    onChildAddedHandler(new GenericUserInfo(otherUser.id, customData.regId, otherUser.displayName));
                                } else {
                                    log(`[getAllUsers] Missing RegId for ${otherUser.displayName}`);
                                }
                            }
                        })

                        
                        // Get the Theme Colors 
                        var tempInfo = GET_GROUP_THEME.replace("<group_id>", firstGroupID)
                        httpReq(tempInfo).then(Colors => {
                         
                            localStorage.setItem("MyCommunity", JSON.stringify(Colors));
                         
                            FGUpdateColors(Colors);
                         
                            httpReq(MS_GRAPH_API_ME_PROFILE_URL).then(Profiledata => {
                                localStorage.setItem("MyProfile", JSON.stringify(Profiledata))
                                resolve();
                            }, (error) => {
                                resolve();
                            });
                        }, (error) => {
                            FGUpdateColors({});
                            httpReq(MS_GRAPH_API_ME_PROFILE_URL).then(Profiledata => {
                                localStorage.setItem("MyProfile", JSON.stringify(Profiledata))
                                resolve();
                            }, (error) => {
                                resolve();
                            });
                        });
                    });
                });
            });
        });

        /**
         * This is private utility function serves to invoke client defined event
         * handler wrapped with try / catch
         * @param {function} eventHandler
         * Event handler defined by the customer
         * @param {object} userInfo
         * Event data to be passed to eventHandler
         */
        var safeHandlerCall = (eventHandler, userInfo) => {
            try {
                eventHandler(userInfo);
            } catch (error) {
                console.warn(`Error while executing event listener: ${error.stack}`);
            }
        }
        /**
         * Triggers all listeners of 'user_added' event
         * @param {object} eventUser 
         */
        var onChildAddedHandler = eventUser => {
            if (m_userMap[eventUser.regId] === undefined) {
                m_userMap[eventUser.regId] = eventUser;
                // Notify clients about new user was added to user list.
                m_eventListeners.user_added.forEach(eventHandler => {
                    safeHandlerCall(eventHandler, eventUser)
                });
            } else {
                log(`Entry with the regId: ${eventUser.regId} already exists. ` + `User: ${eventUser.displayName} will be ignored.`);
            }
        }
        // Make HTTP request to Azure server.
        // This will automatically set the Authorization header for Azure 
        // authentication.
        var httpReq = (url, beforeSend, method, postBody) =>
            //First get the access token, this should be cached, but if it is expired
            //then it will get a new one.
            getTokenFunction().then(accessToken => {
                var httpRequest = new XMLHttpRequest();
                return new Promise((resolve, reject) => {
                    httpRequest.onreadystatechange = () => {
                        if (httpRequest.readyState === 4) {
                            if (httpRequest.status >= 200 && httpRequest.status < 300 && typeof httpRequest.response === 'object') {
                                resolve(httpRequest.response);
                            } else {
                                log(`XMLHttpRequest Response.` + ` Status: ${httpRequest.status}` + ` | Response Type: ${typeof httpRequest.response}` + ` | Status Text: ${httpRequest.statusText}` + ` | Url: ${url}`);
                                if (typeof httpRequest.response === 'object') {
                                    // Azure will normally have detailed error information in
                                    // response object
                                    log('XMLHttpRequest Response: ' + JSON.stringify(httpRequest.response));
                                }
                                reject(httpRequest.statusText);
                            }
                        }
                    }
                    httpRequest.responseType = 'json';
                    httpRequest.open(method ? method : 'GET', url, true);
                    httpRequest.setRequestHeader('Authorization', 'Bearer ' + accessToken);
                    if (beforeSend) {
                        // Allow caller to add/change headers or other values on request
                        beforeSend(httpRequest);
                    }
                    httpRequest.send(postBody);
                });
            });
        /**
         * Adds event listener
         * @param {String} event 
         *  Event to subscribe to. AzureUserManager fires following events:
         *  - user_added: triggered when a new user is added 
         *  - user_changed: triggered when existing user is changed
         *  - user_removed: triggered when existing user is removed
         * @param {function} eventListener
         *  Event handler function. When invoked, it contains userInfo object as
         *  parameter. UserInfo object contains following properties:
         *  - userRegId: The regId of the user
         *  - userName: Name of the user
         *  - avatarUrl: user avatar image URL
         * @throws {InvalidArgumentError}
         *  Thrown if the eventListener is not a function
         */
        this.addEventListener = function(event, eventListener) {
            if (typeof eventListener !== 'function') {
                throw new InvalidArgumentError('Event handler must be a function');
            }
            var eventListeners = m_eventListeners[event];
            if (eventListeners) {
                // Do not add event listener if it was already added previously
                var index = eventListeners.indexOf(eventListener);
                if (index === -1) {
                    eventListeners.push(eventListener);
                }
            } else {
                console.warn(`Trying to subscribe to the unknown event: ${event}`);
            }
        }
        /**
         * Removes previously added event listener
         * @param {String} event 
         *  Event to unsubscribe from. AzureUserManager fires following events:
         *  - user_added: triggered when a new user is added
         *  - user_changed: triggered when existing user is changed
         *  - user_removed: triggered when existing user is removed
         * @param {function} eventListener
         *  Previously added event handler function
         * @throws {InvalidArgumentError}
         *  Thrown if the eventListener is not a function
         */
        this.removeEventListener = function(event, eventListener) {
            if (typeof eventListener !== 'function') {
                throw new InvalidArgumentError('Event handler must be a function');
            }
            var eventListeners = m_eventListeners[event];
            if (eventListeners) {
                var index = eventListeners.indexOf(eventListener);
                if (index !== -1) {
                    eventListeners.splice(index, 1);
                }
            } else {
                console.warn(`Trying to unsubscribe from the unknown event: ${event}`);
            }
        }
        /**
         * Get the user avatar image URL by regId if it is already cached
         * or null otherwise.
         * Azure doesn't allow loading avatars from a simple URL that you can put in
         * HTML elements.
         * You should use the loadUserAvatar() function instead which will load
         * the avatar if needed.
         *
         * @param {String} regId
         *  The regId of the user
         * @returns {String}
         *  In case of success returns image URL of the cached user avatar image.
         *  Returns null if failed
         */
        this.getUserAvatar = function(regId) {
            // Azure AD doesn't allow simple avatar image access by URL.
            // The caller will need to call loadUserAvatar which will pass proper
            // auth headers for the user
            var blob = m_avatarBlobCache.get(regId);
            if (blob) {
                return URL.createObjectURL(blob);
            }
            return null;
        }
        /**
         * Load the user avatar image by regId if it is not already cached.
         * @param {String} [regId] 
         *  The regId of the user. If undefined this will load for local user.
         * @returns {Promise<Blob>} Promise of the binary data for the avatar image.
         */
        this.loadUserAvatar = regId => {
            var avatarUrl;
            var user;
            var blob = m_avatarBlobCache.get(regId);
            if (blob) {
                return Promise.resolve(blob);
            }
            if (regId) {
                user = m_userMap[regId];
                if (user && user.userId) {
                    avatarUrl = MS_GRAPH_API_USER_AVATAR_URL_PREFIX + user.userId + MS_GRAPH_API_USER_AVATAR_URL_POSTFIX;
                } else {
                    return Promise.reject(new Error(`Missing userId. RegId: ${regId}`));
                }
            } else {
                // The local user was requested
                user = m_userMap[m_localUserRegId];
                avatarUrl = MS_GRAPH_API_ME_AVATAR_URL;
            }
            return httpReq(avatarUrl, httpRequest => httpRequest.responseType = 'blob').then(data => {
                if (data instanceof Blob) {
                    m_avatarBlobCache.set(regId, data);
                    // Notify app that user info has changed so they can update avatar.
                    user.avatarUrl = URL.createObjectURL(data);
                    // Notify client the user has new information
                    m_eventListeners.user_changed.forEach(function(eventHandler) {
                        safeHandlerCall(eventHandler, user);
                    });
                    return data;
                } else {
                    user.avatarUrl = "images/defaultAvatar.png";
                    m_eventListeners.user_changed.forEach(function(eventHandler) {
                        safeHandlerCall(eventHandler, user);
                    });
                    return;
                    // throw new Error(`Invalid image response: ${typeof data}`);
                }
            });
        };
        /**
         * Gets the user name by regId
         * @param {String} regId 
         * The regId of the user
         * @returns {String}
         * User name in case of success
         * Returns null if failed
         */
        this.getDisplayName = regId => {
            var userInfo = m_userMap[regId];
            // If the user is in the user map, return their name.
            if (typeof userInfo !== 'undefined' && typeof userInfo.displayName !== 'undefined') {
                return userInfo.displayName;
            }
            return null;
        };
        /**
         * Gets user information by regId.
         * @param {String} regId The regId of the user.
         * @returns {GenericUserInfo} User info if found, undefined otherwise.
         */
        this.getUser = regId => m_userMap[regId];
        /**
         * Gets information about the local user.
         * @returns {GenericUserInfo} Local user information.
         */
        this.getLocalUser = () => this.getUser(m_localUserRegId);
        /**
         * Gets UID of the user by Reg Id.
         * @param {String} regId Reg Id of user.
         * @returns {Promise<String>} Resolves with the Uid which corresponds to
         * provided Reg Id.
         */
        this.getUid = regId => {
            var userInfo = m_userMap[regId];
            if (userInfo && userInfo.userId) {
                return Promise.resolve(userInfo.userId);
            }
            console.warn(`getUid: Failed to get user Id for ${regId}`);
            return Promise.reject('Failed to get user Id');
        }
        /**
         * Registers local user in Azure AD. Retrieves all contacts from Azure AD.
         * @returns {Promise} Promise resolves with no data in case of success.
         * Rejects with error if case of failure.
         */
        this.initialize = () => checkLocalUser().then(getAllUsers);
    }
    AzureUserManager.prototype = Object.create(Object.prototype);
    AzureUserManager.prototype.constructor = AzureUserManager;
    return AzureUserManager;
});
