//****************************************************************************
// Copyright 2017 BlackBerry.  All Rights Reserved.
//
// You must obtain a license from and pay any applicable license fees to
// BlackBerry before you may reproduce, modify or distribute this software, or
// any work that includes all or part of this software.
//

"use strict";

/**
 * Contains functions for storing and retrieving users from a firebase database.
 *
 * @memberof Support
 * @class FirebaseUserManager
 */
(function(FirebaseUserManager) {
  // This function will always be called with the correct global context to
  // which this module may export.
  var global = this;

  // Where do we place the module?  Do we have an exports object to use?
  if (typeof exports !== 'undefined') {
    if( typeof module !== 'undefined' && module.exports ) {
      exports = module.exports = FirebaseUserManager();
    }
    exports.FirebaseUserManager = FirebaseUserManager();
  }
  else {
    global.FirebaseUserManager = FirebaseUserManager();
  }

}).call(this, function() {

  // To be thrown when user failed to register in database
  function RegistrationFailedError(message) {
    this.name = "RegistrationFailedError";
    this.message = (message || "");
  }
  RegistrationFailedError.prototype = Error.prototype;

  // To be thrown if FirebaseUserManager has invalid parameter
  function InvalidArgumentError(message) {
    this.name = "InvalidArgumentError";
    this.message = (message || "");
  }
  InvalidArgumentError.prototype = Error.prototype;

  /**
   * Tracks user contact list changes and triggers following events:
   *  - user_added: when a new user is added
   *  - user_changed: when existing user is changed
   *  - user_removed: when existing user is removed
   * Provides functions to get user user information:
   *  - getUserAvatar: gets user avatar image URL
   *  - getUserName: gets user name
   *  - getUser: gets user information (regId, name, avatar)
   *
   * @param {object} userRegId
   *   regId of the user.
   *
   * @param {object} authManager
   *   An authentication manager, which will be used to retrieve authentication
   *   tokens to get access to the user information from cloud storage and
   *   user information to identify which information in the cloud storage is
   *   for the local user.
   *
   * @param {function} userInfo
   *   A constructor for the GenericUserInfo class. This class is provided by
   *   the support library. Once ES6 imports are available in all browsers, this
   *   should be replaced by an ES6 import.
   *
   * @throws {RegistrationFailedError}
   *  Thrown when user failed to register
   *
   * @throws {InvalidArgumentError}
   *  Thrown when userInfo parameter is invalid 
   */ 
  var FirebaseUserManager = function(userRegId, authManager, userInfo) {
    if (typeof userRegId !== "string") {
      throw new InvalidArgumentError("userRegId must be an string.");
    }

    var m_initDone = false;
    var m_userMap = {};
    var m_localUser = authManager.getLocalUserInfo();
    m_localUser.regId = userRegId;
    var m_eventListeners = {
      user_added : [],
      user_changed : [],
      user_removed : []
    }

    // Get a reference to the database service for "user" node
    var database = firebase.database();
    var ref = database.ref("bbmsdk/identity/users/");

    // If the local user isn't already registered in firebase, add them now
    database.ref('bbmsdk/identity/users/' + firebase.auth().currentUser.uid).set({
      regId: m_localUser.regId,
      email: m_localUser.email,
      avatarUrl: m_localUser.avatarUrl,
      name: m_localUser.displayName
    }).catch(function(error) {
      throw new RegistrationFailedError(error);
    });

    /**
     * This is private utility function serves to invoke client defined event
     * handler wrapped with try / catch
     * @param {function} eventHandler
     * Event handler defined by the customer
     * @param {object} userInfo
     * Event data to be passed to eventHandler
     */
    var safeHandlerCall = function(eventHandler, userInfo) {
      try {
        eventHandler(userInfo);
      }
      catch (error) {
        console.warn("Error while executing event listener: " + error);
      }
    }

    var callListeners = function(eventListeners, userInfo) {
      function callListenersNow() {
        eventListeners.forEach(function(eventHandler) {
          safeHandlerCall(eventHandler, userInfo);
        });
      };

      if (m_initDone) {
        // The listeners should be added, call now.
        callListenersNow();
      } else {
        // In the middle of init, creator of this object won't have a chance to
        // add listeners yet so delay until this is done.
        setTimeout(callListenersNow, 1);
      }
    }

    var createGenericUserInfo = firebaseUserInfo => new userInfo(
      firebaseUserInfo.key,
      firebaseUserInfo.val().regId,
      firebaseUserInfo.val().name,
      firebaseUserInfo.val().email,
      firebaseUserInfo.val().avatarUrl);

    /**
     * Handles 'child_added' event (invoked by user management service provider)
     * Triggers all listeners of 'user_added' event
     * @param {object} userInfo 
     * User information object defined by user management service provider
     */
    var onChildAddedHandler = function(firebaseUserInfo) {
      var regId = firebaseUserInfo.val().regId;
      if (typeof regId === "undefined") {
        console.log("User '" + firebaseUserInfo.key
          + "' from the database doesn't have a BBM registration ID");
      } else {
        if (m_userMap[regId] ===  undefined) {
          var userInfo = createGenericUserInfo(firebaseUserInfo);
          m_userMap[regId] = userInfo;
          // Notify clients about new user was added to user list.
          callListeners(m_eventListeners.user_added, userInfo);
        }
        else {
          console.log("FirebaseUserManager already has an entry with the same RegId: " + regId
          + ". This 'User' of name: " + firebaseUserInfo.val().name + " key: " + firebaseUserInfo.key + " will be ignored.");
        }
      }
    }

    /**
     * Handles 'child_changed' event (invoked by user management service provider)
     * Triggers all listeners of 'user_changed' event
     * @param {object} userInfo 
     * User information object defined by user management service provider
     */
    var onChildChangedHandler = function(firebaseUserInfo) {
      var regId = firebaseUserInfo.val().regId;

      if (regId !== undefined) {
        // Check if user is already in user map
        if(m_userMap[regId] !==  undefined) {
          // Replace the existing user information in the map with 
          // the new user information
          var userInfo = createGenericUserInfo(firebaseUserInfo);
          m_userMap[regId] = userInfo;
          // Notify client the user has new information
          callListeners(m_eventListeners.user_changed, userInfo);
        }
        else {
          console.log("Firebase 'User' of RegId: " + regId
            + " has never been added to map before.");
        }
      } else {
        console.log("User '" + firebaseUserInfo.key +
          "' from the database doesn't have a BBM registration ID");
      }
    }

    /**
     * Handles 'child_removed' event (invoked by user management service provider)
     * Triggers all listeners of 'user_removed' event
     * @param {object} userInfo 
     * User information object defined by user management service provider
     */
    var onChildRemovedHandler = function(firebaseUserInfo) {
      var regId = firebaseUserInfo.val().regId;

      if(regId !== undefined) {
        // Check if the user map contains a user that has the same regId
        var userInfo = m_userMap[regId];
        if(userInfo !== undefined) {
          // Remove the user from the user map
          delete m_userMap[regId];
          // Notify client the user got removed
          callListeners(m_eventListeners.user_removed, userInfo);
        } else {
          console.log("Firebase 'User' of RegId: " + regId
            + " has never been added to map before. Ignore this child_removed");
        }
      } else {
        console.log("User '" + firebaseUserInfo.key
          + "' from the database doesn't have a BBM registration ID");
      }
    }

    ref.on('child_added', onChildAddedHandler);
    ref.on('child_changed', onChildChangedHandler);
    ref.on('child_removed', onChildRemovedHandler);

    /**
     * Adds event listener
     * @param {string} event 
     *  Event to subscribe to. FirebaseUserManager fires following events:
     *  - user_added: triggered when a new user is added 
     *  - user_changed: triggered when existing user is changed
     *  - user_removed: triggered when existing user is removed
     * @param {function} eventListener
     *  Event handler function. When invoked, it contains userInfo object as
     *  parameter. UserInfo object contains following properties:
     *  - userRegId: The regId of the user
     *  - userName: Name of the user
     *  - avatarUrl: user avatar image URL
     * @throws {InvalidArgumentError}
     *  Thrown if the eventListener is not a function
     */
    this.addEventListener = function(event, eventListener) {
      if (typeof eventListener !== "function") {
        throw new InvalidArgumentError("Event handler must be a function");
      }

      var eventListeners = m_eventListeners[event];
      if (eventListeners) {
        // Do not add event listener if it was already added previously
        var index = eventListeners.indexOf(eventListener);
        if (index === -1) {
          eventListeners.push(eventListener);
        }
      }
      else {
        console.warn("Trying to subscribe to the unknown event: " + event);
      }
    }

    /**
     * Removes previously added event listener
     * @param {string} event 
     *  Event to unsubscribe from. FirebaseUserManager fires following events:
     *  - user_added: triggered when a new user is added
     *  - user_changed: triggered when existing user is changed
     *  - user_removed: triggered when existing user is removed
     * @param {function} eventListener
     *  Previously added event handler function
     * @throws {InvalidArgumentError}
     *  Thrown if the eventListener is not a function
     */
    this.removeEventListener = function (event, eventListener) {
      if (typeof eventListener !== "function") {
        throw new InvalidArgumentError("Event handler must be a function");
      }
  
      var eventListeners = m_eventListeners[event];
      if (eventListeners) { 
        var index = eventListeners.indexOf(eventListener);
        if (index !== -1) {
          eventListeners.splice(index, 1);
        }
      }
      else {
        console.warn("Trying to unsubscribe from the unknown event: " + event);
      }
    }

    /**
     * Get the user avatar image URL by regId
     * @param {string} regId 
     *  The regId of the user
     * @returns {string}
     *  In case of success returns image URL of the user
     *  Returns null if failed
     */
    this.getUserAvatar = function(regId) {
      var userInfo = m_userMap[regId];
      if (userInfo) {
        return userInfo.avatarUrl;
      }
      return null;
    }

    /**
     * Gets the user name by regId
     * @param {string} regId 
     * The regId of the user
     * @returns {string}
     * User name in case of success
     * Returns null if failed
     */
    this.getDisplayName = function(regId) {
      var userInfo = m_userMap[regId];
      // If the user is in the user map, return their name.
      if (userInfo) {
        return userInfo.displayName;
      }
      return null;
    };

    /**
     * Gets user information by regId
     * @param {string} regId
     *  The regId of the user
     * @returns {object} 
     *  In case of success returns user object which 
     *  has properties:
     *  - userRegId: The regId of the user
     *  - userName: Name of the user
     *  - avatarUrl: user avatar image URL
     *  Returns null if failed
     */
    this.getUser = function(regId) {
      return m_userMap[regId];
    }

    /**
     * Gets information about the local user
     * @returns {object} 
     *  Returns user object which has properties:
     *  - userRegId: The regId of the user
     *  - userName: Name of the user
     *  - avatarUrl: user avatar image URL
     */
    this.getLocalUser = function() {
      var userInfo = m_userMap[m_localUser.regId];
      if (userInfo) {
        return userInfo;
      } else {
        return m_localUser;
      }
    }
    
    /**
     * Gets UID of the user by Reg Id.
     * @param {Promise<String>} regId Promise of user Id.
     */
    this.getUid = function(regId) {
      var userInfo = m_userMap[regId];
      // If the user is in the user map, return their user Id.
      if (userInfo) {
        return Promise.resolve(userInfo.userId);
      }
      else {
        console.log(`getUid: Requested user ${regId} is not in the map.`
          + ' Requesting Firebase database for the user info.');
        return new Promise((resolve, reject) => {
          const GET_UID_ASYNC_TIMEOUT = 5000; // Milliseconds.
          // If the user is not in the user map, query the Firebase.
          const db = firebase.database();
          const dbRef = db.ref('bbmsdk/identity/users/');
          const timeout = setTimeout(() => {
            // Request to the Firebase has timed out.
            console.warn(`getUid: Failed to get UID of the user ${regId}`
              + ` Request has timed out after ${GET_UID_ASYNC_TIMEOUT} ms.`);
            dbRef.orderByChild('regId').equalTo(regId).off('child_added',
              onUserAdded);
            reject('Failed to get UID by RegId: Timeout.');
          }, GET_UID_ASYNC_TIMEOUT);
          var onUserAdded = user => {
            // User is found, clear the timeout timer, unsubscribe from
            // 'child_added', and return UID.
            clearTimeout(timeout);
            dbRef.orderByChild('regId').equalTo(regId).off('child_added',
              onUserAdded);
            resolve(user.key);
          };
          dbRef.orderByChild('regId').equalTo(regId).on('child_added',
            onUserAdded);
        });
      }
    }

    /**
     * Firebase implementation of user manager does not require to wait for
     * users map to get populated. This function returns resolved promise.
    */
    this.initialize = () => Promise.resolve();

    m_initDone = true;
  }

  FirebaseUserManager.prototype = Object.create(Object.prototype);

  FirebaseUserManager.prototype.constructor = FirebaseUserManager;

  FirebaseUserManager.factory = {
   /** 
    * Creates instance of FirebaseUserManager.
    * @returns {Promise<FirebaseUserManager>} Promise resolves with new instance
    * of FirebaseUserManager
    */
    createInstance : (firebaseConfig, userRegId, authManager, userInfo) => {
      var userManager = null;
      // Check if Firebase application was already initialized.
      if (!firebase.apps.length) {
        // Get access token from the auth manager.
        return authManager.getUserManagerToken().then(accessToken => {
          firebase.initializeApp(firebaseConfig);
          var credential =
            firebase.auth.GoogleAuthProvider.credential(null, accessToken);
          // Sign-in to our Firebase project using the already logged
          // in google user's credentials.
          return firebase.auth().signInWithCredential(credential).then(user => {
            return new FirebaseUserManager(userRegId, authManager, userInfo);
          });
        })
      }
      else {
        // Firebase application is initialized.
        // Just return Promise of new user manager instance.
        return Promise.resolve(new FirebaseUserManager(userRegId, authManager,
                                                       userInfo));
      }
    }
  }

  return FirebaseUserManager;
});
