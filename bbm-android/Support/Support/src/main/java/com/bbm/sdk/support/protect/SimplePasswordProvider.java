/*
 * Copyright (c) 2018 BlackBerry.  All Rights Reserved.
 *
 * You must obtain a license from and pay any applicable license fees to
 * BlackBerry before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.
 *
 * This file may contain contributions from others. Please review this entire
 * file for other proprietary rights or license notices.
 */

package com.bbm.sdk.support.protect;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bbm.sdk.support.R;

import java.lang.annotation.Retention;
import java.lang.ref.WeakReference;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Simple implementation of the PasswordProvider interface.
 * Challenges the user to provide a password to lock/unlock the keys stored through the ProtectedManager.
 */
public class SimplePasswordProvider implements PasswordProvider {

    private static final int ACTION_NONE = 0;
    private static final int ACTION_REQUEST_EXISTING_PASSWORD = 1;
    private static final int ACTION_REQUEST_NEW_PASSWORD = 2;
    @Retention(SOURCE)
    @IntDef({ACTION_NONE, ACTION_REQUEST_EXISTING_PASSWORD, ACTION_REQUEST_NEW_PASSWORD})
    @interface PASSWORD_REQUEST_ACTION {
    }

    private WeakReference<Activity> mActivity;
    private boolean mAllowCancel;
    private static SimplePasswordProvider sInstance;
    private @PASSWORD_REQUEST_ACTION int mPendingAction = ACTION_NONE;
    private PasswordResultConsumer mConsumer;
    public AlertDialog mAlertDialog;

    private SimplePasswordProvider() {
    }

    public static synchronized SimplePasswordProvider getInstance() {
        if (sInstance == null) {
            sInstance = new SimplePasswordProvider();
        }
        return sInstance;
    }

    public void setActivity(Activity activity) {
        mActivity = new WeakReference<>(activity);
        if (mConsumer != null) {
            switch (mPendingAction) {
                case ACTION_REQUEST_EXISTING_PASSWORD:
                    provideExistingPassword(mAllowCancel, mConsumer);
                    break;
                case ACTION_REQUEST_NEW_PASSWORD:
                    requestNewPassword(mConsumer);
                    break;
            }
        }
    }

    @Override
    public void provideExistingPassword(boolean allowCancel, @NonNull PasswordResultConsumer passwordConsumer) {
        if (mActivity != null && mActivity.get() != null) {
            if (mAlertDialog == null) {
                mPendingAction = ACTION_NONE;
                mConsumer = null;

                LayoutInflater inflater = LayoutInflater.from(mActivity.get());
                @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.dialog_password_prompt, null);

                EditText passwordTextView = view.findViewById(R.id.password_input);
                TextView errorTextView = view.findViewById(R.id.password_error_message);

                mAlertDialog = new AlertDialog.Builder(mActivity.get())
                        .setTitle(R.string.provide_existing_password)
                        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        })
                        .setNegativeButton(R.string.forgot_password, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mAlertDialog = null;
                                forgotPassword(allowCancel, passwordConsumer);
                            }
                        })
                        .setView(view)
                        .setCancelable(allowCancel)
                        .create();
                mAlertDialog.show();
                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String password = passwordTextView.getText().toString().trim();
                        if (!passwordConsumer.passwordResult(password)) {
                            errorTextView.setText(R.string.incorrect_password);
                            errorTextView.setVisibility(View.VISIBLE);
                        } else {
                            mAlertDialog.dismiss();
                            mAlertDialog = null;
                        }
                    }
                });
            }
        } else {
            mPendingAction = ACTION_REQUEST_EXISTING_PASSWORD;
            mConsumer = passwordConsumer;
            mAllowCancel = allowCancel;
        }
    }

    @Override
    public void requestNewPassword(@NonNull PasswordResultConsumer passwordConsumer) {
        if (mActivity != null && mActivity.get() != null) {
            if (mAlertDialog == null) {
                mPendingAction = ACTION_NONE;
                mConsumer = null;

                LayoutInflater inflater = LayoutInflater.from(mActivity.get());
                @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.dialog_password_prompt, null);

                EditText passwordTextView = view.findViewById(R.id.password_input);
                EditText passwordConfTextView = view.findViewById(R.id.password_confirmation_input);
                TextView errorTextView = view.findViewById(R.id.password_error_message);
                passwordTextView.setHint(R.string.new_password_hint);
                passwordTextView.setVisibility(View.VISIBLE);
                passwordConfTextView.setVisibility(View.VISIBLE);
                TextView titleView = view.findViewById(R.id.password_prompt_title);
                titleView.setText(R.string.new_password_text);
                titleView.setVisibility(View.VISIBLE);

                mAlertDialog = new AlertDialog.Builder(mActivity.get())
                        .setTitle(R.string.provide_new_password)
                        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String password = passwordTextView.getText().toString().trim();
                                String passwordConf = passwordConfTextView.getText().toString().trim();
                                if (password.equals(passwordConf) && meetsPasswordRules(password)) {
                                    passwordConsumer.passwordResult(password);
                                    mAlertDialog = null;
                                } else {
                                    errorTextView.setVisibility(View.VISIBLE);
                                    errorTextView.setText(R.string.password_not_valid);
                                }
                            }
                        })
                        .setView(view)
                        .setCancelable(false)
                        .create();
                mAlertDialog.show();
                //OK button starts disable until a valid password is entered
                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);

                TextWatcher watcher = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String password = passwordTextView.getText().toString().trim();
                        String passwordConf = passwordConfTextView.getText().toString().trim();
                        if (meetsPasswordRules(password)) {
                            passwordTextView.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_check, 0);
                            if (password.equals(passwordConf)) {
                                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                passwordConfTextView.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_check, 0);
                            } else {
                                passwordConfTextView.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_clear, 0);
                            }
                        } else {
                            passwordTextView.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_clear, 0);
                            mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        }
                    }
                };
                passwordTextView.addTextChangedListener(watcher);
                passwordConfTextView.addTextChangedListener(watcher);
            }
        } else {
            mPendingAction = ACTION_REQUEST_NEW_PASSWORD;
            mConsumer = passwordConsumer;
        }
    }

    private void forgotPassword(boolean allowCancel, PasswordResultConsumer passwordResultConsumer) {
        if (mActivity != null && mActivity.get() != null) {
            AlertDialog dialog = new AlertDialog.Builder(mActivity.get())
                    .setMessage(R.string.forgot_password_warning)
                    .setPositiveButton(R.string.button_continue, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ProtectedManager.getInstance().forgotPassword();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            provideExistingPassword(allowCancel, passwordResultConsumer);
                        }
                    })
                    .create();
            dialog.show();
        }
    }

    private boolean meetsPasswordRules(String password) {
        if (password.length() >= 8) {
            return true;
        }

        return false;
    }
}
