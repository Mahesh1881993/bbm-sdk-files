/*
 * Copyright (c) 2017 BlackBerry.  All Rights Reserved.
 *
 * You must obtain a license from and pay any applicable license fees to
 * BlackBerry before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.
 *
 * This file may contain contributions from others. Please review this entire
 * file for other proprietary rights or license notices.
 */

package com.bbm.sdk.support.identity.user;

import android.support.annotation.UiThread;

import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.bbmds.internal.lists.ObservableList;
import com.bbm.sdk.reactive.Mutable;
import com.bbm.sdk.reactive.ObservableValue;
import com.bbm.sdk.support.reactive.ArrayObservableList;
import com.bbm.sdk.support.util.Logger;

import java.util.HashMap;


/**
 * This manages the app user data.
 * The data source for app users should update this with its user data.
 * For sample app purposes only.
 * This must only be used from the main UI thread.
 */
public class UserManager implements AppUserListener {
    private static UserManager sInstance;


    public static synchronized UserManager getInstance() {
        if(sInstance == null) {
            sInstance = new UserManager();
        }
        return sInstance;
    }

    /**
     * Store by regId to user.
     * This contains the same objects as mUserList
     */
    private HashMap<Long, Mutable<AppUser>> mRegIdToUserMap = new HashMap<>();

    /**
     * This is only to temp store any users that were requested, but we don't have yet.
     * If we returned null, or a new
     */
    private HashMap<Long, Mutable<AppUser>> mPendingRegIdToUserMap = new HashMap<>();

    /**
     * Keep list of users so that we can return them by constant index (unless one is removed) by order they were added
     * This contains the same objects as mRegIdToUserMap, except the AppUser objects are not wrapped in observerables
     */
    private ArrayObservableList<AppUser> mUserList = new ArrayObservableList();

    private Mutable<AppUser> mLocalAppUser = new Mutable<>(new AppUser());

    private UserManager() {
    }

    /**
     * Clears all the data from the manager. Should only be called when the user has
     * decided to logout.
     */
    public void clear() {
        mRegIdToUserMap.clear();
        mPendingRegIdToUserMap.clear();
        mUserList.clear();
    }

    @UiThread
    @Override
    public void localUserUpdated(AppUser localAppUser) {
        //we don't want the local user in our list otherwise it would need to be filtered from everywhere
        //if needed we could store it separately (to display in profile UI...)
        //remove does nothing if already removed
        removeUser(localAppUser);
        mLocalAppUser.set(localAppUser);
    }

    @UiThread
    @Override
    public void remoteUserAdded(AppUser remoteAppUser) {
        remoteUserUpdated(remoteAppUser);
    }

    @UiThread
    @Override
    public void remoteUserChanged(AppUser remoteAppUser) {
        remoteUserUpdated(remoteAppUser);
    }

    @UiThread
    @Override
    public void remoteUserRemoved(AppUser remoteAppUser) {
        removeUser(remoteAppUser);
    }

    @UiThread
    public void remoteUserUpdated(AppUser appUser) {
        final long regId = appUser.getRegId();
        if (regId == 0) {
            Logger.w("Ignoring appUser with invalid regId AppUser="+appUser);
            return;
        }
        //first put in map, this will replace old one if it is already there and return old one
        Mutable<AppUser> appUserOv = mRegIdToUserMap.get(regId);
        if (appUserOv == null) {
            //doesn't exist yet, but see if in the pending, if it is then remove it
            appUserOv = mPendingRegIdToUserMap.remove(regId);
            if (appUserOv != null) {
                //it was previously requested, before we knew about it, need to keep and update that OV for any listeners
                //first put in map in case the observer requests it immediately
                mRegIdToUserMap.put(regId, appUserOv);
                //update the value, this could trigger a Observer changed method to call the getUser() so this needs to be after its in the map
                appUserOv.set(appUser);
                mUserList.add(appUser);
                Logger.d("add: added from pending, have " + mUserList.size() + " users. " + appUser);
            } else {
                //it wasn't in map, so create new one and add to end of list
                appUserOv = new Mutable<>(appUser);
                mRegIdToUserMap.put(regId, appUserOv);
                mUserList.add(appUser);
                Logger.d("add: added, have " + mUserList.size() + " users. " + appUser);
            }
        } else {
            //it was already in map, so just set the new value in it
            //This updates the same OV in both the map and list, and will trigger notifications for any listeners
            appUserOv.set(appUser);
            Logger.d("add: updated, have "+mUserList.size()+" users. "+appUser);
        }
    }

    @UiThread
    public void removeUser(AppUser appUser) {
        ObservableValue<AppUser> removed;
        Logger.d("remove appUser="+appUser);

        removed = mRegIdToUserMap.remove(appUser.getRegId());
        if (removed != null) {
            if (!mUserList.remove(removed.get())) {
                Logger.e("remove: Failed to find remove user in list old="+removed.get());
            }
        }

        mPendingRegIdToUserMap.remove(appUser.getRegId());
    }

    @UiThread
    public ObservableValue<AppUser> getUser(long regId) {
        Mutable<AppUser> appUser = mRegIdToUserMap.get(regId);

        if (appUser == null) {
            //see if in the pending list
            appUser = mPendingRegIdToUserMap.get(regId);

            if (appUser == null) {
                //we can't just return null, need to return OV with default
                appUser = new Mutable<>(new AppUser(regId, Existence.MAYBE));
                //save the OV in our collections so the caller can observe the value and be notified if it is added later
                mPendingRegIdToUserMap.put(regId, appUser);

                Logger.d("getUser: created placeholder appUser for regId="+regId);
            }
        }
        return appUser;
    }

    public ObservableList<AppUser> getUsers() {
        return mUserList;
    }



    public ObservableValue<AppUser> getLocalAppUser() {
        return mLocalAppUser;
    }
}
