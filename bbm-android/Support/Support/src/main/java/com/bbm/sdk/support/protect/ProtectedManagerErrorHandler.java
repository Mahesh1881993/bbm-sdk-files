/*
 * Copyright (c) 2018 BlackBerry.  All Rights Reserved.
 *
 * You must obtain a license from and pay any applicable license fees to
 * BlackBerry before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.
 *
 * This file may contain contributions from others. Please review this entire
 * file for other proprietary rights or license notices.
 */

package com.bbm.sdk.support.protect;

import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.inbound.Endpoints;
import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.bbmds.outbound.EndpointDeregister;
import com.bbm.sdk.bbmds.outbound.EndpointsGet;
import com.bbm.sdk.reactive.SingleshotMonitor;
import com.bbm.sdk.service.InboundMessageObservable;

import java.util.UUID;

public class ProtectedManagerErrorHandler implements ErrorHandler {

    @Override
    public void onError(int errorType) {
        switch (errorType) {
            case DECRYPTION_ERROR:
            case ENCRYPTION_ERROR:
            case DEVICE_KEYSTORE_ERROR:
                //For simplicity this error handler treats all decryption, encryption or keystore errors as fatal
                //We will kick this endpoint and restart.
                //Note: if errors persist the user can choose the "forgot password" option when setting up
                //  Forgot password will remove all data stored in the key storage provider
                //  The user would be required to re-join any existing chats
                handleFatalError();
                break;
            case KEY_STORAGE_PROVIDER_ERROR:
                //Keystorage actions are currently retried automatically based on user actions like opening a chat.
                //An application could choose to implement a different approach
                break;
        }
    }

    /**
     * To handle a fatal error we will remove ourselves as an active endpoint
     * This will result in the application being shutdown.
     * Upon restarting the user will be required to login again and provide their application password (or create a new password)
     */
    private void handleFatalError() {
        // Create a cookie to track the request.
        final String requestCookie = UUID.randomUUID().toString();

        // Create a one time inbound message observer that will have the results.
        final InboundMessageObservable<Endpoints> endpointsObserver = new InboundMessageObservable<>(
                new Endpoints(), requestCookie, BBMEnterprise.getInstance().getBbmdsProtocolConnector());

        SingleshotMonitor.run(new SingleshotMonitor.RunUntilTrue() {
            @Override
            public boolean run() {
                Endpoints endpoints = endpointsObserver.get();
                if (endpoints.exists == Existence.MAYBE) {
                    return false;
                }
                for (Endpoints.RegisteredEndpoints registeredEndpoint : endpoints.registeredEndpoints) {
                    if (registeredEndpoint.isCurrent) {
                        String requestCookie = UUID.randomUUID().toString();
                        //Leave the current endpoint
                        //The goAway handler will kill the app
                        BBMEnterprise.getInstance().getBbmdsProtocol().send(new EndpointDeregister(requestCookie, registeredEndpoint.endpointId));
                    }
                }
                return true;
            }
        });

        // Activate the monitor and issue the protocol request.
        BBMEnterprise.getInstance().getBbmdsProtocol().send(new EndpointsGet(requestCookie));
    }
}
