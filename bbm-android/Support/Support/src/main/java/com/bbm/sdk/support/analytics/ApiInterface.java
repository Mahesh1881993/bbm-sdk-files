package com.bbm.sdk.support.analytics;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * ApiInterface for accessing Profile image Provided by Azure
 */

public interface ApiInterface {


    @GET("users/{id}/Photo/$value")
    Call<ResponseBody> getAllProfileImage(@Path("id") String id, @Header("Content-Type") String type, @Header("Authorization") String Authorization);


}
