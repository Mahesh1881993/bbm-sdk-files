/*
 * Copyright (c) 2018 BlackBerry.  All Rights Reserved.
 *
 * You must obtain a license from and pay any applicable license fees to
 * BlackBerry before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.
 *
 * This file may contain contributions from others. Please review this entire
 * file for other proprietary rights or license notices.
 */

package com.bbm.sdk.support.protect;


import android.app.Activity;
import android.support.annotation.NonNull;

/**
 * Used to query the user for a password or query the user to create a new password
 */
public interface PasswordProvider {

    /**
     * Consumes the provided password
     */
    interface PasswordResultConsumer {
        /**
         * Consumes a supplied password.
         * @param password the user supplied password
         * @return true if the password was accepted
         */
        boolean passwordResult(@NonNull String password);
    }

    /**
     * Challenge the user to provide a previously created password
     * @param allowCancel true if the password prompt can be cancelled
     * @param passwordConsumer will consume the provided password
     */
    void provideExistingPassword(boolean allowCancel, @NonNull PasswordResultConsumer passwordConsumer);

    /**
     * Ask the user to create a new password.
     * Password rules such as length or character requirements are the responsibility of the PasswordProvider.
     * @param passwordConsumer will consume the newly created password
     */
    void requestNewPassword(@NonNull PasswordResultConsumer passwordConsumer);

    /**
     * Set a valid activity context which can be used by the password provider to prompt the user.
     * @param activity android activity
     */
    void setActivity(Activity activity);

}
