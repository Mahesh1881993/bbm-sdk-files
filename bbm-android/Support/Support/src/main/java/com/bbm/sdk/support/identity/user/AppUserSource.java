/*
 * Copyright (c) 2017 BlackBerry.  All Rights Reserved.
 *
 * You must obtain a license from and pay any applicable license fees to
 * BlackBerry before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.
 *
 * This file may contain contributions from others. Please review this entire
 * file for other proprietary rights or license notices.
 */

package com.bbm.sdk.support.identity.user;

import android.support.annotation.NonNull;

/**
 * A source for app users.
 * This is normally a server, remote DB, cloud storage...
 */
public interface AppUserSource {
    void addListener(@NonNull AppUserListener listener);
    void removeListener(@NonNull AppUserListener listener);
}
