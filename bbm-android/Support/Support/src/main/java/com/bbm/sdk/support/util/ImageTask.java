

package com.bbm.sdk.support.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.ImageView;

import com.bbm.sdk.common.IOHelper;

import java.io.InputStream;

/**
 * This is just a simple image loader implementation that doesn't do any caching.
 * A real application would probably use an existing image loading framework.
 */
public class ImageTask extends AsyncTask<Void, Void, Bitmap> {

    private String mUrl;
    private ImageView mImageView;

    /* Url is Used to access Contact info and Profile info from Azure */

    public static String mProfileURL="https://graph.microsoft.com/beta/";
    public static String mContactURL_part1="https://graph.microsoft.com/beta/users/";
    public static String mContactURL_part2="/Photo/$value";


    public static ImageTask load(String url, ImageView imageView) {
        if (TextUtils.isEmpty(url) || imageView == null) {
            Logger.e("Invalid url="+url+" imageView="+imageView);
            return null;
        }

        ImageTask task = new ImageTask(url, imageView);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return task;
    }

    public ImageTask(String url, ImageView imageView) {
        mUrl = url;
        mImageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        InputStream is = null;
        try {
            Bitmap bmp = StringToBitMap(mUrl);
            if (bmp != null) {
                Logger.d("Loaded bmp=" + bmp + " W=" + bmp.getWidth() + " H=" + bmp.getHeight() + " for mUrl=" + mUrl);
            } else {
                Logger.d("Null bitmap loaded for mUrl=" + mUrl);
            }
            return bmp;
        } catch (Exception e) {
            Logger.i(e, "Failed to load image for mUrl=" + mUrl);
        } catch (OutOfMemoryError oome) {
            Logger.i(oome, "Can not load image for mUrl=" + mUrl);
        } finally {
            IOHelper.safeClose(is);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        //if this was cancelled then this view is now used for a different user
        if (!isCancelled()) {
            mImageView.setImageBitmap(bitmap);
            Logger.d("Just set bitmap="+bitmap+" for mUrl="+mUrl);
        } else {
            Logger.d("Discarding cancelled bitmap="+bitmap+" for mUrl="+mUrl);
        }
    }

    public static Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }
}
