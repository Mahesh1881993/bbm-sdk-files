package com.bbm.sdk.support.analytics;


import retrofit2.Retrofit;

/**
 * ApiClient Created for Accessing User Profile Image from Azure
 */

public class ApiClient {
    private static Retrofit retrofit = null;
    public static String mProfileURL="https://graph.microsoft.com/beta/";

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(mProfileURL)
                    .build();
        }
        return retrofit;
    }
}
