package com.bbm.sdk.support.OfflineDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.microsoft.graph.extensions.Contact;

import java.util.ArrayList;

/**
 * Created For Offline Data Handling.
 */

public class SqliteDB extends SQLiteOpenHelper {

    public static SQLiteDatabase FGdatabase;
    private static final String DATABASE_NAME = "Fg.sqlite";
    private static final int DATABASE_VERSION = 3;

    /*Current User and Community Info*/
    private static final String Profile_TABLE = "profile_info";
    public static final String id = "_id";
    public static final String Profile_pic = "profilepic";
    public static final String Account_name = "accountname";
    public static final String Account_display_name = "accountdisplayname";
    public static final String Community_pic = "communitypic";
    public static final String Community_name = "communityname";

   /* USER TABLE*/
    private static final String User_TABLE = "User_info";
    public static final String User_Profile_pic = "userprofilepic";
    public static final String User_Account_name = "useraccountname";
    public static final String User_Account_email = "useraccountemail";
    public static final String User_Azure_id = "User_Azure_id";
    public static final String User_BBM_id = "User_BBM_id";

    /*Contact_table*/
    private static final String Contact_TABLE = "Contact_info";
    public static final String Contact_Profile_pic = "contactprofilepic";
    public static final String Contact_Account_name = "contactaccountname";
    public static final String Contact_Account_email = "contactaccountemail";
    public static final String Contact_Azure_id = "contact_Azure_id";
    public static final String Contact_BBM_id = "contact_BBM_id";

    public SqliteDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * Just to be used Offline Storage Which are Current User Profile Table, Azure UserManager Table,Azure Community Contact User Manager.
     *
     * @param db The SQLiteDatabase DB which is the identifier used for Data Storage
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        createProfileTable(db);
        createUserTable(db);
        createContactTable(db);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    /**
     * The Open function is used to Open Db Before Store data
     */
    public  void open(){

        FGdatabase=this.getWritableDatabase();
        return;

    }
    /**
     * The Close function is used to Close Db After Stored data
     */
    public  void close(){

        FGdatabase.close();

    }
    /**
     * The createUserTable function is used to Create User Table
     */
    private void createUserTable(SQLiteDatabase db) {

        String createCustomerTableQuery = "CREATE TABLE " + User_TABLE + " ( " +
                id + " INTEGER PRIMARY KEY, " +
                User_Profile_pic + " BLOB, " +
                User_Account_name + " text, " +
                User_Account_email + " text, " +
                User_BBM_id + " text, " +
                User_Azure_id + " text " +
                ");";
        db.execSQL(createCustomerTableQuery );
    }
    /**
     * Store Azure User Details
     *
     * @param image The User Profile Pic which is the identifier used with Local Database
     * @param cust_location  The User Name which is the identifier used with Local Database
     * @param email  The User Email which is the identifier used with Local Database
     * @param cust_name  The User BBM ID which is the identifier used with Local Database
     * @param azure_id  The User Azure ID which is the identifier used with Local Database
     */
    public long insertUserContacts(String image,String cust_location,String email,Long cust_name,String azure_id){
        FGdatabase = this.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(User_Profile_pic, image);
        initialValues.put(User_Account_name, cust_location);
        initialValues.put(User_Account_email, email);
        initialValues.put(User_BBM_id, cust_name);
        initialValues.put(User_Azure_id, azure_id);

        return FGdatabase.insert(User_TABLE, null, initialValues);

    }
    /**
     * Fetch All USer Details to list User in app
     */
    public ArrayList<User_getter> getAllUserItems() {

        ArrayList<User_getter> itemList = new ArrayList<User_getter>();
        FGdatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + User_TABLE;
            Cursor itemCursor = FGdatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();
            User_getter customer_values;

            if(!itemCursor.isAfterLast()) {

                do {

                    customer_values = new User_getter();
                    customer_values.setProfile_pic(itemCursor.getString(1));
                    customer_values.setUser_name(itemCursor.getString(2));
                    customer_values.setUser_email(itemCursor.getString(3));
                    customer_values.setBbm_id(itemCursor.getLong(4));
                    customer_values.setAzure_id(itemCursor.getString(5));

                    itemList.add(customer_values);
                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
            FGdatabase.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return itemList;
    }
    /**
     * Fetch Selected USer Details Which is Used to Show User Name is Offline in App
     * @param bbm_id The BBM ID which is Unique id to Fetch Selected User
     */
    public ArrayList<User_getter> getselectedUserItems(Long bbm_id) {

        ArrayList<User_getter> itemList = new ArrayList<User_getter>();
        FGdatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + User_TABLE+ " where " + User_BBM_id + " = '" + bbm_id + "'";
            Cursor itemCursor = FGdatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();
            User_getter customer_values;

            if(!itemCursor.isAfterLast()) {

                do {

                    customer_values = new User_getter();
                    customer_values.setProfile_pic(itemCursor.getString(1));
                    customer_values.setUser_name(itemCursor.getString(2));
                    customer_values.setUser_email(itemCursor.getString(3));
                    customer_values.setBbm_id(itemCursor.getLong(4));
                    customer_values.setAzure_id(itemCursor.getString(5));

                    itemList.add(customer_values);
                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
            FGdatabase.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return itemList;
    }
    /**
     * Fetch Selected USer Details Which is Used to Show User Name is Offline in App
     * @param bbm_id The BBM ID which is Unique id to Fetch Selected User Name
     */
    public String getselectedUserName(Long bbm_id) {

        String customer_values="";
        FGdatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + User_TABLE+ " where " + User_BBM_id + " = '" + bbm_id + "'";
            Cursor itemCursor = FGdatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();


            if(!itemCursor.isAfterLast()) {

                do {

                    customer_values=itemCursor.getString(2);


                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
            FGdatabase.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return customer_values;
    }
    /**
     * Wipe User Table When before Store new Data
     */
    public void deleteUserContact()
    {
        FGdatabase = this.getWritableDatabase();
        FGdatabase.execSQL("delete from "+ User_TABLE);

    }
    /**
     * The createProfileTable function is used to Create Current USer Profile Table
     */
    private void createProfileTable(SQLiteDatabase db) {

        String createCustomerTableQuery = "CREATE TABLE " + Profile_TABLE + " ( " +
                id + " INTEGER PRIMARY KEY, " +
                Profile_pic + " BLOB, " +
                Account_name + " text, " +
                Account_display_name + " text, " +
                Community_pic + " BLOB, " +
                Community_name + " text " +
                ");";
        db.execSQL(createCustomerTableQuery );
    }
    /**
     * Store BBM Current Profile Details
     *
     * @param image The User Profile Pic which is the identifier used with Local Database
     * @param cust_location  The User Name which is BBM Current User Name
     * @param cust_name  The User Email ID which is BBM Current User Email
     * @param communtiy_pic  The User Community Pic which is BBM Current User Community Pic
     * @param community_name  The User Community Name which is BBM Current User Community Name
     */
    public long insertContacts(byte[] image,String cust_location,String cust_name,byte[] communtiy_pic,String community_name){
        FGdatabase = this.getWritableDatabase();
        ContentValues initialValues = new ContentValues();
        initialValues.put(Profile_pic, image);
        initialValues.put(Account_name, cust_location);
        initialValues.put(Account_display_name, cust_name);
        initialValues.put(Community_pic, communtiy_pic);
        initialValues.put(Community_name, community_name);

        return FGdatabase.insert(Profile_TABLE, null, initialValues);

    }
    /**
     * Fetch Current Profile Details to Show Current Details in Profile Page in app
     */
    public ArrayList<Profile_getter> getAllItems() {

        ArrayList<Profile_getter> itemList = new ArrayList<Profile_getter>();
        FGdatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + Profile_TABLE;
            Cursor itemCursor = FGdatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();
            Profile_getter customer_values;

            if(!itemCursor.isAfterLast()) {

                do {

                    customer_values = new Profile_getter();
                    customer_values.setImage(itemCursor.getBlob(1));
                    customer_values.setProfile_name(itemCursor.getString(2));
                    customer_values.setProfile_url(itemCursor.getString(3));
                    customer_values.setCommunity_pic(itemCursor.getBlob(4));
                    customer_values.setCommunity_name(itemCursor.getString(5));

                    itemList.add(customer_values);
                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
            FGdatabase.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return itemList;
    }
    /**
     * Wipe Profile Table Before Add New data
     */
    public void deleteContact()
    {
        FGdatabase = this.getWritableDatabase();
        FGdatabase.execSQL("delete from "+ Profile_TABLE);
    }
    /**
     * The createProfileTable function is used to Create Current User Profile Table
     */
    private void createContactTable(SQLiteDatabase db) {

        String createCustomerTableQuery = "CREATE TABLE " + Contact_TABLE + " ( " +
                id + " INTEGER PRIMARY KEY, " +
                Contact_Profile_pic + " BLOB, " +
                Contact_Account_name + " text, " +
                Contact_Account_email + " text, " +
                Contact_BBM_id + " text, " +
                Contact_Azure_id + " text " +
                ");";
        db.execSQL(createCustomerTableQuery );
    }
    /**
     * Store Azure Community User Details
     *
     * @param image The User Profile Pic which is Azure User pic
     * @param cust_location  The User Name which is BBM User Name
     * @param email  The User Email which is BBM User Email
     * @param cust_name  The User ID which is BBM User ID
     * @param azure_id  The User ID which is Azure User ID
     */
    public long insertcommunityContacts(String image,String cust_location,String email,Long cust_name,String azure_id){
        FGdatabase = this.getWritableDatabase();
        ContentValues initialValues = new ContentValues();

        initialValues.put(Contact_Profile_pic, image);
        initialValues.put(Contact_Account_name, cust_location);
        initialValues.put(Contact_Account_email, email);
        initialValues.put(Contact_BBM_id, cust_name);
        initialValues.put(Contact_Azure_id, azure_id);

        return FGdatabase.insert(Contact_TABLE, null, initialValues);

    }
    /**
     * Fetch All Community Member Details to list Contact page in app
     */
    public ArrayList<ContactGetter> getAllContactItems() {

        ArrayList<ContactGetter> itemList = new ArrayList<ContactGetter>();
        FGdatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + Contact_TABLE;
            Cursor itemCursor = FGdatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();
            ContactGetter customer_values;

            if(!itemCursor.isAfterLast()) {

                do {

                    customer_values = new ContactGetter();
                    customer_values.setConatact_profile_pic(itemCursor.getString(1));
                    customer_values.setContact_name(itemCursor.getString(2));
                    customer_values.setContact_email(itemCursor.getString(3));
                    customer_values.setContact_bbm_id(itemCursor.getLong(4));
                    customer_values.setConatact_azure_id(itemCursor.getString(5));

                    itemList.add(customer_values);
                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
            FGdatabase.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return itemList;
    }
    /**
     * Fetch Selected Community member Details Which is Used to Show User Name is Offline in App
     * @param bbm_id The BBM ID which is Unique id to Fetch Selected User
     */
    public ArrayList<ContactGetter> getselectedcontactItems(Long bbm_id) {

        ArrayList<ContactGetter> itemList = new ArrayList<ContactGetter>();
        FGdatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + Contact_TABLE+ " where " + Contact_BBM_id + " = '" + bbm_id + "'";
            Cursor itemCursor = FGdatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();
            ContactGetter customer_values;

            if(!itemCursor.isAfterLast()) {

                do {
                    customer_values = new ContactGetter();
                    customer_values.setConatact_profile_pic(itemCursor.getString(1));
                    customer_values.setContact_name(itemCursor.getString(2));
                    customer_values.setContact_email(itemCursor.getString(3));
                    customer_values.setContact_bbm_id(itemCursor.getLong(4));
                    customer_values.setConatact_azure_id(itemCursor.getString(5));

                    itemList.add(customer_values);
                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
            FGdatabase.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return itemList;
    }
    /**
     * Fetch Selected Community member Details Which is Used to Show User Name is Offline in App
     * @param bbm_id The BBM ID which is Unique id to Fetch Selected User
     */
    public String getselectedcontactName(Long bbm_id) {

        String customer_values="";
        FGdatabase = this.getWritableDatabase();

        try {
            String grepItemsQuery = "select * from " + Contact_TABLE+ " where " + Contact_BBM_id + " = '" + bbm_id + "'";
            Cursor itemCursor = FGdatabase.rawQuery(grepItemsQuery, null);

            itemCursor.moveToFirst();


            if(!itemCursor.isAfterLast()) {

                do {

                    customer_values=itemCursor.getString(2);


                } while (itemCursor.moveToNext());
            }

            itemCursor.close();
            FGdatabase.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return customer_values;
    }
    /**
     * Wipe Community User Table When before Store new Data
     */
    public void deleteCommunityContact()
    {
        FGdatabase = this.getWritableDatabase();
        // TODO Auto-generated method stub
        FGdatabase.execSQL("delete from "+ Contact_TABLE);


    }
}
