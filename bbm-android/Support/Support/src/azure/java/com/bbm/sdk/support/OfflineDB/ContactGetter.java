package com.bbm.sdk.support.OfflineDB;

/**
 * This class contains some data from the Azure User and potentially for the application also.
 */

public class ContactGetter {
    /**
     * The User Name which is BBM User Name
     */
    String Contact_name;
    /**
     * The User Email which is BBM User Email
     */
    String Contact_email;
    /**
     * The User ID which is BBM User ID
     */
    long   Contact_bbm_id;
    /**
     * The User ID which is Azure User ID
     */
    String Conatact_azure_id;
    /**
     * The User Profile Pic which is Azure User pic
     */
    String Conatact_profile_pic;

    public ContactGetter() {
    }

    public String getContact_name() {
        return Contact_name;
    }

    public void setContact_name(String contact_name) {
        Contact_name = contact_name;
    }

    public String getContact_email() {
        return Contact_email;
    }

    public void setContact_email(String contact_email) {
        Contact_email = contact_email;
    }

    public long getContact_bbm_id() {
        return Contact_bbm_id;
    }

    public void setContact_bbm_id(long contact_bbm_id) {
        Contact_bbm_id = contact_bbm_id;
    }

    public String getConatact_azure_id() {
        return Conatact_azure_id;
    }

    public void setConatact_azure_id(String conatact_azure_id) {
        Conatact_azure_id = conatact_azure_id;
    }

    public String getConatact_profile_pic() {
        return Conatact_profile_pic;
    }

    public void setConatact_profile_pic(String conatact_profile_pic) {
        Conatact_profile_pic = conatact_profile_pic;
    }


}
