package com.bbm.sdk.support.OfflineDB;

/**
 * This class contains some data from the Azure Current User, Community Details and potentially for the application also.
 */

public class Profile_getter {
    /**
     * The User Name which is BBM Current User Name
     */
    String profile_name;
    /**
     * The User Email ID which is BBM Current User Email
     */
    String profile_url;
    /**
     * The User Profile Pic which is BBM Current User Profile Pic
     */
    byte[] image;
    /**
     * The User Community Pic which is BBM Current User Community Pic
     */
    byte[] community_pic;
    /**
     * The User Community Name which is BBM Current User Community Name
     */
    String community_name;

    /**
     * Handling Offline Data of User Details
     *
     * @param profile_name The Profile Name which is from BBM Current User Name
     * @param profile_email  The Profile Email provided by the BBM Current User Email.
     * @param image  The Profile Image provided by the BBM Current User Image.
     */
    public Profile_getter(String profile_name, String profile_email, byte[] image) {
        this.profile_name = profile_name;
        this.profile_url = profile_email;
        this.image = image;
    }

    public Profile_getter() {
    }


    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getCommunity_pic() {
        return community_pic;
    }

    public void setCommunity_pic(byte[] community_pic) {
        this.community_pic = community_pic;
    }

    public String getCommunity_name() {
        return community_name;
    }

    public void setCommunity_name(String community_name) {
        this.community_name = community_name;
    }
}
