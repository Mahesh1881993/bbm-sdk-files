package com.bbm.sdk.support.OfflineDB;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created to Detect Network Connectivity
 */

public class NetworkDetect {
    private Context _context;

    public NetworkDetect(Context context){
        this._context = context;
    }

    public boolean isConnectingToInternet(){

        ConnectivityManager cm = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null) {
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE")) {

                if (netInfo.isConnected() && netInfo.isAvailable()) {

                    return true;
                }
            }

            if (netInfo.getTypeName().equalsIgnoreCase("WIFI")) {

                if (netInfo.isConnected() && netInfo.isAvailable()) {

                    return true;
                }
            }
        }

        return false;
    }

}
