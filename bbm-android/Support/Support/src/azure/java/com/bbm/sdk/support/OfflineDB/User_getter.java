package com.bbm.sdk.support.OfflineDB;

/**
 * This Class Contains User Info Detail From Azure.
 */

public class User_getter {
    /**
     * The User Name which is the identifier used with Local Database
     */
    String User_name;
    /**
     * The User Email which is the identifier used with Local Database
     */
    String User_email;
    /**
     * The User BBM ID which is the identifier used with Local Database
     */
    long bbm_id;
    /**
     * The User Azure ID which is the identifier used with Local Database
     */
    String azure_id;
    /**
     * The User Profile Pic which is the identifier used with Local Database
     */
    String profile_pic;

    public User_getter() {
    }

    public String getUser_name() {
        return User_name;
    }

    public void setUser_name(String user_name) {
        User_name = user_name;
    }

    public long getBbm_id() {
        return bbm_id;
    }

    public void setBbm_id(long bbm_id) {
        this.bbm_id = bbm_id;
    }

    public String getAzure_id() {
        return azure_id;
    }

    public void setAzure_id(String azure_id) {
        this.azure_id = azure_id;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getUser_email() {
        return User_email;
    }

    public void setUser_email(String user_email) {
        User_email = user_email;
    }
}
