/*
 * Copyright (c) 2018 BlackBerry.  All Rights Reserved.
 *
 * You must obtain a license from and pay any applicable license fees to
 * BlackBerry before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.
 *
 * This file may contain contributions from others. Please review this entire
 * file for other proprietary rights or license notices.
 */

package com.bbm.sdk.support.identity.user;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.reactive.ObservableMonitor;
import com.bbm.sdk.support.OfflineDB.SqliteDB;
import com.bbm.sdk.support.analytics.ApiClient;
import com.bbm.sdk.support.analytics.ApiInterface;
import com.bbm.sdk.support.identity.auth.AzureAdAuthenticationManager;
import com.bbm.sdk.support.util.BbmUtils;
import com.bbm.sdk.support.util.Logger;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.microsoft.graph.authentication.IAuthenticationProvider;
import com.microsoft.graph.concurrency.ICallback;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.core.DefaultClientConfig;
import com.microsoft.graph.core.IClientConfig;
import com.microsoft.graph.extensions.Extension;
import com.microsoft.graph.extensions.GraphServiceClient;
import com.microsoft.graph.extensions.IExtensionCollectionPage;
import com.microsoft.graph.extensions.IGraphServiceClient;
import com.microsoft.graph.extensions.IUserCollectionPage;
import com.microsoft.graph.extensions.User;
import com.microsoft.graph.http.IHttpRequest;
import com.microsoft.identity.client.AuthenticationResult;

import java.io.ByteArrayOutputStream;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Handles syncing the user list from the Azure Active Directory
 * and syncing the local users registration id into the AD
 * and Handles user list in Offline using Sqlite DB
 *
 */
public class AzureAdUserSync extends AppSourceNotifier {

    private static AzureAdUserSync sInstance;
    public static SqliteDB db;
    public static Context context;
    public  boolean loading=false;

    private static final String BBME_SDK_EXTENSION = "com.bbm.sdk.example.azure.userValues";

    /**
     * Observe the local user and sync any time it changes
     */
    private final ObservableMonitor mLocalBbmUserObserver = new ObservableMonitor() {
        @Override
        protected void run() {
            com.bbm.sdk.bbmds.User localUser = BbmUtils.getLocalBbmUser().get();
            if (localUser.exists == Existence.YES && localUser.regId != 0) {
                syncLocalUser(localUser);
            }
        }
    };

    /**
     * Get the AzureAdUserSync instance
     * @return instance of AzureAdUserSync
     */
    public static synchronized AzureAdUserSync getInstance(Context context1) {
        if (sInstance == null) {
            sInstance = new AzureAdUserSync();
        }
        context=context1;
        db=new SqliteDB(context);

        return sInstance;
    }

    /**
     * Start the AzureAdUserSync
     */
    public void start() {
        //connect the user manager to our azure user DB implementation
        addListener(UserManager.getInstance());

        updateUsers();
    }

    /**
     * Stop the AzureAdUserSync
     */
    public void stop() {
        removeListener(UserManager.getInstance());
        mLocalBbmUserObserver.dispose();
    }

    /**
     * Creates an instance of IGraphServiceClient that can be used to make requests to retrieve and update users from the directory.
     * @return instance of IGraphServiceClient
     */
    private synchronized IGraphServiceClient getGraphServiceClient(AuthenticationResult authResult) {
        IClientConfig clientConfig = DefaultClientConfig.createWithAuthenticationProvider(new IAuthenticationProvider() {
            @Override
            public void authenticateRequest(IHttpRequest request) {
                //Add the token and uid to the authentication header
                request.addHeader("Authorization", "Bearer "
                        + authResult.getAccessToken());

                Logger.d("Request: " + request.toString());
            }
        });
        return new GraphServiceClient.Builder().fromConfig(clientConfig).buildClient();
    }

    /**
     * Fetch the user list and update the values stored for the local user
     */
    private void updateUsers() {
        mLocalBbmUserObserver.activate();
        syncUsersList();
    }

    /**
     * Update the stored registration id for the local user
     * @param localUser the local bbmds user obtained from bbmcore
     */
    private void syncLocalUser(com.bbm.sdk.bbmds.User localUser) {
        AzureAdAuthenticationManager.getInstance().getGraphAccessToken(new AzureAdAuthenticationManager.TokenCallback() {
            @Override
            public void onToken(AuthenticationResult authResult) {
                IGraphServiceClient graphServiceClient = getGraphServiceClient(authResult);
                //Request our own user
                graphServiceClient.getMe().buildRequest().get(new ICallback<User>() {
                    @Override
                    public void success(com.microsoft.graph.extensions.User msGraphUser) {


                        //Update our own user information in the UserManager
                        final AppUser appUser = new AppUser(localUser, msGraphUser.id, msGraphUser.displayName, msGraphUser.mail, null);
                        appUser.setExists(Existence.YES);
                        notifyAppUserListeners(EventToNotify.LOCAL_UPDATED, appUser);

                        //Get the extensions for the local user from the MSGraph
                        graphServiceClient.getMe().getExtensions().buildRequest().get(new ICallback<IExtensionCollectionPage>() {
                            @Override
                            public void success(IExtensionCollectionPage extensionCollectionPage) {
                                Extension bbmSdkExtension = null;
                                while (bbmSdkExtension == null && extensionCollectionPage.getCurrentPage() != null) {
                                    for (Extension extension : extensionCollectionPage.getCurrentPage()) {
                                        //Get the bbme sdk extension
                                        if (extension.id.equals(BBME_SDK_EXTENSION)) {
                                            bbmSdkExtension = extension;
                                            break;
                                        }
                                    }
                                    if (extensionCollectionPage.getNextPage() == null) {
                                        break;
                                    }
                                }

                                String regIdAsString = Long.toString(localUser.regId);

                                //If the extension doesn't exist then write a new extension with our regid
                                if (bbmSdkExtension == null) {
                                    AsyncTask.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            Extension extension = new Extension();
                                            extension.id = BBME_SDK_EXTENSION;
                                            extension.getAdditionalDataManager().put("regId", new JsonPrimitive(regIdAsString));
                                            //Create a new extension entry
                                            graphServiceClient.getMe().getExtensions().buildRequest().post(extension);
                                        }
                                    });
                                } else {
                                    updateBbmSdkExtension(graphServiceClient, regIdAsString);
                                }
                            }


                            @Override
                            public void failure(ClientException ex) {
                                Logger.e(ex,"Failure to read bbm sdk extension");
                            }
                        });

                    }

                    @Override
                    public void failure(ClientException ex) {
                        Logger.e(ex,"Failure to read local user (me) from MicrosoftGraph");
                    }
                });
            }
        }, false);
    }

    /**
     * Check if we need to update the existing registration id value for the local AD user
     * @param graphServiceClient the graph services client instance
     * @param regIdAsString the users registration id as a string
     */
    private void updateBbmSdkExtension(IGraphServiceClient graphServiceClient, String regIdAsString) {
        graphServiceClient.getMe().getExtensions(BBME_SDK_EXTENSION).buildRequest()
                .getExtension(new ICallback<Extension>() {
            @Override
            public void success(Extension extension) {
                JsonElement regIdElement = extension.getAdditionalDataManager().get("regId");
                String existingRegId = regIdElement != null ? regIdElement.getAsString() : null;

                //RegId isn't set in the extension for this user or the value is wrong
                if (existingRegId == null || !existingRegId.equals(regIdAsString)) {
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            extension.getAdditionalDataManager().put("regId", new JsonPrimitive(regIdAsString));
                            //Update the existing extension entry
                            graphServiceClient.getMe().getExtensions(BBME_SDK_EXTENSION).buildRequest().patch(extension);
                        }
                    });
                }
            }

            @Override
            public void failure(ClientException ex) {
                Logger.e(ex,"Failure to read bbm sdk extension");
            }
        });
    }

    /**
     * Sync the active directory user list with the UserManager.
     * Any user which has a registration id is included.
     */
    private void syncUsersList() {
        AzureAdAuthenticationManager.getInstance().getGraphAccessToken(new AzureAdAuthenticationManager.TokenCallback() {
            @Override
            public void onToken(AuthenticationResult authResult) {
                //Build a request to get the extensions for all users
                getGraphServiceClient(authResult).getUsers().buildRequest()
                        .expand("extensions").get(new ICallback<IUserCollectionPage>() {
                    @Override
                    public void success(IUserCollectionPage iUserCollectionPage) {
                        List<User> users = iUserCollectionPage.getCurrentPage();
                        //Wipe Table Before adding new data
                        db.deleteUserContact();
                        //For each user
                        for (com.microsoft.graph.extensions.User user : users) {

                            Extension bbmSdkExtension = null;
                            while (bbmSdkExtension == null && user.extensions != null && user.extensions.getCurrentPage() != null) {
                                for (Extension extension : user.extensions.getCurrentPage()) {
                                    //Get the bbme sdk extension
                                    if (extension.id.equals(BBME_SDK_EXTENSION)) {
                                        bbmSdkExtension = extension;
                                        break;
                                    }
                                }
                                if (user.extensions.getNextPage() == null) {
                                    break;
                                }
                            }
                            //If the extension includes a regId value
                            if (bbmSdkExtension != null && bbmSdkExtension.getAdditionalDataManager().containsKey("regId")) {
                                try {

                                    long regId = Long.parseLong(bbmSdkExtension.getAdditionalDataManager().get("regId").getAsString());
                                    //Check if the regId is ourselves (ignore)
                                    if (regId != UserManager.getInstance().getLocalAppUser().get().getRegId()) {
                                        //Add the user profile url to the UserManager
                                        allProfile_url(regId, user.id, user.displayName, user.mail,user.userPrincipalName);
                                    }
                                } catch (NumberFormatException nfe) {
                                    Logger.i(nfe, "MicrosoftGraph invalid regId in user: " + user.displayName + " " + user.mail);
                                }
                            }
                        }


                    }

                    @Override
                    public void failure(ClientException ex) {
                        Logger.e(ex,"Failure to read user list from MicrosoftGraph");
                    }
                });
            }
        }, false);
    }
    /**
     * Attempt to acquire an User Profile pic from Azure,
     * if a token cannot be obtained the user must be prompted for credentials via callAcquireToken.
     *
     * @param regId the user BBM ID
     * @param uid the user Azure id
     * @param name the user name
     * @param email the user email
     * @param useremail the user email
     */
    private void allProfile_url(long regId, String uid, String name, String email,String useremail) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getAllProfileImage(uid,"application/json",AzureAdAuthenticationManager.getInstance().getUserAccess().get());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    //Add User Info in UserManager
                    Bitmap bm = BitmapFactory.decodeStream(response.body().byteStream());
                    AppUser appUser = new AppUser(regId, uid, name, email, BitMapToString(bm));
                    appUser.setExists(Existence.YES);
                    Logger.d("Add user " + appUser.toString());
                    notifyAppUserListeners(EventToNotify.ADD, appUser);
                   //Add User info in local Sqlite DB For Offline
                    db.open();
                    db.insertUserContacts(BitMapToString(bm),name,useremail,regId,uid);
                    db.close();

                }catch (Exception e){
                    //Add User Info in UserManager
                    AppUser appUser = new AppUser(regId, uid, name, email,"");
                    appUser.setExists(Existence.YES);
                    Logger.d("Add user " + appUser.toString());
                    notifyAppUserListeners(EventToNotify.ADD, appUser);
                    //Add User info in local Sqlite DB For Offline
                    db.open();
                    db.insertUserContacts("",name,useremail,regId,uid);
                    db.close();

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
   //Converting Bitmap to String
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

}