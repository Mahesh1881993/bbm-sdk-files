/*
 * Copyright (c) 2017 BlackBerry.  All Rights Reserved.
 *
 * You must obtain a license from and pay any applicable license fees to
 * BlackBerry before you may reproduce, modify or distribute this
 * software, or any work that includes all or part of this software.
 *
 * This file may contain contributions from others. Please review this entire
 * file for other proprietary rights or license notices.
 */

package com.bbm.sdk.support.util;

import android.content.Context;
import android.support.annotation.NonNull;

import com.bbm.sdk.reactive.SingleshotMonitor;
import com.bbm.sdk.support.identity.user.FirebaseUserDbSync;
import com.bbm.sdk.support.identity.user.UserManager;
import com.bbm.sdk.support.protect.DefaultKeyImportFailureListener;
import com.bbm.sdk.support.protect.PasswordProvider;
import com.bbm.sdk.support.protect.ProtectedManager;
import com.bbm.sdk.support.protect.ProtectedManagerErrorHandler;
import com.bbm.sdk.support.protect.providers.FirebaseKeyStorageProvider;
import com.google.firebase.auth.FirebaseUser;

/**
 * Helper to initialize both the example app user storage and protected key storage using a Firebase DB.
 */
public class FirebaseHelper {

    /**
     * This will initialize the Firebase DB for app user storage/sync and
     * will wait until both the Firebase DB and BBM SDK are in a state ready to
     * start the ProtectedManager.
     * @param context Android application context
     * @param passwordProvider A password provider to be used by the ProtectedManager
     */
    public static void initProtected(@NonNull Context context,
                                     @NonNull PasswordProvider passwordProvider) {

        SingleshotMonitor.run(new SingleshotMonitor.RunUntilTrue() {
            @Override
            public boolean run() {
                //Once the FirebaseUser is available start the protected manager
                FirebaseUser fbUser = FirebaseUserDbSync.getInstance().getFireBaseUser().get();
                if (fbUser != null) {
                    //Initialize the ProtectedManager with our FirebaseKeyStorageProvider
                    FirebaseKeyStorageProvider fbKeyStorageProvider = new FirebaseKeyStorageProvider(fbUser);
                    //Set a default KeyImportFailureListener.
                    ProtectedManager.getInstance().setKeyImportFailureListener(new DefaultKeyImportFailureListener(fbKeyStorageProvider));
                    //Start the ProtectedManager to provide keys for bbmcore
                    ProtectedManager.getInstance().start(
                            context,
                            fbUser.getUid(),
                            fbKeyStorageProvider,
                            passwordProvider,
                            new ProtectedManagerErrorHandler()
                    );
                    return true;
                }
                return false;
            }
        });
    }



    /**
     * This will stop listening for Firebase DB changes and clear local content.
     */
    public static void stop() {
        // Remove the listener
        FirebaseUserDbSync.getInstance().removeListener(UserManager.getInstance());

        // Clear the User Manager.
        UserManager.getInstance().clear();

        // Stop the DB.
        FirebaseUserDbSync.getInstance().stop();
    }
}
